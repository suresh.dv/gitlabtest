global class PurgeCashierExceptions implements Schedulable{

    global void execute(SchedulableContext ctx){
        List<LOM_Cashier_Exception__c> toDelete = new List<LOM_Cashier_Exception__c>();
        Date purgeStartDate = System.today().addDays(-92);
        
        try{
            for(LOM_Cashier_Exception__c ar : [SELECT id, date_time__c from LOM_Cashier_Exception__c 
                                            WHERE Date_Time__c < :purgeStartDate LIMIT 9990]){ 
                LOM_Cashier_Exception__c exp = new LOM_Cashier_Exception__c(Id=(Id)ar.get('id'),Date_Time__c=(DateTime)ar.get('Date_Time__c'));
                toDelete.add(exp);
            }
            
            delete toDelete;    
        } catch(Exception exp) {
            System.debug('Exception : ' + exp);
        }
    }
}