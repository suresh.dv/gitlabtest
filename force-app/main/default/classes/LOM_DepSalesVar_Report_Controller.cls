/// controller classs for reports displayed in customer portals
public with sharing class LOM_DepSalesVar_Report_Controller {

/// Gets the corect base URL regardless if the page is called from platform 
/// or from a Force.com site

    public LOM_DepSalesVar_Report_Controller (){
    
        Integer month = Date.Today().Month();
        Integer year = Date.Today().year();
        if(Month<10)
            ReportPeriod  = '0' + month + '/' + year;
        else
            ReportPeriod  = month + '/' + year;
        ShowReport = true;
    }

public String ReportName {get;set;}
public String ReportDescription {get;set;}
Public String ReportID {get;set;}
Public Boolean ShowReport {get;set;}

Public String ReportPeriod  {get;set;}

public string baseURL {
    get{
        return Site.getCurrentSiteUrl() == null ? URL.getSalesforceBaseUrl().toExternalForm() +'/' : Site.getCurrentSiteUrl();
    }
}

public string ReportURL {
    get{
        return baseURL  +  ReportID ;
    }
}


public string ReportURLwithParameters {get;set;}


    public PageReference RefreshData()
    {
        retrieveReportInfo();  
        RefreshReportUrl();     
        return null;
    }   
    
    public PageReference RefreshReportUrl()
    {
        /* Example */
        /*?pv0=06%2F2013&isdtp=vw*/
        
        ReportURLwithParameters =  baseURL  +  ReportID +  '?pv0=' +    ReportPeriod.replace('/','%2F') + '&isdtp=vw';
        
        ShowReport = true;
        return null;
    }  

    public void retrieveReportInfo() {
        ReportID = System.currentPagereference().getParameters().get('ReportID');
        
        LIST<Report> AgR;

        AgR = [select Name, Description from Report where id = :ReportID]; 

        // Loop through the list and update the Name field
        for(Report s : AgR){
            ReportName = String.valueOf(s.get('Name'));
            ReportDescription = String.valueOf(s.get('Description'));
         }
        
    }

}