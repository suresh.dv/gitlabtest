public class ServiceRequestNotificationJob implements Schedulable 
{
    public void execute(SchedulableContext ctx) 
    {
        if (!Test.isRunningTest())
            ServiceRequestNotificationManager.process();
    }
}