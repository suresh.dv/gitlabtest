/*-----------------------------------------------------------------------------------------------
Author     : Miro Zelina
Company    : Husky Energy
Description: A utility class creating test data for the HOG_SAPFLOCServiceTest
History    : 15-Mar-2017 - Created
-------------------------------------------------------------------------------------------------*/
@isTest 


public class HOG_SAPFLOCServiceTestData
{
    /*****************************     DT_SAP_RIHIFLO_LIST     ********************************/
    public static HOG_SAPApiUtils.DT_SAP_RIHIFLO_LIST createDT_SAP_RIHIFLO_LIST
    (
    	String PLTXT,   //Name
    	String TPLNR,   //level
        String FLTYP,   //category
        String OBJNR,
        String TPLMA,
        String INGRP,
        String BEBER,
        String SWERK,
        String TPLNR_INT,
        String KOSTL,
        String TPLNR_2,
        String TPLMA_INT,
        String ABCKZ,
        String EQART,
        String DATAB,
        String TPLNR_1
    )
    {                
        HOG_SAPApiUtils.DT_SAP_RIHIFLO_LIST input = new HOG_SAPApiUtils.DT_SAP_RIHIFLO_LIST();
        
        input.PLTXT = PLTXT;
        input.FLTYP = FLTYP;
        input.OBJNR = OBJNR;
        input.TPLNR = TPLNR; 
        input.TPLMA = TPLMA;
        input.INGRP = INGRP;
        input.BEBER = BEBER;
        input.SWERK = SWERK;
        input.TPLNR_INT = TPLNR_INT;
        input.KOSTL = KOSTL;
        input.TPLNR_2 = TPLNR_2;
        input.TPLMA_INT = TPLMA_INT;
        input.ABCKZ = ABCKZ;
        input.EQART = EQART;
        input.DATAB = DATAB;
        input.TPLNR_1 = TPLNR_1;

        input.SPRAS = 'X';
        input.IWERK = 'X';
        input.IWERKI = 'X';
        input.SWERKI = 'X';
        input.STORT = 'X';
        input.STORTI = 'X';
        input.MSGRP = 'X';
        input.MSGRPI = 'X';
        input.BEBERI = 'X';
        input.ABCKZI = 'X';
        input.EQFNR = 'X';
        input.EQFNRI = 'X';
        input.BUKRS = 'X';
        input.BUKRSI = 'X';
        input.ANLNR = 'X';
        input.ANLNRI = 'X';
        input.ANLUN = 'X';
        input.RKEOBJNR = 'X';
        input.GSBER = 'X';
        input.GSBERI = 'X';
        input.KOSTLI = 'X';
        input.DAUFN = 'X';
        input.DAUFNI = 'X';
        input.AUFNR = 'X';
        input.AUFNRI = 'X';
        input.TRPNR = 'X';
        input.TPLKZ = 'X';
        input.IEQUI = 'X';
        input.IEQUII = 'X';
        input.EINZL = 'X';
        input.EINZLI = 'X';
        input.POSNR = 'X';
        input.SUBMT = 'X';
        input.SUBMTI = 'X';
        input.MAPAR = 'X';
        input.MAPARI = 'X';
        input.ARBPL = 'X';
        input.PPSIDI = 'X';
        input.GEWRK = 'X';
        input.LGWIDI = 'X';
        input.ERDAT = 'X';
        input.ERNAM = 'X';
        input.AEDAT = 'X';
        input.AENAM = 'X';
        input.BEGRU = 'X';
        input.LVORM = 'X';
        input.RBNR = 'X';
        input.RBNR_I = 'X';
        input.INGRPI = 'X';
        input.KOKRS = 'X';
        input.KOKRSI = 'X';
        input.PROID = 'X';
        input.PROIDI = 'X';
        input.STTXT = 'X';
        input.ILOAN = 'X';
        input.VKORG = 'X';
        input.VKORGI = 'X';
        input.VTWEG = 'X';
        input.VTWEGI = 'X';
        input.SPART = 'X';
        input.SPARTI = 'X';
        input.USTXT = 'X';
        input.NAME_LIST = 'X';
        input.TEL_NUMBER = 'X';
        input.POST_CODE1 = 'X';
        input.CITY1 = 'X';
        input.CITY2 = 'X';
        input.COUNTRY = 'X';
        input.REGION = 'X';
        input.STREET = 'X';
        input.MANDT = 'X';
        input.ADRNR = 'X';
        input.ADRNRI = 'X';
        input.KZLTX = 'X';
        input.SUBMTKTX = 'X';
        input.INVNR = 'X';
        input.BRGEW = 'X';
        input.GEWEI = 'X';
        input.GROES = 'X';
        input.ANSWT = 'X';
        input.WAERS = 'X';
        input.ANSDT = 'X';
        input.HERST = 'X';
        input.HERLD = 'X';
        input.TYPBZ = 'X';
        input.BAUJJ = 'X';
        input.BAUMM = 'X';
        input.SERGE = 'X';
        input.VKBUR = 'X';
        input.VKGRP = 'X';
        input.TPLNR_0 = 'X';
        input.TPLNR_3 = 'X';
        input.ALKEY = 'X';
        input.GWLDT_K = 'X';
        input.GWLEN_K = 'X';
        input.MGANR_K = 'X';
        input.GARTX_K = 'X';
        input.GWLDT_L = 'X';
        input.GWLEN_L = 'X';
        input.MGANR_L = 'X';
        input.GARTX_L = 'X';
        input.DFPS_CP = 'X';
        input.DFPS_CP_SYS = 'X';
        input.DFPS_CP_SYST = 'X';  

        return input;
    }
    
    /*****************************       DT_SAP_CI_IFLOT       ********************************/
    public static HOG_SAPApiUtils.DT_SAP_CI_IFLOT createDT_SAP_CI_IFLOT
    (
        String ZZ_LSD,
        String ZZ_WELL_TYPE,
        String ZZ_UNIT_CONFIG,
        String ZZ_WELL_ORIENT,
        String ZZ_OIL_GRAVITY,
        String ZZ_NOEQ,
        String ZZ_SWM_FLAG,
        String ZZ_H2S_LOCATION
    )
    {
        HOG_SAPApiUtils.DT_SAP_CI_IFLOT wellData = new HOG_SAPApiUtils.DT_SAP_CI_IFLOT();
        
        wellData.ZZ_LSD = ZZ_LSD;
        wellData.ZZ_WELL_TYPE = ZZ_WELL_TYPE;
        wellData.ZZ_UNIT_CONFIG = ZZ_UNIT_CONFIG;
        wellData.ZZ_WELL_ORIENT = ZZ_WELL_ORIENT;
        wellData.ZZ_OIL_GRAVITY = ZZ_OIL_GRAVITY;
        wellData.ZZ_NOEQ = ZZ_NOEQ;
        wellData.ZZ_SWM_FLAG = ZZ_SWM_FLAG;
        wellData.ZZ_H2S_LOCATION = ZZ_H2S_LOCATION;
        
    
        wellData.ZZ_WINTER_ACCESS= 'X';
        wellData.ZZ_SWB= 'X';
        wellData.ZZ_ZONE= 'X';
        wellData.ZZ_POOL= 'X';
        wellData.ZZ_CORRELATION= 'X';
        wellData.ZZ_STREAM_DAY= 'X';
        wellData.ZZ_SCADA_ID= 'X';
        wellData.ZZ_MTU_NO= 'X';
        wellData.ZZ_STREAM_DAY_EM= 'X';

        return wellData;
    }
    
    /*****************************    DT_ECC_FLOC_CHAR_DATA    ********************************/
    public static HOG_SAPApiUtils.DT_ECC_FLOC_CHAR_DATA createDT_ECC_FLOC_CHAR_DATA()

    {
        HOG_SAPApiUtils.DT_ECC_FLOC_CHAR_DATA charData = new HOG_SAPApiUtils.DT_ECC_FLOC_CHAR_DATA();
        
        String TPLNR = 'X';
  		//String CLASS = 'X';
  		String ATNAM = 'X';
  		String ZVAL = 'X';
  		String MSEHI = 'X';
  		String MSEHL = 'X';

        return charData;
    }
    
    /*****************************       DT_SAP_FUNC_LOC       ********************************/
    public static HOG_SAPApiUtils.DT_SAP_FUNC_LOC createDT_SAP_FUNC_LOC
    (
        
        HOG_SAPApiUtils.DT_SAP_RIHIFLO_LIST RIHIFLO_LST,
        HOG_SAPApiUtils.DT_SAP_CI_IFLOT CI_IFLOT,
        HOG_SAPApiUtils.DT_ECC_FLOC_CHAR_DATA FLOC_CHAR,
        String RECTYPE,
        String USER_STAT

    )
    {
        HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocData = new HOG_SAPApiUtils.DT_SAP_FUNC_LOC();
        
        flocData.RIHIFLO_LST = RIHIFLO_LST;
        flocData.CI_IFLOT = CI_IFLOT;
        flocData.FLOC_CHAR = FLOC_CHAR;
        flocData.RECTYPE = RECTYPE;
        flocdata.USER_STAT = USER_STAT;

        flocData.GTEXT = 'X';
        flocData.RBNRX = 'X';
        flocData.TYPTX = 'X';
        flocData.BEZEI = 'X';
        flocData.BUTXT = 'X';
        flocData.KTEXT = 'X';
        flocData.KTEXT1 = 'X';
        flocData.NAME1 = 'X';
        flocData.EARTX = 'X';
        flocData.NAME2 = 'X';
        flocData.FING = 'X';
        //flocdata.CLASS = 'X';
        flocData.KSCHL = 'X';
        flocData.KTEXT2 = 'X';
        flocData.MAKTX = 'X';
        flocData.ABCTX = 'X';
        flocData.INNAM = 'X';
        flocData.SYSID_MANDT = 'X';
        flocData.SYST_STAT = 'X';

        return flocData;
    }
    
}