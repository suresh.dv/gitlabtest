@isTest
private class ProductionLossEventTest {
    
    static testMethod void testProductionLossEvent() {
        
        Date testDate = Date.today();
        String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
        
        Business_Unit__c businessUnit = TestUtilities.createBusinessUnit('Sunrise Field');
        insert businessUnit;
                
        // rbo 12.03.13
        Business_Department__c businessDepartment = new Business_Department__c(Name = 'Test Business Department');            
        insert businessDepartment;
        // rbo 12.03.13

        // rbo 12.03.13            
        //Operating_District__c operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, 'Sunrise');
        Operating_District__c operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, businessDepartment.Id, 'Sunrise1');
        // rbo 12.03.13
        insert operatingDistrict;       
        
        List<Plant__c> plantList = PlantTestData.createPlants(operatingDistrict.Id, 3); 
        
        List<Unit__c> unitList = UnitTestData.createUnits(plantList, 2);
        
        List<Equipment__c> equipmentList = EquipmentTestdata.createEquipment(unitList, 3);
        equipmentList[0].Description_of_Equipment__c = 'This is more than 50 characters in description. So it will be cut off.';
        equipmentList[1].Description_of_Equipment__c = 'This is less than 50 characters.';
        update equipmentList;
        
        System.debug('Running Production Loss Events Controller Extension test: testProductionLossEvent()...');
        try {
            Production_Loss_Event__c ple = new Production_Loss_Event__c();
            ple.Start_Date_Time__c = Date.today();
            
            ApexPages.Standardcontroller sc = New ApexPages.StandardController(ple);
            Test.SetCurrentPageReference(New PageReference('Page.ProductionLossEventEdit'));
            ProductionLossEventControllerExtension evControllerExt = new ProductionLossEventControllerExtension(sc);
            
            if (evControllerExt != null) { 
                System.assertNotEquals(evControllerExt.level1Items.size(), 0);
                System.assertEquals(evControllerExt.level2Items.size(), 0);
                System.assertEquals(evControllerExt.level3Items.size(), 0);
                System.assertNotEquals(evControllerExt.equipmentSearchList.size(), 0);
                
                evControllerExt.selectedLevel1 = plantList[0].Id;
                evControllerExt.resetUnitAndEquipment();
                System.assertNotEquals(evControllerExt.level2Items.size(), 0);
                System.assertEquals(evControllerExt.level3Items.size(), 0);
                
                evControllerExt.selectedLevel2 = unitList[0].Id;
                System.assertNotEquals(evControllerExt.level3Items.size(), 0);
                
                evControllerExt.searchByTagID = 'Equipment';
                System.assertNotEquals(evControllerExt.level3Items.size(), 0);
                
                evControllerExt.selectedLevel1 = plantList[1].Id;
                evControllerExt.resetUnitAndEquipment();
                System.assertNotEquals(evControllerExt.level2Items.size(), 0);
                System.assertEquals(evControllerExt.level3Items.size(), 0);
                System.assertNotEquals(evControllerExt.equipmentSearchList.size(), 0);
                
                evControllerExt.selectedEquipmentBySearch = equipmentList[3].Id;
                evControllerExt.populatePlantUnitEquipment();
                evControllerExt.save();
                evControllerExt.saveAndNew();
            }
            
            System.assert((evControllerExt != null));
        } catch(Exception ex) {
            System.assert(false, 'Error creating new OpEventControllerExtension instance. ' + ex.getMessage());
        }   
    }
}