/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Extension for Load Confirmation Functionality
Test Class:     FM_LoadConfirmationEditCtrlTest
History:        jschn 01.02.2018 - Created.
                jschn 01.08.2018 - Added custom validation for Scale Ticket Number on Destination Facility
                jschn 02.06.2018 - Added Functionality for Unit Change Reason
                jschn 05.07.2018 - Added Stuck Truck indicator for External Dispatchers + change Vendor validation based on PS
                jschn 05.31.2018 - Added existConfirmationOnTruckTrip validation - US W-001114
**************************************************************************************************/
public with sharing class FM_LoadConfirmationEditCtrl {

    private static final String DEBUG_PREFIX = 'FM_LoadConfirmationEditCtrl: ';

    public FM_Load_Confirmation__c  confirmation     {get;set;}
    public String                   confirmationId   {get;set;}

    private String                  oldUnit;
    private String                  oldUnitChangeReason;

    public Boolean                  originalIsStuckTruck {get;set;}
    public Boolean                  isStuckTruck       {get;set;}
    public FM_Stuck_Truck__c        stuckTruck         {get;set;}

    public Boolean isExternalDispatcher     {get; private set;}

    public FM_LoadConfirmationEditCtrl(ApexPages.StandardController stdController) {
        initializeValues();
        initializeConfirmation();
        initializeStuckTruck();
    }

    /**
     * Initialize general values.
     */
    private void initializeValues() {
        confirmationId = getUrlParam(FM_LoadConfirmation_Utilities.CONFIRMATION_ID_PARAM_KEY);
        isStuckTruck = false;
        isExternalDispatcher = FM_LoadConfirmation_Utilities.userHasExtDispatcherPS();
    }

    /**
     * Initialize Load Confirmation record for edit and new modes.
     */
    private void initializeConfirmation() {
        if(getIsEdit())
            confirmation = getConfirmation();
        else {
            confirmation = getRecordFromParams();
            prefillData();
        }
        fillOriginalValues();
    }

    /**
     * Initialize Stuck Truck record, isStuckTruck flag and originalIsStuckTruck.
     */
    private void initializeStuckTruck() {
        if(isExternalDispatcher) {
            stuckTruck = getStuckTruck();
            isStuckTruck = !isStuckTruck && String.isNotBlank(stuckTruck.Id);
            originalIsStuckTruck = isStuckTruck;
        }
    }

    /**
    * Initialize Load Confirmation record and retrieve data for Truck Trip (for prefill) if
    * truck trip Id passed in URL.
    */
    private FM_Load_Confirmation__c getRecordFromParams() {
        String ttID = getUrlParam(FM_LoadConfirmation_Utilities.TRUCKTRIP_ID_PARAM_KEY);
        confirmation = new FM_Load_Confirmation__c(Truck_Trip__c = ttID);
        confirmation.Truck_Trip__r = getTruckTripData();
        return confirmation;
    }

    /**
     * prefill Original values for validations of changes.
     * Unit Original value
     * Unit Change Reason original value.
     */
    private void fillOriginalValues() {
        if(confirmation != null && confirmation.Truck_Trip__c != null) {
            oldUnit = confirmation.Truck_Trip__r.Unit__c;
            oldUnitChangeReason = confirmation.Truck_Trip__r.Unit_Change_Reason__c;
        }
    }

    /*************************
    *********GETTERS**********
    *************************/

    /**
    * Get true if controller is in Edit mode.
    * Edit mode is when there is confirmationId passed as URL parameter.
    */
    public Boolean getIsEdit() {
        return String.isNotBlank(confirmationId);
    }

    /**
    * Get true if unit has been changed on Truck Trip.
    * Also false if confirmation or truck trip unpresent.
    */
    public Boolean getUnitChanged() {
        System.debug(DEBUG_PREFIX + ' getUnitChanged -> confirmation: ' + confirmation);
        if (confirmation != null && confirmation.Truck_Trip__r != null) {
            System.debug(DEBUG_PREFIX + ' getUnitChanged -> oldUnit: ' + oldUnit);
            return String.isNotBlank(oldUnit)
                && confirmation.Truck_Trip__r.Unit__c != oldUnit;
        }
        return false;
    }

    /**
     * Get's stuck truck record.
     * Edit mode: gets Last Stuck Truck for TT.
     * New mode: gets blank Stuck Truck with external stuck Truck record type
     * @return Stuck Truck record
     */
    private FM_Stuck_Truck__c getStuckTruck() {
        FM_Stuck_Truck__c stuckTruck;
        if(confirmation != null && String.isNotBlank(confirmation.Truck_Trip__c))
            stuckTruck = getLastStuckTruckByTruckTripId(confirmation.Truck_Trip__c);
        if(stuckTruck == null) 
            stuckTruck = FM_LoadConfirmation_Utilities.getExternalStuckTruck();
        return stuckTruck;
    }


    /*************************
    **DATA RETRIEVE METHODS***
    *************************/

    /**
     * Gets General Carrier unit for Carrier.
     * @param  carrierId Carrier Id on which General Unit should be search for.
     * @return           Carrier Unit Id.
     */
    private Id getGeneralCarrierUnit(Id carrierId) {
        return FM_LoadConfirmation_Utilities.getGeneralCarrierUnit(carrierId);
    }    

    /**
     * Return last stuck truck record on Truck Trip(Edit).
     * @param  truckTripId On which stuck Truck record should be searched for.
     * @return             Stuck Truck record.
     */
    private FM_Stuck_Truck__c getLastStuckTruckByTruckTripId(String truckTripId) {
        return FM_LoadConfirmation_Utilities.getLastStuckTruckByTruckTripId(truckTripId);
    }

    /**
    * If confirmation id is passed to controller it will load Load Confirmation data.
    * Used when initializing controller for edit mode.
    */
    private FM_Load_Confirmation__c getConfirmation() {
        return FM_LoadConfirmation_Utilities.getConfirmation(confirmationId);
    }

    /**
    * Loads truck trip data for prefill Load Confirmation data.
    */
    private FM_Truck_Trip__c getTruckTripData() {
        return FM_LoadConfirmation_Utilities.getTruckTripData(confirmation.Truck_Trip__c);
    }

    /**
    * Loads Load Confirmations based on selected Destination Facility for validation
    * of Scale Ticket Number (STN should be unique per Facility. Other facility can have
    * same STN).
    */
    private List<FM_Load_Confirmation__c> getLoadConfirmationOnFacilityWithScaleTicketNumber() {
        return FM_LoadConfirmation_Utilities.getLoadConfirmationOnFacilityWithScaleTicketNumber(
                                                                confirmation.Destination_Facility__c,
                                                                confirmation.Scale_Ticket_Number__c,
                                                                confirmation.Id);
    }

    /**
     * Gets Load Confirmation on TT (excluded the one provided as parameter)
     * @param  truckTripId    ID for which Truck Trip query will be filtering Confirmations
     * @param  confirmationId ID of Confirmation which should be excluded from search
     * @return                First Load Confirmation retrieved from query.
     */
    private FM_Load_Confirmation__c getConfirmationOnTT(Id truckTripId, Id confirmationId) {
        return FM_LoadConfirmation_Utilities.getLoadConfirmation(truckTripId, confirmationId);
    }


    /*************************
    *******PAGE ACTIONS*******
    *************************/

    /**
     * Handle IsStuckTruck value change. 
     * prefill Carrier and unit based on carrier/TruckTrip.
     */
    public void handleStuckTruckCheckboxChange() {
        if(confirmation.Truck_Trip__c != null
            && isStuckTruck) {
            stuckTruck.Carrier__c = confirmation.Truck_Trip__r.Carrier__c;
            if(confirmation.Truck_Trip__r.Unit__c != null)
                stuckTruck.Carrier_Unit__c = confirmation.Truck_Trip__r.Unit__c;
            else stuckTruck.Carrier_Unit__c = getGeneralCarrierUnit(confirmation.Truck_Trip__r.Carrier__c);
        }
    }

    /**
    * Handle when Truck Trip lookup is changed.
    * Load new Truck Trip data and prefill them to Load Confirmation.
    */
    public void handleTruckTripChange() {
        oldUnit = '';
        oldUnitChangeReason = '';
        if (confirmation != null && confirmation.Truck_Trip__c != null) {
            confirmation.Truck_Trip__r = getTruckTripData();
            fillOriginalValues();
        } else confirmation.Truck_Trip__r = null;
        prefillData();
    }

    /**
    * Handle when Destination Facility lookup is changed.
    * If entered Destination Facility, get confirmations for it(Scale Ticket Number validation)
    * Clear Destination Location lookup.
    */
    public void handleDestinationFacilityChange() {
        if (confirmation != null && String.isNotBlank(confirmation.Destination_Facility__c))
            confirmation.Destination_Location__c = null;
    }

    /**
    * Clear Destination Facility lookup
    */
    public void handleDestinationLocationChange() {
        if (confirmation != null && String.isNotBlank(confirmation.Destination_Location__c))
            confirmation.Destination_Facility__c = null;
    }

    /**
    * Handle action when unit change.
    * Reset Unit Change Reason when Unit changed to original value.
    */
    public void handleUnitChange() {
        if (!getUnitChanged())
            confirmation.Truck_Trip__r.Unit_Change_Reason__c = oldUnitChangeReason;
        handleChangeReasonOther();
   }

    /**
    * Handle Change reason other. 
    * Reset Unit Change Other Reason if there is not Other reason selected.
    */
    public void handleChangeReasonOther() {
      if (String.isBlank(confirmation.Truck_Trip__r.Unit_Change_Reason__c)
        || confirmation.Truck_Trip__r.Unit_Change_Reason__c != FM_LoadConfirmation_Utilities.UNIT_CHANGE_REASON_OTHER)
        confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = '';
    }

    /**
    * Do not save changes. If return Url passed as parameter, redirect to that URL, if not redirect
    * to Load Confirmation list view.
    */
    public PageReference cancel() {
        PageReference pr;
        String returnUrl = getUrlParam(FM_LoadConfirmation_Utilities.RETURN_URL_PARAM_KEY);
        if (String.isNotBlank(returnUrl))
            pr = new PageReference(returnUrl);
        else pr = new PageReference('/'+FM_Load_Confirmation__c.SObjectType.getDescribe().getKeyPrefix());
        pr.setRedirect(true);
        return pr;
    }

    /**
    * Save data(with validation running). If Save Url passed as parameter redirect to that URL, if not,
    * redirect to Detail page of Truck Trip for which was Load Confirmation created.
    */
    public PageReference save() {
        if (saveData()) {
            String saveReturnUrl = getUrlParam(FM_LoadConfirmation_Utilities.SAVERETURN_URL_PARAM_KEY);
            PageReference pr;
            if (String.isNotBlank(saveReturnUrl))
                pr = new PageReference(saveReturnUrl);
            else pr = new ApexPages.StandardController(confirmation.Truck_Trip__r).view();
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }

    /**
    * Save data(with validation running). and refresh Load Confirmation form.
    */
    public PageReference saveAndNew() {
        if (saveData()) {
            PageReference pr = Page.FM_LoadConfirmationEdit;
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }


    /*************************
    *******SAVE HELPERS*******
    *************************/

    /**
    * Run validation for Unit Changed reason and Scale Ticket Number (if necessary).
    * Then upsert Load Confirmation. If successfull update truck trip and return true if all went sucessfull.
    * Otherwise Create error msg and return false.
    */
    private Boolean saveData() {
        Savepoint sp = Database.setSavepoint();
        try {
            if(isDataValid()) {
                updateTruckTrip();
                if(doStuckTruckUpsert())
                    upsertStuckTruck();
                if (!ApexPages.hasMessages()) {
                    upsert confirmation;
                    return true;
                }
                Database.rollback(sp);
            }
        } Catch (Exception e) {
            System.debug(DEBUG_PREFIX + 'Error' + e);
            FM_LoadConfirmation_Utilities.addPageError('Error occured: ' + e.getMessage());
            Database.rollback(sp);
        }
        return false;
    }

    /**
    * Update Truck Trip.
    */
    private void updateTruckTrip() {
        update new FM_Truck_Trip__c(Id = confirmation.Truck_Trip__c,
                                    Unit_Change_Reason__c = confirmation.Truck_Trip__r.Unit_Change_Reason__c,
                                    Unit_Change_Other_Reason__c = confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c,
                                    Unit__c = confirmation.Truck_Trip__r.Unit__c);
    }

    /**
     * if new Stuck Truck record - populate some of the fields with default values.
     * Upsert.
     */
    private void upsertStuckTruck() {
        if(String.isBlank(stuckTruck.Id)) {
            Datetime now = Datetime.now();
            stuckTruck.Stuck_From__c = now;
            stuckTruck.Stuck_To__c = now;
            stuckTruck.Status__c = FM_LoadConfirmation_Utilities.STUCK_TRUCK_STATUS_EXTERNAL_DEFAULT;
            stuckTruck.Truck_Trip__c = confirmation.Truck_Trip__c;
            stuckTruck.Location__c = confirmation.Truck_Trip__r.Load_Request__r.Source_Location__c;
            stuckTruck.Facility__c = confirmation.Truck_Trip__r.Load_Request__r.Source_Facility__c;
        }
        upsert stuckTruck;
    }


    /*************************
    ********VALIDAITONS*******
    *************************/

    /**
     * Validate if Stuck Truck upsert can be performed.
     * No error msges can be present
     * User has to be External Dispatcher
     * isStuckTruck has to be set to true.
     */
    private Boolean doStuckTruckUpsert() {
        return !ApexPages.hasMessages() 
            && isExternalDispatcher 
            && isStuckTruck;
    }

    /**
     * Validate if data populated for LoadConfirmation and Stuck truck are valid
     */
    private Boolean isDataValid() {
        return isValidTruckTrip() 
            && isValidDestination() 
            && isValidConfirmation() 
            && isValidStuckTruck();
    }

    /**
     * Validate if Stuck Truck is valid.
     * Only external dispatcher can create Stuck Truck from this place.
     * Validation on Stuck Truck fields is runned only whe isStucktruck is true.
     */
    private Boolean isValidStuckTruck() {
        return !isExternalDispatcher
            || (!isStuckTruck
                || isValidStuckTruckFields());
    }

    /**
     * Validate if Stuck Truck has all required fields populated.
     */
    private Boolean isValidStuckTruckFields() {
        return isValidStuckTruckCarrier()
            && isValidStuckTruckCarrierUnit();
    }

    /**
     * Validate if Stuck Truck has Carrier assigned.
     */
    private Boolean isValidStuckTruckCarrier() {
        Boolean isValid = String.isNotBlank(stuckTruck.Carrier__c);
        if(!isValid)
            FM_LoadConfirmation_Utilities.addPageError('Carrier is required field.');
        return isValid;
    }

    /**
     * Validate if Stuck Truck has Carrier Unit assigned.
     */
    private Boolean isValidStuckTruckCarrierUnit() {
        Boolean isValid = String.isNotBlank(stuckTruck.Carrier_Unit__c);
        if(!isValid)
            FM_LoadConfirmation_Utilities.addPageError('Carrier Unit is required field.');
        return isValid;
    }

    /**
     * Validate if Truck Trip doesn't have Confirmation already
    * Validate if truck trip for which is Confirmation created is valid.
    * Vendor user has ability to confirm only exported truck trips.
    * Internal user has ability to confirm Dispatched, Add on and Exported truck trips.
    */
    private Boolean isValidTruckTrip() {
        if(existConfirmationOnTruckTrip()) return false;
        if(isTruckTripCompleted()) return true;
        if(isExternalDispatcher) return validateTruckTripExported();
        else return isTruckTripStatusValid();
    }

    /**
     * Checks if there already is Confirmation on Selected truck trip.
     * If there is put error msg on page.
     * @return true if exist
     */
    private Boolean existConfirmationOnTruckTrip() {
        Boolean exist = false;
        Id confirmationId = confirmation.Id;
        FM_Load_Confirmation__c truckTripConfirmation = getConfirmationOnTT(confirmation.Truck_Trip__c, confirmationId);
        exist =  truckTripConfirmation != null;
        if(exist) 
            FM_LoadConfirmation_Utilities.addPageError('There already exist Confirmation (' 
                                                        + truckTripConfirmation.Name 
                                                        + ') for this Truck Trip');
        return exist;
    }

    /**
    * Validate if only one Destination is filled up.
    * If not, throw error.
    */
    private Boolean isValidDestination() {
        Boolean valid = (confirmation != null && String.isBlank(confirmation.Destination_Facility__c) && String.isNotBlank(confirmation.Destination_Location__c))
                    || (confirmation != null && String.isNotBlank(confirmation.Destination_Facility__c) && String.isBlank(confirmation.Destination_Location__c));
        if (!valid)
            FM_LoadConfirmation_Utilities.addPageError('You have to fill in one Destination.');
        return valid;
    }

    /**
    * Validate if truck trip is exported. If not throw error.
    */
    private Boolean validateTruckTripExported() {
        Boolean isExported = isTruckTripExported();
        if (!isExported)
            FM_LoadConfirmation_Utilities.addPageError('Only exported truck trip can be confirmed.');
        return isExported;
    }

    /**
    * Check if truck trip is completed - editing existing LC.
    */
    private Boolean isTruckTripCompleted() {
        return confirmation != null
            && confirmation.Truck_Trip__r != null
            && confirmation.Truck_Trip__r.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_COMPLETED;
    }

    /**
    * Check if truck trip is exported.
    */
    private Boolean isTruckTripExported() {
        return confirmation != null
            && confirmation.Truck_Trip__r != null
            && confirmation.Truck_Trip__r.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_EXPORTED;
    }

    /**
    * Check if truck trip is addon.
    */
    private Boolean isTruckTripAddOn() {
        return confirmation != null
            && confirmation.Truck_Trip__r != null
            && confirmation.Truck_Trip__r.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_ADDON;
    }

    /**
    * Check if truck trip is dispatched.
    */
    private Boolean isTruckTripDispatched() {
        return confirmation != null
            && confirmation.Truck_Trip__r != null
            && confirmation.Truck_Trip__r.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED;
    }

    /**
    * Validate if truck trip is exported, addon or dispatched. Otherwise throw error.
    */
    private Boolean isTruckTripStatusValid() {
        Boolean isValid = isTruckTripExported()
                        || isTruckTripAddOn()
                        || isTruckTripDispatched();
        if (!isValid)
            FM_LoadConfirmation_Utilities.addPageError('Only dispatched, add on or exported truck trips can be confirmed.');
        return isValid;
    }

    /**
    * Return true if confirmation is valid.
    * Valid = valid unit change reason and scalit ticket number
    */
    private Boolean isValidConfirmation() {
        return isValidUnitChangeReason() && isValidScaleTicket();
    }

    /**
    * Return true if unit # was changed and Unit Change Reason was populated
    * or unit # wasn't changed and Unit Change Reason was not populate (is blank)
    */
    private Boolean isValidUnitChangeReason() {
        if (getUnitChanged()) return hasUnitChangeReason() 
                                && (isOtherUnitChangeReason() ? hasOtherUnitChangeReason() : true);
        else return hasNoUnitChangeReason();
    }

    /**
    * Return true if Unit Change reason is not blank. Otherwise return false and rise error.
    */
    private Boolean hasUnitChangeReason() {
        Boolean valid = String.isNotBlank(confirmation.Truck_Trip__r.Unit_Change_Reason__c);
        if (!valid) FM_LoadConfirmation_Utilities.addPageError('Unit # was changed. You need to specify Unit Change Reason.');
        return valid;
    }

    /**
    * Return true if Unit Change reason is blank. Otherwise return false and rise error.
    */
    private Boolean hasNoUnitChangeReason() {
        Boolean valid = String.isBlank(confirmation.Truck_Trip__r.Unit_Change_Reason__c)
                    || confirmation.Truck_Trip__r.Unit_Change_Reason__c == oldUnitChangeReason;
        if (!valid) FM_LoadConfirmation_Utilities.addPageError('Unit # was not changed. You cannot specify Unit Change Reason.');
        return valid;
    }

    /**
    * Return true if Unit Change Reason Other is not blank. Otherwise return false and rise error.
    */
    private Boolean hasOtherUnitChangeReason() {
        Boolean valid = String.isNotBlank(confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c);
        if (!valid) FM_LoadConfirmation_Utilities.addPageError('Please state the Other Reason for Unit change');
        return valid;
    }

    /**
    * Checks if there is 'Other' unit change reason.
    */
    private Boolean isOtherUnitChangeReason() {
        return confirmation != null
            && confirmation.Truck_Trip__r != null
            && confirmation.Truck_Trip__r.Unit_Change_Reason__c == FM_LoadConfirmation_Utilities.UNIT_CHANGE_REASON_OTHER;
    }

    /**
    * Custom validation on Scale Ticket Number.
    * If no STN entered - return true(valid)
    * If Destination Location entered - run validation on it.
    * If Destination Facility entered - run validation on it.
    * Otherwise return false(invalid) - no scenario where it can go to this point
    * as either Destination location or facility is required.
    */
    private Boolean isValidScaleTicket() {
        if (String.isBlank(confirmation.Scale_Ticket_Number__c)) return true;
        if (String.isNotBlank(confirmation.Destination_Location__c)) return isValidScaleTicketOnLocation();
        if (String.isNotBlank(confirmation.Destination_Facility__c)
            && String.isNotBlank(confirmation.Scale_Ticket_Number__c)) return isValidScaleTicketOnFacility();
        return false;
    }

    /**
    * As location doesn't have scales(there for no STN is entered or there shouldn't be validation
    * if it is unique) it will always return true. (Method is present in here just to leave here
    * note about this and for future if Husky decide that they want validation on location too).
    */
    private Boolean isValidScaleTicketOnLocation() {
        //Location doesn't have Scales (so no scale ticket number is expected) NOTE: add loc != null STN is not blank...
        //not sure if there is possibility to have location with scales.
        return true;
    }

    /**
    * Loops thru Facility confirmations list comparing entered STN with previous STNs for selected Destionation
    * facility. If there found equal STN there is an error msg logged in and validation is returned as false(invalid).
    * otherwise returned true(valid)
    */
    private Boolean isValidScaleTicketOnFacility() {
        List<FM_Load_Confirmation__c> confirmationsSTNDuplicate = getLoadConfirmationOnFacilityWithScaleTicketNumber();
        Boolean valid = (confirmationsSTNDuplicate == null || confirmationsSTNDuplicate.size() == 0);
        if (!valid)
            FM_LoadConfirmation_Utilities.addPageError('Scale Ticket Number is already used on Confirmation for Truck Trip ' 
                                                        + confirmationsSTNDuplicate.get(0).Truck_Trip__r.Name);
        return valid;
    }    

    /*************************
    *********HELPERS**********
    *************************/

    /**
    * Resets Load confirmation data (product and both destinations) and if there is Truck Trip lookup entered,
    * it will lad Destination and product from it. If there is destination facility loaded it will also reload,
    * Facility Confirmations list for Scale Ticket Number validation.
    */
    private void prefillData() {
        resetData();
        if (confirmation != null && confirmation.Truck_Trip__r != null) {
            setDestination();
            setProduct();
        }
    }

    /**
    * Resets Product, Destination Location and Destination Facility fields for Load Confirmation.
    */
    private void resetData() {
        confirmation.Product__c = null;
        confirmation.Destination_Location__c = null;
        confirmation.Destination_Facility__c = null;
    }

    /**
    * It will prefill Destination Facility or Destination Location based on Truck Trip Destination Id
    * and facility Type field.
    * If Facility type is Facility, it will load Destination Id to Destination Facility,
    * if Facility type is Location, it will load Destination Id to Destination Location,
    * If Facility type is blank, it will load Destination Id to Destination Location.
    * If Destination Id is blank, it will load Location Lookup value (if not blank) to Destination Location.
    */
    private void setDestination() {
        Map<String,Id> destinationIdMap = FM_LoadConfirmation_Utilities.getDestinationIdMap(confirmation.Truck_Trip__r);
        if(destinationIdMap.containsKey(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION)) 
            confirmation.Destination_Location__c = destinationIdMap.get(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION);
        if(destinationIdMap.containsKey(FM_TruckTrip_Utilities.FACILITY_TYPE_FACILITY)) 
            confirmation.Destination_Facility__c = destinationIdMap.get(FM_TruckTrip_Utilities.FACILITY_TYPE_FACILITY);
    }

    /**
    * It will prefill Product for Load Confirmation from Truck Trip Product field if present.
    */
    private void setProduct() {
        if (String.isNotBlank(confirmation.Truck_Trip__r.Product__c))
            confirmation.Product__c = confirmation.Truck_Trip__r.Product__c;
    }

    /**
    * Will return value from url parameter by key.
    */
    private String getUrlParam(String urlKey) {
        return ApexPages.currentPage().getParameters().get(urlKey);
    }

}