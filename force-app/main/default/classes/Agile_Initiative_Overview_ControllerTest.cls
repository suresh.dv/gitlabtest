@isTest
private class Agile_Initiative_Overview_ControllerTest {
	private static AA_Initiative__c record = new AA_Initiative__c();

	@isTest static void Test_Entire_Controller() {
		User runningUser  = AA_TestData.createAgileInitiativeUser();
		
		System.runAs(runningUser) {	
			record = AA_TestData.createInitiative(runningUser);
			PageReference pageRef = Page.Agile_Initiative_Overview;
            Test.setCurrentPageReference(pageRef);  
            Agile_Initiative_Overview_Controller controller = new Agile_Initiative_Overview_Controller(
            	new ApexPages.StandardController(record));

            System.assertEquals(AA_Utilities.getPhasePickListValues().size(), controller.getStageList().size());
		}
	}

	@isTest static void Test_Save() {
		User runningUser  = AA_TestData.createAgileInitiativeUser();
		
		System.runAs(runningUser) {	
			record = AA_TestData.createInitiative(runningUser);
			PageReference pageRef = Page.Agile_Initiative_Overview;
            Test.setCurrentPageReference(pageRef);  
            Agile_Initiative_Overview_Controller controller = new Agile_Initiative_Overview_Controller(
            	new ApexPages.StandardController(record));
            
            System.assertNotEquals(null, controller.getStageList());

            for(Agile_Initiative_Stage__c d: controller.getStageList()){
            	d.Name = 'tName';
            }

            controller.saveEdits();

            for(Agile_Initiative_Stage__c d: controller.getStageList()){
            	System.assertEquals('tName', d.Name);
            }

		}
	}
	
}