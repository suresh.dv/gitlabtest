@isTest
private class TestAddProductRelatedListOnAccount {
    
    // create test data
    @testSetup static void setup() {        
        
        //create account
        Account acc= new Account(
            Name = 'testAcc',
            BillingStreet = 'testStreet',
            BillingCity = 'tectcity',
            BillingState = 'testState',
            BillingPostalCode = '123',
            BillingCountry = 'testcountry',
            Description = 'testdesc'
        );       	
        insert acc;    
        
        //create opportunity
        Opportunity opp = new Opportunity(
            AccountId = acc.id,
            Amount = 1000.00,
            Description = 'testdesc',
            Name = 'testOpp',
            StageName = 'Prospecting',
            CloseDate = System.Today()
        );        
       	insert opp;
        
        //create opportunity for the same account to test product duplicate
        Opportunity oppDuplic = new Opportunity(
            AccountId = acc.id,
            Amount = 2000.00,
            Description = 'testdesc',
            Name = 'testOppDuplic',
            StageName = 'Prospecting',
            CloseDate = System.Today()
        );        
       	insert oppDuplic;
        
        // Insert a test product.
        Product2 prod = new Product2(
            Name = 'testProductName 01', 
            Family = 'Hardware'
        );
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
            UnitPrice = 10000, 
            IsActive = true
        );
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook', 
            isActive=true
        );
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = prod.Id,
            UnitPrice = 12000, 
            IsActive = true
        );
        insert customPrice;
                               
        // create opportunityLineItem
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice, 
            ServiceDate = System.today()
		);        
      	insert oli; 
                
        // create opportunityLineItem to test product duplicate
        OpportunityLineItem oliDuplic = new OpportunityLineItem(
            OpportunityId = oppDuplic.Id,
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice, 
            ServiceDate = System.today()
		);        
      	insert oliDuplic;        
    }
    
    @isTest static void TestAddProductRelatedListOnAccount() {
        
        Opportunity[] oppFirst = [SELECT Id, Name FROM Opportunity];
        oppFirst[0].StageName = 'Offer Issued – Won';
        update oppFirst[0];        
        
        Opportunity[] oppSecond = [SELECT Id, Name FROM Opportunity];
        oppFirst[1].StageName = 'Offer Issued – Won';
        update oppFirst[1];        
		        
        // Perform test
        Test.startTest();            
            cpm_AccountProductAssociation__c[] apaRes = [SELECT Id, Name, Product_Name__c FROM cpm_AccountProductAssociation__c];
        Test.stopTest();
                        
        System.assert(apaRes != null);
        System.assertEquals('testProductName 01', apaRes[0].Product_Name__c);
        System.assert(apaRes.size() == 1);
    }
}