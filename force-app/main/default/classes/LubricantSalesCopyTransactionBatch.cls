/*This batch class is used to copy Lubricant Sales Transaction records from current year to next year
To schedule this job, please run this script in Developer Console

    //this job will run at midnight Jan 20, 2016
    try
    {
        String CRON_EXP = '0 0 1 20 1 ? *';
        System.schedule('CopySalesTransactionGoalJob', CRON_EXP, new LubricantSalesCopyTransactionBatch());
    }
    catch(Exception e)
    {
        System.debug('The job cannot be scheduled. Please contact development team!');
    }
*/
global class LubricantSalesCopyTransactionBatch implements Database.Batchable<sObject>, Schedulable
{
    //check why there is duplicate trans????????
    global Set<String> latestTransactionSet = new Set<String>();
    global integer lastYear;
    global Integer recordCount = 0;
    global integer currentYear;
    global LubricantSalesCopyTransactionBatch()
    {
        currentYear = date.today().year();
        lastYear = currentYear - 1;
/*
        //find the latest transactions of each(Account, Product Sector, Product Segment, Sales Rep)
        List<AggregateResult> latestTrans = [select  Account__c, Product_Sector__c, Product_Segment__c, max(Latest_Transaction_Date__c) LatestDate, Sales_Rep__c
                                       from Lubricant_Sales_Transaction_By_Quarter__c
                                        where CALENDAR_YEAR(Latest_Transaction_Date__c) =: lastYear
                                        group by Account__c, Product_Sector__c, Product_Segment__c, Sales_Rep__c];
       
        for(AggregateResult trans : latestTrans)
        {
            String key = trans.get('Account__c') + '/' + trans.get('Product_Sector__c') + '/' + 
                        trans.get('Product_Segment__c') + '/' + trans.get('LatestDate') + '/' + trans.get('Sales_Rep__c');
                    
            latestTransactionSet.add(key);
        }
*/        
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        
        return Database.getQueryLocator([select Latest_Transaction_Date__c, Account__c, Product_Sector__c, Product_Segment__c, Sales_Rep__c,
                    Volume_Actual_L__c, Gross_Profit_Actual__c, SOW_Gross_Profit_Actual__c, SOW_Volume_Actual_L__c, Quarter_1_to_4__c 
                    from Lubricant_Sales_Transaction_By_Quarter__c
                    where CALENDAR_YEAR(Latest_Transaction_Date__c) =: lastYear]);
    }
   
    global void execute(Database.BatchableContext BC, List<Lubricant_Sales_Transaction_By_Quarter__c> scope) 
    {
        System.debug('latestTransactionSet =' + latestTransactionSet);
        this.recordCount = scope.size();
        List<Lubricant_Sales_Transaction_By_Quarter__c> transNextYear = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
       
        for(Lubricant_Sales_Transaction_By_Quarter__c obj : scope)
        {
//KK 2015-02-06:  don't really need to do any key checking.  Each record from previous year needs to be copied as a brand new record to the following year.
// So the mapping is 1 to 1.
/*        
            String key = obj.Account__c + '/' + obj.Product_Sector__c + '/' + obj.Product_Segment__c + '/' + obj.Latest_Transaction_Date__c + '/' + obj.Sales_Rep__c;
            if (latestTransactionSet.contains(key))
            {
*/
            
               Lubricant_Sales_Transaction_By_Quarter__c newTrans = new Lubricant_Sales_Transaction_By_Quarter__c();
/*KK 2015-02-10:
The information that needs to be copied over includes the following, although not necessarily one for one.  For example, the 4 Actual fields are copied into their
corresponding Goal fields.
- OwnerId (THIS IS THE MOST IMPORTANT because this affects who can view the record)
- Account__c
- Gross_Profit_Actual__c
- Latest_Transaction_Date__c
- Product_Sector__c
- Product_Segment__c
- Sales_Rep__c
- SOW_Gross_Profit_Actual__c
- SOW_Volume_Actual_L__c
- Volume_Actual_L__c
*/               
               newTrans.Account__c = obj.Account__c;
               newTrans.Product_Sector__c = obj.Product_Sector__c;
               newTrans.Product_Segment__c = obj.Product_Segment__c;
               if (obj.Quarter_1_to_4__c == 'Q1')
                 newTrans.Latest_Transaction_Date__c = date.newInstance(currentYear, 1, 1);
               else if (obj.Quarter_1_to_4__c == 'Q2')
                 newTrans.Latest_Transaction_Date__c = date.newInstance(currentYear, 4, 1);
               else if (obj.Quarter_1_to_4__c == 'Q3')
                 newTrans.Latest_Transaction_Date__c = date.newInstance(currentYear, 7, 1);
               else if (obj.Quarter_1_to_4__c == 'Q4')
                 newTrans.Latest_Transaction_Date__c = date.newInstance(currentYear, 10, 1);
                  
               newTrans.Volume_Goal_L__c = obj.Volume_Actual_L__c;
               newTrans.Gross_Profit_Goal__c = obj.Gross_Profit_Actual__c;
               newTrans.SOW_Volume_Goal_L__c = obj.SOW_Volume_Actual_L__c;
               newTrans.SOW_Gross_Profit_Goal__c = obj.SOW_Gross_Profit_Actual__c;
               newTrans.Sales_Rep__c = obj.Sales_Rep__c;
               newTrans.OwnerId = obj.Sales_Rep__c;
               transNextYear.add(newTrans);
/*
            }
*/            
        }
        insert transNextYear;
    }   
    
    global void finish(Database.BatchableContext BC) {
        String sysMessage = '';
/*
King Koo:  2015-02-24:  NOTE that no email messages are being sent out when the job is completed.
There have been no business requirements for this.

Also note that the role hierarchy has changed, so even if in the future an email is required to be sent out,
the code needs to be revisited to reflect the new role names.
*/      

/*        
        sysMessage += 'Dear Sir/Madam\n\r';
        sysMessage += 'The following is the statistics from batch job LubricantSalesCopyTransactionBatch:\n\r';
        sysMessage += 'You have requested to copy Chevron Transform Fuels & Lubricants Transactions actuals from '+ lastYear + ' to goals ' + currentYear + '\n\r';
        sysMessage += 'The number of records copied is ' + this.recordCount + '\n\r';
        sysMessage += 'Thank you very much.\n\r';
        sysMessage += 'Husky Retail Team';
        
       
              
        List<String> managers = new List<String>();
        for (User user : [ SELECT Email FROM User 
*/        
/*                        WHERE (UserRole.DeveloperName = 'Lubricant_Inside_Sales_Manager' */
/*                                or UserRole.DeveloperName = 'Lubricant_Sales_Manager') And IsActive = true])*/
/*
        {
            
             managers.add(user.Email);
            
        }
        if (managers.size() > 0)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            mail.setToAddresses(managers);
            mail.setSenderDisplayName('Husky Retail Team');
            mail.setSubject('Retail Transaction Batch Schedule results');
            mail.setPlainTextBody(sysMessage);
            mail.setBccSender(false);
            mail.setSaveAsActivity(false);
            mail.setUseSignature(false);
            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
*/        
    }
    // This is for the scheduling
    global void execute(SchedulableContext SC)
    {
        LubricantSalesCopyTransactionBatch b = new LubricantSalesCopyTransactionBatch();
        Database.executeBatch(b);
    }
}