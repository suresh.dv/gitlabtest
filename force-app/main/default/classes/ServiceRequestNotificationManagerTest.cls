/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for ServiceRequestNotificationManager
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class ServiceRequestNotificationManagerTest
{
    static List<HOG_Maintenance_Servicing_Form__c> riglessServicingForm;
	static List<HOG_Service_Request_Notification_Form__c> serviceRequest;
	static List<Well_Tracker__c> wellTracker;
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static List<HOG_Work_Order_Type__c> workOrderType;
	static Equipment__c equipmentLocation;    
    static HOG_Service_Code_MAT__c serviceCodeMAT;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static Business_Unit__c businessUnit;
	static Business_Department__c businessDepartment;
	static Operating_District__c operatingDistrict;
	static Field__c field;
	static Route__c route;
	static Location__c location;	
	static Facility__c facility;		
	static Account account;			
	static HOG_Settings__c settings;
	static Id recordTypeId;
	static String NOTIFICATIONOBJECTNAME = 'HOG_Service_Request_Notification_Form__c';
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_Test()
    {                                                
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[0].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }
 
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Facility_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[1].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[1].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[1].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }    

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Equipment_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[2].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[2].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[2].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }

/*
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_WO_Mock_Test()
    {                                                
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[0].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrdersResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }
    
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_NO_Mock_Test()
    {                                                
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[2].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrdersResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Facility_Mock_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[1].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[1].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[1].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGCreateNotificationsResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }    
*/
    
	private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--
        
        //-- Setup Notification Type
	    notificationType = new List<HOG_Notification_Type__c>();        
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, true, true, true));
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, true, true));
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]);
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
     	notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, false));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, false));                
        insert notificationTypePriority;        
        //--
                
        //-- Setup Work Order Type
 	    workOrderType = new List<HOG_Work_Order_Type__c>();
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[1].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);
		location.DLFL__c = false;		
        insert location;

 		equipmentLocation = EquipmentTestData.createEquipment(location);

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;

		wellTracker = new List<Well_Tracker__c>();
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
        insert wellTracker;
        //--

	    ServiceRequestNotificationUtilities.executeTriggerCode = false;
    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;

		serviceRequest = new List<HOG_Service_Request_Notification_Form__c>();

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'7521256',
				'98765430',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));
			
		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[1].Id,
				workOrderType[1].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[3].Id,
				notificationTypePriority[2].Id,
				account.Id,
				'7521257',
				'98765431',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[1].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'7521258',
				'98765432',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));

		 serviceRequest[0].Location_Lookup__c = location.Id;
         serviceRequest[0].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Well Tracker');        
		 
		 serviceRequest[1].Facility_Lookup__c = facility.Id;
         serviceRequest[1].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Facility Tracker');        
		 		 
		 serviceRequest[2].Location_Lookup__c = location.Id;
		 serviceRequest[2].Equipment__c = equipmentLocation.Id;
         serviceRequest[2].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Equipment');        

         insert serviceRequest;
        //-- End setup of data needed to test the Service Request Notification controller --//

        //-- Start setup data for Work Order        
        riglessServicingForm = new List<HOG_Maintenance_Servicing_Form__c>();
        
		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

    	request.workOrderName = 'Test Location';    	
    	request.workOrderCount = 1;
		request.notificationType = notificationType[0].Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;		
        					
		request.record = serviceRequest[0];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[1];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[2];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
        
        insert riglessServicingForm;                               
        //-- End setup data for Work Order        

		settings = HOGSettingsTestData.createHOGSettings();
		insert settings;
	}    
}