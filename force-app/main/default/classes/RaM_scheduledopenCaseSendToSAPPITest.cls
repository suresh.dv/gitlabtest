@isTest
private class RaM_scheduledopenCaseSendToSAPPITest{
    @testSetUp static void setup()
  {
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
     User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='testnewuser@testorg.com');
      insert u;
      
      System.runAs(u){
        
      // Insert account
      Account accountRecord = new Account(Name = 'Husky Account',VendorGroup__c='BUNN-O-MATIC');
      accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
      insert accountRecord;
      
      //Insert Contact
      Contact contactRecord = new Contact(LastName = 'Glenn Mundi', ContactLogin__c='PRQ1234');
      contactRecord.AccountId = accountRecord.id;
      insert contactRecord;    
      
     // Insert Location
      RaM_Location__c locationRecord = new RaM_Location__c(Name = '10001',Service_Area__c = 'Island',SAPFuncLOC__c = 'C1-02-002-01006', Location_Classification__c ='Urban', R_M_Location__c=true);
      insert locationRecord;
     
      //Insert Equipment
      RaM_Equipment__c equipmentRecord = new RaM_Equipment__c(Name = 'Test1');
      //equipmentRecord.FunctionalLocation__c = locationRecord.id;
      equipmentRecord.Equipment_Type__c = 'ELECTRICAL'; 
      equipmentRecord.Status__c = 'Active';
      equipmentRecord.Equipment_Class__c = 'ELECTRICAL PANEL';
      equipmentRecord.FunctionalLocation__c='C1-02-002-01006';
      insert equipmentRecord;
      
      // Insert Vendor Assignment 
      RaM_VendorAssignment__c vaRecord = new RaM_VendorAssignment__c();
      vaRecord.Account__c = accountRecord.id;
      vaRecord.Equipment_Type_VA__c = 'ELECTRICAL';
      vaRecord.Equipment_Class_VA__c = 'ELECTRICAL PANEL'; 
      vaRecord.Priority__c = 1;
      vaRecord.ServiceArea__c='Island';
      vaRecord.Location_Classification_VA__c='Urban';
      insert vaRecord; 
      
      //Insert Entitlement Record
      Entitlement entitlementRecord = new Entitlement(Name = 'SLA Husky Energy',AccountId = AccountRecord.id);
      insert entitlementRecord; 
      
      // Insert Ram Setting 
      RaM_Setting__c cusSetting=new RaM_Setting__c();
      cusSetting.name='Case Next Counter';
      cusSetting.R_M_Ticket_Number_Next_Sequence__c=1234567;
      insert cusSetting;
      
      Id childTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId(); 
      
    Case c1 = new Case(RecordTypeId = childTicketRecordTypeId,Status = 'New',Priority = 'P2-Break/Fix',Origin = 'Maintenance Tech',LocationNumber__c = LocationRecord.id,EquipmentName__c=equipmentRecord.id, Location_Contact__c='Amit Singh',Equipment_Type__c='ELECTRICAL',Equipment_Class__c='ELECTRICAL PANEL', Location_Classification__c='Urban',Service_Area__c='Island' , subject='test', description='test'  );    
    System.assertEquals(c1.status,'New');
    insert c1;
            
        //insert cases  
      
      c1.Status='Assigned';
      c1.Vendor_Assignment__c=vaRecord.id;
      update c1;
            
    }
    }    
    static testMethod void testschedule() {
        Test.StartTest();
        RaM_scheduledopenCaseSendToSAPPI sh1 = new RaM_scheduledopenCaseSendToSAPPI();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test SAP PI Check', sch, sh1); Test.stopTest(); 
    }
}