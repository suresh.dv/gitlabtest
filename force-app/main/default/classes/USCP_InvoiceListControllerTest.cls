/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_InvoiceListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_Invoices;
            Test.setCurrentPageReference(pageRef);


            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);

            
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);



            Date invdate =  Date.today();
            //create test eft
            USCP_EFT__c eft = USCP_TestData.createEFT(account.Id, invdate , 1000.0, true);


            //create test invoice and link it to the second eft
            USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100',  eft.id,  invdate , invdate , invdate , 1000.0,  true);
            USCP_TestData.createInvoice(account.Id, 'INV200',  eft.id, invdate , invdate , invdate , 2000.0,  true);

            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);
            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL1', invoice.id,  'Carrier 1', terminal.id,   invdate , product.id ,  true);

            //test account search method for autocomplete
            System.AssertEquals(1,USCP_InvoiceListController.searchAccount('100').size());

            //test terminal search method for autocomplete
            System.AssertEquals(1,USCP_InvoiceListController.searchTerminal('trm1').size());

            //now lets create controller
            USCP_InvoiceListController controller = new USCP_InvoiceListController ();
            //by default controller will not query data so we need to setup some filters and query it
            controller.AccountID = account.id;            
            controller.DateRangeType = 'invoicedate' ;            
            controller.dateFrom = Date.Today()-1;
            controller.RefreshData();              
            //we should have 2 records           
            System.AssertEquals(2, controller.getWInvoices().size());             
            //we should have 2 records            
            System.AssertEquals(2, controller.getAllWInvoices().size());              

            //set search parameters for the controller
            controller.InvoiceNumber  = '100';
            controller.BOLNumber  = 'BOL1';            
            controller.DateRangeType = 'invoicedate' ;
            controller.dateFrom = Date.Today()-1;
            controller.dateTo = Date.Today(); 
            controller.Location = 'trm1';
            
            System.AssertNotEquals(null,controller.dateFromStr);      
            System.AssertNotEquals(null,controller.dateToStr);            

            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getInvoices().size());    

            controller.ClearFilter();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getInvoices().size());    


            controller.InvoiceNumber  = '100';
            //controller.BOLNumber  = 'BOL1';            
            controller.DateRangeType = 'shipdate';
            controller.dateFrom = Date.Today()-1;
            controller.dateTo = Date.Today(); 
            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getInvoices().size());   
            controller.ClearFilter();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getInvoices().size());  
            
            controller.InvoiceNumber  = '100';
            //controller.BOLNumber  = 'BOL1';            
            controller.DateRangeType = 'duedate';
            controller.dateFrom = Date.Today()-1;
            controller.dateTo = Date.Today();
            controller.selectedTerminalID = terminal.id;  
            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getInvoices().size());  

            System.AssertNotEquals(null, controller.SaveToExcel());             
             
            controller.first(); 
            controller.previous();            
            controller.next();                        
            controller.last(); 
            //we should have 1 record for page 1
            System.AssertEquals(1, controller.getInvoices(1).size());             
            //we should have 1 record
            System.AssertEquals(1, controller.getAllInvoices().size());                        
             
            //no selected invoices
            System.AssertEquals(null, controller.getSelected());    
             
            controller.ClearFilter();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getInvoices().size()); 
             
            controller.InvoiceNumber  = '100';
            //controller.BOLNumber  = 'BOL1';            
            controller.DateRangeType = 'duedate';
            controller.dateFromStr = '01/01/2014';
            controller.dateToStr = '01/01/2014';
            controller.RefreshData();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getInvoices().size());              



             
        }
    }

}