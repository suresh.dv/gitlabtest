public class PREPMatSerGroupPlannedScheduleTrgHelper
{
    public static boolean runTrigger = false;
    public static List<PREP_Mat_Serv_Group_Planned_Schedule__c> newPlannedScheduleList = new List<PREP_Mat_Serv_Group_Planned_Schedule__c>();
    public static List<PREP_Mat_Serv_Group_Planned_Schedule__c> oldPlannedScheduleList = new List<PREP_Mat_Serv_Group_Planned_Schedule__c>();
    public static Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c> newPlannedScheduleMap = new Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c>();
    public static Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c> oldPlannedScheduleMap = new Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c>();
    
    public static void addUpdateAcqCycle(){
        Set<Id> matServiceAllocationIds = new Set<Id>();
        Id matACId = Schema.getGlobalDescribe().get( 'PREP_Acquisition_Cycle__c' ).getDescribe().getRecordTypeInfosByName().get( 'Mat Acquisition Cycle' ).getRecordTypeId();
        Id servACId = Schema.getGlobalDescribe().get( 'PREP_Acquisition_Cycle__c' ).getDescribe().getRecordTypeInfosByName().get( 'Serv Acquisition Cycle' ).getRecordTypeId();
        Id matScheduleId = Schema.getGlobalDescribe().get( 'PREP_Mat_Serv_Group_Planned_Schedule__c' ).getDescribe().getRecordTypeInfosByName().get( 'Mat Group Baseline Schedule' ).getRecordTypeId();
        Id servScheduleId = Schema.getGlobalDescribe().get( 'PREP_Mat_Serv_Group_Planned_Schedule__c' ).getDescribe().getRecordTypeInfosByName().get( 'Serv Group Baseline Schedule' ).getRecordTypeId();
        
        for( PREP_Mat_Serv_Group_Planned_Schedule__c mat : newPlannedScheduleList ){
            if( mat.Acquisition_Cycle_Type__c != null && mat.Acquisition_Cycle_Type__c != ''
                && mat.Acquisition_Cycle_Type__c != oldPlannedScheduleMap.get( mat.Id ).Acquisition_Cycle_Type__c ){
                if( mat.Mat_Serv_Group_Baseline_Allocation_Id__c != null )
                    matServiceAllocationIds.add( mat.Mat_Serv_Group_Baseline_Allocation_Id__c );
            }
        }
        
        Map<Id, PREP_Mat_Serv_Group_Baseline_Allocation__c> baselineAllocations = new Map<Id, PREP_Mat_Serv_Group_Baseline_Allocation__c>([ Select Id, Name, Baseline_Id__c
                                                                                    from PREP_Mat_Serv_Group_Baseline_Allocation__c
                                                                                    where Id IN :matServiceAllocationIds
                                                                                    AND Baseline_Id__c != null ]);
        
        Set<Id> baselineIds = new Set<Id>();
        for( PREP_Mat_Serv_Group_Baseline_Allocation__c alloc : baselineAllocations.values() ){
            baselineIds.add( alloc.Baseline_Id__c );
        }
        
        Map<Id, PREP_Baseline__c> baselines = new Map<Id, PREP_Baseline__c>([ Select Id, Name, Initiative_Id__c from PREP_Baseline__c where
                                                                                Id IN :baselineIds AND Initiative_Id__c != null ]);
    
        Set<Id> initiativeIDs = new Set<Id>();
        for( PREP_Baseline__c base : baselines.values() ){
            initiativeIDs.add( base.Initiative_Id__c );
        }
        
        Map<Id, PREP_Initiative__c> initiatives = new Map<Id, PREP_Initiative__c>([ Select Id, ( Select Id, Type__c, Name, RecordTypeId, RecordType.Name from Acquisition_Cycles__r)
                                                                                    from PREP_Initiative__c where Id IN :initiativeIDs ]);
    
        for( PREP_Mat_Serv_Group_Planned_Schedule__c mat : newPlannedScheduleList ){
            if( mat.Acquisition_Cycle_Type__c != null && mat.Acquisition_Cycle_Type__c != ''
                && mat.Acquisition_Cycle_Type__c != oldPlannedScheduleMap.get( mat.Id ).Acquisition_Cycle_Type__c ){
                if( mat.Mat_Serv_Group_Baseline_Allocation_Id__c != null
                    && baselineAllocations.containsKey( mat.Mat_Serv_Group_Baseline_Allocation_Id__c ))
                {
                    PREP_Mat_Serv_Group_Baseline_Allocation__c baselineAlloc = baselineAllocations.get( mat.Mat_Serv_Group_Baseline_Allocation_Id__c );
                    if( baselineAlloc.Baseline_Id__c != null && baselines.containsKey( baselineAlloc.Baseline_Id__c ))
                    {
                        PREP_Baseline__c base = baselines.get( baselineAlloc.Baseline_Id__c );
                        if( base.Initiative_Id__c != null && initiatives.containsKey( base.Initiative_Id__c ))
                        {
                            for( PREP_Acquisition_Cycle__c acq : initiatives.get( base.Initiative_Id__c ).Acquisition_Cycles__r )
                            {
                                if( acq.RecordType.Name.equalsIgnoreCase( 'Mat Acquisition Cycle' ) && acq.Type__c == mat.Acquisition_Cycle_Type__c
                                    && mat.RecordTypeId == matScheduleId )
                                {
                                    mat.Acquisition_Cycle__c = acq.Id;
                                    break;
                                }
                                else if( acq.RecordType.Name.equalsIgnoreCase( 'Serv Acquisition Cycle' ) && acq.Type__c == mat.Acquisition_Cycle_Type__c
                                    && mat.RecordTypeId == servScheduleId )
                                {
                                    mat.Acquisition_Cycle__c = acq.Id;
                                    break;
                                }
                                /*if( acq.RecordType.Name.equalsIgnoreCase( 'Serv Acquisition Cycle' ) && acq.Type__c == mat.Acquisition_Cycle_Type__c
                                    && mat.RecordTypeId == matScheduleId )
                                {
                                    mat.Acquisition_Cycle__c = acq.Id;
                                    break;
                                }
                                else if( acq.RecordType.Name.equalsIgnoreCase( 'Serv Acquisition Cycle' ) && acq.Type__c == mat.Acquisition_Cycle_Type__c
                                    && mat.RecordTypeId == servScheduleId )
                                {
                                    mat.Acquisition_Cycle__c = acq.Id;
                                    break;
                                }*/
                            }
                        }
                    }
                }
            }
        }
    }
   /* 

    public static void InsertAcqCycle(list<PREP_Mat_Serv_Group_Planned_Schedule__c> mspsalist)// = [SELECT Id, Name, RecordType.Name, Mat_Serv_Group_Baseline_Allocation_Id__c, Acquisition_Cycle_Type__c, Acquisition_Cycle__c FROM PREP_Mat_Serv_Group_Planned_Schedule__c])
     { 
     System.debug('88'+mspsalist);
     list<PREP_Initiative__c> initlist =[SELECT Id, Name,(select Id,Type__c,Name,Initiative__c from Acquisition_Cycles__r) FROM PREP_Initiative__c where RecordType.Name ='Projects Initiative'];
list<PREP_Baseline__c> baselist=[SELECT Id, Name,Initiative_Id__c FROM PREP_Baseline__c where Initiative_Id__c IN : initlist];
list<PREP_Mat_Serv_Group_Baseline_Allocation__c> msalist =[SELECT Baseline_Id__c,RecordType.Name, Id FROM PREP_Mat_Serv_Group_Baseline_Allocation__c where Baseline_Id__c IN : baselist]; 
     list<PREP_Mat_Serv_Group_Planned_Schedule__c> ms =new list<PREP_Mat_Serv_Group_Planned_Schedule__c>();
     list<PREP_Mat_Serv_Group_Planned_Schedule__c> Msplist =[SELECT Id, Name, RecordType.Name, Mat_Serv_Group_Baseline_Allocation_Id__c, Acquisition_Cycle_Type__c, Acquisition_Cycle__c FROM PREP_Mat_Serv_Group_Planned_Schedule__c where ID IN :mspsalist AND Mat_Serv_Group_Baseline_Allocation_Id__c IN : msalist];
     for(PREP_Mat_Serv_Group_Planned_Schedule__c ms1 :Msplist )
     {
     ms.add(ms1);
     }
     System.debug('99'+Msplist );
     
//Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c> mspsToUpdate = new map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c>();
List<PREP_Mat_Serv_Group_Planned_Schedule__c> mspsToUpdate = new List<PREP_Mat_Serv_Group_Planned_Schedule__c>();
//Map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c> mspsToUpdate1 = new map<Id, PREP_Mat_Serv_Group_Planned_Schedule__c>();
List<PREP_Mat_Serv_Group_Planned_Schedule__c> mspsToUpdate1 = new List<PREP_Mat_Serv_Group_Planned_Schedule__c>();
List<PREP_Acquisition_Cycle__c> acqcycles = [Select Id,Type__c,RecordType.Name,Initiative__c from PREP_Acquisition_Cycle__c where Initiative__c IN :initlist and (recordtype.name='Serv Acquisition Cycle' OR recordtype.name='Mat Acquisition Cycle')];// AND Type__c ='Standard 12' ];
for(PREP_Initiative__c ini : initlist) 
    {
        if(ini.Id != null)
        {   system.debug('ini.name'+ini.name);
            for(PREP_Baseline__c bsl : baselist)
                {
                    if(bsl.Initiative_Id__c== ini.Id)
                        {
                            for(PREP_Mat_Serv_Group_Baseline_Allocation__c msa : msalist)
                                {     
                                    if(msa.Baseline_Id__c== bsl.Id )
                                    {   
                                                          
                                        for(PREP_Mat_Serv_Group_Planned_Schedule__c msps : mspsalist)
                                            {
                                            system.debug('mspsalist'+msps ); 
                                            for(PREP_Mat_Serv_Group_Planned_Schedule__c ms1 :Msplist )
                                                 {
                                                     if(ms1.Id == msps.Id)
                                                     {
                                            
                                                if(msps.Mat_Serv_Group_Baseline_Allocation_Id__c== msa.Id )
                                                {
                                                           for( PREP_Acquisition_Cycle__c acqcycle :  acqcycles)
                                        {   
                                            system.debug('acqcycles'+acqcycles);  
                                                                
                                                                if(acqcycle.Initiative__c==ini.Id && acqcycle.Type__c==msps.Acquisition_Cycle_Type__c)
                                                                {
                                                                    system.debug('1acqcy'+acqcycle);
                                                                if(acqcycle.RecordType.Name=='Serv Acquisition Cycle' && acqcycle.Type__c==msps.Acquisition_Cycle_Type__c)// || acqcycle.Type__c =='Standard 6' || acqcycle.Type__c =='Standard 2'))
                                                                    { system.debug('2acqcy'+acqcycle);
                                                                        if(ms1.RecordType.Name == 'Serv Group Baseline Schedule')                                           
                                                                            {
                                                                            system.debug('1serv'+msps);

                                                                            msps.Acquisition_Cycle__c = acqcycle.id;                                                            
                                                                            //mspsToUpdate.put(msps.id, msps);
                                                                            //mspsToUpdate.add(msps);
                                                                            //system.debug('service'+mspsToUpdate);
                                                                             //update mspsToUpdate.values();        
                                                                            }
                                                                    }
                                                                    if(ms1.RecordType.Name == 'Mat Group Baseline Schedule' && acqcycle.Type__c== msps.Acquisition_Cycle_Type__c)                                           
                                                                            {
                                                                            if(acqcycle.RecordType.Name=='Mat Acquisition Cycle')//)// || acqcycle.Type__c =='Standard 6' || acqcycle.Type__c =='Standard 2'))
                                                                                {
                                                                        
                                                                            
                                                                            msps.Acquisition_Cycle__c = acqcycle.id;                                                            
                                                                            //mspsToUpdate1.put(msps.id, msps);
                                                                           // mspsToUpdate1.add(msps);
                                                                           
                                                                             //update mspsToUpdate1.values();        
                                                                            }
                                                                    }
                                                                
                                                                }        
                                                           
                                                           }
                                                           }                                                                         
                                                             
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                }
        }
    }

    }*/
}