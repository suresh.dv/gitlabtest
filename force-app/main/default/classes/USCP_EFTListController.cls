public  with sharing class USCP_EFTListController extends DynamicListController {

    public Boolean IsAdmin {get;set;}
    public String AccountId {get;set;}    
    public String Account {get;set;}  


    public String InvoiceNumber {get;set;}
    public String BOLNumber {get;set;}
    public String DateRangeType {get;set;}
    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}  
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            if(d==null)
            {
                return null;
            }
            return d.format() ;
        }
        set
        {
            dateFrom = USCP_Utils.convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            if(d==null)
            {
                return null;
            }            
            return d.format() ;
        }
        set
        {
            dateTo =  USCP_Utils.convertDate(value); //Date.valueOf(value);
        }
    }


    public USCP_EFTListController () {
        // TODO: call the parent class constructor with a base query
        super('select Id, Name, Account__c, Account__r.Name, Draft_Date__c,Amount__c, CreatedDate, (select id, name, Amount__c from Invoices__r order by name) from USCP_EFT__c');
        pageSize = 20;
        //query();
        
        sortColumn = 'Draft_Date__c';        
        sortAsc = false;        
        
        IsAdmin = USCP_Utils.IsAdminUser();   

        ResetFilters();
    }

    // cast the resultset
    public List<USCP_EFT__c> getEFTRecords() {
        // TODO: cast returned records into list of Accounts
        return (List<USCP_EFT__c>) getRecords();
    }
    public List<EFTWrapper> getWEFTRecords() {
        List<EFTWrapper> result = new List<EFTWrapper>();   
        for(USCP_EFT__c obj: (List<USCP_EFT__c>) getRecords())
        {
              result.add(new EFTWrapper(obj));
        }
        return result;
    }  
    
    
    // cast the resultset
    public List<USCP_EFT__c> getAllEFTRecords() {
        // TODO: cast returned records into list of Accounts
        return (List<USCP_EFT__c>) getAllRecords();
    }    

    private void ResetFilters()
    {
        AccountId      ='';
        Account = '';
        InvoiceNumber  = '';
        BOLNumber ='';    
        dateFrom = null; //Date.Today()-1;
        dateTo = null; //Date.Today()-1;
        DateRangeType ='na';
    }    
    public PageReference ClearFilter() {
    
        ResetFilters();  
        this.WhereClause  = ' Name=null '; //always false, no data load
        query();
    
        return null;    
    }    
    public PageReference RefreshData() {  
            String FilteredSOQL = ' Name!=null '; // always true
            Boolean filtersON = false; 

            if(AccountId!='')
                {
                filteredSOQL  = filteredSOQL + ' and Account__c = \'' +  String.escapeSingleQuotes(AccountId) + '\'';
                filtersON = true;
                }  

            if(BOLNumber != '')
            {
                 Set<ID> idSet= new Set<ID>();
                 List<USCP_BOL_AccTransaction__c > resultList =  Database.query('select invoice__r.eft__c from USCP_BOL_AccTransaction__c where invoice__c != null and invoice__r.eft__c != null and BOL__c like \'%' +  String.escapeSingleQuotes(BOLNumber) + '%\'');
                 for(USCP_BOL_AccTransaction__c  rec:resultList){
                    idSet.add(rec.invoice__r.eft__c);
                 }
                if(idSet.size()!=0)
                {
                    string idlist = inClausify(idSet);
                    filteredSOQL  = filteredSOQL + ' and id in ' + idlist ;
                    //filteredSOQL  = filteredSOQL + ' and id in :idSet' ;
                    System.debug(filteredSOQL);
                    filtersON = true;                
                }
                else //didn't find anything, there is no ppoint to inspect any other filters
                {
                    this.WhereClause  = ' Name=null ';
                    query();                   
                    return null;
                }
            }
                
            if(InvoiceNumber  != '')
            {
                filteredSOQL  = filteredSOQL + ' and id in (select EFT__c from USCP_Invoice__c where name like \'%' +  String.escapeSingleQuotes(InvoiceNumber) + '%\')';
                filtersON = true;                
            }
                        
            if(dateFrom != null)
                {
                filteredSOQL  = filteredSOQL + ' and Draft_Date__c >= ' +  String.valueof(dateFrom);
                filtersON = true; 
                }
            if(dateTo != null)
                {
                filteredSOQL  = filteredSOQL + ' and Draft_Date__c <= ' +  String.valueof(dateTo);
                filtersON = true; 
                }                            
                        
            
            
            
                      
         System.debug(FilteredSOQL);         
         if(filtersON )
         {         
             this.WhereClause  = FilteredSOQL;
         }  
         else
         {
            this.WhereClause  = ' Name=null '; //always false, no data load
         }                   
         query();                   
         return null;
    }
     
     @RemoteAction
    public static List<Account > searchAccount(String searchAccount) {
        System.debug('Account Name is: '+searchAccount);
        List<Account> result = Database.query('Select id, Name, SMS_Number__c from Account where id in (select Account__c from USCP_EFT__c) and (name like \'%' + String.escapeSingleQuotes(searchAccount) + '%\' or Customer_Number__c  like \'%'  + String.escapeSingleQuotes(searchAccount) + '%\' )  order by Name ');
        return result ;
    }       
       
    public String inClausify(Set<Id> ids) {
        /*
        String inClause = String.format('(\'{0}\')', 
                             new List<String> { String.join( new List<Id>(ids) , '\',\'') });
        */
                             
        List<Id> idlist = new List<Id>(ids);
        System.debug(idlist);
        String idliststr = String.join( idlist , '\',\'');
        System.debug(idliststr);
        String inClause = '(\'' +  idliststr + '\')';
        System.debug(inClause);                             
                                     
        return inClause;
    }
            
  public PageReference SaveToExcel() 
   {

       if(this.getResultSize() == 0) return null;
       
       if(this.getResultSize() > 1000)
       {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'More than 1000 records selected. Please narrow your filter.'));
            return null;   
       }   
   
        return page.USCP_EFTListExcel;
   }    
   
    /*Wrapper class*/   
    public class EFTWrapper
    {
        public USCP_EFT__c EFTObject {get;set;}
        public integer InvoiceCount {get;set;}        
        EFTWrapper(USCP_EFT__c eftrec)
        {
            this.EFTObject = eftrec;
            
            InvoiceCount = 0;
            for(USCP_Invoice__c invoice: eftrec.Invoices__r)
            {
                InvoiceCount++; 
            }


        }
    }       
            
}