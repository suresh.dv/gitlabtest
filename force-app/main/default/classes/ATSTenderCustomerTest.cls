/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATSTenderCustomerTest {

    static testMethod void myUnitTest() {
        Account account = AccountTestData.createAccount('Test Account', Null);
        
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'Asphalt'];
        account.RecordTypeId = RecType.Id;
        
        insert account;
        
        Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
        insert cont;
        
        List<ATS_Parent_Opportunity__c> tenderList = ATSParentOpportunityTestData.createTender(5);
        
        List<ATS_Tender_Customers__c> tenderCustomerList = new List<ATS_Tender_Customers__c>();
        
        for(ATS_Parent_Opportunity__c tender : tenderList) {
        	ATS_Tender_Customers__c tenderCustomer = new ATS_Tender_Customers__c();
        	tenderCustomer.Account__c = account.Id;
        	tenderCustomer.ATS_Parent_Opportunity__c = tender.Id;
        	tenderCustomer.Contact__c = cont.Id;
        	tenderCustomerList.add(tenderCustomer);	
        }
        
        insert tenderCustomerList;
        
        tenderCustomerList = [SELECT Id, Name, Account__r.Name, ATS_Parent_Opportunity__r.Name FROM ATS_Tender_Customers__c where Id in : tenderCustomerList];
        
        for(ATS_Tender_Customers__c tenderCustomer : tenderCustomerList) {
        	System.assertEquals(tenderCustomer.Name, tenderCustomer.ATS_Parent_Opportunity__r.Name + ' - ' + tenderCustomer.Account__r.Name);
        }
    }
}