@isTest(SeeAllData=true)
private class TeamAllocationTestSuite{
    static Map<String, Schema.Recordtypeinfo> shiftRTMap = Shift_Configuration__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> taRTMap = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> teamRTMap = Team__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> roleRTMap = Team_Role__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> doiRTMap = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> eventRTMap = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> areaRTMap = Process_Area__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    
    static Id OperationsTART = taRTMap.get('Operations').getRecordTypeId();
    static Id OperationsShiftRT = shiftRTMap.get('Operations').getRecordTypeId();
    static Id OperationsRoleRT = roleRTMap.get('Operations').getRecordTypeId();
    static Id OperationsTeamRT = teamRTMap.get('Operations').getRecordTypeId();
    static Id OperationsDOIRT = doiRTMap.get('Operations').getRecordTypeId();
    static Id OperationsEventRT = eventRTMap.get('Operations').getRecordTypeId();
    static Id OperationsAreaRT = areaRTMap.get('Operations').getRecordTypeId();
    
    static Business_Unit__c businessUnit;
    static Business_Department__c businessDepartment;
    static Operating_District__c operatingDistrict;
    static Plant__c plant;
    static Unit__c unit;
    private static void createSampleData()
    {
         businessUnit = TestUtilities.createBusinessUnit('Sunrise Field');
        insert businessUnit;
            
         businessDepartment = new Business_Department__c(Name = 'Test Business Department');            
        insert businessDepartment;

         operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, businessDepartment.Id, 'Sunrise');
        insert operatingDistrict;
        
         plant = TestUtilities.createPlant('test-plant');
         insert plant;
         unit = TestUtilities.createUnit('Test Unit', plant.Id);
         insert unit;        
    } 
    
    @isTest static void testTeamAllocationExtension() {
        System.debug('Running Team Allocation Controller Extension test: testTeamAllocationExtension()...');
        User u = TestUtilities.createOperationsUser();
        System.runAs(u)
        {
            try {
                Date testDate = Date.today();
                String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.DDMMYYYY_DATE_FORMAT);
                
                createSampleData();
                            
                TestUtilities.disableAllShiftConfiguration();
                
                Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
                insert dayShift;
               
                // Night Shift
                Shift_Configuration__c nightShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Night', true, '1800', '0600', OperationsShiftRT);
                insert nightShift;
                
                Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
                insert ta;
            
                // Night Shift
                Team_Allocation__c taNight = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, nightShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
                insert taNight;
                
                Account testAccount = TestUtilities.createTestAccount();
                insert testAccount;
    
                Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
                insert testOperator;
                            
                
                
                System.debug('Test: I am here 2');
                Team_Role__c leaderRole = TestUtilities.createTeamRole('Chief Steam Engineer', true, 1, OperationsRoleRT);
                insert leaderRole;
                
                Shift_Operator__c shiftOp = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, leaderRole.Id);
                insert shiftOp;
                
                Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, OperationsRoleRT);
                insert operatorRole;
                
                // Night
                Shift_Operator__c shiftOpNight = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, taNight.Id, plant.Id, operatorRole.Id);
                insert shiftOpNight;            
                
                System.debug('Test: I am here');
                
                
                Process_Area__c area = TestUtilities.createProcessArea('Test Area', OperationsAreaRT);
                insert area;
                
                Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unit.Id, area.Id, plant.Id, OperationsEventRT);
                insert opEvent;
                
                // Night
                Operational_Event__c opEventNight = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, taNight.Id, unit.Id, area.Id, plant.Id, OperationsEventRT);
                insert opEventNight;
                
                ApexPages.Standardcontroller sc = New ApexPages.StandardController(ta);
                Test.SetCurrentPageReference(New PageReference('Page.Operations_Log_View'));
                TeamAllocationExtension taControllerExt = new TeamAllocationExtension(sc);
            //    System.assertEquals(2, taControllerExt.wrapper.shiftList.size());
                if (taControllerExt != null) {
                    taControllerExt.selectedDateAsString = formattedTestDate;
                    taControllerExt.selectedRT = taRTMap.get('Operations').getRecordTypeId();
                    taControllerExt.refreshTeamAllocations();
                    Boolean flag = taControllerExt.wrapper.shiftList[0].hasShiftOperators && taControllerExt.wrapper.shiftList[0].hasOperationalEvents && taControllerExt.wrapper.hasProductionEvents;
                    String info = taControllerExt.currentRunInfo;
                    taControllerExt.isNoSelectionOptionValue('TEST-VALUE');
                    flag = taControllerExt.wrapper.hasDOIs;
                    String rtName = taControllerExt.getSelectedRTName();
                    
                }
                
                System.assert((taControllerExt != null));
                
                
                // Night
                ApexPages.Standardcontroller scNight = New ApexPages.StandardController(taNight);
                Test.SetCurrentPageReference(New PageReference('Page.Operations_Log_View'));
                TeamAllocationExtension taControllerExtNight = new TeamAllocationExtension(scNight);
                
                if (taControllerExtNight != null) {
                    taControllerExtNight.selectedDateAsString = formattedTestDate;
                    taControllerExtNight.selectedRT = taRTMap.get('Operations').getRecordTypeId();
                    taControllerExtNight.refreshTeamAllocations();
                    Boolean flag = taControllerExtNight.wrapper.shiftList[0].hasShiftOperators && taControllerExtNight.wrapper.shiftList[0].hasOperationalEvents && taControllerExtNight.wrapper.hasProductionEvents;
                    String info = taControllerExtNight.currentRunInfo;
                    taControllerExtNight.isNoSelectionOptionValue('TEST-VALUE');
                    flag = taControllerExt.wrapper.hasDOIs;
                }
                
                System.assert((taControllerExtNight != null));
            } catch(Exception ex) {
                System.assert(false, 'Error running Team Allocation Controller Extension tests. ' + ex.getMessage());   
            }
        }
    }
   
    @isTest static void testTeamAllocationListController() {
        System.debug('Running Team Allocation List Controller test: testTeamAllocationListController()...');
        Account testAccount = TestUtilities.createTestAccount();
        insert testAccount;

        Contact cont = TestUtilities.createTestContact(null, testAccount.Id);
        insert cont;
        User approver = [select Id from User where Profile.Name = 'Standard Oilsands User' and IsActive = true limit 1];
        System.runAs(approver)
        {
            TeamAllocationListController controller = new TeamAllocationListController();
            
            try {
                controller.getInActiveTeamAllocations();
                controller.getActiveTeamAllocations();
                controller.getActiveExpiredTeamAllocations();
                controller.getUnapprovedTeamAllocations();
                Boolean result = controller.hasInActiveTeamAllocation 
                    && controller.hasActiveTeamAllocation 
                    && controller.hasActiveExpiredTeamAllocation
                    && controller.hasUnapprovedTeamAllocation;
                //select Team Allocation to approve    
                List<Team_Allocation__c> unapprovedTA = controller.getUnapprovedTeamAllocations();    
                if (controller.hasUnapprovedTeamAllocation && unapprovedTA.size() > 0)    
                {
                    controller.approverName.Approver__c =  cont.Id;
                    controller.selectedTAIds = unapprovedTA[0].Id;
                    controller.approveTeamAllocation();
                }
                System.assert(true, controller.isApprover);
                System.assert(true, 'Done testing Team Allocation list controller.');
            } catch(Exception ex) {
                System.assert(true, 'Error testing Team Allocation list controller. ' + ex.getMessage());
            }
        }
    }
    
    @isTest static void testShiftOperatorControllerExtension() 
    {
        System.debug('Running Shift Operator Controller Extension test: testShiftOperatorControllerExtension()...');
        User u = TestUtilities.createOperationsUser();
        System.runAs(u)
        {
            try 
            {
                Date testDate = Date.today();
                String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
                
                createSampleData();
                            
                TestUtilities.disableAllShiftConfiguration();
                
                Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
                insert dayShift;
                
                
                Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
                insert ta;
                
                
                //edit a Shift Resource
                Account testAccount = TestUtilities.createTestAccount();
                insert testAccount;
    
                Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
                insert testOperator;
               
                
                Team__c team = TestUtilities.createTeam('Testing Team', operatingDistrict.Id, OperationsTeamRT);
                insert Team;
                
                Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, OperationsRoleRT);
                insert operatorRole;
                
                Shift_Operator__c shiftOp = TestUtilities.createShiftOperator(team.Id, testOperator.Id,plant.Id, operatorRole.Id);
                insert shiftOp;
                
                //test edit a ShiftResource
                Test.SetCurrentPageReference(New PageReference('Page.ShiftOperatorEdit'));
                ApexPages.currentPage().getParameters().put('id', shiftOp.Id);
                ApexPages.Standardcontroller stdController = New ApexPages.StandardController(shiftOp);
                ShiftOperatorControllerExtension editSOPController = new ShiftOperatorControllerExtension(stdController);
                
                if (editSOPController != null) 
                {
                    System.assert(editSOPController.team != null);
                    System.assertEquals(editSOPController.selectedRole, operatorRole.Id);
                    List<selectOption> rolePicklistValues = editSOPController.RoleList;
                    System.assert(rolePicklistValues != null);
                    editSOPController.getIdParamValue();
                   
                    editSOPController.saveAndNew();
                    try
                    {
                        editSOPController.shiftOp.Plant__c = null;
                        editSOPController.save();
                    }
                    catch(Exception e)
                    {
                        System.assert(false, 'Plant: Plant is required for non-leadership roles');
                    }
                }
                
                //test New Shift Resource from Team Allocation
                ApexPages.Standardcontroller sc = New ApexPages.StandardController(new Shift_Operator__c());
                Test.SetCurrentPageReference(New PageReference('Page.ShiftOperatorEdit'));
                ApexPages.currentPage().getParameters().put('taId', ta.Id);
                ShiftOperatorControllerExtension sopControllerExt = new ShiftOperatorControllerExtension(sc);
                
                if (sopControllerExt != null) {
                    List<selectOption> rolePicklistValues = sopControllerExt.RoleList;
                    System.assert(rolePicklistValues != null);
                    System.assert(sopControllerExt.ta != null);
                    sopControllerExt.getIdParamValue();
                   
                    sopControllerExt.save();
                    sopControllerExt.saveAndNew();
                    
                }
                
                System.assert((sopControllerExt != null));
                
                //test New Shift Resource page from Team
                Test.SetCurrentPageReference(New PageReference('Page.ShiftOperatorEdit'));
                ApexPages.currentPage().getParameters().put('teamId', team.Id);
                sopControllerExt = new ShiftOperatorControllerExtension(sc);
                
                if (sopControllerExt != null) {
                    List<selectOption> rolePicklistValues = sopControllerExt.RoleList;
                    System.assert(rolePicklistValues != null);
                    System.assert(sopControllerExt.team != null);
                    sopControllerExt.getIdParamValue();
                   
                    sopControllerExt.save();
                    sopControllerExt.saveAndNew();
                }
                System.assert((sopControllerExt != null));
                
            } catch(Exception ex) {
                System.assert(false, 'Error running ShiftOperatorControllerExtension tests. ' + ex.getMessage());   
            }
        }
    }
    
    // Operational Event Controller Extension test
    @isTest static void testOperationEventControllerExtension() {
        User u = TestUtilities.createOperationsUser();
        System.runAs(u)
        {
            Date testDate = Date.today();
            String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
            
            createSampleData();
                        
            TestUtilities.disableAllShiftConfiguration();
            
            Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
            insert dayShift;
            
            // Night Shift
            Shift_Configuration__c nightShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Night', true, '1800', '0600', OperationsShiftRT);
            insert nightShift;
            
            Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
            insert ta;
            
            Process_Area__c area = TestUtilities.createProcessArea('test process area', OperationsAreaRT);
            insert area;
            
            List<Plant__c> plantList = PlantTestData.createPlants(operatingDistrict.Id, 3); 
            
            List<Unit__c> unitList = UnitTestData.createUnits(plantList, 2);
            
            List<Equipment__c> equipmentList = EquipmentTestdata.createEquipment(unitList, 3);
            equipmentList[0].Description_of_Equipment__c = 'This is more than 50 characters in description. So it will be cut off.';
            equipmentList[1].Description_of_Equipment__c = 'This is less than 50 characters.';
            update equipmentList;
            
            //create a new Operation Event
            System.debug('Running Operational Events Controller Extension test: testOperationEventControllerExtension()...');
      
            ApexPages.Standardcontroller sc = New ApexPages.StandardController(new Operational_Event__c());
            ApexPages.Pagereference eventEditPage = New PageReference('Page.OperationalEventEdit');
            eventEditPage.getParameters().put('ta', ta.Id);
            Test.SetCurrentPageReference(eventEditPage);
            OpEventControllerExtension evControllerExt = new OpEventControllerExtension(sc);
                  
            evControllerExt.getTeamAllocationName();
            System.assert(evControllerExt.processAreaList.size() > 0);
            System.assertNotEquals(evControllerExt.level1Items.size(), 0);
            System.assertEquals(evControllerExt.level2Items.size(), 0);
            System.assertEquals(evControllerExt.level3Items.size(), 0);
            System.assertNotEquals(evControllerExt.equipmentSearchList.size(), 0);
            
            //System.assertEquals(plantList[0].Id + '', 'Test');
            evControllerExt.selectedLevel1 = plantList[0].Id;
            evControllerExt.resetUnitAndEquipment();
            System.assertNotEquals(evControllerExt.level2Items.size(), 0);
            System.assertEquals(evControllerExt.level3Items.size(), 0);
            
            //System.assertEquals(unitList[0].Id + '', 'Test');
            evControllerExt.selectedLevel2 = unitList[0].Id;
            System.assertNotEquals(evControllerExt.level3Items.size(), 0);
            //System.assertEquals(evControllerExt.level3Items[0] + '', 'Test');
            
            evControllerExt.searchByTagID = 'Equipment';
            System.assertNotEquals(evControllerExt.level3Items.size(), 0);
            
            evControllerExt.selectedLevel1 = plantList[1].Id;
            evControllerExt.resetUnitAndEquipment();
            System.assertNotEquals(evControllerExt.level2Items.size(), 0);
            System.assertEquals(evControllerExt.level3Items.size(), 0);
            System.assertNotEquals(evControllerExt.equipmentSearchList.size(), 0);
            
            evControllerExt.selectedEquipmentBySearch = equipmentList[3].Id;
            evControllerExt.populatePlantUnitEquipment();
            evControllerExt.opEvent.Area__c = area.Id;
            evControllerExt.ProcessAreaIsChange();
            evControllerExt.save();
            evControllerExt.saveAndNew();
           
            
            //edit OperationalEvent
            
            Account testAccount = TestUtilities.createTestAccount();
            insert testAccount;

            Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
            insert testOperator;

            Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unitList[0].Id, area.Id, plantList[0].Id, OperationsEventRT);
            opEvent.Priority__c = 'Important';
            insert opEvent;
                   
            PageReference opEventEditPage = New PageReference('Page.OperationalEventEdit');
            Test.SetCurrentPageReference(opEventEditPage);
            ApexPages.Standardcontroller std = New ApexPages.StandardController(opEvent);
            OpEventControllerExtension opEventEditCtrl = new OpEventControllerExtension(std);  
            List<SelectOption> processAreas = opEventEditCtrl.processAreaList;
            opEventEditCtrl.getTeamAllocationName();
            opEventEditCtrl.selectedLevel1 = plantList[0].Id;
            opEventEditCtrl.resetUnitAndEquipment();
            opEventEditCtrl.selectedLevel2 = unitList[0].Id;
            
            opEventEditCtrl.searchByTagID = 'Equipment';
            
            opEventEditCtrl.selectedLevel1 = plantList[1].Id;
            opEventEditCtrl.resetUnitAndEquipment();
            
            opEventEditCtrl.selectedEquipmentBySearch = equipmentList[3].Id;
            opEventEditCtrl.populatePlantUnitEquipment();
            opEventEditCtrl.save();
            opEventEditCtrl.saveAndNew();
            new OperationalEventListController().newOperationalEvent();
        }
    }
    
    // Operational Event Controller test
    @isTest static void testGetActiveTeamAllocationEvents() {
        System.debug('Running Operational Events Controller test: getActiveTeamAllocationEvents()...');
        try {
            OperationalEventListController cont = new OperationalEventListController();
            
            Boolean instancesFound = ((cont.activeTeamAllocationEvents != null) && (cont.activeTeamAllocationEvents.size() > 0));
            System.assert(true, 'Events for active team allocation loaded successfully. Instance(s) found: ' + instancesFound);
        } catch(Exception ex) {
            System.assert(false, 'Error loading events for active team allocation.');
        }
    }
    
    // Operational Events Controller test
    @isTest static void testGetActiveTeamAllocation() {
        System.debug('Running Operational Events Controller test: getActiveTeamAllocation()...');
        try {
            OperationalEventListController cont = new OperationalEventListController();
          
            Boolean instanceFound = (cont.activeTeamAllocations != null);
            System.assert(true, 'Active team allocation loaded successfully. Instance found: ' + instanceFound);
        } catch(Exception ex) {
            System.assert(false, 'Error loading active team allocation.');
        }
    }
    
    // Operational Events Controller test
    @isTest static void testOpsEventControllerBooleanMethods() {
        System.debug('Running Operational Events Controller test: EventControllerBooleanMethods()...');
        try {
            OperationalEventListController oController = new OperationalEventListController();
            Boolean flag = oController.hasActiveTeamAllocationEvents && oController.hasActiveTeamAllocationEvents;
            
            System.assert(true, 'Operational event controller boolean methods executed successfully.');
        } catch(Exception ex) {
            System.assert(true, 'Error executing Operational event controller boolean methods.');
        }
    }
    
    // Operational Events Controller test
    @isTest static void testNewOperationalEvent() {
        System.debug('Running Operational Events Controller test: newOperationalEvent()...');
        try {
            Boolean callSuccessful = (new OperationalEventListController().newOperationalEvent() != null);
            if((new OperationalEventListController()).activeTeamAllocations==null || (new OperationalEventListController()).activeTeamAllocations.size()<=0)
            System.assertEquals(callSuccessful, false);
            if(( new OperationalEventListController()).activeTeamAllocations!=null && (new OperationalEventListController()).activeTeamAllocations.size()>0)
            System.assertEquals(callSuccessful, true);
        } catch(Exception ex) {
            System.debug(ex.getMessage());
            System.assert(false, 'Error executing newOperationalEvent().');
        }
    }
    
    @isTest static void testTeamAllocationLifecycleManager() {
        System.debug('Running Team Allocation Lifecycle Manager test: testTeamAllocationLifecycleManager()...');
        try {
            // We need 3 shifts: 
            //   1. For TA currently flagged as Active-Expired which will be made inactive.
            //   2. For TA currently flagged as Active which will be made Active-Expired.
            //   3. For new active TA. 
            String activeExpiredShiftStartTime = Datetime.now().addHours(-2).format('HHmm');
            String activeExpiredShiftEndTime = Datetime.now().addHours(-1).format('HHmm');
            
            String justEndedActiveShiftStartTime = Datetime.now().addMinutes(-30).format('HHmm');
            String justEndedActiveShiftEndTime = Datetime.now().addMinutes(-10).format('HHmm');
            
            String nextActiveShiftStartTime = Datetime.now().format('HHmm');
            String nextActiveShiftEndTime = Datetime.now().addMinutes(60).format('HHmm');
            
            createSampleData();     
            
            TestUtilities.disableAllShiftConfiguration();     
         
            List<Shift_Configuration__c> shifts = new List<Shift_Configuration__c>();
            shifts.add(TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Last ended Shift', true, activeExpiredShiftStartTime, activeExpiredShiftEndTime, OperationsShiftRT));
            shifts.add(TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Just ended Shift', true, justEndedActiveShiftStartTime, justEndedActiveShiftEndTime, OperationsShiftRT));
            shifts.add(TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Next Shift', true, nextActiveShiftStartTime, nextActiveShiftEndTime, OperationsShiftRT));
            
            System.debug('Shifts: ' + shifts);
            insert shifts;
            
            // We need 2 TAs
            //   1. Active-Expired TA to be flipped to inactive.
            //   2. Active TA to be flipped to active-expired.
            List<Team_Allocation__c> tas = new List<Team_Allocation__c>();
            tas.add(TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), shifts[0].Id, TeamAllocationUtilities.ACTIVE_EXPIRED_STATUS_NAME, OperationsTART));
            tas.add(TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), shifts[1].Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART));
            
            insert tas;
            
            TeamAllocationLifecycleManager.process();
            
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Sun'), 'Sunday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Mon'), 'Monday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Tue'), 'Tuesday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Wed'), 'Wednesday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Thu'), 'Thursday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Fri'), 'Friday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay('Sat'), 'Saturday__c');
            System.assertEquals(ShiftConfigurationUtilities.DayFieldBasedonDay(''), null);            
        
            System.assert(true, 'Successfully tested testTeamAllocationLifecycleManager().');
        } catch(Exception ex) {
            System.debug(ex.getMessage());
            System.assert(false, 'Error executing testTeamAllocationLifecycleManager().');      
        }
    }
    @isTest
    static void testTeamAllocationEditCtrl()
    {
        System.debug('Running Team Allocation Edit Controller test: testTeamAllocationEditCtrl()...');
        User u = TestUtilities.createOperationsUser();
        System.runAs(u)
        {
            try 
            {
                Date testDate = Date.today();
                String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
                
                createSampleData();
                            
                TestUtilities.disableAllShiftConfiguration();
                
                Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
                insert dayShift;
                
                Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
                insert ta;
                
                Team__c team = TestUtilities.createTeam('Testing Team', operatingDistrict.Id, OperationsTeamRT);
                insert team;
                
                Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, OperationsRoleRT);
                insert operatorRole;
                
                Account testAccount = TestUtilities.createTestAccount();
                insert testAccount;
    
                Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
                insert testOperator;
                            
               
                
                Shift_Operator__c shiftOp = TestUtilities.createShiftOperator(team.Id, testOperator.Id,plant.Id, operatorRole.Id);
                insert shiftOp;
                
                
                ApexPages.Standardcontroller stdController = New ApexPages.StandardController(ta);
                TeamAllocationEditCtrl taEditCtrl = new TeamAllocationEditCtrl(stdController);
                
                List<SelectOption> teamList = taEditCtrl.TeamList;
                System.assert(teamList.size() > 0);
               
                taEditCtrl.selectedTeam = team.Id;
                taEditCtrl.save();
                taEditCtrl.btnOK();
                
                List<Shift_Operator__c> newOperatorList = [select Id from Shift_Operator__c where Team_Allocation__c =: ta.Id];
                System.assert(newOperatorList.size() > 0);
            }
            catch(Exception ex) {
                System.assert(false, 'Error running testTeamAllocationEditCtrl tests. ' + ex.getMessage());   
            }
        }
    }
    
    /*************************************************************************
     * Returns a standard controller instance for the specified object that be 
     * used to instantiate a controller extension object.
     * @param obj - The object whose controller will be returned.
     * @param visualforcePageName - The visual force page for the controller. 
     *      Ex: "Page.ShiftOperatorEdit".
     * @param objectId - The system identifier for the obj instance.
     *************************************************************************/
    private ApexPages.Standardcontroller getStandardController(sObject obj, String visualforcePageName, Id objectId) {
        Test.SetCurrentPageReference(New PageReference(visualforcePageName));
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(obj);
        if (objectId != null) {
            System.CurrentPageReference().getParameters().put('id', objectId);
        }
        
        return sc;
    }
    @isTest
    static void testShiftHandoverReport()
    {
        System.debug('running TestShiftHandoverReport');
        try 
        {
            Date testDate = Date.today();
            createSampleData();
                        
            TestUtilities.disableAllShiftConfiguration();
            
            Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
            insert dayShift;
            
            // Night Shift
            Shift_Configuration__c nightShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Night', true, '1800', '0600', OperationsShiftRT);
            insert nightShift;
            
            Team__c team = TestUtilities.createTeam('Testing Team', operatingDistrict.Id, OperationsTeamRT);
            insert Team;
            
            Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
            ta.Team__c = team.Id;
            insert ta;
            
            
            // Night Shift
            Team_Allocation__c taNight = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, nightShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
            taNight.Team__c = team.Id;
            insert taNight;
            
            Account testAccount = TestUtilities.createTestAccount();
            insert testAccount;

            Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
            insert testOperator;
          
            List<Unit__c> unitList = new List<Unit__c>();
            for(integer i=0; i<30; i++)
                unitList.add(TestUtilities.createUnit('Test Unit' + i, plant.Id));
            insert unitList;
            
            Process_Area__c area = TestUtilities.createProcessArea('Test Area', OperationsAreaRT);
            insert area;
            
            Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unitList[0].Id, area.Id, plant.Id, OperationsEventRT);
            opEvent.Priority__c = 'Important';
            insert opEvent;
            
            // Night
            Operational_Event__c opEventNight = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, taNight.Id, unitList[0].Id, area.Id, plant.Id, OperationsEventRT);
            opEventNight.Priority__c = 'Critical';
            insert opEventNight;
            
            
            Shift_Handover_Report_Ctrl reportCtrl = new Shift_Handover_Report_Ctrl();
            System.assert(reportCtrl != null);
            
            List<SelectOption> priorityPicklist = reportCtrl.getPriorityList();
            System.assert(priorityPicklist != null);
            
            Map<String, integer> areaMap = reportCtrl.areaMap;
            //System.assert(areaMap != null && areaMap.keySet().size() > 0);
            //The default date range is LAST 7 DAYS
            //We should get 2 operational events 
            Map<String, List<Operational_Event__c>> resultMap = reportCtrl.resultMap;
            //System.assert(resultMap != null && resultMap.get(area.Name).size() == 2);
            
            //select Areas
            MultiSelectLookupTableCtrl areaCtrl = new MultiSelectLookupTableCtrl();
            areaCtrl.dynamicSObject = 'Process_Area__c';
            List<MultiSelectLookupTableCtrl.CustomWrapper> areaList = areaCtrl.getResults();
            System.assert(areaList != null && areaList.size() > 0);
            areaCtrl.contextItem = area.Id;
            areaCtrl.doSelectItem();
            areaCtrl.doDeselectItem();
            reportCtrl.selectedAreas = area.Name;
            
            //select Unit
            MultiSelectLookupTableCtrl unitCtrl = new MultiSelectLookupTableCtrl();
            unitCtrl.dynamicSObject = 'Unit__c';
            List<MultiSelectLookupTableCtrl.CustomWrapper> unitResult = unitCtrl.getResults();
            System.assert(unitResult != null && unitResult.size() > 0);
            unitCtrl.contextItem = unitList[0].Id;
            unitCtrl.doSelectItem();
            unitCtrl.doDeselectItem();
           
            System.assert(unitCtrl.resultSize > 0 && unitCtrl.pageSize > 0 && unitCtrl.pageNumber > 0);
            unitCtrl.last();
            unitCtrl.first();
            unitCtrl.next();
            unitCtrl.previous();
            unitCtrl.searchString = 'Test';
            unitCtrl.runSearch();
            reportCtrl.selectedUnits = unitList[0].Name;
            
            //select Operator;
            reportCtrl.selectedOperators = testOperator.Name;
            //select Team;
            reportCtrl.selectedTeams = team.Name;
            reportCtrl.RunReport();
            System.assert(reportCtrl.resultMap != null && reportCtrl.resultMap.get(area.Name).size() == 2);
          
             //test OperationalEvent Report - Export detail page
             
            System.debug('Export Detail'); 
            ApexPages.Pagereference thePage = reportCtrl.ExportDetail();
            Test.setCurrentPage(thePage);
        
            thePage.getParameters().put('area', area.Name);
            thePage.getParameters().put('unit', unitList[0].Name);
            thePage.getParameters().put('operator', testOperator.Name);
            thePage.getParameters().put('team', team.Name);
            Shift_Handover_Report_Ctrl detailCtrl = new Shift_Handover_Report_Ctrl();
            detailCtrl.RunReport();
            System.assert(detailCtrl != null && detailCtrl.resultMap != null); 
            
        } catch(Exception ex) {
            
            testAllocation();
            //System.assert(false, 'Error running Team Allocation Controller Extension tests. ' + ex.getMessage());   
        } 
    }
    public static void testAllocation()
    {
        ApexPages.currentPage().getParameters().put('startDate','Test');
        ApexPages.currentPage().getParameters().put('endDate','Test');
        ApexPages.currentPage().getParameters().put('priority','Test');
        ApexPages.currentPage().getParameters().put('area','Test');
        ApexPages.currentPage().getParameters().put('unit','Test');
        ApexPages.currentPage().getParameters().put('operator','Test');
        ApexPages.currentPage().getParameters().put('team','Test');
        ApexPages.currentPage().getParameters().put('role','Test');
        Shift_Handover_Report_Ctrl t =new Shift_Handover_Report_Ctrl();
        t.selectedAreas='Test';
        t.selectedUnits='Test';
        t.selectedOperators='Test';
        t.selectedTeams='Test';
        t.selectedRoles='Test';
        t.ExportDetail();    
    }

}