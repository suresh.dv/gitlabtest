@isTest
public class HOG_SAPUpdateNotificationMockImpl implements WebServiceMock {
  public static final String ITEM_KEY = 'Item';

  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        SAPHOGNotificationServices.UpdateNotificationRequest req = 
          (SAPHOGNotificationServices.UpdateNotificationRequest) request;

        SAPHOGNotificationServices.UpdateNotificationResponse resp = 
          new SAPHOGNotificationServices.UpdateNotificationResponse();
        resp.type_x = true;
        resp.Message = req.NotificationNumber + ' updated successfully.';
        resp.Notification = new SAPHOGNotificationServices.Notification();
        resp.Notification.Items = new SAPHOGNotificationServices.NotificationItems();
        resp.Notification.Items.NotificationItem = new List<SAPHOGNotificationServices.NotificationItem>();
        resp.Notification.Items.NotificationItem.add(new SAPHOGNotificationServices.NotificationItem());
        resp.Notification.Items.NotificationItem[0].ItemKey = ITEM_KEY;

        response.put('response_x', resp);
  }
}