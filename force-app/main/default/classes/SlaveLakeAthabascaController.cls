public class SlaveLakeAthabascaController
{
    public Boolean hasChatterAccess {get;set;}
    
    public String spotfireURL {get;set;}
    
    public String SlaveLakeAthabascaTeamChatterGroup {get;set;}
    
    public List<AssetWrapper> assets {get;set;}
    
    public class AssetWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String icon {get;set;}
        public Boolean isActive {get;set;}
        
        public AssetWrapper(String URL, String name, String icon, Boolean active)
        {
            this.name = name;
            this.URL = URL;
            this.icon = icon;
            isActive = active;
        }
    }
    
    public List<TeamWrapper> teams {get;set;}
    
    public class TeamWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String pic  {get;set;}
        
        public TeamWrapper(String URL, String name, String pic)
        {
            this.name = name;
            this.URL = URL;
            this.pic = pic;
        }
    }
    
    public SlaveLakeAthabascaController()
    {
        AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
        
        spotfireURL = spotfireURLs.SlaveLakeAthSpotfireSession__c;
        
        List<String> chatterNames = new List<String>{'Athabasca/Slave Lake Team','Slave Lake/Athabasca Reservoir','Slave Lake/Athabasca Development','Slave Lake/Athabasca D&C','Slave Lake/Athabasca Production Ops','Slave Lake/Athabasca Finance','Slave Lake/Athabasca G&G','Slave Lake/Athabasca BPI'};
        
        Map<String,Id> chatterName2Id = new Map<String,Id>();
        
        for(CollaborationGroup cg : [SELECT Id,Name FROM CollaborationGroup WHERE Name IN: chatterNames])
        {
            chatterName2Id.put(cg.Name,cg.Id);
        }
        
        SlaveLakeAthabascaTeamChatterGroup = '';
        if(chatterName2Id.containsKey('Athabasca/Slave Lake Team'))
            SlaveLakeAthabascaTeamChatterGroup = chatterName2Id.get('Athabasca/Slave Lake Team');
        
        // Does the current user have access to the Slave Lake/Athabasca Chatter Group?
        hasChatterAccess = false;
        List<CollaborationGroupMember> SlaveLakeAthabascaGroupMembers = [SELECT Id FROM CollaborationGroupMember WHERE CollaborationGroupId =: SlaveLakeAthabascaTeamChatterGroup AND MemberId =: UserInfo.getUserId()];
        if(SlaveLakeAthabascaGroupMembers.size() > 0)
            hasChatterAccess = true;
        
        assets = new List<AssetWrapper>();
        assets.add(new AssetWrapper('/apex/WCPWells?show=Western Canada Production', 'Wells',  'wells.png', true));
        assets.add(new AssetWrapper('#','Routes / Pipelines', 'pipelines.png', false));
        assets.add(new AssetWrapper('#','Facilities', 'facilities.png', false));
        
        teams = new List<TeamWrapper>();
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca Reservoir'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca Reservoir'),'Reservoir','Reservoir.png'));
        else
            teams.add(new TeamWrapper('#','Reservoir','Reservoir.png'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca G&G'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca G&G'),'G & G','GandG.jpg'));
        else
            teams.add(new TeamWrapper('#','G & G','GandG.jpg'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca Development'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca Development'),'Development','Development.jpg'));
        else
            teams.add(new TeamWrapper('#','Development','Development.jpg'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca Production Ops'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca Production Ops'),'Production Ops','Production.jpg'));
        else
            teams.add(new TeamWrapper('#','Production Ops','Production.jpg'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca BPI'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca BPI'),'BPI','BPI.jpg'));
        else
            teams.add(new TeamWrapper('#','BPI','BPI.jpg'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca Finance'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca Finance'),'Finance','Finance.jpg'));
        else
            teams.add(new TeamWrapper('#','Finance','Finance.jpg'));
        
        if(chatterName2Id.containsKey('Slave Lake/Athabasca D&C'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Slave Lake/Athabasca D&C'),'D & C','DandC.png'));
        else
            teams.add(new TeamWrapper('#','D & C','DandC.png'));
    }
}