global virtual with sharing class DSP_CMS_ArticleTitle extends DSP_CMS_ArticleController
{
    global DSP_CMS_ArticleTitle(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global DSP_CMS_ArticleTitle()
    {
        super();
    }
    
    global override String getHTML()
    {
        return articleTitleHTML();
    }
}