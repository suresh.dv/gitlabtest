global with sharing class WeldingTrackerLoginController {
    global String username {get; set;} 
    global String password {get; set;}
    
    global WeldingTrackerLoginController () {}
    
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/WeldingTrackerLoginPage');
    }
    
    global PageReference login() {
        
        Boolean userActiveBeforeLogin = True;
        List<User> usrList = [SELECT Id, Username, Email, IsActive, UserType, IsPortalEnabled, ContactId, lastlogindate 
                    FROM User 
                    WHERE Username =: userName AND UserType = 'PowerPartner' AND IsPortalEnabled = True];
                    
        SelfRegistrationCodes__c sc = SelfRegistrationCodes__c.getInstance('Welding Tracker Community User');        

        if(usrList.size() > 0) {
            if(usrList[0].IsActive) {
                PageReference ref = Site.login(username, password, null);            
                return ref;            
            } else {  
                sendAlertEmail(sc, 'Deactivated user login attempt', 
                    'User ' + userName + ' attempted to login to Welding tracker system.');
                addError('This Username/Password account has been de-activated. A request has been sent to the Administrator to review and re-activate the account');
                return null;            
            }

        } else {
            Date lastDate = date.newinstance((usrList[0].lastlogindate).year(), (usrList[0].lastlogindate).month(), (usrList[0].lastlogindate).day());
            integer numDays =  lastDate.daysBetween(system.today());
            
            Welding_Tracker_Settings__c wc = Welding_Tracker_Settings__c.getInstance();                
            if(numDays >  (wc.MaximumUserInactiveDays__c + (wc.MaximumUserInactiveDays__c * 2))) {
                sendAlertEmail(sc, 'Deactivated user login attempt', 
                    'User ' + userName + ' attempted to login to Welding tracker system.');            
                addError('User account deactivated, no activity in the last 6 months. A request has been sent to the Administrator to review and re-activate the account');
                return null;                   
            }
            else {
                addError('The Username / password you entered is invalid!');
                return null;            
            }
        }
    }
    
    private void addError(String message){
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
        ApexPages.addMessage(msg);        
    }
    
    private static void sendAlertEmail(SelfRegistrationCodes__c sc, String subject, String body){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {sc.Alert_Email__c}; 
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setBccSender(false);
        mail.emailPriority = 'Highest';
        mail.setPlainTextBody(body);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }    
}