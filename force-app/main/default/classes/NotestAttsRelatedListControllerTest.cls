@isTest
private class NotestAttsRelatedListControllerTest {
    static testmethod void atsCreationTest() {
        ATS_Parent_Opportunity__c acc = new ATS_Parent_Opportunity__c( Sales_Type__c = 'Test', Sales_Status__c = 'Test',
                                            Trade_Class__c = 'test', Country__c = 'Canada', Stage__c = 'Bid Won - Full',
                                            Province_State__c = 'British Columbia', Destination_City_Province__c = 'test',
                                            Region__c = 'BC Kootenay', For_Season__c = '2014', Closest_City__c = 'Kamloops',
                                            Marketer__c = UserInfo.getUserId(), Bid_Due_Date_Time__c = Datetime.now(),
                                            Estimate_Start_Date__c = Date.today(), Product_Category__c = 'Asphalt',
                                            Bid_Description__c = 'test', Freight_Due_Date_Time__c = Datetime.now() );
        insert acc;
        
        Account a = new Account( Name = 'Test' );
        insert a;
        
        Contact con = new Contact( LastName = 'Test', AccountId = a.Id, Email = 'test@gmail.com' );
        insert con;
        
        Attachment attach = new Attachment( Name = 'Test', ParentId = acc.Id, Body = Blob.valueOf( 'test' ));
        insert attach;
        
        ATS_Customer_Attachment_Junction__c ats = new ATS_Customer_Attachment_Junction__c( ATS_Customer__c = con.Id, Attachment_Id__c = attach.Id );
        
        test.startTest();
            insert ats;
            
            ApexPages.currentPage().getParameters().put('id', acc.Id);
            NotestAttsRelatedListController controller = new NotestAttsRelatedListController(new ApexPages.StandardController(attach));
            controller.aid = attach.Id;
            controller.SendAttachment();
            
        test.stopTest();
    }
}