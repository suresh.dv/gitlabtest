public class WeldingProcedureReviewGenerator {
    private String[] weldingProcedures = new String[]{};
        
    private List<SelectOption> weldingProcedureOptions = new List<SelectOption>();

    private final Welding_Request__c request;
    
    private Map<Id, String> relatedWeldingMap = new Map<Id, String>();
    
    private String ownerEmailAddress {get; set;}
    
    private String ownerId {get; set;}
    
    private String ownerName {get; set;}
    
    public WeldingProcedureReviewGenerator(ApexPages.StandardController stdController) {
        
        this.request = [SELECT Id, OwnerId, Owner.Name, Owner.Email, CreatedDate FROM Welding_Request__c WHERE Id =:stdController.getRecord().Id];
        
        for(Related_Welding_Procedure__c relatedWeldingProcedure : [SELECT Id, Name, Welding_Request__c, Welding_Procedure__c, Welding_Procedure__r.Name 
                                                                    FROM Related_Welding_Procedure__c
                                                                    WHERE Welding_Request__c =: request.Id]) {
            
            weldingProcedureOptions.add(new SelectOption(relatedWeldingProcedure.Id, relatedWeldingProcedure.Welding_Procedure__r.Name));
            weldingProcedures.add(relatedWeldingProcedure.Id);
            relatedWeldingMap.put(relatedWeldingProcedure.Id, relatedWeldingProcedure.Welding_Procedure__r.Name);
                                                                        
        }
        
    }
    
    public PageReference GenerateAndSend() {
        
        if(weldingProcedures.size() > 0) {
            List<Attachment> attachments = new List<Attachment>();
            List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
            
            for(String relatedWeldingId : weldingProcedures) {
                PageReference weldingReview = new pageReference('/apex/WeldingProcedureReviewTemplate');
                weldingReview.getParameters().put('relatedWeldingProcedureId', relatedWeldingId);
                
                Blob weldingReviewBlob;
                if(Test.isRunningTest()) {
                    weldingReviewBlob = Blob.valueOf('UNIT.TEST');
                }
                else {
                    weldingReviewBlob = weldingReview.getContent();
                }
                
                Attachment attachment = new Attachment();
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = request.Id;
                attachment.Name = relatedWeldingMap.get(relatedWeldingId) + ' Review Sheet.pdf';
                attachment.IsPrivate = False;
                attachment.Body = weldingReviewBlob;
                
                attachments.add(attachment);
                
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setFileName(relatedWeldingMap.get(relatedWeldingId) + ' Review Sheet.pdf');
                emailAttachment.setBody(weldingReviewBlob);
                
                emailAttachments.add(emailAttachment);
            }
            
            try{
                insert attachments;
            }
            catch(DMLException e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error uploading attachment'));
                return null;
            }
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
            
            /*EmailTemplate emailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: 'Welding_Review_Sheet'];
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject('Welding Procedure Review Sheet');
            email.setToAddresses(new String[] {request.Owner.Email});
            email.setFileAttachments(emailAttachments);
            email.setPlainTextBody('Dear ' + request.Owner.Name + ', ' + '\n\nWe have attached the Welding Procedure Review Sheets for the welding request' + 
                                   ' that you have submitted on ' + request.CreatedDate + 
                                   '.\n\nThank you,\n\nSE&P Technical Services\nHusky Oil Operations Ltd.');
            
            email.setReplyTo('welding@huskyenergy.com');
            email.setSenderDisplayName('SE&P Technical Services - Husky Oil Operations Ltd.');
            
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});*/
            
            PageReference pageReference = new PageReference('/' + request.Id);
            pageReference.setRedirect(True);
            return pageReference;
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must have atleast one welding procedure selected!!!'));
            return null;
        }
        
    }
    
    public PageReference cancel() {
        PageReference pageReference = new PageReference('/' + request.Id);
        pageReference.setRedirect(True);
        return pageReference;
    }
    
    public List<SelectOption> getWeldingProcedureItems() {
        return weldingProcedureOptions;
    }
    
    public String[] getSelectedProcedures() {
        return weldingProcedures;
    }
    
    public void setSelectedProcedures(String[] items) {
        this.weldingProcedures = items;
    }
}