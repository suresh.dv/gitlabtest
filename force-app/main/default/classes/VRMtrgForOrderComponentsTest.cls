@istest
public class VRMtrgForOrderComponentsTest{


  public static testMethod void trgOnSubscriptionOrdertest() {

        jbrvrm__Currency__c cur = new jbrvrm__Currency__c(name = 'test');
        insert cur;
        System.assertnotEquals(cur.id, null);

        jbrvrm__Currency__c cur1 = new jbrvrm__Currency__c(name = 'test1');
        insert cur1;
        System.assertnotEquals(cur1.id, null);

        jbrvrm__Currency__c cur2 = new jbrvrm__Currency__c(name = 'test', jbrvrm__Is_Primary_Currency__c = true);
        insert cur2;
        System.assertnotEquals(cur2.id, null);

        jbrvrm__Currency_Exchange__c CurExch = new jbrvrm__Currency_Exchange__c();
        CurExch.jbrvrm__Currency__c = cur.id;
        CurExch.jbrvrm__Effective_Date__c = system.today().adddays(5);
        CurExch.jbrvrm__End_Date__c = system.today().adddays(10);
        CurExch.jbrvrm__Conversion_Rate_to_Primary_Currency_del__c = 1.00;
        insert CurExch;
        System.assertnotEquals(CurExch.id, null);

        jbrvrm__Currency_Exchange__c CurExch1 = new jbrvrm__Currency_Exchange__c();
        CurExch1.jbrvrm__Currency__c = cur1.id;
        CurExch1.jbrvrm__Effective_Date__c = system.today().adddays(10);
        CurExch1.jbrvrm__Conversion_Rate_to_Primary_Currency_del__c = 1.00;
        insert CurExch1;
        System.assertnotEquals(CurExch1.id, null);

        jbrvrm__Vendor_Subscription_Title__c VsubTitle = new jbrvrm__Vendor_Subscription_Title__c(name = 'test');
        insert VsubTitle;
        System.assertnotEquals(VsubTitle.id, null);
        List<jbrvrm__Subscription_Order__c> subOrderList = new List<jbrvrm__Subscription_Order__c>();
        
        jbrvrm__Subscription_Order__c SubOrder = new jbrvrm__Subscription_Order__c();
        SubOrder.jbrvrm__Currency__c = cur.id;
        //SubOrder.jbrvrm__Date_Fee_Due__c=system.today().adddays(8);
        SubOrder.jbrvrm__Annual_Amount__c = 1000;
        SubOrder.jbrvrm__Amount_Corporate_Currency__c = 1000;
        SubOrder.jbrvrm__Subscription_Title__c = VsubTitle.id;
        SubOrder.jbrvrm__Renewal_Status__c = 'Finance Approved';
        SubOrder.jbrvrm__Start_Date__c = system.today().adddays(8);
        subOrder.jbrvrm__end_date__c=system.today().adddays(20);
        insert SubOrder;
        
        System.assertnotEquals(SubOrder.id, null);

        jbrvrm__Subscription_Order__c SubOrder1 = new jbrvrm__Subscription_Order__c();
        SubOrder1.jbrvrm__Currency__c = cur1.id;
        SubOrder1.jbrvrm__Annual_Amount__c = 1000;
        SubOrder1.jbrvrm__Amount_Corporate_Currency__c = 1000;
        SubOrder1.jbrvrm__Subscription_Title__c = VsubTitle.id;
        SubOrder1.jbrvrm__Renewal_Status__c = 'Finance Approved';
        SubOrder1.jbrvrm__Start_Date__c = system.today().adddays(20);
        subOrder1.jbrvrm__end_date__c=system.today().adddays(20);
        insert SubOrder1;
        
        System.assertnotEquals(SubOrder1.id, null);

        jbrvrm__Subscription_Order__c SubOrder2 = new jbrvrm__Subscription_Order__c (jbrvrm__Subscription_Title__c = VsubTitle.id);
        SubOrder2.jbrvrm__Start_Date__c = system.today().adddays(20);
        subOrder2.jbrvrm__end_date__c=system.today().adddays(20);
        insert SubOrder2;
        
        System.assertnotEquals(SubOrder2.id, null);
        
        SubOrder.jbrvrm__Renewal_Status__c = 'Complete';
        subOrderList.add(SubOrder);
        SubOrder1.jbrvrm__Renewal_Status__c = 'Complete';
        subOrderList.add(SubOrder1);
        SubOrder.jbrvrm__Renewal_Status__c = 'Complete';
        subOrderList.add(SubOrder2);
        
        System.assertEquals(subOrderList.size(), 3);
        update subOrderList;

    }
}