/*
* Author: Kishore Chandolu
* Name: HCVC_AssetDetailControllerXTest
* Purpose: This is the test class for HCVC_AssetDetailControllerX.
* Created Date: 23rd September 2014
*/


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class HCVC_AssetDetailControllerXTest {

   public static Id crudeRecordTypeId {get {if(crudeRecordTypeId==null)crudeRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Crude Pipeline').getRecordTypeId(); return crudeRecordTypeId;}set;}
   public static Id overviewRecordTypeId {get {if(overviewRecordTypeId==null)overviewRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Overview').getRecordTypeId(); return overviewRecordTypeId;}set;}
   public static Id refineryTypeId {get {if(refineryTypeId==null)refineryTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery').getRecordTypeId(); return refineryTypeId;}set;}
   public static Id refineryUnitTypeId {get {if(refineryUnitTypeId==null)refineryUnitTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Unit').getRecordTypeId(); return refineryUnitTypeId;}set;}
   public static Id refineryRacksTypeId {get {if(refineryRacksTypeId==null)refineryRacksTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Racks').getRecordTypeId(); return refineryRacksTypeId;}set;}
   public static Id tankTypeId {get {if(tankTypeId==null)tankTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Tank').getRecordTypeId(); return tankTypeId;}set;}  
   public static Id geojsonTypeId {get {if(geojsonTypeId==null)geojsonTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Geojson').getRecordTypeId(); return geojsonTypeId;}set;}  


        static testMethod void constructorTest() {
        	
          System.assertNotEquals(HCVC_AssetDetailControllerX.crudeRecordTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.overviewRecordTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.refineryTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.refineryUnitTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.refineryRacksTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.tankTypeId, null);
		  System.assertNotEquals(HCVC_AssetDetailControllerX.geojsonTypeId, null);

			  
             Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,RecordTypeId=refineryTypeId,
                                                     Details__c='Prince George Refinery',Refinery_Overview__c='',Refinery_History__c='');
             insert parentrefinery;
             Id parentId = parentrefinery.Id;
		    
		    // Make sure it was inserted properly
		    parentrefinery = [SELECT Id, Name, Details__c FROM Asset__c WHERE Id=: parentId];
		    System.assertEquals(parentrefinery.Name, 'Prince George Refinery');


             Asset__c assetunit=new Asset__c(Name='Crude Unit (CRUD - UNIT #:10)',Parent_Refinery_Asset__c=parentrefinery.Id,
                                             Substance__c='',Tank_Number__c='1',order__c=1,Lat__c=53.927528,Long__c=-122.698284,
                                             AssetType__c=refineryUnitTypeId,Details__c='Crude Unit (CRUD – UNIT #: 10)',
                                             Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',
                                             Product_Details__c='',Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',
                                             Blend_indices__c='',Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',
                                             Rack_Layout_Operations__c='',Image__c='',Rail_car_loading_operations__c='',
                                             Splash_Inline_blending__c='',Start_up_Year__c='',Name_Plate_Capacity__c='',
                                             LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                             BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',
                                             Sales_Tanks__c='',Dimensions__c='',MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,
                                             MAX_VOLUME_M3__c=0.0,Notes__c='');

            insert assetunit;
            // Make sure it was inserted properly
                assetunit = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Crude Unit (CRUD – UNIT #: 10)'];
                System.assertEquals(assetunit.Name, 'Crude Unit (CRUD - UNIT #:10)');


            Asset__c assetunit1=new Asset__c(Name='Tank2',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',Tank_Number__c='2',
                                            order__c=2,Lat__c=53.927528,Long__c=-122.698284,AssetType__c=tankTypeId,Details__c='Tank2 Details',
                                            Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',Product_Details__c='',
                                            Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',Blend_indices__c='',
                                            Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',Rack_Layout_Operations__c='',
                                            Image__c='',Rail_car_loading_operations__c='',Splash_Inline_blending__c='',Start_up_Year__c='',
                                            Name_Plate_Capacity__c='',LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                            BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',Sales_Tanks__c='',
                                            Dimensions__c='',MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,MAX_VOLUME_M3__c=0.0,Notes__c='');

            insert assetunit1;
            // Make sure it was inserted properly
                assetunit1 = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Tank2 Details'];
                System.assertEquals(assetunit1.Name, 'Tank2');


            Asset__c assetunit2=new Asset__c(Name='Rack2',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',Tank_Number__c='3',
                                              order__c=3,Lat__c=53.927528,Long__c=-122.698284,AssetType__c=refineryRacksTypeId,Details__c='Rack Details',
                                              Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',Product_Details__c='',
                                              Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',
                                              Blend_indices__c='',Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',
                                              Rack_Layout_Operations__c='',Image__c='',Rail_car_loading_operations__c='',Splash_Inline_blending__c='',
                                              Start_up_Year__c='',Name_Plate_Capacity__c='',LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                              BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',Sales_Tanks__c='',Dimensions__c='',
                                              MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,MAX_VOLUME_M3__c=0.0,Notes__c='');

            insert assetunit2;
                // Make sure it was inserted properly
                assetunit1 = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Rack Details'];
                System.assertEquals(assetunit1.Name, 'Rack2');



             ApexPages.currentPage().getParameters().put('aflag','true');
             ApexPages.currentPage().getParameters().put('assettype',tankTypeId);
             ApexPages.currentPage().getParameters().put('id',assetunit.Id);
             ApexPages.currentPage().getParameters().put('assettypename',tankTypeId);
             ApexPages.currentPage().getParameters().put('assettype',refineryRacksTypeId);

            ApexPages.StandardController controller=null;
            HCVC_AssetDetailControllerX adc=new HCVC_AssetDetailControllerX(controller);
            adc.setEditFlag();
            adc.updateAsset();
            adc.setViewFlag();
            adc.goBackToMap();
            adc.redirectToAssetDetails();
            adc.redirectToDetails();

        }




         static testMethod void redirectToDetailsTest1() {


             Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,RecordTypeId=refineryTypeId,
                                                     Details__c='Prince George Refinery',Refinery_Overview__c='',Refinery_History__c='');
                 insert parentrefinery;


              Asset__c assetunit2=new Asset__c(Name='Rack2',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',Tank_Number__c='3',
                                               order__c=3,Lat__c=53.927528,Long__c=-122.698284,AssetType__c=refineryRacksTypeId,Details__c='Rack Details',
                                               Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',Product_Details__c='',
                                               Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',
                                               Blend_indices__c='',Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',
                                               Rack_Layout_Operations__c='',Image__c='',Rail_car_loading_operations__c='',Splash_Inline_blending__c='',
                                               Start_up_Year__c='',Name_Plate_Capacity__c='',LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                               BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',Sales_Tanks__c='',
                                               Dimensions__c='',MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,MAX_VOLUME_M3__c=0.0,Notes__c='');

                insert assetunit2;


             ApexPages.currentPage().getParameters().put('id',assetunit2.Id);
             ApexPages.StandardController controller=null;
             ApexPages.currentPage().getParameters().put('assettype',refineryRacksTypeId);
             HCVC_AssetDetailControllerX adc=new HCVC_AssetDetailControllerX(controller);
             adc.redirectToDetails();
             ApexPages.currentPage().getParameters().put('assettypename',refineryRacksTypeId);
             adc.redirectToAssetDetails();


        }

             static testMethod void redirectToDetailsTest2() {

                 Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,RecordTypeId=refineryTypeId,
                                                     Details__c='Prince George Refinery',Refinery_Overview__c='',Refinery_History__c='');
                     insert parentrefinery;
                     


                  Asset__c assetunit2=new Asset__c(Name='Rack2',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',Tank_Number__c='3',
                                                   order__c=3,Lat__c=53.927528,Long__c=-122.698284,AssetType__c=crudeRecordTypeId,Details__c='Rack Details',
                                                    Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',Product_Details__c='',
                                                    Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',
                                                    Blend_indices__c='',Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',
                                                    Rack_Layout_Operations__c='',Image__c='',Rail_car_loading_operations__c='',Splash_Inline_blending__c='',
                                                    Start_up_Year__c='',Name_Plate_Capacity__c='',LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                                    BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',Sales_Tanks__c='',
                                                    Dimensions__c='',MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,MAX_VOLUME_M3__c=0.0,Notes__c='');

                    insert assetunit2;


                 ApexPages.currentPage().getParameters().put('id',assetunit2.Id);
                 ApexPages.StandardController controller=null;
                 ApexPages.currentPage().getParameters().put('assettype',crudeRecordTypeId);
                 HCVC_AssetDetailControllerX adc=new HCVC_AssetDetailControllerX(controller);

                 adc.redirectToDetails();
                 
                 adc.loadDefaultMap();

         }
         
         static testMethod void redirectToDetailsTest3() {

                 Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,RecordTypeId=refineryTypeId,
                                                     Details__c='Prince George Refinery',Refinery_Overview__c='',Refinery_History__c='');
                     insert parentrefinery;


                  Asset__c assetunit2=new Asset__c(Name='Rack2',Substance__c='',Tank_Number__c='3',
                                                   order__c=3,Lat__c=53.927528,Long__c=-122.698284,AssetType__c=tankTypeId,Details__c='Rack Details',
                                                    Address__c='',Operating_Conditions_Process__c='',Process_Description__c='',Product_Details__c='',
                                                    Unit_FeedStocks__c='',Unit_History__c='',Catalyst_and_Chemical_Usage__c='',
                                                    Blend_indices__c='',Blending_Flow_Diagram__c='',Blend_specifications__c='',Future_outlook__c='',
                                                    Rack_Layout_Operations__c='',Image__c='',Rail_car_loading_operations__c='',Splash_Inline_blending__c='',
                                                    Start_up_Year__c='',Name_Plate_Capacity__c='',LTRS_INCH__c=0.0,PFD_small_image__c='',LTRS_FOOT__c=0.0,
                                                    BBLS_INCH__c=0.0,BBLS_FOOT__c=0.0,HEEL_BBLS__c=0.0,HEEL_LTRS__c=0.0,Product__c='',Sales_Tanks__c='',
                                                    Dimensions__c='',MAXIMUM_FILL_FEET__c=0.0,MAX_VOLUME_BBL__c=0.0,MAX_VOLUME_M3__c=0.0,Notes__c='');

                    insert assetunit2;


                 ApexPages.currentPage().getParameters().put('id',assetunit2.Id);
                 ApexPages.StandardController controller=null;
                 HCVC_AssetDetailControllerX adc=new HCVC_AssetDetailControllerX(controller);

                 adc.redirectToDetails();
                 
                 adc.loadDefaultMap();

         }

}