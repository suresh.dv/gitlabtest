global with sharing class NCRLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global NCRLoginController () {}
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/NCLoginPage');
    }
    global PageReference login() {
        return Site.login(username, password, null);
    }
}