@isTest
private class TicketController_Test
{
    static testMethod void testTicket()
    {
        Milestone1_Project__c project= new Milestone1_Project__c(Name='Test Project');
        insert project;
        Ticket__c ticketRecord = new Ticket__c(Project__c = project.Id);
        ticketRecord.Ticket_Stage__c = 'New';
        ticketRecord.Requester_Email__c='test@test.com';
        insert ticketRecord;
        Test.startTest();
        PageReference p = new PageReference('ResolveCloseTicket/action=resolve');
        Test.setCurrentPage(p);
        ApexPages.CurrentPage().getParameters().put('action','resolve');
        TicketController tcont = new TicketController(new ApexPages.StandardController(ticketRecord));
        ticketRecord.Reason__c='Cancelled'; 
        ticketRecord.Assigned_To__c = [Select Id from User limit 1][0].Id;       
        tcont.saveTicket();
        ApexPages.CurrentPage().getParameters().put('action','close');
        TicketController tcont2 = new TicketController(new ApexPages.StandardController(ticketRecord));
        ticketRecord.Reason__c='Cancelled'; 
        ticketRecord.Assigned_To__c = [Select Id from User limit 1][0].Id;       
        tcont2.saveTicket();
        EmailMessage[] newEmail = new EmailMessage[0];
 
        newEmail.add(new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        Subject = 'Test subject',
        TextBody = 'plainTextBody',
        HtmlBody = 'htmlBody'
        //ParentId = ticketRecord.Id
        
        //,ActivityId = newTask[0].Id
        ));   
        
         
        insert newEmail;
         
        Test.stopTest();
    }
}