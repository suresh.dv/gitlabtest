@isTest
private class TestOperatingFieldRouteListController 
{
   
    @isTest static void controller_Test()
    {
        /*
        Field__c newField = new Field__c(name='Test Field');
        insert newField;
        */
                                  
        Test.startTest();

        PageReference pageRef = Page.OperatingFieldRouteList;
        Test.setCurrentPage(pageRef);
 
        /*       
        ApexPages.StandardController controller = new ApexPages.StandardController(newField);
     
        OperatingFieldRouteListController operatingController = new OperatingFieldRouteListController(controller);
        
        operatingController.field = newField;
                          
        operatingController.getRoute();
        
        // Access private inner class
        OperatingFieldRouteListController.RouteList routeList = 
            new OperatingFieldRouteListController.RouteList
                (
                    'sRoute_Link',
                    'sRoute', 
                    'sOperator_1', 
                    'sOperator_1_MobilePhone',
                    'sOperator_1_Email',
                    'sOperator_2', 
                    'sOperator_2_MobilePhone',
                    'sOperator_2_Email',
                    'sOperator_3', 
                    'sOperator_3_MobilePhone',
                    'sOperator_3_Email',
                    'sOperator_4',
                    'sOperator_4_MobilePhone',
                    'sOperator_4_Email'                            
                );
        */
                    
        Test.stopTest();
            
    }
    
       
}