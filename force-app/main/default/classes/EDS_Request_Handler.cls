global class EDS_Request_Handler implements Messaging.InboundEmailHandler
{
    global Messaging.InboundEmailResult handleInboundEmail (Messaging.InboundEmail email, Messaging.InboundEnvelope env)
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();   
        System.debug('From Name and Address are '+email.FromName+' and '+email.fromAddress);     
        Contact contactRecord = new Contact(LastName = email.FromName, Email = email.FromAddress);
        Id projectRecordTypeId = SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Project - Ticketing App').getRecordTypeId();
        Ticket__c ticketRecord = new Ticket__c(Requester_Email__c = email.FromAddress);
        List<Milestone1_Project__c> projectRecord = [SELECT Name FROM Milestone1_Project__c WHERE Name LIKE '%Unassigned%'];
        Id contactId;
        if(projectRecord!=null && projectRecord.size()>0)
        {
            ticketRecord.Project__c = projectRecord[0].Id;
        }
        else
        {
            Milestone1_Project__c projRecord = new Milestone1_Project__c(Name='Unassigned Project', RecordTypeId = projectRecordTypeId);
            Database.SaveResult sr = Database.insert(projRecord);
            if(sr!=null && sr.isSuccess())
            {
                ticketRecord.Project__c = sr.getId();
            }            
        }
        try
        {
            List<Contact> isExistingContact = [SELECT Email FROM Contact WHERE Email=:email.FromAddress];
            if( !(isExistingContact!=null && isExistingContact.size()>0) )
            {
                System.debug('Not existing Contact');
                Database.SaveResult saveResult = Database.insert(contactRecord);
                if(saveResult!=null && saveResult.isSuccess())
                {
                    contactId = saveResult.getId();
                }    
            }
            else
            {
                System.debug('Existing Contact');
                contactId = isExistingContact[0].Id;
            }
            if( contactId != null)
            {
                ticketRecord.Request_Contact__c = contactId;
                ticketRecord.Ticket_Description__c = email.Subject;
                ticketRecord.Email_Body__c = email.plainTextBody;                
                Database.SaveResult srTicket = Database.insert(ticketRecord);
                if( srTicket!=null && srTicket.isSuccess() )
                {
                    if( email.textAttachments!=null && (email.textAttachments).size()>0 )
                    {
                        for(Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments)
                        {
                            Attachment attachment = new Attachment();
                            attachment.body = Blob.valueOf(textAttachment.body);
                            attachment.name = textAttachment.fileName;
                            attachment.parentId = ticketRecord.Id;
                            Database.insert(attachment);           
                        }
                    }
                    if( email.binaryAttachments!=null && (email.binaryAttachments).size()>0 )
                    {
                        for(Messaging.InboundEmail.BinaryAttachment binaryAttachment : email.binaryAttachments )
                        {
                            Attachment attachment = new Attachment();
                            attachment.body = (binaryAttachment.body);
                            attachment.name = binaryAttachment .fileName;
                            attachment.parentId = ticketRecord.Id;
                            Database.insert(attachment);           
                        }
                    }                
                }
            }
            
        }
        catch(DMLException dmlExcep)
        {
            result.success = false;
            System.debug('Insert failed due to '+dmlExcep.getMessage());
        }
        result.success = true;
        return result;
    }
}