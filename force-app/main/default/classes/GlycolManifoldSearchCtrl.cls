public with sharing class GlycolManifoldSearchCtrl {

    public String selectedPlant {get; set;}
    public String selectedUnit {get; set;}
    public String selectedMType {get;set;}
    
    public String searchString {get; set;}

    public String searchOption {get; set;}
    //this is to indicate if user search by Plant->Unit or by Keyword 
    public boolean searchByUnit {get;set;}
    
    // Flag to indicate whehter search has been requested either by plant unit hierarchy or through the search box.
    // When the screen loads the "Results" section should not show, not even the header.  But once a search has been
    // done via either method, the "Result" section will display.
    public Boolean searchRequested {get; set;}
    
    // get the number of records returned
    private Integer recordCount;
    /* map to store the search result which key = branch id 
    this is used to retrieve item quickly when user edit/save */
    private Map<Id, GlycolSearchResultWrapper> resultMap;
    /*store all Equipments from database. 
    It is used to reload Equipment's attributes when user change Reference Equipment */
    private Map<Id, Glycol_Piping_Line_Equipment__c> allEquipMap;
    
    public GlycolManifoldSearchCtrl()
    {
        System.debug('GlycolManifoldSearchCtrl');
        selectedMType = 'Supply';
        selectedPlant = '-1';
        searchRequested = false;
        if (ApexPages.currentPage().getParameters().get('searchOption') != null )
        {
            searchOption = ApexPages.currentPage().getParameters().get('searchOption');
            if (searchOption == 'Unit')
            {
                selectedPlant = ApexPages.currentPage().getParameters().get('plant');
                selectedUnit = ApexPages.currentPage().getParameters().get('unit');
                selectedMType = ApexPages.currentPage().getParameters().get('type');
                
            }
            else
                 searchString = ApexPages.currentPage().getParameters().get('searchString');
        }
        //load all equipments from database
        allEquipMap = new Map<Id, Glycol_Piping_Line_Equipment__c>([select Id, Name, Line_Size__c from Glycol_Piping_Line_Equipment__c]);
    }
    //check if the current user is on Glycol Tracer Admin permission set
    public Boolean isUserHasAdminPermission
    {
        get
        {
            Id currentUserId = UserInfo.getUserId();
            PermissionSetAssignment[] userAssigment = [SELECT Id
                                                        FROM PermissionSetAssignment
                                                        WHERE AssigneeId =: currentUserId and PermissionSetId
                                                        IN (SELECT ParentId
                                                            FROM ObjectPermissions
                                                            WHERE SObjectType = 'Glycol_Manifold_Branch__c'  AND
                                                            PermissionsCreate = true)];
            if (userAssigment.size() > 0)
                 return true;
                 
            return false;     
        }
        set;
    }
    // plant picklist
    public List<SelectOption> getPlantItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        // Add a "Choose a Plant" option
        options.add(new SelectOption('-1', '-- Choose A Plant --'));
        for (List<Plant__c> pList : [SELECT Name FROM Plant__c ORDER BY Name])
        {
            for (Plant__c p : pList)
            {
                options.add(new SelectOption(p.ID, p.Name));
            }
        }
        return options;             
    }
  
    // unit picklist
    public List<SelectOption> getUnitItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        System.debug('selected Plant=' + selectedPlant);
        // Add a "Choose a Unit" option
        options.add(new SelectOption('-1', '-- Choose A Unit --'));
        
        if (selectedPlant != null)
        {
            for (List<Unit__c> uList : [SELECT Name FROM Unit__c WHERE Plant__c = : selectedPlant ORDER BY Name])
            {
                for (Unit__c unit : uList) 
                {
                    options.add(new SelectOption(unit.Id, unit.Name));
                }
            }
        }
        return options;
    }
    //manifold type picklist
    public List<SelectOption> getManifoldTypes()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Glycol_Manifold__c.Manifold_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
           options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    public void  RefreshUnitList() 
    {
        selectedUnit = null;
        searchString = '';
        
    } 
    public void SearchByUnit()
    {
        searchString = '';
        searchByUnit = true;
        if (selectedMType == 'Return')
           sortField = 'Return_Manifold__r.Name';
        else
          sortField = 'Supply_Manifold__r.Name';   
        SearchManifold();
        
    } 
    public void SearchByText()
    {
    
        searchByUnit = false;
        if (searchOption == 'Return')
           sortField = 'Return_Manifold__r.Name';
        else
          sortField = 'Supply_Manifold__r.Name'; 
        
        SearchManifold();
    }
    //the list of search results
    public List<GlycolSearchResultWrapper> searchResults
    {
        get 
        {
            if (searchResults == null) 
            {
                searchResults = new List<GlycolSearchResultWrapper>();
            }
            
            return searchResults;
        }
        set;
    }
    public string sortField
    {
        get
        {
            if(sortField == null)
                sortField = 'Supply_Manifold__r.Name';
            
            return sortField; 
        }
        set;
    }
    public String sortDir
    {
        get
        {
            if (sortDir == null)
               sortDir = 'asc';
            return sortDir;   
        }
        set;
    }
    
    
    public void SearchManifold() 
    {
        System.debug('search Option' + searchOption + sortField + sortDir);
        // set the CarsealSearchRequested flag to true so the "Results" section will now display.
        searchRequested = true;
        
        List<Glycol_Manifold_Branch__c> GMBList = new List<Glycol_Manifold_Branch__c>();
        resultMap = new Map<Id, GlycolSearchResultWrapper>();
        searchResults = new List<GlycolSearchResultWrapper>();
        String soql = '';
        try
        {
            if (!searchByUnit)
            {
                String strSearch = '%' + searchString + '%';
                //search by Reference Equipment
                if (searchOption == 'Equip' && searchString != '') 
                {
                    soql = 'SELECT Return_Manifold__r.Name, Return_Branch_Num__c, Supply_Manifold__r.Name, ' +
                                    'Supply_Branch_Num__c, ' +
                                    '(SELECT Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, ' +
                                            'Piping_Line_Equipment__r.Line_Size__c ' +
                                    'FROM Glycol_Branch_Equipment_Association__r) ' +
                            'FROM Glycol_Manifold_Branch__c ' +
                            'WHERE Id in (SELECT Manifold_Branch__c ' + 
                                         'FROM Glycol_Branch_Equipment_Association__c ' +
                                         'WHERE Piping_Line_Equipment__r.Name like :strSearch) ' +
                            'ORDER BY ' + sortField + ' ' + sortDir ;
                }
                //search by Supply Manifold
                else if (searchOption == 'Supply' && searchString != '') 
                {
                    soql = 'SELECT Return_Manifold__r.Name, Return_Branch_Num__c, Supply_Manifold__r.Name, ' +
                                   'Supply_Branch_Num__c, ' +
                                    '(SELECT Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, ' +
                                            'Piping_Line_Equipment__r.Line_Size__c ' +
                                     'FROM Glycol_Branch_Equipment_Association__r) ' +
                            'FROM Glycol_Manifold_Branch__c ' +
                            'WHERE Supply_Manifold__r.Name like :strSearch ' +
                            'ORDER BY ' + sortField + ' ' + sortDir;
                }
                //search by Return Manifold
                else if (searchOption == 'Return' && searchString != '') 
                {
                    soql = 'SELECT Return_Manifold__r.Name, Return_Branch_Num__c, Supply_Manifold__r.Name, ' +
                                   'Supply_Branch_Num__c,  ' +
                                    '(SELECT Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, ' +
                                            'Piping_Line_Equipment__r.Line_Size__c ' +
                                     'FROM Glycol_Branch_Equipment_Association__r) ' +
                            'FROM Glycol_Manifold_Branch__c ' +
                            'WHERE Return_Manifold__r.Name like :strSearch ' +
                            'ORDER BY ' + sortField + ' ' + sortDir;
                } 
            }
            //Search by Plant -> Unit
            else 
            {
                searchOption = '';
                if (selectedMType == 'Supply')
                    soql = 'SELECT Return_Manifold__r.Name, Return_Branch_Num__c, Supply_Manifold__r.Name, ' +
                                  'Supply_Branch_Num__c, ' +
                                  '(SELECT Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, ' +
                                            'Piping_Line_Equipment__r.Line_Size__c ' +
                                    'FROM Glycol_Branch_Equipment_Association__r) ' +
                            'FROM Glycol_Manifold_Branch__c ' +
                            'WHERE Supply_Manifold__r.Unit__c =: selectedUnit ' +
                            'ORDER BY ' + sortField + ' ' + sortDir; 
                            
                else if (selectedMType == 'Return')
                    soql = 'SELECT Return_Manifold__r.Name, Return_Branch_Num__c, Supply_Manifold__r.Name, ' +
                                  'Supply_Branch_Num__c, ' +
                                  '(SELECT Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, ' +
                                            'Piping_Line_Equipment__r.Line_Size__c ' +
                                    'FROM Glycol_Branch_Equipment_Association__r) ' +
                            'FROM Glycol_Manifold_Branch__c ' +
                            'WHERE Return_Manifold__r.Unit__c =: selectedUnit ' +
                            'ORDER BY ' + sortField + ' ' + sortDir;                            
            } 
            if (soql != '')
              GMBList = Database.query(soql);
            for(Glycol_Manifold_Branch__c item : GMBList)
            {
                if (item.Glycol_Branch_Equipment_Association__r != null && item.Glycol_Branch_Equipment_Association__r.size() > 0)
                {
                    GlycolSearchResultWrapper obj = new GlycolSearchResultWrapper(item);
                    resultMap.put(item.Id, obj);
                    searchResults.add(obj);
                }
            }
        
            // Set the number of records returned
            recordCount = searchResults.size();
        }
        catch(DMLException e)
        {
            ApexPages.addMessages(e);
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
    }
    // provide the frontend the number of records returned
    public String displayRecordCountInfo
    {
        get
        {
            String returnString = recordCount + ' manifold' + (recordCount > 1 ? 's' : '') + ' returned.';
            
            return returnString;
        } 
        private set;
    }
    
    public PageReference ExportDetail()
    {
        string url = '/apex/Glycol_Manifold_Search_Export_Detail?';
        url += '&searchOption=' + searchOption;
        
        if (searchByUnit)
        {
            if (selectedPlant != '')
                url += '&plant=' + selectedPlant;
            if (selectedUnit != '')
                url += '&unit=' + selectedUnit;
            url += '&type=' + selectedMType;    
        }
        else
        {
            if (searchString != '')
               url += '&searchString=' + searchString;
        }
            
        PageReference detailPage = new PageReference(url);
        
        return detailPage;
    }
    public PageReference GoToNewManifold()
    {
        
        PageReference nextPage = new PageReference('/a2i/e');
        nextPage.setRedirect(true);
        return nextPage;
    }
    public PageReference GoToManifoldDetail()
    {
        String branchId = System.currentPageReference().getParameters().get('branchId');
        PageReference nextPage = new PageReference('/' + branchId);
        nextPage.setRedirect(true);
        return nextPage;
    }
    public class GlycolSearchResultWrapper 
    {
    
        public Id BranchId {get;set;}
        public String SupplyManifoldNum {get;set;}
        public integer SupplyBranch {get;set;}
        public String ReturnManifoldNum {get;set;}
        public integer ReturnBranch {get;set;}
     
        public List<Glycol_Branch_Equipment_Association__c> EquipList {get;set;}
      
       
        public GlycolSearchResultWrapper(Glycol_Manifold_Branch__c gmb)
        {
        
            BranchId = gmb.Id;
            SupplyManifoldNum = gmb.Supply_Manifold__r.Name;
            SupplyBranch = Integer.valueOf(gmb.Supply_Branch_Num__c);
            ReturnManifoldNum = gmb.Return_Manifold__r.Name;
            ReturnBranch = Integer.valueOf(gmb.Return_Branch_Num__c);
      
            EquipList = new List<Glycol_Branch_Equipment_Association__c>();
            
            if (gmb.Glycol_Branch_Equipment_Association__r != null && gmb.Glycol_Branch_Equipment_Association__r.size() > 0)
            {
                EquipList = gmb.Glycol_Branch_Equipment_Association__r;
            }
          
        }
    }
}