global virtual with sharing class DSP_CMS_Contact extends DSP_CMS_ContactController
{
    global override String getHTML()
    {
        return contactHTML();
    }
}