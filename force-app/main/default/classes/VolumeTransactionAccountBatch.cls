global class VolumeTransactionAccountBatch implements Database.Batchable<AggregateResult>
{ 
    global class VolumeTransactionAccountIterator implements Iterator<AggregateResult>
    {
        AggregateResult[] results {get; set;}
        Integer index {get; set;}
        
        global VolumeTransactionAccountIterator()
        {
            index = 0;
            results = Database.query('SELECT Account__c FROM Volume_Transaction_Summary__c GROUP BY Account__c');
        }
        
        global Boolean hasNext()
        {
            return results != null && !results.isEmpty() && index < results.size();
        }
        
        global AggregateResult next()
        {
            return results[index++];
        }
    }
    
    global class VolumeTransactionAccountIterable implements Iterable<AggregateResult>
    {
        global Iterator<AggregateResult> Iterator()
        {
            return new VolumeTransactionAccountIterator();
        }
    }

    global Iterable<AggregateResult> start(Database.BatchableContext BC)
    {
        return new VolumeTransactionAccountIterable();
    }
     
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        //Set<Id> accountIds = new Set<Id>();
        Decimal volumeSum; 
        List<Account> accounts = new List<Account>();
        Id acctId;
        
        for (SObject vts: scope)
        {
            acctId = (Id)vts.get('Account__c');
            Account acct = new Account(id = acctId);
            
            List<Volume_Transaction_Summary__c> accountVolList = new List<Volume_Transaction_Summary__c>();
            accountVolList = [SELECT Age__c, Volume__c FROM Volume_Transaction_Summary__c WHERE Account__c = :acctId ORDER BY Age__c];
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c >= 14 && accountVolRecord.Age__c < 28)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Prior_Rolling_2_Weeks__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c >= 28 && accountVolRecord.Age__c < 56)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Prior_Rolling_Month__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c >= 364 && accountVolRecord.Age__c < 728)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Prior_Rolling_Year__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c < 14)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Rolling_2_Weeks__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c < 182)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Rolling_6_Months__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c < 28)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Rolling_Month__c = volumeSum;
            
            volumeSum = 0;
            for (Volume_Transaction_Summary__c accountVolRecord : accountVolList)
            {
                if (accountVolRecord.Age__c < 364)
                    volumeSum += accountVolRecord.Volume__c;
            }
            acct.Volume_Rolling_Year__c = volumeSum;
                
            accounts.add(acct);
         }
         update accounts;
    }
    
    global void finish(Database.BatchableContext BC)
    {
    /*
        // need to abort the cron job otherwise it'll execute again.
        Cardlock_Settings__c cs = Cardlock_Settings__c.getOrgDefaults();
        System.abortJob(cs.Volume_Transaction_Scheduled_Job_Id__c);
     */
    }
}