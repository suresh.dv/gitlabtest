public class ATSWrapperUtilities 
{
    public class ATSQuoteWrapper 
    {
        public Id accountId {get; set;}
        public String accountName {get; set;}
        public String accountNumber {get; set;}    
        public String CustomerComments {get; set;}      
        public ATS_Parent_Opportunity__c tender {get;set;}
        
        
        public String opportunityType{get; set;}
        
        public Id contactId {get; set;}
        public String contactName {get; set;}
        public String contactFaxNumber {get; set;}
        public String contactComments {get; set;}
        public String contactEmailAddress {get; set;}

        
        public Id tenderId {get; set;}
        public String tenderNumber {get; set;}    
        public String tenderName {get; set;}
        
        
        public String project {get; set;}
        public String fob {get; set;}
        public String priceValid {get; set;}
        public DateTime acceptanceDeadline {get; set;}
        public DateTime offerEndDate {get; set;}
        public String offerEndDateString {get; set;}    
        
        public String additionalComments {get; set;}
        
        public String Broker {get; set;}
        public String ForSeason {get;set;}
        public String TradeClass{get;set;}    
        public String Destination{get;set;}        
    
        
        public String signatureName {get; set;}
        public String signatureImageURL {get; set;}
        public String marketerFax {get; set;}
        public String marketerEmail {get; set;}
        List<ATSOpportunityWrapper> opportunities;
        public List<ATSOpportunityWrapper> getOpportunities()
        {
            return opportunities;
        } 
        public void setOpportunities(List<Opportunity> oppList, String CustomerType)
        {
            opportunities = new List<ATSOpportunityWrapper>();
            for(Opportunity opp : oppList)
            {
                String opportynityType = opp.RecordType.Name;
                opportynityType = opportynityType.Replace('ATS:','');
                opportynityType = opportynityType.Replace('Product Category','').trim();   
                
                //use in generate SMS report
                if(CustomerType != null && CustomerType.containsIgnoreCase(opportynityType)) {
                    opportunities.add(new ATSOpportunityWrapper(opp));
                }
                else if (CustomerType == null) //use in genereate Quote
                   opportunities.add(new ATSOpportunityWrapper(opp));                
            }            
        }              
    }
    
    public class ATSOpportunityWrapper
    {
        public Opportunity obj {get;set;}
        public String RecordType {get;set;}
        public String ProductCategoryComments {get;set;}
        //public List<OpportunityLineItem> Products{get; set;}
        public List<ATSOpportunityProductWrapper> Products {get; set;}
        public ATS_Freight__c Freight {get;set;}
        public Boolean IncludeAxle5{get; set;}
        public Boolean IncludeAxle6{get; set;}
        public Boolean IncludeAxle7{get; set;}
        public Boolean IncludeAxle8{get; set;}
    
        public ATSOpportunityWrapper(Opportunity opp)
        {
            obj = opp;
            //Products = opp.OpportunityLineItems;

            IncludeAxle5 = False;
            IncludeAxle6 = False;
            IncludeAxle7 = False;
            IncludeAxle8 = False;

            for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                IncludeAxle5 = IncludeAxle5 || oli.Include_Axle_5__c;
                IncludeAxle6 = IncludeAxle6 || oli.Include_Axle_6__c;
                IncludeAxle7 = IncludeAxle7 || oli.Include_Axle_7__c;
                IncludeAxle8 = IncludeAxle8 || oli.Include_Axle_8__c;
            }

        
            if (opp.Freight_Ats__r != null && opp.Freight_Ats__r.size() > 0)
                Freight = opp.Freight_Ats__r[0];  

            Products = new List<ATSOpportunityProductWrapper>();
            for(OpportunityLineItem product : opp.OpportunityLineItems) {
                Products.add(new ATSOpportunityProductWrapper(product, Freight, opp));
            } 
        }
    }
    public class ATSOpportunityProductWrapper
    {
        public OpportunityLineItem obj {get; set;}
        public ATS_Freight__c Freight {get; set;}
        public Opportunity ParentOpportunity{get; set;}
        public Decimal SalesPrice {get; set;}
        public Decimal Axle5Price {get; set;}
        public Decimal Axle6Price {get; set;}
        public Decimal Axle7Price {get; set;}
        public Decimal Axle8Price {get; set;}

        private Decimal productDensity;

        public ATSOpportunityProductWrapper(OpportunityLineItem product, ATS_Freight__c fr, Opportunity parentOpp) {
            obj = product;
            Freight = fr;
            ParentOpportunity = parentOpp;
            productDensity = product.PricebookEntry.Product2.Density__c;

            System.debug('ATSOpportunityProductWrapper');
            if( (ParentOpportunity.RecordType.Name != 'ATS: Asphalt Product Category' && 
                ParentOpportunity.RecordType.Name != 'ATS: Emulsion Product Category') ||
                Freight == null ||
                (Freight.Prices_F_O_B__c == 'Destination' || 
                Freight.Prices_F_O_B__c == 'Origin')){
                System.debug('Not Asphalt and Not Emulsion Or Freight == null Or Destination or Origin');
                SalesPrice = obj.Sales_Price__c;
                Axle5Price = obj.Axle_5_Price__c;
                Axle6Price = obj.Axle_6_Price__c;
                Axle7Price = obj.Axle_7_Price__c;
                Axle8Price = obj.Axle_8_Price__c;
            } else { //if Freight.Prices_F_O_B__c == 'Origin + Freight'
                if(Freight.Husky_Supplier_1_Selected__c == true) {
                    Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
                    SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
                    Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
                    Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
                    Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
                    Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
                } else {
                    Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_2_Unit__c, productDensity);
                    SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_2_Rate__c, conversionRatio);
                    Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier2__c, conversionRatio);
                    Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier2__c, conversionRatio);
                    Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier2__c, conversionRatio);
                    Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier2__c, conversionRatio);
                }
            }
        }

        private Decimal getConvertedPrice(Decimal salePrice, Decimal freightPrice, Decimal convRatio) {
            return ((salePrice == null) ? 0 : salePrice) 
                + ((freightPrice == null) ? 0 : freightPrice) / convRatio;
        }
    }
}