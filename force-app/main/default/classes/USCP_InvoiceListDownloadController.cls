/*
I am going to read zip blob from husky web service as a byte array and save it in the personal document folder, then display it to the user
*/
public with sharing class USCP_InvoiceListDownloadController{

    private final String DOC_NAME = 'invoicelist.zip';
    public Boolean initialised{get; private set;} 
    public Boolean inprogress{get; private set;} 
    public String docUrl {get; private set;}
    
    public string DocumentID {get; private set;}
    public string InvoiceNumbers {get; private set;}
    public string CustomerNumber {get; private set;}    
    public List<USCP_Invoice__c> invoicelist {get;private set;}

    public USCP_InvoiceListDownloadController() {
        initialised = false;
        inprogress = true;
        System.debug('idlist');
        String idlist = ApexPages.currentPage().getParameters().get('idlist');
        System.debug(idlist);
        
        String[] invoiceIDs = idlist.split('-');
        invoicelist = [select id, name, Account__r.Customer_Number__c, Invoice_Date__c, Ship_Date__c, Due_Date__c, Amount__c from USCP_Invoice__c where id in :invoiceIDs];
        InvoiceNumbers = '';
        CustomerNumber = '';
        for(USCP_Invoice__c inv : invoicelist)
        {
            InvoiceNumbers += inv.name + '-';
            if(CustomerNumber =='')
            {
                CustomerNumber = inv.Account__r.Customer_Number__c;
            }
        }
         System.debug('InvoiceNumbers');
         System.debug(InvoiceNumbers);         
    }
    
    public PageReference RedirectToDocument()
    {
        if(initialised)
        {
            PageReference page = new PageReference(docUrl);
            //page.setRedirect(true);
            return page;
        }
        return null;  
    }
    
    public PageReference DownloadDocument()
    {
            try
            {
                USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
                String WS_Endpoint = mc.HEI_WS_Endpoint_MI__c; 
                if(WS_Endpoint==null)
                {
                     USCP_Utils.logError('Web service endpoint is not specified.');
                     return null;
                } 
                
                String WS_CertificateName = mc.HEI_WS_CertificateName__c;              
                String endpointUrl= WS_Endpoint.replace('{0}',CustomerNumber ).replace('{1}', InvoiceNumbers    ); 
                System.debug(endpointUrl);
                
                // Instantiate a new http object
                Http h = new Http();
                // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
                HttpRequest req = new HttpRequest();
                
                System.debug(WS_CertificateName);
                req.setEndpoint(endpointUrl);
                if(WS_CertificateName != null) req.setClientCertificateName(WS_CertificateName);
                req.setTimeout(60000);
                System.debug('req.getBody()>>>>>>'+req.getBody());
                req.setMethod('GET');
                // Send the request, and return a response
                HttpResponse res;
                if (Test.isRunningTest()) 
                { 
                    //return true ;
                    res = new HttpResponse();
                    res.SetBody('Great success!!!!');
                    res.SetStatus('Ok');
                    res.SetStatusCode(200);
                }
                else
                {
                    res = h.send(req);                
                }
                
                System.debug('res is>>>>>>'+res);
                System.debug('res.getBody()>>>>>>'+res.tostring());
                    
                
        if(res.getStatusCode()>299) {
          System.debug('ERROR: '+res.getStatusCode()+': '+res.getStatus());
          System.debug(res.getBody());

         
          String errorMessage = '';
          if(USCP_Utils.IsAdminUser())
          {
             errorMessage = 'Web service call to retrieve invoices zip file failed with status: ' + res.getStatusCode()+' '+res.getStatus() ;
          }
          else
          {
            errorMessage ='Invoice PDFs are currently unavailable.  Please try again shortly.';    
          }


          
        } else {                
                System.debug('res.getBody()>>>>>>'+res.getBody());
                //Blob pdfContent= Blob.valueOf(res.getBody());
                Blob fileContent= res.getBodyAsBlob();
                
                //let see if Invoice document with name 'invoicelist.zip' already there
                Document d;
                List<Document> doclist = [select id, name from Document where folderid = :UserInfo.getUserId() and name = :DOC_NAME ];
                if(doclist.size() == 0 )
                {
                    d = new Document();
                    d.name = DOC_NAME ;
                    d.body = fileContent;
                    d.folderId = UserInfo.getUserId();
                    insert d;
                }
                else
                {
                    d = doclist[0];
                    d.body = fileContent;
                    update d;
                } 
                system.debug(d.id); 
                DocumentID = d.id;
                inprogress = false;
                
                
                docUrl = baseURL  + '/servlet/servlet.FileDownload?file='+ d.Id + '&id=' + Math.random();
                initialised = true;                
                //PageReference page = new PageReference(docUrl);  
                //page.setRedirect(true);
                
                //return page;
                return null;
            }
        }
        catch (Exception e) 
            {  
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+ e));        
                USCP_Utils.logError(e);
            } 
              
        inprogress = false;        
        return null;
            
    }
    
    public string baseURL {
    get{
        return Site.getBaseUrl()  == null ? URL.getSalesforceBaseUrl().toExternalForm() +'/' : Site.getBaseUrl() ;
    }
}        
    
}