@isTest (seeAllData=true)
private class TestOpportunityProductCompetitor
{
    static testMethod void testAsphalt()
    {
        // Create Account
        Account acc = new Account(Name = 'Test Account');
        Database.saveResult accountResult = Database.insert(acc);
        
        
        // Get Product2 record type
        RecordType productRecordType = [SELECT Id
                                        FROM RecordType
                                        WHERE SobjectType   = 'Product2' AND
                                              DeveloperName = 'Asphalt_Product'];
        
        // Create Product
        Product2 prod = new Product2(Name         = 'Test Product',
                                     Density__c   = 2,
                                     RecordTypeId = productRecordType.Id,
                                     IsActive = true);
        Database.saveResult productResult = Database.insert(prod);
        
        // Make ATS Tender
        ATS_Parent_Opportunity__c atstender = new ATS_Parent_Opportunity__c(Sales_Type__c                = 'Tender',
                                                                            Sales_Status__c              = 'Active',
                                                                            Trade_Class__c               = 'Construction',
                                                                            Country__c                   = 'Canada',
                                                                            Stage__c                     = 'Bid Initiation - Marketer',
                                                                            Destination_City_Province__c = 'Destination',
                                                                            For_Season__c                = '2014',
                                                                            Marketer__c                  = UserInfo.getUserId(),
                                                                            Province_State__c            = 'British Columbia',
                                                                            Region__c                    = 'BC Kootenay',
                                                                            Closest_City__c              = 'Kamloops',
                                                                            Bid_Due_Date_Time__c         = Datetime.now(),
                                                                            Freight_Due_Date_Time__c     = Datetime.now(),
                                                                            Product_Category__c          = 'Asphalt',
                                                                            Bid_Description__c           = 'Description');
        Database.saveResult atstenderResult = Database.insert(atstender);
        
        // Get Opportunity record type
        RecordType rectype = [SELECT Id
                              FROM RecordType
                              WHERE SobjectType   = 'Opportunity' AND
                                    DeveloperName = 'ATS_Asphalt_Product_Category'];
        
        // Create Asphalt Opportunity
        Opportunity opp = new Opportunity(Name                                = 'Test Asphalt',
                                          StageName                           = 'Opportunity Initiation - Marketer',
                                          CloseDate                           = Date.newInstance(2014,1,1),
                                          RecordTypeId                        = rectype.Id,
                                          Opportunity_ATS_Product_Category__c = atstenderResult.getId(),
                                          AccountId                           = accountResult.getId(),
                                          Pay_out_Handling_Fees__c            = 'No');
        Database.saveResult opportunityResult = Database.insert(opp);
        
        // Fetch price book entry
        PriceBookEntry pricebook = [SELECT Id
                                    FROM PriceBookEntry
                                    WHERE Product2Id      =: productResult.getId() AND
                                          Pricebook2.Name =: 'Asphalt Price Book'
                                    LIMIT 1];
        
        // Start test
        test.startTest();
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opportunityResult.getId(),
                                                          PricebookEntryId = pricebook.Id,
                                                          Competitor_1__c  = 'Cenex-Grand Forks',
                                                          Competitor_2__c  = 'Cenex-Pablo',
                                                          Competitor_3__c  = 'Cenex-Mandan',
                                                          Competitor_4__c  = 'Cenex-Lorall',
                                                          Competitor_1_Bid_Price__c = 100,
                                                          Competitor_2_Bid_Price__c = 100,
                                                          Competitor_3_Bid_Price__c = 100,
                                                          Competitor_4_Bid_Price__c = 100,
                                                          Competitor_1_Terminal_Freight_CAD__c = 5,
                                                          Competitor_2_Terminal_Freight_CAD__c = 5,
                                                          Competitor_3_Terminal_Freight_CAD__c = 5,
                                                          Competitor_4_Terminal_Freight_CAD__c = 5);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
/*
        Competitor_1__c,
        Competitor_2__c,
        Competitor_3__c,
        Competitor_4__c,
        Competitor_1_Bid_Price__c,
        Competitor_2_Bid_Price__c,
        Competitor_3_Bid_Price__c,
        Competitor_4_Bid_Price__c,
        Competitor_1_Terminal_Freight__c,
        Competitor_2_Terminal_Freight__c,
        Competitor_3_Terminal_Freight__c,
        Competitor_4_Terminal_Freight__c,
        Competitor_1_Refinery_Netback_CAD__c,
        Competitor_2_Refinery_Netback_CAD__c,
        Competitor_3_Refinery_Netback_CAD__c,
        Competitor_4_Refinery_Netback_CAD__c,
        Competitor_1_Terminal_Netback_CAD__c,
        Competitor_2_Terminal_Netback_CAD__c,
        Competitor_3_Terminal_Netback_CAD__c,
        Competitor_4_Terminal_Netback_CAD__c
*/
        
        // Check competitor
        OpportunityLineItem newOli = [SELECT Id,
                                             Handling_Fees_CAD__c,
                                             AC_Premium_CAD__c,
                                             Additives_CAD__c,
                                             Competitor_1_Refinery_Netback_CAD__c,
                                             Competitor_1_Terminal_Netback_CAD__c,
                                             Competitor_2_Refinery_Netback_CAD__c,
                                             Competitor_2_Terminal_Netback_CAD__c,
                                             Competitor_3_Refinery_Netback_CAD__c,
                                             Competitor_3_Terminal_Netback_CAD__c,
                                             Competitor_4_Refinery_Netback_CAD__c,
                                             Competitor_4_Terminal_Netback_CAD__c
                                      FROM OpportunityLineItem
                                      WHERE Id =: opportunityLineItemResult.getId()];
        
        System.assertEquals(null, newOli.Competitor_1_Refinery_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_2_Refinery_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_3_Refinery_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_4_Refinery_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_1_Terminal_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_2_Terminal_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_3_Terminal_Netback_CAD__c);
        System.assertEquals(null, newOli.Competitor_4_Terminal_Netback_CAD__c);
        
        // Create Freight record
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opportunityResult.getId(),
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter',
                                                    Emulsion_Rate5_Competitor1__c = 10,
                                                    Emulsion_Rate5_Competitor2__c = 10,
                                                    Emulsion_Axle5_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle5_Rate_Competitor4__c = 10,
                                                    Emulsion_Rate6_Competitor1__c = 10,
                                                    Emulsion_Rate6_Competitor2__c = 10,
                                                    Emulsion_Axle6_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle6_Rate_Competitor4__c = 10,
                                                    Emulsion_Rate7_Competitor1__c = 10,
                                                    Emulsion_Rate7_Competitor2__c = 10,
                                                    Emulsion_Rate7_Competitor3__c = 10,
                                                    Emulsion_Rate7_Competitor4__c = 10,
                                                    Emulsion_Rate8_Competitor1__c = 10,
                                                    Emulsion_Rate8_Competitor2__c = 10,
                                                    Emulsion_Axle8_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle8_Rate_Competitor4__c = 10);
        Database.saveResult freightResult = Database.insert(freight);
        
        ATS_Freight__c freight2 = new ATS_Freight__c(Id = freightResult.getId(),
                                                     Emulsion_Rate5_Competitor1__c = 15,
                                                     Emulsion_Rate5_Competitor2__c = 15,
                                                     Emulsion_Axle5_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle5_Rate_Competitor4__c = 15,
                                                     Emulsion_Rate6_Competitor1__c = 15,
                                                     Emulsion_Rate6_Competitor2__c = 15,
                                                     Emulsion_Axle6_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle6_Rate_Competitor4__c = 15,
                                                     Emulsion_Rate7_Competitor1__c = 15,
                                                     Emulsion_Rate7_Competitor2__c = 15,
                                                     Emulsion_Rate7_Competitor3__c = 15,
                                                     Emulsion_Rate7_Competitor4__c = 15,
                                                     Emulsion_Rate8_Competitor1__c = 15,
                                                     Emulsion_Rate8_Competitor2__c = 15,
                                                     Emulsion_Axle8_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle8_Rate_Competitor4__c = 15);
        update freight2;
        
        delete freight2;
        
        // Finish test
        test.stopTest();
    }
    
    static testMethod void testEmulsion()
    {
        // Create Account
        Account acc = new Account(Name = 'Test Account');
        Database.saveResult accountResult = Database.insert(acc);
        
        
        // Get Product2 record type
        RecordType productRecordType = [SELECT Id
                                        FROM RecordType
                                        WHERE SobjectType   = 'Product2' AND
                                              DeveloperName = 'Emulsion'];
        
        // Create Product
        Product2 prod = new Product2(Name         = 'Test Product',
                                     Density__c   = 2,
                                     RecordTypeId = productRecordType.Id,
                                     IsActive = true);
        Database.saveResult productResult = Database.insert(prod);
        
        // Make ATS Tender
        ATS_Parent_Opportunity__c atstender = new ATS_Parent_Opportunity__c(Sales_Type__c                = 'Tender',
                                                                            Sales_Status__c              = 'Active',
                                                                            Trade_Class__c               = 'Construction',
                                                                            Country__c                   = 'Canada',
                                                                            Stage__c                     = 'Bid Initiation - Marketer',
                                                                            Destination_City_Province__c = 'Destination',
                                                                            For_Season__c                = '2014',
                                                                            Marketer__c                  = UserInfo.getUserId(),
                                                                            Province_State__c            = 'British Columbia',
                                                                            Region__c                    = 'BC Kootenay',
                                                                            Closest_City__c              = 'Kamloops',
                                                                            Bid_Due_Date_Time__c         = Datetime.now(),
                                                                            Freight_Due_Date_Time__c     = Datetime.now(),
                                                                            Product_Category__c          = 'Emulsion',
                                                                            Bid_Description__c           = 'Description');
        Database.saveResult atstenderResult = Database.insert(atstender);
        
        // Get Opportunity record type
        RecordType rectype = [SELECT Id
                              FROM RecordType
                              WHERE SobjectType   = 'Opportunity' AND
                                    DeveloperName = 'ATS_Emulsion_Product_Category'];
        
        // Create Asphalt Opportunity
        Opportunity opp = new Opportunity(Name                                = 'Test Emulsion',
                                          StageName                           = 'Opportunity Initiation - Marketer',
                                          CloseDate                           = Date.newInstance(2014,1,1),
                                          RecordTypeId                        = rectype.Id,
                                          Opportunity_ATS_Product_Category__c = atstenderResult.getId(),
                                          AccountId                           = accountResult.getId(),
                                          Pay_out_Handling_Fees__c            = 'No');
        Database.saveResult opportunityResult = Database.insert(opp);
        
        // Fetch price book entry
        PriceBookEntry pricebook = [SELECT Id
                                    FROM PriceBookEntry
                                    WHERE Product2Id      =: productResult.getId() AND
                                          Pricebook2.Name =: 'Emulsion Price Book'
                                    LIMIT 1];
        
        
        // Start test
        test.startTest();
        
         // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opportunityResult.getId(),
                                                          PricebookEntryId = pricebook.Id,
                                                          Competitor_1__c  = 'Cenex-Grand Forks',
                                                          Competitor_2__c  = 'Cenex-Pablo',
                                                          Competitor_3__c  = 'Cenex-Mandan',
                                                          Competitor_4__c  = 'Cenex-Lorall',
                                                          Competitor_1_Bid_Price__c = 100,
                                                          Competitor_2_Bid_Price__c = 100,
                                                          Competitor_3_Bid_Price__c = 100,
                                                          Competitor_4_Bid_Price__c = 100,
                                                          Competitor_1_Emulsion_Cost_CAD__c = 5,
                                                          Competitor_2_Emulsion_Cost_CAD__c = 5,
                                                          Competitor_3_Emulsion_Cost_CAD__c = 5,
                                                          Competitor_4_Emulsion_Cost_CAD__c = 5);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
/*
        Competitor_1__c,
        Competitor_2__c,
        Competitor_3__c,
        Competitor_4__c,
        Competitor_1_Bid_Price__c,
        Competitor_2_Bid_Price__c,
        Competitor_3_Bid_Price__c,
        Competitor_4_Bid_Price__c,
        Competitor_1_Terminal_Freight__c,
        Competitor_2_Terminal_Freight__c,
        Competitor_3_Terminal_Freight__c,
        Competitor_4_Terminal_Freight__c,
        Competitor_1_Refinery_Netback_CAD__c,
        Competitor_2_Refinery_Netback_CAD__c,
        Competitor_3_Refinery_Netback_CAD__c,
        Competitor_4_Refinery_Netback_CAD__c,
        Competitor_1_Terminal_Netback_CAD__c,
        Competitor_2_Terminal_Netback_CAD__c,
        Competitor_3_Terminal_Netback_CAD__c,
        Competitor_4_Terminal_Netback_CAD__c
*/
        
        // Create Freight record
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opportunityResult.getId(),
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter',
                                                    Emulsion_Rate5_Competitor1__c = 10,
                                                    Emulsion_Rate5_Competitor2__c = 10,
                                                    Emulsion_Axle5_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle5_Rate_Competitor4__c = 10,
                                                    Emulsion_Rate6_Competitor1__c = 10,
                                                    Emulsion_Rate6_Competitor2__c = 10,
                                                    Emulsion_Axle6_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle6_Rate_Competitor4__c = 10,
                                                    Emulsion_Rate7_Competitor1__c = 10,
                                                    Emulsion_Rate7_Competitor2__c = 10,
                                                    Emulsion_Rate7_Competitor3__c = 10,
                                                    Emulsion_Rate7_Competitor4__c = 10,
                                                    Emulsion_Rate8_Competitor1__c = 10,
                                                    Emulsion_Rate8_Competitor2__c = 10,
                                                    Emulsion_Axle8_Rate_Competitor3__c = 10,
                                                    Emulsion_Axle8_Rate_Competitor4__c = 10);
        Database.saveResult freightResult = Database.insert(freight);
        
        ATS_Freight__c freight2 = new ATS_Freight__c(Id = freightResult.getId(),
                                                     Emulsion_Rate5_Competitor1__c = 15,
                                                     Emulsion_Rate5_Competitor2__c = 15,
                                                     Emulsion_Axle5_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle5_Rate_Competitor4__c = 15,
                                                     Emulsion_Rate6_Competitor1__c = 15,
                                                     Emulsion_Rate6_Competitor2__c = 15,
                                                     Emulsion_Axle6_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle6_Rate_Competitor4__c = 15,
                                                     Emulsion_Rate7_Competitor1__c = 15,
                                                     Emulsion_Rate7_Competitor2__c = 15,
                                                     Emulsion_Rate7_Competitor3__c = 15,
                                                     Emulsion_Rate7_Competitor4__c = 15,
                                                     Emulsion_Rate8_Competitor1__c = 15,
                                                     Emulsion_Rate8_Competitor2__c = 15,
                                                     Emulsion_Axle8_Rate_Competitor3__c = 15,
                                                     Emulsion_Axle8_Rate_Competitor4__c = 15);
        update freight2;
        
        delete freight2;
        
        // Finish test
        test.stopTest();
    }
}