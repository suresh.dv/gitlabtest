global class BatchUpdateCRDelayedIndicator implements Database.Batchable<sObject>, Schedulable 
{
    global static Integer EMail1NumSent = 0;
    global static Integer EMail2NumSent = 0;
    global static Integer EMail3NumSent = 0;
    
/*******************************************************************************************************
This execute method is for Schedulable.
The following schedules the job at 6am every day.
BatchUpdateCRDelayedIndicator p = new BatchUpdateCRDelayedIndicator();
String sch = '0 0 6 * * ?';
system.schedule('Delayed Indicator ' + System.Now().format('yyyyMMdd'), sch, p);
*******************************************************************************************************/
    global void execute(SchedulableContext pSC)
    {
        //BatchUpdateCRDelayedIndicator b = new BatchUpdateCRDelayedIndicator();
        //Database.executeBatch(b);
    }
    
/*******************************************************************************************************
The following methods, start, execute and finish are for Database.Batchable.
*******************************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext pBC)
    {
        /*return Database.getQueryLocator([
            SELECT c.Id, c.Name, c.Change_Status2__c, c.CR_Deadline__c, c.Delayed_Indicator__c,
            c.Project_Area__r.Change_Coordinator__c, 
            c.Project_Area__r.Project_Manager__c,
            c.Project_Area__r.SPA__c,
            (SELECT r.Reviewed_Date__c, r.Recommendation__c, r.Program_Area__r.Name, r.Reviewer__c 
                FROM 
                    CR_Reviews__r r 
                WHERE
                    r.Reviewed_Date__c = NULL OR r.Recommendation__c = NULL),
            (SELECT apa.Required_By_Date__c, apa.Name 
                FROM 
                    CR_Affected_Program_Areas__r apa),
            (SELECT ia.Assessor__c, ia.Impacted_Function__r.Name, ia.Impact_Assessment_Complete__c
                FROM
                    Impact_Assessors__r ia
                WHERE
                    ia.Impact_Assessment_Complete__c = False),
            (SELECT aif.Required_By_Date__c, aif.Name
                FROM 
                    CR_Affected_Impacted_Funtions__r aif)
            FROM Change_Request__c c WHERE c.Delayed_Indicator__c = False 
        ]);*/
        
        return null;
    }

    global void execute(Database.BatchableContext pBC, List<Change_Request__c> pScope)
    {
        /*List<Messaging.SingleEmailMessage> CRMailList = new List<Messaging.SingleEmailMessage>();
        EmailTemplate CREmailTemplate = [SELECT Id, Subject, Body FROM EmailTemplate WHERE Name = 'Change Request Missed Deadline'];
        Boolean isChanged = false;
        
        // 1. if current date has passed the CR deadline, and CR is not approved or rejected
        for (Change_Request__c cr : pScope)
        {
            if ((System.today() > cr.CR_Deadline__c) && (cr.Change_Status2__c <> 'Approved') && (cr.Change_Status2__c <> 'Rejected'))
            {
                isChanged = true;
                cr.Delayed_Indicator__c = True;
                Set<Id> mailToIds = new Set<Id>();
                mailToIds.add(cr.Project_Area__r.Change_Coordinator__c);
                mailToIds.add(cr.Project_Area__r.Project_Manager__c);
                mailToIds.add(cr.Project_Area__r.SPA__c);
                for (Id mailToId : mailToIds)
                {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    mail.setBccSender(false);
                    mail.setSaveAsActivity(false);
                    mail.setUseSignature(false);
                    mail.setSenderDisplayName('Change Management Team');
                    mail.setTemplateId(CREmailTemplate.Id);
                    mail.setTargetObjectId(mailToId);
                    mail.setWhatId(cr.Id);
                    CRMailList.add(mail);
                }
            }
        }
        if (isChanged)
        {
            update pScope;      
        }
        // Send email
        if (CRMailList.size() > 0)
        {
            List<Messaging.sendEmailResult> email1results = Messaging.sendEmail(CRMailList);
            BatchUpdateCRDelayedIndicator.EMail1NumSent += email1results.size();
        }
        
        // 2. Some of the Delayed Indicator has already been set, so go through pScope again but this
        // time skip through those that have Delayed Indicator set, so we won't email twice.
        // Also skip this if the status is already approved or rejected.  This second condition is only
        // applicable for non-approved or non-rejected CRs.
        List<Messaging.SingleEmailMessage> CRReviewMailList = new List<Messaging.SingleEmailMessage>();
        EmailTemplate CRReviewEmailTemplate = [SELECT Id, Subject, Body FROM EmailTemplate WHERE Name = 'CR Review Missed Deadline'];
        
        isChanged = false;
        for (Change_Request__c cr : pScope)
        {
            if ((cr.Delayed_Indicator__c == False) && (cr.Change_Status2__c <> 'Approved') && (cr.Change_Status2__c <> 'Rejected'))
            {
                // First create a map from Affected Program Area name to its deadline
                Map<String, Datetime> ProgramAreaDeadline = new Map<String, Datetime>();
                for (CR_Affected_Program_Area__c apa : cr.CR_Affected_Program_Areas__r)
                {
                    ProgramAreaDeadline.put(apa.Name, apa.Required_By_Date__c); 
                }
                // Second create a map from CR Review's Program Area to the Program Area's associated deadline
                // as set by the previous step above.
                Map<CR_Review__c, Datetime> ReviewDeadline = new Map<CR_Review__c, Datetime>();
                for (CR_Review__c r : cr.CR_Reviews__r)
                {
                    ReviewDeadline.put(r, ProgramAreaDeadline.get(r.Program_Area__r.Name));
                }
                // Now everything that we need to check is already in ReviewDeadline.  CR Review already has
                // a review date field.  If it is not set and the Affected Program Area's deadline has already
                // passed, then it's missed the deadline.  Same goes for the recommendation.
                for (CR_Review__c r : cr.CR_Reviews__r)
                {
                    if ((ReviewDeadline.get(r) <= System.today()) && (r.Reviewed_Date__c == NULL || r.Recommendation__c == NULL))
                    {
                        isChanged = true;
                        cr.Delayed_Indicator__c = True;
                        Set<Id> mailToIds = new Set<Id>();
                        //KK 2013/Jul/18: Jamie indicated CC and Reviewer are sufficient.  So not sending to PM or SPA for now.
                        mailToIds.add(cr.Project_Area__r.Change_Coordinator__c);
                        //mailToIds.add(cr.Project_Area__r.Project_Manager__c);
                        mailToIds.add(cr.Project_Area__r.SPA__c);
                        mailToIds.add(r.Reviewer__c);
                        for (Id mailToId : mailToIds)
                        {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                            mail.setBccSender(false);
                            mail.setSaveAsActivity(false);
                            mail.setUseSignature(false);
                            mail.setSenderDisplayName('Change Management Team');
                            mail.setTemplateId(CRReviewEmailTemplate.Id);
                            mail.setTargetObjectId(mailToId);
                            mail.setWhatId(r.Id);
                            CRReviewMailList.add(mail);
                        }
                    }
                }
            }
        }
        if (isChanged)
        {
            update pScope;      
        }
        // Send email
        if (CRReviewMailList.size() > 0)
        {
            List<Messaging.sendEmailResult> email2results = Messaging.sendEmail(CRReviewMailList);
            BatchUpdateCRDelayedIndicator.EMail2NumSent += email2results.size();
        }
        
        // 3. Some of the Delayed Indicator has already been set, so go through pScope again but this
        // time skip through those that have Delayed Indicator set, so we won't email twice.
        // Also skip this if the status is already approved or rejected.  This third condition is only
        // applicable for non-approved or non-rejected CRs.
        List<Messaging.SingleEmailMessage> CRImpactAssessorMailList = new List<Messaging.SingleEmailMessage>();
        EmailTemplate CRImpactAssessorEmailTemplate = [SELECT Id, Subject, Body FROM EmailTemplate WHERE Name = 'CR Impact Assessor Missed Deadline'];
        
        isChanged = false;
        for (Change_Request__c cr : pScope)
        {
            if ((cr.Delayed_Indicator__c == False) && (cr.Change_Status2__c <> 'Approved') && (cr.Change_Status2__c <> 'Rejected'))
            {
                // First create a map from Affected Impacted Funtion name to its deadline
                Map<String, Datetime> ImpactedFuntionDeadline = new Map<String, Datetime>();
                for (CR_Affected_Impacted_Function__c aif : cr.CR_Affected_Impacted_Funtions__r)
                {
                    ImpactedFuntionDeadline.put(aif.Name, aif.Required_By_Date__c); 
                }
                // Second create a map from Impact Assessor's to the Impacted Funtion's associated deadline
                // as set by the previous step above.
                Map<Impact_Assessor__c, Datetime> ReviewDeadline = new Map<Impact_Assessor__c, Datetime>();
                for (Impact_Assessor__c r : cr.Impact_Assessors__r)
                {
                    ReviewDeadline.put(r, ImpactedFuntionDeadline.get(r.Impacted_Function__r.Name));
                }
                // Now everything that we need to check is already in ReviewDeadline.  Impact Assessor already has
                // a Impact Assessment Complete field.  If it is not set and the Affected Impacted Funtion's deadline has already
                // passed, then it's missed the deadline.
                for (Impact_Assessor__c r : cr.Impact_Assessors__r)
                {
                    if ((ReviewDeadline.get(r) <= System.today()) && (!r.Impact_Assessment_Complete__c))
                    {
                        isChanged = true;
                        cr.Delayed_Indicator__c = True;
                        Set<Id> mailToIds = new Set<Id>();
                        mailToIds.add(cr.Project_Area__r.Change_Coordinator__c);
                        mailToIds.add(cr.Project_Area__r.SPA__c);
                        mailToIds.add(r.Assessor__c);
                        for (Id mailToId : mailToIds)
                        {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                            mail.setBccSender(false);
                            mail.setSaveAsActivity(false);
                            mail.setUseSignature(false);
                            mail.setSenderDisplayName('Change Management Team');
                            mail.setTemplateId(CRImpactAssessorEmailTemplate.Id);
                            mail.setTargetObjectId(mailToId);
                            mail.setWhatId(r.Id);
                            CRImpactAssessorMailList.add(mail);
                        }
                    }
                }
            }
        }
        if (isChanged)
        {
            update pScope;      
        }
        // Send email
        if (CRImpactAssessorMailList.size() > 0)
        {
            List<Messaging.sendEmailResult> email3results = Messaging.sendEmail(CRImpactAssessorMailList);
            BatchUpdateCRDelayedIndicator.EMail3NumSent += email3results.size();
        }*/
        
    }

    global void finish(Database.BatchableContext pBC)
    {
    }

}