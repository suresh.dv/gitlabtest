public with sharing class TeamAllocationEditCtrl 
{
    public Team_Allocation__c ta {get;set;}
    public String selectedTeam {get;set;}
    public string selectedNone = '';
    public TeamAllocationEditCtrl(ApexPages.StandardController stdController)
    {
    	ta = (Team_Allocation__c) stdController.getRecord();
    	
    	//load a existing Team Allocation
        if(ta.Id != null) 
            ta = [SELECT Id, Name, Team_Allocation_Name__c, Shift__c, Date__c, Status__c, RecordType.Name, RecordTypeId,
                    Team__c, Description__c, Operating_District__c, OwnerId
                     FROM Team_Allocation__c WHERE Id =: ta.Id]; 
       	selectedTeam = ta.Team__c;
    }
    
    public List<selectOption> TeamList 
    {
        get {
            System.debug('retrieve TeamLead =' + ta.Operating_District__c);
            List<selectOption> options = new List<selectOption>();
            
            options.add(new SelectOption(selectedNone,'-- Choose a Team --'));
            if (ta.Operating_District__c != null)
            {
	            for (Team__c team : [SELECT Id, Name FROM Team__c 
	                                   WHERE Operating_District__c =: ta.Operating_District__c 
	                                   and RecordType.Name =: ta.RecordType.Name
	                                   ORDER By Name limit 999])
	                options.add(new SelectOption(team.Id, team.Name));
            }
            return options;           
        }
        set;
    }
    public void ResetTeamSelection()
    {
    	System.debug('ta.Operating_District__c =' + ta.Operating_District__c);
    	
       selectedTeam = selectedNone;
    }
    public PageReference save() 
    {
    	Savepoint sp = Database.setSavepoint();
        try
        {
            System.debug('selectedTeam =' + selectedTeam);
            System.debug('ta.Team__c = ' + ta.Team__c);
            if (selectedTeam != ta.Team__c)
            {    
                ta.Team__c = selectedTeam;
                if (selectedTeam != null)
                {
                    //remove any existing Shift Resources for the Team Allocation
                    List<Shift_Operator__c> existingResources = [SELECT Id FROM Shift_Operator__c WHERE Team_Allocation__c =: ta.Id];
                    delete existingResources;
                    System.debug('delete existing resource');
                    
                    //add all Team Members from selected Team to the Team Allocation       
                    List<Shift_Operator__c> shiftMembers = new List<Shift_Operator__c>();
                    for(Shift_Operator__c teamM : [SELECT Id, Team__c, Role__c, Operator_Contact__c, Process_Area__c, Plant__c, Description__c 
                                                      FROM Shift_Operator__c WHERE Team__c =: selectedTeam 
                                                      order by Role__r.Leader_Ind__c desc, Name asc])
                    {
                        Shift_Operator__c shiftM = new Shift_Operator__c();
                        shiftM.Team_Allocation__c = ta.Id;
                        shiftM.Role__c = teamM.Role__c;
                        shiftM.Operating_District__c = ta.Operating_District__c;
                        shiftM.Operator_Contact__c = teamM.Operator_Contact__c;
                        shiftM.Process_Area__c = teamM.Process_Area__c;
                        shiftM.Plant__c = teamM.Plant__c;
                        shiftM.Description__c = teamM.Description__c;
                        
                        shiftMembers.add(shiftM);
                    }
                    if (shiftMembers.size() > 0)
                        insert shiftMembers;
                }               
            }   
               
            System.debug('Save action is invoked');
            return new ApexPages.StandardController(ta).save();
             
        }
        catch(Exception ex)
        {
            System.debug('TeamAllocationEditCtrl Exception =' + ex.getMessage());
            ApexPages.addMessages(ex); //show save error(s) on the visualforce page
            Database.rollback(sp); //rollback any completed saves if the transaction has errors
            return null;
        }
    }
    public PageReference btnOK()
    {
        closePopup();
        return null;
    }
    public boolean displayPopup {get; set;}     
    
    public void closePopup() 
    {        
        displayPopup = false;  
        selectedTeam = ta.Team__c;  
    }     
    public void showPopup() 
    {        
    	//only show confirmation box when user change Team picklist from something to something
    	//don't show when the previous value or new value of Team picklist is blank
        if (selectedTeam != null && ta.Team__c != null)
            displayPopup = true;    
    }
}