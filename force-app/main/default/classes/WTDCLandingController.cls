/**
  An apex page controller that takes the user to the right start page based on credentials or lack thereof
**/
public with sharing class WTDCLandingController {

    // Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/WTDCLoginPage');
        }
        else{
            return new PageReference('/_ui/core/chatter/ui/ChatterPage');
        }
    }

    public WTDCLandingController() {}
}