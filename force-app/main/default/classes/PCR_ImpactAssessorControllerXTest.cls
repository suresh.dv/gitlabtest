@isTest
private class PCR_ImpactAssessorControllerXTest{
    testmethod static void defaultMethods(){
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id, Contractor__c = uid,
                                                        Change_Status__c = 'Verification' );
        insert chq;
                
        Project__c projectVal = new Project__c( Name = 'Test', VP__c = uid, Project_Manager__c = uid );
        insert projectVal;
        
        ApexPages.StandardController sc = new ApexPages.StandardController( chq );
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.PCR_Impact_Assessment_View'));
            System.currentPageReference().getParameters().put( 'Change_Request', chq.Id );
            
            PCR_ImpactAssessorControllerX controller = new PCR_ImpactAssessorControllerX( sc );
            controller.save();
            controller.cancel();
        test.stopTest();
    }
    
    testmethod static void defaultMethods2(){
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id, Contractor__c = uid,
                                                        Change_Status__c = 'Impact Assessment and Review' );
        insert chq;
        
        Project__c projectVal = new Project__c( Name = 'Test', VP__c = uid, Project_Manager__c = uid );
        insert projectVal;
        
        ApexPages.StandardController sc = new ApexPages.StandardController( chq );
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.PCR_Impact_Assessment_View'));
            System.currentPageReference().getParameters().put( 'Change_Request', chq.Id );
            
            PCR_ImpactAssessorControllerX controller = new PCR_ImpactAssessorControllerX( sc );
            controller.save();
            controller.cancel();
        test.stopTest();
    }
}