/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentMissingFormEdit/EquipmentMissingFormView page

Test Class:    EquipmentMissingFormTest
History:       28-Feb-17 Miro Zelina - included Well Events into Equipment Missing Form Request
               03-July-17 Miro Zelina - included System & Sub-System into Equipment Missing Form Request
               04.09.17    Marcel Brimus - added FEL and Yard 
               25-Oct-2017 Miro Zelina - added check for user who can process/close equipment form
------------------------------------------------------------*/
public with sharing class EquipmentMissingFormControllerX {
    
    
    public Equipment_Missing_Form__c form {get;set;}
    private String locId;
    private String facilityId;
    private String eventId;
    private String sysId;
    private String subSysId;
    private String felId;
    private String yardId;
    private ApexPages.StandardController controller;
    public String formId {get;set;}
    public Attachment attachment {get;set;}
    public Attachment serialPlate {get;set;}
    public String attId {get;set;} 
    public EquipmentUtilities.UserPermissions userPermissions;
    
    public EquipmentMissingFormControllerX(ApexPages.StandardController std)
    {
        if (!Test.isRunningTest())
        {
            std.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Comments__c', 'P_ID__c', 'Part_of_Package__c',
                        'Manufacturer__c', 'Model_Number__c', 'Manufacturer_Serial_No__c', 'Tag_Number__c', 
                        'Location__c', 'Serial_plate_of_asset__c', 'Attachment__c',
                        'Location__r.Route__r.Route_Number__c', 'Location__r.Planner_Group__c',
                        'Well_Event__c', 'Well_Event__r.Route__r.Route_Number__c', 'Well_Event__r.Planner_Group__c',
                        'System__c', 'System__r.Route__r.Route_Number__c','System__r.Planner_Group__c',
                        'Sub_System__c', 'Sub_System__r.Route__r.Route_Number__c','Sub_System__r.Planner_Group__c',
                        'Functional_Equipment_Level__c', 'Functional_Equipment_Level__r.Route__r.Route_Number__c','Functional_Equipment_Level__r.Planner_Group__c',
                        'Yard__c', 'Yard__r.Route__r.Route_Number__c','Yard__r.Planner_Group__c',
                        'CreatedById', 'CreatedDate','LastModifiedDate', 'LastModifiedById', 'Description__c',
                        'Facility__c', 'Facility__r.Plant_Section__r.Route_Number__c', 'Facility__r.Planner_Group__c'});
        }
        controller = std;     
        form = (Equipment_Missing_Form__c)std.getRecord(); 
        system.debug('equipment missing form : '+form);  
        attachment = new Attachment();
        serialPlate = new Attachment();
        userPermissions = new EquipmentUtilities.UserPermissions();
        //get location or facility Id from Url parameters
        locId = ApexPages.currentPage().getParameters().get('locId');
        facilityId = ApexPages.currentPage().getParameters().get('facId');
        eventId = ApexPages.currentPage().getParameters().get('eventId');
        sysId = ApexPages.currentPage().getParameters().get('sysId');
        subSysId = ApexPages.currentPage().getParameters().get('subSysId');
        felId = ApexPages.currentPage().getParameters().get('felId');
        yardId = ApexPages.currentPage().getParameters().get('yardId');

        //User have to new a record from Equipment detail view
        if (form.Id == null && (locId != null || facilityId != null || eventId != null || sysId != null || subSysId != null || felId != null || yardId != null)) //creating a new form
            setDefault();
        else
        {
            getAttachmentPhoto();
        }
         
    } 
    private void getAttachmentPhoto()
    {
        List<Attachment> att = [select Id, Name, Body, ContentType from Attachment where ParentId =: form.Id];
        if (att.size() > 0)
        {
            for(Attachment a : att)
            {
                if (a.Id == form.Serial_plate_of_asset__c)
                   serialPlate = a;
                else if (a.Id == form.Attachment__c)
                   attachment = a;  
            }
        }
       
    }
    private void setDefault()
    {
    	//if location Id is in Url, then query location record
    	if(locId != null)
    	{
        	Location__c location = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
				                    from Location__c 
				                    where Id =: locId];
		           
        	form.Location__c = locId;
        	form.Location__r = location;
    	}
    	//if facility Id is in Url, then query facility record
    	else if(facilityId != null)
    	{
    		Facility__c facility = [Select Id, Name, Plant_Section__r.Route_Number__c, Planner_Group__c
    								From Facility__c 
    								Where Id =: facilityId];
    								
    		form.Facility__c = facilityId;
    		form.Facility__r = facility;
    	}
    	//if well event Id is in Url, then query well event record
    	else if(eventId != null)
    	{
        	Well_Event__c wellEvent = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
				                       from Well_Event__c 
				                       where Id =: eventId];
		           
        	form.Well_Event__c = eventId;
        	form.Well_Event__r = wellEvent;
    	}
    	//if System Id is in Url, then query System record
    	else if(sysId != null)
    	{
        	System__c syst = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
				              from System__c 
				              where Id =: sysId];
		           
        	form.System__c = sysId;
        	form.System__r = syst;
    	}
    	//if Sub-System Id is in Url, then query Sub-System record
    	else if(subSysId != null)
    	{
        	Sub_System__c subSyst = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
				                     from Sub_System__c 
				                     where Id =: subSysId];
		           
        	form.Sub_System__c = subSysId;
        	form.Sub_System__r = subSyst;
    	}
        //if FEL Id is in Url, then query FEL record
        else if(felId != null)
        {
            Functional_Equipment_Level__c fel = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
                                     from Functional_Equipment_Level__c 
                                     where Id =: felId];
                   
            form.Functional_Equipment_Level__c = felId;
            form.Functional_Equipment_Level__r = fel;
        }
        //if Yard Id is in Url, then query Yard record
        else if(yardId != null)
        {
            Yard__c yard = [select Id, Name, Route__r.Route_Number__c, Planner_Group__c
                                     from Yard__c 
                                     where Id =: yardId];
                   
            form.Yard__c = yardId;
            form.Yard__r = yard;
        }
    	
        form.Status__c = 'Open';
        form.Equipment_Status__c = 'Missing';
       
    }
    private void clearViewState()
    {
        if (attachment.Id == null)
        {
            attachment.body = null;
            attachment = new Attachment();
        }
        if (serialPlate.Id == null)
        {
            serialPlate.Body = null;
            serialPlate = new Attachment();
        }
    }
    public PageReference save() {


        if (form.Description__c == null) {
            clearViewState();
            form.Description__c.addError('You must enter a value');
            return null;
        }

         if (form.Model_Number__c == null) {
            clearViewState();
            form.Model_Number__c.addError('You must enter a value');
            return null;
        }

        if (form.Manufacturer__c == null) {
            clearViewState();
            form.Manufacturer__c.addError('You must enter a value');
            return null;
        }

        if (form.Manufacturer_Serial_No__c == null) {
            clearViewState();
            form.Manufacturer_Serial_No__c.addError('You must enter a value');
            return null;
        }

       
        if (serialPlate.Body == null) {
            clearViewState();
            form.Serial_plate_of_asset__c.addError('You must attach a file');
            return null;
        }

        if(form.Location__c == null && form.Facility__c == null && form.Well_Event__c == null && form.System__c == null & form.Sub_System__c == null
            & form.Yard__c == null & form.Functional_Equipment_Level__c == null) {
            clearViewState();
            form.addError('Please go to a Well Location / Facility / Well Event / System or Sub-System / Yard / FEL detail page and click the "Add Equipment Form" button on the Equipment related list.');
            return null;
        }
        
        try {
           PageReference nextPage = controller.save();
           form = (Equipment_Missing_Form__c)controller.getRecord();   
           if (attachment.Id == null && attachment.Body != null) {
               attachment.ParentId = controller.getId();
               insert attachment;
               System.debug('insert attachment');
               form.Attachment__c = attachment.Id;
               //clear body of uploaded file to remove from view state limit error
               attachment.body = null;
               attachment = new Attachment();
           }
           if (serialPlate.Id == null && serialPlate.Body != null) {
               serialPlate.ParentId = controller.getId();
               insert serialPlate;
               System.debug('insert serial plate =' + serialPlate.Id);
               form.Serial_plate_of_asset__c = serialPlate.Id;
               //clear body of uploaded file to remove from view state limit error
               serialPlate.Body = null;
               serialPlate = new Attachment();
           }
           System.debug('update form =' + form);
           update form;
           if (ApexPages.currentPage().getParameters().get('retURL') != null) {
               System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
               nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
               nextPage.setRedirect(true);
           }
           return nextPage;
        }
        catch(Exception e) {
            System.debug('save error');
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }
    }
    public void DeletePhoto()
    {
        try
        {
            if (attachment.Id == attId)
            {
                delete attachment;
                form.Attachment__c = '';
                attachment.body = null;
                attachment = new Attachment();
                
            }
            else if (serialPlate.Id == attId)
            {
                delete serialPlate;
                form.Serial_plate_of_asset__c = '';
                serialPlate.Body = null;
                serialPlate = new Attachment();
            }    
            if (form.Id != null)
                update form;
        }
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
            
        } 
     
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File deleted successfully'));
        
    }
    public PageReference CloseRequest()
    {
        try
        {
           if (!userPermissions.isSystemAdmin && !userPermissions.isSapSupportLead && !userPermissions.isHogAdmin){
           
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
               return null;
           } 
           else {
                
                form.Status__c = 'Closed'; 
                update form;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
                return null;
           }
        }
        catch(Exception ex)    
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
           return null;
        }     
        
    }
}