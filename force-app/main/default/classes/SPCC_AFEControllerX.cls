/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Controller for AFE create, edit and view page
History:        ssd 09.20.2016 - Initial Revision
                mz 15-Jun-18   - FIX bug found during R23 (W-001176) - added null check for calculations 
**************************************************************************************************/
public with sharing class SPCC_AFEControllerX {
    private ApexPages.StandardController stdController;
    private List<SPCC_Cost_Element__c> costElementListToDelete;
    private UrlParams pageParams;
    private ViewMode mode;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SPCC_AFEControllerX(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.afe = (SPCC_Authorization_for_Expenditure__c) this.stdController.getRecord();

        initialize();

        //Generic Setup
        setCostElementCategoryOrder();
        setUrlParams();
        pageInitializationError = !setViewMode();
        
        if(mode == ViewMode.CREATE) {
            //'Create' Specific Setup
            buildCostElementHierarchy();
        } else {
            //Edit and View Specific Setup
            setAfeRecord(pageParams.afeId);
            setCostElementHierarchy(pageParams.afeId);
        }
        
        //final steps
        refreshApprovedLtdGross();
    }

    public SPCC_Authorization_for_Expenditure__c afe {get; private set;}
    
    //15-Jun-18 mz: fix - added null checks
    public Map<String, List<SPCC_Cost_Element__c>> costElementCategoryMap {
        get{
            for(String category : costElementCategoryMap.keySet()){
                for(SPCC_Cost_Element__c costElement : costElementCategoryMap.get(category)){
                    if (costElement.Original_Auth_Amt__c == null){
                        costElement.Original_Auth_Amt__c = 0.00;
                    }
                    if (costElement.Trend__c == null){
                        costElement.Trend__c = 0.00;
                    }
                }
            }
            return costElementCategoryMap;
        } 
        private set;
    }
    
    public List<MiscGLCodeWrapper> miscGlCodes {get; private set;}
    public List<String> costElementCategoryOrder { get; private set;}
    public Decimal approvedLtdGross {get; private set;}
    public Map<String, Decimal> currentAuthAmountPerGlCode {get; private set;}
    public Map<String, SubTotal> subTotalPerCategory {get; private set;}
    public Boolean pageInitializationError {get; private set;}

    public PageReference save() {
        List<SPCC_Cost_Element__c> costElementsToUpsert = new List<SPCC_Cost_Element__c>();
        Savepoint sp = Database.setSavepoint();

        //DML
        try {
            //First Upsert AFE
            afe.AFE_Number__c = afe.Name;
            if(mode == ViewMode.CREATE) afe.Id = null;
            upsert afe;

            //Check for dupliated GL Numbers
            Set<String> glNumbersAssigned = new Set<String>();

            //Parent all GL Codes and prepare GL Code to upsert
            System.debug('save->costElementCategoryMap: ' + costElementCategoryMap);
            for(String category : costElementCategoryMap.keySet()) {
                System.debug('save->category: ' + category + ' costElementCategoryMap.get(category): ' + costElementCategoryMap.get(category));
                for(SPCC_Cost_Element__c costElement : costElementCategoryMap.get(category)) {
                    if(this.mode == ViewMode.CREATE) 
                        costElement.AFE__c = afe.Id;

                    System.debug('save->costElement.GL_Number__c ' + costElement.GL_Number__c);
                    glNumbersAssigned.add(costElement.GL_Number__c);
                    costElementsToUpsert.add(costElement);
                }
            }

            //Handle New Miscellaneous Gl codes
            System.debug('glNumbersAssigned: ' + glNumbersAssigned);
            List<SPCC_Cost_Element__c> miscCostElementList = new List<SPCC_Cost_Element__c>();
            for(MiscGLCodeWrapper miscGLCode : miscGlCodes) {
                System.debug('miscGlCode.costElement.GL_Number__c: ' + miscGlCode.costElement.GL_Number__c);
                if(glNumbersAssigned.contains(miscGlCode.costElement.GL_Number__c)) {
                    SPCC_Utilities.logErrorOnPage('GL Number ' + miscGlCode.costElement.GL_Number__c + ' duplicated!');
                    costElementsToUpsert.clear();
                    Database.rollback(sp);
                    return null;
                }
                costElementsToUpsert.add(miscGLCode.costElement);
                glNumbersAssigned.add(miscGLCode.costElement.GL_Number__c);
                if(miscGlCode.costElement.AFE__c != afe.Id) miscGLCode.costElement.AFE__c = afe.Id;
            }

            //Upsert GL Codes
            upsert costElementsToUpsert;

            //delete misc GL Codes to be deleted
            delete costElementListToDelete;
        } catch (DmlException ex) {
            SPCC_Utilities.logErrorOnPage(ex);
            costElementsToUpsert.clear();
            Database.rollback(sp);
            return null;
        }

        return redirect();
    }
    
    public PageReference edit() {
        PageReference editPage = Page.SPCC_AFEEdit;
        editPage.getParameters().put('id', afe.Id);
    	editPage.setRedirect(true);
        return editPage;
    }

    public PageReference redirect() {
        if(afe.Id != null) {
            PageReference viewPage = Page.SPCC_AFEView;
            viewPage.getParameters().put('id', afe.Id);
            viewPage.setRedirect(true);
            return viewPage;
        }

        return stdController.cancel();
    }

    public PageReference showGLBreakdown() {
        System.debug('afe.Engineering_Work_Request__c: ' + afe.Engineering_Work_Request__c + ' afe.AFE_Number__c: ' + afe.AFE_Number__c);
        return null;
    }

    public PageReference refreshApprovedLtdGross() {
        approvedLtdGross = 0.00;
        if(costElementCategoryMap != null && !costElementCategoryMap.isEmpty()) {
            for(String cat : costElementCategoryMap.keySet()) {
                SubTotal subTotal = subTotalPerCategory.get(cat);
                subTotal.clearValues();
                for(SPCC_Cost_Element__c costElement : costElementCategoryMap.get(cat)) {
                    approvedLtdGross += costElement.Original_Auth_Amt__c  + costElement.Trend__c;
                    currentAuthAmountPerGlCode.put(costElement.GL_Number__c, costElement.Original_Auth_Amt__c + costElement.Trend__c);

                    //Update Subtotals
                    subTotal.originalAuthAmtTotal += costElement.Original_Auth_Amt__c;
                    subTotal.trendTotal += costElement.Trend__c;
                    subTotal.currentAuthAmtTotal += costElement.Original_Auth_Amt__c + costElement.Trend__c;
                }
            }
        }

        //Handle Miscellaneous Cost Codes
        SubTotal miscSubTotal = subTotalPerCategory.get(SPCC_Utilities.GL_CODE_CATEGORY_OTHER);
        miscSubTotal.clearValues();
        for(MiscGLCodeWrapper miscGLCode : miscGlCodes) {
            approvedLtdGross += miscGLCode.currentAuthAmount;

            //Subtotal
            System.debug('miscSubTotal: ' + miscSubTotal);
            miscSubTotal.originalAuthAmtTotal += miscGLCode.costElement.Original_Auth_Amt__c;
            miscSubTotal.trendTotal += miscGLCode.costElement.Trend__c;
            miscSubTotal.currentAuthAmtTotal += miscGLCode.currentAuthAmount;
        }

        return null;
    }

    public PageReference addMiscellaneousGLCode() {
        SPCC_Cost_Element__c glCode = new SPCC_Cost_Element__c(
            Category__c = SPCC_Utilities.GL_CODE_CATEGORY_OTHER,
            Original_Auth_Amt__c = 0.00,
            Trend__c = 0.00
        );
        miscGlCodes.add(new MiscGLCodeWrapper(glCode));
        return null;
    }

    public PageReference removeMiscGLCode() {
        Integer miscGlIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('miscGLIndex'));
        System.debug(miscGLIndex);
        MiscGLCodeWrapper miscGLCodeToRemove = miscGlCodes.get(miscGlIndex);
        if(miscGLCodeToRemove.costElement.Id != null) costElementListToDelete.add(miscGLCodeToRemove.costElement);
        miscGlCodes.remove(miscGlIndex);
        return null;
    }

    private void initialize() {
        costElementCategoryOrder = new List<String>();
        costElementCategoryMap = new Map<String, List<SPCC_Cost_Element__c>>();
        costElementListToDelete = new List<SPCC_Cost_Element__c>();
        currentAuthAmountPerGlCode = new Map<String, Decimal>();
        subTotalPerCategory = new Map<String, SubTotal>();
        miscGlCodes = new List<MiscGLCodeWrapper>();
    }

    private void setCostElementCategoryOrder() {
        Schema.DescribeFieldResult fieldResult = SPCC_GL_Code_Entry__mdt.Category__c.getDescribe();
        for(Schema.PicklistEntry f : fieldResult.getPicklistValues()) {
            costElementCategoryOrder.add(f.getLabel());
            subTotalPerCategory.put(f.getLabel(), new SubTotal());
        }
        subTotalPerCategory.put(SPCC_Utilities.GL_CODE_CATEGORY_OTHER, new SubTotal());
    }

    private void setUrlParams() {
        pageParams = new UrlParams();
        pageParams.afeId = (ApexPages.currentPage().getParameters().get('id') != null) ? 
        	EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('id'), 'utf-8') : null;
    }

    private Boolean setViewMode() {
        String siteUrl = ApexPages.currentPage().getUrl();
        System.debug('siteUrl: ' + siteUrl);
        
        mode = (siteUrl.containsIgnoreCase(Page.SPCC_AFECreate.getUrl())) ? ViewMode.CREATE : 
        		(siteUrl.containsIgnoreCase(Page.SPCC_AFEEdit.getUrl())) ? ViewMode.EDIT :
        		(siteUrl.containsIgnoreCase(PAge.SPCC_AFEView.getUrl())) ? ViewMode.VIEW :
        		ViewMode.CREATE;
        		
        if(mode == ViewMode.CREATE && pageParams.afeId != null) {
        	SPCC_Utilities.logErrorOnPage('Cannot specify \'afeId\' for create page.');
        	return false; //Cannot specify afe Id on create page
        }
        
        return true;
    }

    private void buildCostElementHierarchy() {
        List<SPCC_GL_Code_Entry__mdt> costElementConfigItemList = [Select DeveloperName, MasterLabel, Category__c, 
        															GL_Number__c
                                                                   From SPCC_GL_Code_Entry__mdt
                                                                   Where Category__c <> :SPCC_Utilities.GL_CODE_CATEGORY_OTHER];

        //Add all categories to the map
        for(String category : costElementCategoryOrder) {
            costElementCategoryMap.put(category, new List<SPCC_Cost_Element__c>());
        }

        for(SPCC_GL_Code_Entry__mdt costElementConfigItem : costElementConfigItemList) {

            //Create New Cost Element Record for AFE
            SPCC_Cost_Element__c newCostElement = new SPCC_Cost_Element__c(
                    Label__c = costElementConfigItem.MasterLabel,
                    GL_Number__c = costElementConfigItem.GL_Number__c,
                    Category__c = costElementConfigItem.Category__c,
                    Original_Auth_Amt__c = 0.00,
                    Trend__c = 0.00
                );

            //Add to Category Map
            costElementCategoryMap.get(costElementConfigItem.Category__c).add(newCostElement);
        }
    }

    private void setAfeRecord(String recordId) {
        try {
           	this.afe = [Select Id, Name, AFE_Number__c, Approved_LTD_Gross__c, Incurred_Cost__c, 
            					Description__c, Engineering_Work_Request__c,
            					CreatedById, CreatedBy.Name, CreatedDate, OH_Capital__c,
            					Non_PO_Incurred_Cost__c, Total_Incurred_Cost__c,
            					LastModifiedById, LastModifiedBy.Name, LastModifiedDate
                         From SPCC_Authorization_for_Expenditure__c
                         Where Id =: recordId Limit 1];
        } catch (QueryException ex) {
            SPCC_Utilities.logErrorOnPage(ex);
            pageInitializationError = true;
            return;
        }
    }
    
    private void setCostElementHierarchy(String parentAfeId) {
        List<SPCC_Cost_Element__c> costElementTempList;
        
        //Get cost Element List
        try {
	        costElementTempList = [Select Id, Name, AFE__c, Category__c, Description__c,
	        				   		      GL_Number__c, Label__c, Original_Auth_Amt__c, Trend__c,
                                          Current_Auth_Amt__c
	        				       From SPCC_Cost_Element__c
	        				       Where AFE__c =: parentAfeId];
        } catch (QueryException ex) {
        	SPCC_Utilities.logErrorOnPage(ex);
            pageInitializationError = true;
            return;
        }
        
        //Build Category Map
        //Add all categories to the map
        for(String category : costElementCategoryOrder) {
            costElementCategoryMap.put(category, new List<SPCC_Cost_Element__c>());
        }
        for(SPCC_Cost_Element__c costElement : costElementTempList) {
            if(costElement.Category__c != SPCC_Utilities.GL_CODE_CATEGORY_OTHER) {
                System.debug('setCostElementHierarchy->costElement.Category__c: ' + costElement.Category__c);
        	    costElementCategoryMap.get(costElement.Category__c).add(costElement);
            }
            else
                miscGlCodes.add(new MiscGLCodeWrapper(costElement));
        }
    }

    public Enum ViewMode {
        CREATE,
        EDIT,
        VIEW
    }

    public Class MiscGLCodeWrapper {
        public MiscGLCodeWrapper(SPCC_Cost_Element__c costElement) {
            this.costElement = costElement;
        }
        
        //15-Jun-18 mz: fix - added null checks
        public SPCC_Cost_Element__c costElement {
            get {
                if (costElement.Original_Auth_Amt__c == null){
                    costElement.Original_Auth_Amt__c = 0.00;
                }
                if (costElement.Trend__c == null){
                    costElement.Trend__c = 0.00;
                }
                return costElement;
            } 
            private set;
        }
            
        public Decimal currentAuthAmount {
            get {
                currentAuthAmount = costElement.Original_Auth_Amt__c + costElement.Trend__c;
                return currentAuthAmount;
            }
            private set;
        }
    }

    public Class SubTotal {
        public Decimal originalAuthAmtTotal {get; set;}
        public Decimal trendTotal {get; set;}
        public Decimal currentAuthAmtTotal {get; set;}

        public SubTotal() {
            originalAuthAmtTotal = 0.00;
            trendTotal = 0.00;
            currentAuthAmtTotal = 0.00;
        }

        public void clearValues() {
            this.originalAuthAmtTotal = 0.00;
            this.trendTotal = 0.00;
            this.currentAuthAmtTotal = 0.00;
        }
    }

    private Class UrlParams {
        String afeId;
    }
}