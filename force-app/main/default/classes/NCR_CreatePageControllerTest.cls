@isTest
private class NCR_CreatePageControllerTest{

    static testMethod void testNCRCARCreatePageAsNcrUser(){
        //Create NCR user
        User ncrNormalUser = NCRUserTestData.createNCRUser();
        
        System.runAs(ncrNormalUser) {
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('Created', 'Submit Non-Conformance to QA Lead', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Created', 'Case', 'Project', true);
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('Submitted', 'Review and Assess Non-Conformance Report', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Submitted', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('Submit NCR', 'Submit NCR', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, 'Submit NCR to QA Lead?', 'Submit_NCR', 
                    state2, '', null, true);
                    
            System.AssertNotEquals(ncrNormalUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);
                       
            PageReference pageRef = Page.NCR_CreatePage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
            
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
            'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
            'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'Created', true);
              
            System.AssertNotEquals(ncrCase.Id, null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);                        
            NCR_CreatePageControllerX controller = new NCR_CreatePageControllerX(standardController);   
            
            //PageReference saveReference = controller.NCR_Save();        
            //System.AssertNotEquals(saveReference, null);        
            
            PageReference projectReference = controller.NCR_Redirect();        
            System.AssertNotEquals(projectReference, null);
            
            PageReference cancelEditReference = controller.NCR_CancelEdit();                                      
            System.AssertNotEquals(projectReference, null);
            
            System.AssertNotEquals(controller.exit(), Null);
            System.AssertNotEquals(controller.getBusinessUnits(), Null);
            System.AssertNotEquals(controller.getProjects(), Null);
            
            Document d = new Document();
            d.name = 'Document Name';
            d.body = Blob.valueOf('Unit Test Document Body');        
            
            controller.documentFile = d;
            controller.uploadFile();  
            
            controller.selectedAttachmentId = d.id;
            System.AssertNotEquals(controller.selectedAttachmentId, Null);
            
            controller.removeFile();         
        }             
        
    }
    
    static testMethod void testNCRCreatePageSave(){
        //Create NCR user
        User ncrNormalUser = NCRUserTestData.createNCRUser();
        
        System.runAs(ncrNormalUser) {
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('Created', 'Submit Non-Conformance to QA Lead', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Created', 'Case', 'Project', true);
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('Submitted', 'Review and Assess Non-Conformance Report', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Submitted', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('Submit NCR', 'Submit NCR', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, 'Submit NCR to QA Lead?', 'Submit_NCR', 
                    state2, '', null, true);
                    
            System.AssertNotEquals(ncrNormalUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);
                       
            PageReference pageRef = Page.NCR_CreatePage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
            
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
            'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
            'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'Created', false);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);                        
            NCR_CreatePageControllerX controller = new NCR_CreatePageControllerX(standardController);   
            
            PageReference saveReference = controller.NCR_Save();        
            System.AssertNotEquals(saveReference, null);        
                 
        }         
    }
    
    static testMethod void testNCRCreatePageSaveSubmit(){
        //Create NCR user
        User ncrNormalUser = NCRUserTestData.createNCRUser();
        
        System.runAs(ncrNormalUser) {
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('Created', 'Submit Non-Conformance to QA Lead', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Created', 'Case', 'Project', true);
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('Submitted', 'Review and Assess Non-Conformance Report', 'NCR_Case_Detail,Location,Non_conformance_Details,Disposition_Plan', 
                'Submitted', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('Submit NCR', 'Submit NCR', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, 'Submit NCR to QA Lead?', 'Submit_NCR', 
                    state2, '', null, true);
                    
            System.AssertNotEquals(ncrNormalUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);
                       
            PageReference pageRef = Page.NCR_CreatePage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
            
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
            'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
            'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'Created', false);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);                        
            NCR_CreatePageControllerX controller = new NCR_CreatePageControllerX(standardController);   
            
            PageReference saveReference = controller.NCR_SaveAndSubmit();        
            System.AssertNotEquals(saveReference, null);        
                 
        }         
    }    

}