public with sharing class SPCC_MassServiceEntrySheetEditCtrlX {

    //Sort Columns
    public static String SORT_BY_PURCHASEORDER_NUMBER = 'SORT_BY_PURCHASEORDER_NUMBER';
    public static String SORT_BY_VENDOR = 'SORT_BY_VENDOR';
    public static String SORT_BY_TICKET_NUMBER = 'SORT_BY_TICKET_NUMBER';
    public static String SORT_BY_AFE_NUMBER = 'SORT_BY_AFE_NUMBER';
    public static String SORT_BY_GL_NUMBER = 'SORT_BY_GL_NUMBER';
    public static String SORT_BY_AMOUNT = 'SORT_BY_AMOUNT';
    public static String SORT_BY_ESTIMATE_TO_FINISH = 'SORT_BY_ESTIMATE_TO_FINISH';
    public static String SORT_BY_DATE = 'SORT_BY_DATE';
    public static String SORT_BY_PO_OVERUNDER = 'SORT_BY_PO_OVERUNDER';
    public static String SORT_BY_LINE_ITEM = 'SORT_BY_LINE_ITEM';
    public static String SORT_BY_INCURRED_COST_RATE = 'SORT_BY_INCURRED_COST_RATE';
    public static String SORT_COLUMN = SORT_BY_DATE; //Default Sort By Date
    public static String SORT_ASCENDING = 'SORT_ASCENDING';
    public static String SORT_DESCENDING = 'SORT_DESCENDING';
    public static String SORT_ORDER = SORT_DESCENDING; //Default Order Descending

    private final ApexPages.StandardController stdController;
    private final SPCC_Engineering_Work_Request__c ewr;
    private final SPCC_Utilities.UserPermissions userPermissions;
    

    public SPCC_MassServiceEntrySheetEditCtrlX(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.ewr = (SPCC_Engineering_Work_Request__c)stdController.getRecord();
        this.serviceEntrySheets = new List<ServiceEntrySheetWrapper>();
        this.serviceEntrySheetsToDelete = new List<SPCC_Service_Entry_Sheet__c>();
        this.userPermissions = new SPCC_Utilities.UserPermissions();
        this.pageNotification = new Notification();
        this.descriptionPopup = new DescriptionPopup();

        //Setup
        getExistingServiceEntrySheets();
        validateReport();
        populatePOandAccounts();
    }


    /**********************
    * Getters and Setters *
    **********************/

    public Notification pageNotification {get; private set;}
    public List<ServiceEntrySheetWrapper> serviceEntrySheets {
        get {
            if((serviceEntrySheets == null || serviceEntrySheets.isEmpty()) && serviceEntrySheetsToDelete.isEmpty()) {  //m.z.: 9.3.2018 fix for last SES entry deletion bug
                serviceEntrySheets = getExistingServiceEntrySheets();
            }
            return serviceEntrySheets;
        }
        private set;
    }
    
    public List<SPCC_Service_Entry_Sheet__c> serviceEntrySheetsToDelete {get; private set;}
    public Map<Id, SPCC_Purchase_Order__c> purchaseOrderMap {
        get{
            if(purchaseOrderMap == null || purchaseOrderMap.isEmpty()) {
                Integer optionsLimit = SPCC_Utilities.COLLECTION_SIZE_LIMIT - 1;
                purchaseOrderMap = new Map<Id, SPCC_Purchase_Order__c>([Select Id, Name, Vendor_Account__r.Name, PO_Over_Under__c, Estimate_to_Complete__c,
                                                                               Purchase_Order_Number__c, Authorization_for_Expenditure__r.AFE_Number__c,
                                                                               (Select Id, Name, Item__c,Short_Text__c
                                                                                From Line_Items__r)
                                                                        From SPCC_Purchase_Order__c
                                                                        Where Engineering_Work_Request__c =: ewr.Id
                                                                        ORDER BY  Vendor_Account__r.Name ASC
                                                                        LIMIT :optionsLimit]);
            }
            return purchaseOrderMap;
        }
        private set;
    }
    
    
    
    
    public Map<Id, SPCC_Line_Item__c> lineItemMap {
        get {
            if(lineItemMap == null || lineItemMap.isEmpty()) {
                Integer optionsLimit = SPCC_Utilities.COLLECTION_SIZE_LIMIT - 1;
                lineItemMap = new Map<Id, SPCC_Line_Item__c>([Select Id, Name, Item__c,Short_Text__c,Incurred_Cost_Rate__c,
                                                                     Purchase_Order__c, Cost_Element__r.GL_Number__c,
                                                                     Purchase_Order__r.Name,
                                                                     Purchase_Order__r.Vendor_Account__r.Name,
                                                                     Purchase_Order__r.Estimate_to_Complete__c
                                                              From SPCC_Line_Item__c
                                                              Where Purchase_Order__c In :purchaseOrderMap.keySet()
                                                              LIMIT :optionsLimit]);
            }
            return lineItemMap;
        }
        private set;
    }

    
    public List<String> poAccountOptionsList {get;  set;}
    public Map<String, String> poNumberToPOIdMap {get;  set;}
    
    private void populatePOandAccounts() {
        
        poAccountOptionsList = new List<String>();
        poNumberToPOIdMap = new Map<String, String>();

        for(SPCC_Purchase_Order__c po : purchaseOrderMap.values()) {
            for(SPCC_Line_Item__c li : po.Line_Items__r) {
                poAccountOptionsList.add(po.Name + ' - ' + po.Vendor_Account__r.Name + ' --- ' + li.Item__c + '-' + li.Short_Text__c);
                poNumberToPOIdMap.put(po.Name, po.Id);
            }
        }
    }
    
    
    public Boolean areChangedRecords {
        get {
            for(ServiceEntrySheetWrapper sesWrapper : serviceEntrySheets) {
                if(sesWrapper.getIsChanged()) return true;
            }
            return false;
        }
        private set;
    }


    public Boolean getIsInEditMode{
            
        get {
            for(ServiceEntrySheetWrapper sesWrapper : serviceEntrySheets) {
                if(sesWrapper.mode == ServiceEntrySheetMode.Edit) {
                    return true;
                }
            }    
            return false;
             
        }
        private set;
    }
    
    //m.z.: 3-Sep-2017 - W-000803
    public void validateReport(){

        List<String> filterList;
        // Get report
        List<Report> reportList = [SELECT Id FROM Report WHERE DeveloperName = 'EWRs_PO_Amount_by_AFE'];   //get report ID for CSR report
        // report reuslt for unit test
        String jsonReport;
        Reports.ReportResults jsonResults;

        if (Test.isRunningTest()){
            // mock report
            jsonReport = '{"allData":true, "reportMetadata":{"scope":"user","reportType":{"type":"EWR_Cost_Breakdown_Per_Cost_Element","label":"EWR Cost Breakdown Per Cost Element"},'+
                            '"reportFormat":"TABULAR","reportFilters":[{"value": "12345","column": "SPCC_Engineering_Work_Request__c.Name","operator": "Equal"}],'+
                            '"reportBooleanFilter":"","name":"EWR Cost Breakdown Per Cost Element","id":"00O16000007JSON","developerName":"EWR_Cost_Breakdown_Per_Cost_Element"}}';

            jsonResults = (Reports.ReportResults)JSON.deserialize(jsonReport, Reports.ReportResults.class);
        }

        // Check if report exists
        if(reportList.Size() == 0 && !Test.isRunningTest()){

            reportIsValid = false;
            validateResult = 'reportDoesNotExist';
        }

        else{
            // Run a report
            Reports.ReportResults results = Test.isRunningTest() ? jsonResults : Reports.ReportManager.runReport(reportList[0].Id);

            // Get the report metadata
            Reports.ReportMetadata rm = results.getReportMetadata();

            // Check for correct filter set on report - first filter condition has to be EWR Name
            filterList = new List<String>();
            for(Reports.ReportFilter rf : rm.getreportFilters()){

                filterList.add(rf.getColumn());
            }

            if(filterList.Size() > 0 && filterList[0] == 'SPCC_Engineering_Work_Request__c.Name'){

                reportIsValid = true;
                validateResult = 'reportIsValid';
                reportId = Test.isRunningTest() ? rm.getId()  : reportList[0].Id;
            }
            else {

                reportIsValid = false;
                validateResult = 'filterError';
            }
        }
    }

    public String sortColumn {get {return SORT_COLUMN;} private set;}
    public String sortOrder {get {return SORT_ORDER;} private set;}
    public Boolean isUserAllowedToViewDetail {get {return userPermissions.isCSR ||  userPermissions.isEPC || userPermissions.isAdmin || userPermissions.isAdminNonPlatform || 
                                                          userPermissions.isSystemAdmin || userPermissions.isProjectController || userPermissions.isProjectLead;} private set;} //All SPCC Users should have access to EWR Detail Page - W-000981
    public Boolean isUserAllowedToViewReport {get {return userPermissions.isCSR ||  userPermissions.isEPC || userPermissions.isAdmin || userPermissions.isAdminNonPlatform || 
                                                          userPermissions.isSystemAdmin || userPermissions.isProjectController || userPermissions.isProjectLead;} private set;} //All SPCC Users should have access to Report - W-000981
    public DescriptionPopup descriptionPopup {get; private set;}
    public Boolean reportIsValid {get; private set;}
    public String validateResult {get; private set;}
    public String reportId {get; private set;}

    /***************
    * Page Actions *
    ***************/

    public PageReference redirectToDetail() {
        PageReference standardDetailPage = stdController.view();
        standardDetailPage.getParameters().put('nooverride', '1');
        return standardDetailPage;
    }

    //m.z.: 3-Sep-2017 - W-000803
    public PageReference redirectToReport() {

        String reportURL;
        PageReference reportPage;
        
        
        if(getIsInEditMode) {
            
            reportPage = null;
            pageNotification.show = true;
            pageNotification.mode = NotificationMode.ERROR;
            pageNotification.message = 'You are in Edit mode, please cancel or submit your changes first. ';
        } 
        
        else if(!reportIsValid && validateResult == 'reportDoesNotExist'){

            reportPage = null;
            pageNotification.show = true;
            pageNotification.mode = NotificationMode.ERROR;
            pageNotification.message = 'Report does not exist or you don\'t have access to it. ';
        }
        else {

            if(reportIsValid && validateResult == 'reportIsValid'){

               reportURL =  URL.getSalesforceBaseUrl().toExternalForm() + '/' + reportId + '?pv0=' + ewr.Name;   //built report URL with filter for currently viewing EWR
               reportPage = new PageReference(reportURL);
               reportPage.setRedirect(true);
            }
            else if (!reportIsValid && validateResult == 'filterError') {

                reportPage = null;
                pageNotification.show = true;
                pageNotification.mode = NotificationMode.ERROR;
                pageNotification.message = 'Invalid Report filter criteria set. First filter condition has to be EWR Name. ';
            }

        }
        return reportPage;
    }


    public PageReference noAction() {
        System.debug('noAction called ...');
        pageNotification.show = false;
        return null;
    }

    //mz fix: 12-Feb-2018 (added empty check)
    public PageReference addNewServiceEntrySheet() {
        System.debug('serviceEntrySheets.size: ' + this.serviceEntrySheets.size());
        
        if (this.serviceEntrySheets.isEmpty()){
            
            this.serviceEntrySheets.add(new ServiceEntrySheetWrapper(
                new SPCC_Service_Entry_Sheet__c(), ServiceEntrySheetMode.EDIT,
                ServiceEntrySheetState.CREATED, purchaseOrderMap,lineItemMap,poNumberToPOIdMap));
        }
        else {
            
            this.serviceEntrySheets.add(0,new ServiceEntrySheetWrapper(
                new SPCC_Service_Entry_Sheet__c(), ServiceEntrySheetMode.EDIT,
                ServiceEntrySheetState.CREATED, purchaseOrderMap,lineItemMap,poNumberToPOIdMap));
        } 
        pageNotification.show = false;
        return null;
    }

    public PageReference editServiceEntrySheet() {
        Integer sesIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('sesIndex'));
        if(serviceEntrySheets.get(sesIndex).mode == ServiceEntrySheetMode.VIEW) {
            serviceEntrySheets.get(sesIndex).mode = ServiceEntrySheetMode.EDIT;
        }
        pageNotification.show = false;
        return null;
    }

    public PageReference removeServiceEntrySheet() {
        Integer sesIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('sesIndex'));
        if(serviceEntrySheets.get(sesIndex).state == ServiceEntrySheetState.CREATED) {
            serviceEntrySheets.remove(sesIndex);
        }
        pageNotification.show = false;
        return null;
    }

    public PageReference deleteServiceEntrySheet() {
        Integer sesIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('sesIndex'));
        if(serviceEntrySheets.get(sesIndex).state == ServiceEntrySheetState.EXISTING) {
            ServiceEntrySheetWrapper ses = serviceEntrySheets.get(sesIndex);
            serviceEntrySheetsToDelete.add(ses.serviceEntrySheet);
            serviceEntrySheets.remove(sesIndex);
        }
        pageNotification.show = false;
        return null;
    }

    public PageReference saveServiceEntrySheet() {
        Integer sesIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('sesIndex'));
        if(serviceEntrySheets.get(sesIndex).mode == ServiceEntrySheetMode.EDIT) {

            SPCC_Purchase_Order__c po = purchaseOrderMap.get(serviceEntrySheets.get(sesIndex).serviceEntrySheet.Purchase_Order__c);
            
            //m.z. fix - check if po is slected 
            if (po != null){

                serviceEntrySheets.get(sesIndex).mode = ServiceEntrySheetMode.VIEW;
                po.Estimate_To_Complete__c = (serviceEntrySheets.get(sesIndex).estimateToFinish == null || 
                                              serviceEntrySheets.get(sesIndex).estimateToFinish == '' || 
                                             !SPCC_Utilities.isNumeric(serviceEntrySheets.get(sesIndex).estimateToFinish)) ? null : Decimal.valueof(serviceEntrySheets.get(sesIndex).estimateToFinish);
    
                for (ServiceEntrySheetWrapper ses : serviceEntrySheets)
                    if (po.Id == ses.serviceEntrySheet.Purchase_Order__c){
                        ses.estimateToFinish = String.valueof(po.Estimate_To_Complete__c);
                    }
            }
        }
        pageNotification.show = false;
        return null;
    }
    

    public PageReference closeNotification() {
        pageNotification.show = false;
        return null;
    }

    public PageReference sortServiceEntrySheets() {
        String sortOptions = ApexPages.currentPage().getParameters().get('sortOptions');
        List<String> options = sortOptions.split(';');
        SPCC_MassServiceEntrySheetEditCtrlX.SORT_COLUMN = options.get(0);
        SPCC_MassServiceEntrySheetEditCtrlX.SORT_ORDER = options.get(1);
        System.debug('SPCC_MassServiceEntrySheetEditCtrlX.SORT_COLUMN: ' + SPCC_MassServiceEntrySheetEditCtrlX.SORT_COLUMN +
            ' SPCC_MassServiceEntrySheetEditCtrlX.SORT_ORDER: ' + SPCC_MassServiceEntrySheetEditCtrlX.SORT_ORDER);
        this.serviceEntrySheets.sort();
        return null;
    }
    
    

    public PageReference submit() {
        
        
        //Create Database Savepoint
        Savepoint sp = Database.setSavePoint();

        try {
            //Save Current Sheets (new or edited)
            //Get changed service entry sheets only
            List<SPCC_Service_Entry_Sheet__c> sesUpsertList = new List<SPCC_Service_Entry_Sheet__c>();
            Set<SPCC_Purchase_Order__c> poUpsertSet = new Set<SPCC_Purchase_Order__c>();

            for(ServiceEntrySheetWrapper sesWrapper : serviceEntrySheets) {
                if(sesWrapper.getIsChanged()) {

                    poUpsertSet.add(sesWrapper.purchaseOrderMap.get(sesWrapper.serviceEntrySheet.Purchase_Order__c));
                    sesUpsertList.add(sesWrapper.serviceEntrySheet);
                }
            }
            System.debug('serviceEntrySheetsSubmit: ' + serviceEntrySheets);
            System.debug('purchaseOrderMap: ' + purchaseOrderMap);
            System.debug('poUpsertSet: ' + poUpsertSet);
            System.debug('upsertlist: ' + sesUpsertList);
            
            upsert sesUpsertList;

            if (poUpsertSet.Size() > 0){

                List<SPCC_Purchase_Order__c> poUpsertList = new List<SPCC_Purchase_Order__c>(poUpsertSet);
                upsert poUpsertList;
                System.debug('debug_poUpsertSet : ' + poUpsertSet);
                System.debug('debug_poUpsertList : ' + poUpsertList);
            }



            //Delete
            delete serviceEntrySheetsToDelete;
        } catch (DmlException ex) {
            //SPCC_Utilities.logErrorOnPage(ex);
            pageNotification.show = true;
            pageNotification.mode = NotificationMode.ERROR;
            pageNotification.message = 'Errors:\n';
            for(Integer i=0; i < ex.getNumDml(); i++) {
                pageNotification.message += (ex.getDmlFieldNames(i) + ':' + ex.getDmlMessage(i) + '\n');
            }

            //restore savepoint
            Database.rollback(sp);
            return null;
        }

        //Success
        serviceEntrySheetsToDelete.clear();
        serviceEntrySheets.clear();
        pageNotification.show = true;
        pageNotification.mode = NotificationMode.SUCCESS;
        pageNotification.message = 'All operations completed successfully. ';
        if(!Test.isRunningTest()) stdController.reset();
        return null;
    }

    public PageReference showDescriptionEditPopup() {
        Integer sesIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('sesIndex'));
        this.descriptionPopup.ses = serviceEntrySheets.get(sesIndex);
        this.descriptionPopup.show = true;
        this.descriptionPopup.oldDescription = serviceEntrySheets.get(sesIndex).serviceEntrySheet.Description__c;
        System.debug('showDescriptionEditPopup');
        return null;
    }

    public PageReference saveDescriptionEditPopup() {
        System.debug('saveDescriptionEditPopup: ' + this.descriptionPopup.ses.serviceEntrySheet.Description__c);
        this.descriptionPopup.show = false;
        this.descriptionPopup.ses = null;
        return null;
    }

    public PageReference cancelDescriptionEditPopup() {
        this.descriptionPopup.show = false;
        this.descriptionPopup.ses.serviceEntrySheet.Description__c = this.descriptionPopup.oldDescription;
        this.descriptionPopup.ses = null;
        return null;
    }
    
                

    /********************
    * Utility functions *
    ********************/
    private List<ServiceEntrySheetWrapper> getExistingServiceEntrySheets() {
        List<ServiceEntrySheetWrapper> sesWrapperList = new List<ServiceEntrySheetWrapper>();
        for(SPCC_Service_Entry_Sheet__c ses : [Select Id, Name, Purchase_Order__c, Amount__c,
                                                      Date_Entered__c, Ticket_Number__c,
                                                      Purchase_Order__r.Name,
                                                      Purchase_Order__r.Vendor_Account__r.Name,
                                                      Purchase_Order__r.PO_Over_Under__c,
                                                      Purchase_Order__r.Estimate_to_Complete__c,
                                                      Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c,
                                                      Line_Item__c, Line_Item__r.Item__c,
                                                      Line_Item__r.Incurred_Cost_Rate__c,
                                                      Line_Item__r.Cost_Element__r.GL_Number__c,
                                                      Line_Item__r.Incurred_Cost__c,
                                                      Line_Item__r.Committed_Amount__c,
                                                      Description__c
                                               From SPCC_Service_Entry_Sheet__c
                                               Where Purchase_Order__r.Engineering_Work_Request__c =: ewr.Id
                                               LIMIT :SPCC_Utilities.COLLECTION_SIZE_LIMIT]) {
            sesWrapperList.add(new ServiceEntrySheetWrapper(ses, ServiceEntrySheetMode.VIEW,
                ServiceEntrySheetState.EXISTING, purchaseOrderMap,lineItemMap,poNumberToPOIdMap));
        }
        sesWrapperList.sort(); //Sort with exisiting column
        return sesWrapperList;
    }

    /******************************
    * Service Entry Sheet Wrapper *
    ******************************/
    public class ServiceEntrySheetWrapper implements Comparable {
        private ServiceEntrySheetMode mode;
        private ServiceEntrySheetState state;
        private Map<Id, SPCC_Purchase_Order__c> purchaseOrderMap;
        private Map<Id, SPCC_Line_Item__c> lineItemMap;
        private Map<String, String> poNumberToPOIdMap;
        private Id startPurchaseOrderId;
        private String startTicketNumber;
        private Decimal startAmount;
        private String startEstimateToFinish;
        private Date startDate;
        private Id startLineItemId;
        private String startDescription;
        private String poVendorLineItemUniqueKey;

        public ServiceEntrySheetWrapper(SPCC_Service_Entry_Sheet__c serviceEntrySheet,
            ServiceEntrySheetMode mode, ServiceEntrySheetState state,
            Map<Id, SPCC_Purchase_Order__c> purchaseOrderMap, 
            Map<Id, SPCC_Line_Item__c> lineItemMap, 
            Map<String, String> poNumberToPOIdMap) {
            this.serviceEntrySheet = serviceEntrySheet;
            this.mode = mode;
            this.state = state;
            this.purchaseOrderMap = purchaseOrderMap;
            this.lineItemMap = lineItemMap;
            this.poNumberToPOIdMap = poNumberToPOIdMap;
            this.startPurchaseOrderId = this.serviceEntrySheet.Purchase_Order__c;
            this.startTicketNumber = this.serviceEntrySheet.Ticket_Number__c;
            this.startAmount = this.serviceEntrySheet.Amount__c;
            this.estimateToFinish = String.valueOf(this.serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
            this.liCostRate = this.serviceEntrySheet.Line_Item__r.Incurred_Cost_Rate__c;
            this.poOverUnder = this.serviceEntrySheet.Purchase_Order__r.PO_Over_Under__c;
            this.startEstimateToFinish = String.valueOf(this.serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
            this.startDate = this.serviceEntrySheet.Date_Entered__c;
            this.startLineItemId = this.serviceEntrySheet.Line_Item__c;
            this.startDescription = this.serviceEntrySheet.Description__c;
        }

        /**********************
        * Getters and Setters *
        **********************/
        public SPCC_Service_Entry_Sheet__c serviceEntrySheet {get; private set;}
        public Decimal poOverUnder {
            get{
                return serviceEntrySheet.Purchase_Order__r.PO_Over_Under__c;
            }
            private set;
        }

        public String estimateToFinish {
            get;
            set;
        }
        
       public Decimal liCostRate {
            get{
                return serviceEntrySheet.Line_Item__r.Incurred_Cost_Rate__c;
            }
            private set;
        }
        

        public String poId{
            get{
                return serviceEntrySheet.Purchase_Order__r.Name;
            }
            set{
                serviceEntrySheet.Purchase_Order__c =  poNumberToPOIdMap.get(value.left(10));
                serviceEntrySheet.Purchase_Order__r = purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c);
                
                if (serviceEntrySheet.Purchase_Order__c != null){
                    poVendorLineItemUniqueKey = value;
                    estimateToFinish = String.valueOf(purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c).Estimate_To_Complete__c); 
                    poOverUnder = purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c).PO_Over_Under__c;
                    setLineItem();
                    System.debug('poId->Set->poOverUnder: ' + this.purchaseOrderMap);
                    System.debug('poId->Set->estimateToFinish: ' + this.estimateToFinish);
                }
                else{
                    estimateToFinish = null;
                    poOverUnder = null;
                    setLineItem();
                }
            }
        }


        public String getMode() {
            if(mode != null)
                return mode.name();
            return null;
        }
        public String getState() {
            if(state != null)
                return state.name();
            return null;
        }
        public Boolean getIsChanged() {
            return (this.startPurchaseOrderId != this.serviceEntrySheet.Purchase_Order__c) ||
                (this.startTicketNumber != this.serviceEntrySheet.Ticket_Number__c) ||
                (this.startAmount != this.serviceEntrySheet.Amount__c) ||
                (this.startEstimateToFinish != (estimateToFinish == null ? null : this.estimateToFinish)) ||
                (this.startDate != this.serviceEntrySheet.Date_Entered__c) ||
                (this.startLineItemId != this.serviceEntrySheet.Line_Item__c) ||
                (this.startDescription != this.serviceEntrySheet.Description__c) ||
                (this.state == ServiceEntrySheetState.CREATED);
        }
        
        public void setEstimateToFinish() {
            
            if (purchaseOrderMap.containskey(serviceEntrySheet.Purchase_Order__c)){
                
                estimateToFinish = purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c) == null ? null : String.valueof(purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c).Estimate_To_Complete__c);
                serviceEntrySheet.Purchase_Order__r.Estimate_To_Complete__c = (estimateToFinish == null ? null : Decimal.valueof(estimateToFinish));
            }
        }
        
        public void setNewEstimateToFinish() {
            
                if (serviceEntrySheet.Purchase_Order__c != null){
                    serviceEntrySheet.Purchase_Order__r.Estimate_To_Complete__c = ((estimateToFinish == null || !SPCC_Utilities.isNumeric(estimateToFinish)) ? null : Decimal.valueof(estimateToFinish));
                    
                    if (purchaseOrderMap.containskey(serviceEntrySheet.Purchase_Order__c)){
                        purchaseOrderMap.get(serviceEntrySheet.Purchase_Order__c).Estimate_To_Complete__c = ((estimateToFinish == null || !SPCC_Utilities.isNumeric(estimateToFinish))  ? null : Decimal.valueof(estimateToFinish));
                    }
                }
        }

        public void setLineItem() {
            if (serviceEntrySheet.Purchase_Order__c != null && !lineItemMap.isEmpty()){
                String lineItemNumber = poVendorLineItemUniqueKey.substringAfter('---').left(5).trim();
                System.debug('poId : ' + poVendorLineItemUniqueKey + ' lineItemNumber: ' + lineItemNumber);
                for (SPCC_Line_Item__c li : lineItemMap.values()){
                    if (li.Purchase_Order__c != null && li.Purchase_Order__r.Name == serviceEntrySheet.Purchase_Order__r.Name 
                        && li.Item__c == lineItemNumber){   
                        serviceEntrySheet.Line_Item__c = li.Id;
                        serviceEntrySheet.Line_Item__r = li;
                        liCostRate = li.Incurred_Cost_Rate__c;
                    }
                }
            }
            
            else {
                serviceEntrySheet.Line_Item__c = null;
                serviceEntrySheet.Line_Item__r = null;
                liCostRate = null;
            }
        }


        /***********************
        * Comparable Interface *
        ***********************/
        public Integer compareTo(Object obj) {
            ServiceEntrySheetWrapper ses = (ServiceEntrySheetWrapper) obj;

            if(SORT_COLUMN == SORT_BY_PURCHASEORDER_NUMBER) return this.sortByPurchaseOrderNumber(ses);
            if(SORT_COLUMN == SORT_BY_VENDOR) return this.sortByVendorName(ses);
            if(SORT_COLUMN == SORT_BY_AFE_NUMBER) return this.sortByAfeNumber(ses);
            if(SORT_COLUMN == SORT_BY_GL_NUMBER) return this.sortByGlNumber(ses);
            if(SORT_COLUMN == SORT_BY_TICKET_NUMBER) return this.sortByTicketNumber(ses);
            if(SORT_COLUMN == SORT_BY_AMOUNT) return this.sortByAmount(ses);
            if(SORT_COLUMN == SORT_BY_ESTIMATE_TO_FINISH) return this.sortByEstimateToFinish(ses);
            if(SORT_COLUMN == SORT_BY_DATE) return this.sortByDate(ses);
            if(SORT_COLUMN == SORT_BY_PO_OVERUNDER) return this.sortByPOOverUnder(ses);
            if(SORT_COLUMN == SORT_BY_LINE_ITEM) return this.sortByLineItem(ses);
            if(SORT_COLUMN == SORT_BY_INCURRED_COST_RATE) return this.sortByIncurredCostRate(ses);
            return 0; //preserve existing order
        }

        /****************************
        * Private Utility Functions *
        ****************************/
        private Integer sortByPurchaseOrderNumber(ServiceEntrySheetWrapper compareTo) {
            String currentPurchaseOrderNumber = String.isBlank(this.serviceEntrySheet.Purchase_Order__r.Name) ?
                purchaseOrderMap.get(this.serviceEntrySheet.Purchase_Order__c).Name :
                this.serviceEntrySheet.Purchase_Order__r.Name;
            String compareToPurchaseOrderNumber = String.isBlank(compareTo.serviceEntrySheet.Purchase_Order__r.Name) ?
                purchaseOrderMap.get(compareTo.serviceEntrySheet.Purchase_Order__c).Name :
                compareTo.serviceEntrySheet.Purchase_Order__r.Name;
            if(currentPurchaseOrderNumber > compareToPurchaseOrderNumber) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if(currentPurchaseOrderNumber < compareToPurchaseOrderNumber) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0; //preserve order
        }

        private Integer sortByVendorName(ServiceEntrySheetWrapper compareTo) {
            System.debug('compareTo.serviceEntrySheet.Purchase_Order__c: ' + compareTo.serviceEntrySheet.Purchase_Order__c);
            System.debug('purchaseOrderMap.get(compareTo.serviceEntrySheet.Purchase_Order__c): ' +
                purchaseOrderMap.get(compareTo.serviceEntrySheet.Purchase_Order__c));
            String currentVendorName = String.isBlank(this.serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name) ?
                purchaseOrderMap.get(this.serviceEntrySheet.Purchase_Order__c).Vendor_Account__r.Name :
                this.serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name;
            String compareToVendorName = String.isBlank(compareTo.serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name) ?
                purchaseOrderMap.get(compareTo.serviceEntrySheet.Purchase_Order__c).Vendor_Account__r.Name :
                compareTo.serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name;
            if(currentVendorName > compareToVendorName) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if(currentVendorName < compareToVendorName) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByLineItem(ServiceEntrySheetWrapper compareTo) {
            Integer currentLineItem = Integer.valueOf(this.serviceEntrySheet.Line_Item__r.Item__c);
            Integer compareToLineItem = Integer.valueOf(compareTo.serviceEntrySheet.Line_Item__r.Item__c);
            if(currentLineItem > compareToLineItem) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if(currentLineItem < compareToLineItem) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByTicketNumber(ServiceEntrySheetWrapper compareTo) {
            if(this.serviceEntrySheet.Ticket_Number__c > compareTo.serviceEntrySheet.Ticket_Number__c) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (this.serviceEntrySheet.Ticket_Number__c < compareTo.serviceEntrySheet.Ticket_Number__c) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByAfeNumber(ServiceEntrySheetWrapper compareTo) {
            if(this.serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__c > compareTo.serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__c) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (this.serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__c < compareTo.serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__c) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByGlNumber(ServiceEntrySheetWrapper compareTo) {
            if(this.serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c > compareTo.serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (this.serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c < compareTo.serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByAmount(ServiceEntrySheetWrapper compareTo) {
            if(this.serviceEntrySheet.Amount__c > compareTo.serviceEntrySheet.Amount__c) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (this.serviceEntrySheet.Amount__c < compareTo.serviceEntrySheet.Amount__c) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByEstimateToFinish(ServiceEntrySheetWrapper compareTo) {
            Integer currentEstimateToFinish = Integer.valueOf(this.serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
            Integer compareToEstimateToFinish = Integer.valueOf(compareTo.serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
            if (currentEstimateToFinish == null && compareToEstimateToFinish == null) return 0;
            if (compareToEstimateToFinish == null || currentEstimateToFinish > compareToEstimateToFinish) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (currentEstimateToFinish == null || currentEstimateToFinish < compareToEstimateToFinish) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByDate(ServiceEntrySheetWrapper compareTo) {
            if(this.serviceEntrySheet.Date_Entered__c > compareTo.serviceEntrySheet.Date_Entered__c) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (this.serviceEntrySheet.Date_Entered__c < compareTo.serviceEntrySheet.Date_Entered__c) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByPOOverUnder(ServiceEntrySheetWrapper compareTo) {
            Integer currentPOOverUnder = Integer.valueOf(this.serviceEntrySheet.Purchase_Order__r.PO_Over_Under__c);
            Integer compareToPOOverUnder = Integer.valueOf(compareTo.serviceEntrySheet.Purchase_Order__r.PO_Over_Under__c);
            if(currentPOOverUnder > compareToPOOverUnder) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if (currentPOOverUnder < compareToPOOverUnder) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }

        private Integer sortByIncurredCostRate(ServiceEntrySheetWrapper compareTo) {
            Integer currentIncurredCostRate = Integer.valueOf(this.serviceEntrySheet.Line_Item__r.Incurred_Cost_Rate__c);
            Integer compareToIncurredCostRate = Integer.valueOf(compareTo.serviceEntrySheet.Line_Item__r.Incurred_Cost_Rate__c);
            if(currentIncurredCostRate > compareToIncurredCostRate) return (SORT_ORDER == SORT_ASCENDING) ? 1 : -1;
            if(currentIncurredCostRate < compareToIncurredCostRate) return (SORT_ORDER == SORT_ASCENDING) ? -1 : 1;
            return 0;
        }
    }

    public class Notification {
        private NotificationMode mode;
        public String message {get; set;}
        public Boolean show {get; set;}

        public Notification() {
            this.mode = NotificationMode.SUCCESS;
            this.message = '';
            this.show = false;
        }

        public String getMode() {
            if(mode != null)
                return mode.Name();
            return null;
        }
    }

    public class DescriptionPopup {
        public ServiceEntrySheetWrapper ses {get; private set;}
        public String oldDescription {get; private set;}
        public Boolean show {get; private set;}

        public DescriptionPopup() {
            this.ses = null;
            this.show = false;
            this.oldDescription = null;
        }
    }

    public enum ServiceEntrySheetMode {
        VIEW,
        EDIT
    }

    public enum ServiceEntrySheetState {
        CREATED,
        EXISTING
    }

    public enum NotificationMode {
        SUCCESS,
        WARNING,
        ERROR
    }
}