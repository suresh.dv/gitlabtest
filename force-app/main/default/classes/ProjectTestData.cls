public class ProjectTestData {
    
    public static User seniorVP {get; set;}
    public static User VP {get; set;}
    public static User projectManager {get; set;}
    
    public static Project__c createProject(String Name) {
        seniorVP = UserTestData.createSunriseTestUser(1)[0]; 
        
        VP = UserTestData.createSunriseTestUser(1)[0];
        
        projectManager = UserTestData.createSunriseTestUser(1)[0];
        Project__c project = new Project__c();
        project.Name = Name;
        project.Senior_VP__c = seniorVP.Id;
        project.VP__c = VP.Id;
        project.Project_Manager__c = projectManager.Id;
        
        Program__c program = new Program__c();
        program.Name = 'Sunrise';
        insert program;
        
        //project.Program__c = program.Id;
        
        return project;
    }
}