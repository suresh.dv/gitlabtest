global virtual with sharing class DSP_CMS_ContactController extends cms.ContentTemplateController{
    global DSP_CMS_ContactController(cms.CreateContentController cc){
        super(cc);
    }
    
    global DSP_CMS_ContactController(cms.GenerateContent cc){
        super(cc);
    }

    global DSP_CMS_ContactController(){}

    global virtual override String getHTML(){return '';}

    public String anchor {
        get{
            return getProperty('anchor');
        }
        set;
    }
    
    public String name {
        get{
            return String.isNotBlank(getProperty('name')) ?  getProperty('name') : '';
        }
        set;
    }
    
    public String description {
        get{
            return String.isNotBlank(getProperty('description')) ?  getProperty('description') : '';
        }
        set;
    }
    
    public String users {
        get{
            return getProperty('users');
        }
        set;
    }
    
    public String responsibleFor {
        get{
            return getProperty('responsibleFor');
        }
        set;
    }
    
    public class UsersJSON
    {
        public UsersJSON()
        {
            users = new List<UserRecord>();
        }
        
        public List<UserRecord> users;
    }
    
    public class UserRecord
    {
        public String name;
        public String role;
        public String email;
        public String phone;
    }
    
    
    public class ResponsibleForJSON
    {
        public ResponsibleForJSON()
        {
            responsibleFors = new List<String>();
        }
        
        public List<String> responsibleFors;
    }
    
    public String contactHTML(){
        String html='';
        
        html += '<div class="col s12 m6 l6">';
        html +=     '<div class="col s12 m12 l12">';
        html +=         '<a name="'+anchor+'"></a><h5 class="title-smallcaps">'+name+'</h5>';
        html +=     '</div>';
        html +=     '<div class="col s12 m12 l12">';
        html +=         '<p>'+description+'</p>';
        html +=     '</div>';
        
        try
        {
            UsersJSON userList = new UsersJSON();
            if(users != null)
            {
                userList = (UsersJSON)JSON.deserialize(users,UsersJSON.class);
            
                for(UserRecord u : userList.users)
                {
                    html += '<div class="col s12 m12 l12">';
                    html +=     '<p><div>'+u.name+'</div>';
                    html +=     '<div>'+u.role+'</div>';
                    html +=     '<div><a href="mailto:'+u.email+'">'+u.email+'</a></div>';
                    html +=     '<div><a href="tel:+13068251664">+1-306-825-1664</a></div></p>';
                    html += '</div>';
                }
            }
        }
        catch(Exception e)
        {
            
        }
        
        try
        {
            html +=     '<div class="col s12 m12 l12">';
            html +=         '<h6>RESPONSIBLE FOR ...</h6>';
            html +=     '</div>';
            html +=     '<div class="col s12 m12 l6">';
            html +=         '<ul>';
            
            ResponsibleForJSON responsibleForList = new ResponsibleForJSON();
            if(responsibleFor != null)
            {
                responsibleForList = (ResponsibleForJSON)JSON.deserialize(responsibleFor,ResponsibleForJSON.class);
            
                for(String r : responsibleForList.responsibleFors)
                {
                    html +=         '<li>'+r+'</li>';
                }
            }
            html +=         '</ul>';
            html +=     '</div>';
        }
        catch(Exception e)
        {
            
        }

        html += '</div>';
        
        return html;
    }
}