@isTest
private class FM_StuckTruckTest {
    static string BusinessUnitID;
    static string BusinessDepartmentID;
    static string OperatingDistrictID;
    static string FieldID; 
    static string Routeid;
    static string LocationId;
    static Carrier__c oCarrier;
    static Carrier_Unit__c oCarrierUnit;
    static Towing_Company__c oTowingCompany;

    static Facility__c oFacility;
    //static string sTestSelectedWellList;
    //static String filterLoadRequests;
    static List<FM_Load_Request__c> loadRequests;
    static List<FM_Truck_Trip__c> truckTrips;

	static testmethod void testStuckTruck() {

        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        PermissionSet ps = [select id, name from PermissionSet where name = 'HOG_FM_Dispatcher' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = runningUser.id,PermissionSetId = ps.id);
        insert psa;

        ps = [select id, name from PermissionSet where name = 'HOG_Field_Operator' limit 1];
        psa = new PermissionSetAssignment(AssigneeId = runningUser.id,PermissionSetId = ps.id);
        insert psa;   

        Map<String, FM_Truck_Trip__c> truckTripsMap = new Map<String, FM_Truck_Trip__c>();

        System.runAs(runningUser)
        {

            setup();

            // load all saved TruckTrips
            List<FM_Truck_Trip__c> allTruckTrips = [SELECT Id, Name, Location_Lookup__c, Carrier__c, Unit__c, Truck_Trip_Status__c FROM FM_Truck_Trip__c];
            

			// creating and testing StuckTruck for ordinary TruckTrip
			FM_Truck_Trip__c truckTripOne = allTruckTrips.get(0);
			FM_Stuck_Truck__c stuckTruck1 = new FM_Stuck_Truck__c(
					Truck_Trip__c = truckTripOne.Id,
					Stuck_From__c = System.now().addDays(-7),
					Location__c = truckTripOne.Location_Lookup__c,
					Carrier__c = truckTripOne.Carrier__c,
					Carrier_Unit__c = truckTripOne.Unit__c,
					Towing_Company_Lookup__c = oTowingCompany.Id,
					Status__c = FM_Utilities.STUCK_TRUCK_STATUS_STUCK
				);
			insert stuckTruck1;
			
			FM_Stuck_Truck__c testStuckTruck1 = [SELECT Id, Truck_Trip__r.Name, Stuck_From__c, Location__r.Name, Carrier__r.Carrier_Name_For_Fluid__c, Carrier_Unit__r.Unit_Email__c, Status__c FROM FM_Stuck_Truck__c][0];
			System.assertEquals('Stuck', testStuckTruck1.Status__c);

			stuckTruck1.Stuck_To__c = System.now().addDays(-1);
			update stuckTruck1;
			
			testStuckTruck1 = [SELECT Id, Truck_Trip__r.Name, Stuck_From__c, Location__r.Name, Carrier__r.Carrier_Name_For_Fluid__c, Carrier_Unit__r.Unit_Email__c, Status__c FROM FM_Stuck_Truck__c LIMIT 1][0];										
			System.assertEquals('Unstuck', testStuckTruck1.Status__c);

			// creating and testing StuckTruck for AddOn TruckTrip
			FM_Truck_Trip__c truckTripTwo = allTruckTrips.get(1);
			FM_Stuck_Truck__c stuckTruck2 = new FM_Stuck_Truck__c(
					Truck_Trip__c = truckTripTwo.Id,
					Stuck_From__c = System.now().addDays(-7),
					Location__c = truckTripTwo.Location_Lookup__c,
					Carrier__c = truckTripTwo.Carrier__c,
					Carrier_Unit__c = truckTripTwo.Unit__c,
					Towing_Company_Lookup__c = oTowingCompany.Id,
					Status__c = FM_Utilities.STUCK_TRUCK_STATUS_STUCK
				);
			insert stuckTruck2;
			
			FM_Stuck_Truck__c testStuckTruck2 = [SELECT Id, Truck_Trip__r.Name, Stuck_From__c, Location__r.Name, Carrier__r.Carrier_Name_For_Fluid__c, Carrier_Unit__r.Unit_Email__c, Status__c FROM FM_Stuck_Truck__c][1];
			System.assertEquals('Stuck', testStuckTruck2.Status__c);

			stuckTruck2.Stuck_To__c = System.now().addDays(-1);
			update stuckTruck2;
			
			testStuckTruck2 = [SELECT Id, Truck_Trip__r.Name, Stuck_From__c, Location__r.Name, Carrier__r.Carrier_Name_For_Fluid__c, Carrier_Unit__r.Unit_Email__c, Status__c FROM FM_Stuck_Truck__c LIMIT 1][0];										
			System.assertEquals('Unstuck', testStuckTruck2.Status__c);

		}
	}
	
	public static void setup()
	{
			//create test BU
			Business_Unit__c  testbu = FM_RunSheetTestData.createBusinessUnit('Test BU',  true);
			BusinessUnitID = testbu.id;

			//create test BD            
			Business_Department__c testbd = FM_RunSheetTestData.createBusinessDepartment('Test BD',  true);
			BusinessDepartmentID = testbd.id;
			//create test OD
			Operating_District__c testOD = FM_RunSheetTestData.createOperatingDistrict('Test OD', testbd.Id, testbu.Id,  true) ;
			OperatingDistrictID = testOD.id; 
			//create test field
			Field__c testField = FM_RunSheetTestData.createField('Test Field', testOD.Id, true) ;
			FieldID = testField.id;
			//create test route
			Route__c testRoute = FM_RunSheetTestData.createRoute('999', true);

			//create test well
			Location__c location = FM_RunSheetTestData.createLocation('Test Well 1', testField.id, testRoute.id , false, true);
			//we need location with category = 4
			location.Functional_Location_Category__c =4;
			location.Well_Type__c = 'Oil';
			//and setup unit configuration
			location.Unit_Configuration__c = 'T5X';
			location.Status__c = 'PROD';
			upsert location;

			//and lets create well without category 4
			Location__c location1 = FM_RunSheetTestData.createLocation('Test Well 2', testField.id, testRoute.id , false, true);
			
			//save route and well id in static variables
			Routeid = testRoute.id;
			Locationid = location.id;

			// Test Account
			Account oAccount = new Account();
			oAccount.Name = 'Carrier Account';
			insert oAccount;

			// Test Carrier
			oCarrier = new Carrier__c();
			oCarrier.Carrier__c = oAccount.Id;
			oCarrier.Carrier_Name_For_Fluid__c = 'Test Carrier';
			insert oCarrier;

			// Set Test Carrier as Default Carrier for test Route
			testRoute.Default_Carrier__c = oCarrier.Id;
			update testRoute;

			// Test Carrier Unit
			oCarrierUnit = new Carrier_Unit__c();
			oCarrierUnit.Carrier__c = oCarrier.Id;
			oCarrierUnit.Unit_Email__c = 'testing@testing.com';
			insert oCarrierUnit;

			// Test Towing Company
			oTowingCompany = new Towing_Company__c(Name = 'Magnetic Towing Inc.');
			insert oTowingCompany;

			// Test LoadRequest
			List<FM_Load_Request__c> lrs = new List<FM_Load_Request__c>();
			lrs.add(new FM_Load_Request__c( Name = 'Load Request # 1', 
											Product__c = 'O', 
											Shift__c = 'Night', 
											Carrier__c = oCarrier.Id,
											Source_Location__c = location.Id));
			lrs.add(new FM_Load_Request__c( Name = 'Load Request # 2', 
											Product__c = 'W', 
											Shift__c = 'Night', 
											Carrier__c = oCarrier.Id,
											Source_Location__c = location.Id));

			insert lrs;

			// Test TruckTrips
			List<FM_Truck_Trip__c> truckTrips = new List<FM_Truck_Trip__c>();
			Integer i = 1;
			for (FM_Load_Request__c lr : lrs) {
				truckTrips.add(new FM_Truck_Trip__c(Load_Request__c	= lr.id,
					Location_Lookup__c = location.Id,
					Product__c = lr.Product__c,
					Shift__c = lr.Shift__c,
					Shift_Day__c = 'Today',
					Carrier__c = lr.Carrier__c,
					Axle__c = '5',
			  		Run_Sheet_Date__c = Date.today(),
			  		// booking info
			  		Unit__c = oCarrierUnit.Id,
			  		// dipsatch info
			  		Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED,
			  		// first truck trip is ordinary truckTrip, second one is AddOn
			  		Add_On__c = (i == 1) ? false : true,
			  		TL_Dispatched_On__c = System.now()
			  		));
				i++;
			}
			insert truckTrips;
			System.debug('TTT: ' + truckTrips);
	}
	
}