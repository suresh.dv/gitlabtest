global class USCP_DataCleanup implements Database.Batchable<SObject>, Database.Stateful {

    global Integer numDaysOfDataToKeep;
    global Date cutoffDate ;
    global String query;
    global String ObjectType;
    global Database.Batchable<SObject> nextStepProcess;
   
    global USCP_DataCleanup(String pObjectType, Database.Batchable<SObject> chainBatchprocess)
    {
        this(pObjectType);
        this.nextStepProcess = chainBatchprocess;
    }

    global USCP_DataCleanup(String pObjectType)
    {
        this.ObjectType = pObjectType;
        USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
        numDaysOfDataToKeep = integer.valueOf(mc.Delete_Data_After_N_Days__c);  
        cutoffDate = System.today() - numDaysOfDataToKeep ;
        
        if(pObjectType== 'EFT') 
        {
            query='SELECT Id FROM USCP_EFT__c WHERE draft_date__c < :cutoffDate';
        }
        if(pObjectType== 'BOL') 
        {
            query='SELECT Id FROM USCP_BOL_AccTransaction__c where Movement_Date__c < :cutoffDate';
        } 
        if(pObjectType== 'INV') 
        {
            query='SELECT Id FROM USCP_Invoice__c where invoice_date__c < :cutoffDate';
        }                
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('USCP ' + ObjectType + ' Record Clean Up Started');
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
      /*  
      List<USCP_EFT__c> reclist = new List<USCP_EFT__c>();
      for (SObject s : scope)
      {
        USCP_EFT__c rec = (USCP_EFT__c)s;
        reclist.add(rec);
      }
      delete reclist;
      */
      /* 
      List<USCP_EFT__c> reclist = new List<USCP_EFT__c>();
      for (SObject s : scope)
      {
        USCP_EFT__c rec = new USCP_EFT__c(Id=s.Id);
        reclist.add(rec);
      }
      delete reclist;
      */
      System.debug('USCP ' + ObjectType + ' Records deleting...' + scope.size());
      delete scope;
      Database.emptyRecycleBin(scope);
    }
    
    global void finish(Database.BatchableContext BC)
    {  
         
       AsyncApexJob a = 
           [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
       /*                   
       // Send an email to the Apex job's submitter 
       //   notifying of job completion. 
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('USCP ' + ObjectType + ' Record Clean Up Status: ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
       */ 
       System.debug('USCP ' + ObjectType + ' Record Clean Up Status: ' + a.Status);
       System.debug('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       
       if(this.nextStepProcess!= null)
       {
           System.debug('Starting next chain batch process');
           Database.executeBatch(this.nextStepProcess); 
       }
       
    }
}