@isTest
public class HOG_PVRAverageVolumeTest 
{
	static testMethod void verifyPVRAvgFields()
	{
		List<Location__c> locationList = new List<Location__c>();
		
		//create business department
		Business_Department__c busDept = BusinessDepartmentTestData.createBusinessDepartment('Test Bus Dept 1');
		insert busDept;
		
		//create operating district
		Operating_District__c  opDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Op District 1', busDept.Id, null);
		insert opDistrict; 

		//create operating field
		String opFieldRtId = Schema.SObjectType.Field__c.getRecordTypeInfosByName().get('HOG - Primary Oil Record').getRecordTypeId();
		Field__c  opField = FieldTestData.createField('Test Op Field 1', opDistrict.Id, opFieldRtId);
		insert opField;
		
		//create operator route
		Route__c  opRoute = RouteTestData.createRoute('Test100');
		insert opRoute;
		
		//create well Id 1
		String wellIdRtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Well ID').getRecordTypeId();
		Location__c wellId1 = LocationTestData.createLocation('Test Well ID 1', opRoute.Id, opField.Id);
		wellId1.RecordTypeId = wellIdRtId;
		wellId1 = (Location__c) fillPVRFields(wellId1, 0);
				
		locationList.add(wellId1);
		insert locationList;
		
		//create well event records
		String wellEventRtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Well Event').getRecordTypeId();
		
		//create event 1
		Well_Event__c event1 = WellEventTestData.createEvent('Test Well Event 1', wellId1.Id);
		event1 = (Well_Event__c) fillPVRFields(event1, 10);
		system.debug('event 1 values '+event1);
		event1.Status__c = 'PROD';
		update event1;
		
		//create event 2
		Well_Event__c event2 = WellEventTestData.createEvent('Test Well Event 2', wellId1.Id);
		event2 = (Well_Event__c) fillPVRFields(event2, 10);		
		system.debug('event 2 values '+event2);
		event1.Status__c = 'PROD';
		update event1;
		
		Test.startTest();
		//call the visualforce controller to set PVR Average fields at Well Id level
		PageReference pvrPage = Page.HOG_PVRAverageVolume;
		Test.setCurrentPage(pvrPage); 		
		ApexPages.currentPage().getParameters().put('id',wellId1.Id);
		
		ApexPages.StandardController sc = new ApexPages.standardController(wellId1);
		HOG_PVRAverageVolumeController pvrAvgProd = new HOG_PVRAverageVolumeController(sc);
		pvrAvgProd.calculatePVRAverageVolume();
		
		//query updated well ID record
		List<Location__c> wellIdList = [Select Id, Name, RecordTypeId, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c, 
	                                          PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c, 
	                                          PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,
	                                          PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,
	                                          PVR_AVGVOL_5D_PROD__c, PVR_AVGVOL_30D_PROD__c
	                                          From Location__c
	                                          Where Id =: wellId1.Id];
	                                          
		if(wellIdList.size() > 0)
		{
			//assert if the values are populated correctly
			system.assertEquals(wellIdList[0].PVR_AVGVOL_5D_OIL__c, event1.PVR_AVGVOL_5D_OIL__c);
			system.assertEquals(wellIdList[0].PVR_AVGVOL_30D_OIL__c, event1.PVR_AVGVOL_30D_OIL__c);
		}	                                          
		
		Test.stopTest();		
		
	}
	
	static SObject fillPVRFields(SObject record, Integer value)
	{
		record.put('PVR_AVGVOL_30D_COND__c', value);
		record.put('PVR_AVGVOL_30D_GAS__c', value);
		record.put('PVR_AVGVOL_30D_OIL__c', value);
		record.put('PVR_AVGVOL_30D_SAND__c', value); 
		record.put('PVR_AVGVOL_30D_WATER__c', value);
		record.put('PVR_AVGVOL_5D_GAS__c', value);
		record.put('PVR_AVGVOL_5D_OIL__c', value);
		record.put('PVR_AVGVOL_5D_COND__c', value);
		record.put('PVR_AVGVOL_5D_SAND__c', value);
		record.put('PVR_AVGVOL_5D_WATER__c', value);
		return record;
	}
}