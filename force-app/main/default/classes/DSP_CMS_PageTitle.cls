global virtual with sharing class DSP_CMS_PageTitle extends DSP_CMS_PageTitleController
{
    global override String getHTML()
    {
        return titleHTML();
    }
}