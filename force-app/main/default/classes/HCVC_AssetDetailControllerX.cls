/*
* Author: Kishore Chandolu
* Name: HCVC_AssetDetailControllerX
* Purpose: This Class action related to the Pages HCVC_AssetDetails and HCVC_RacksDetails.
* Created Date: 24th July 2014
*/

public class HCVC_AssetDetailControllerX{



   public Asset__c asset{get;set;}
   public Asset__c rackasset{get;set;}
   public Boolean EditFlag{get;set;}
   public Boolean isasset{get;set;}
   public String assetId{get;set;}
   public String assetHId{get;set;}
   public String assetHtype{get;set;}
   public ID userId{get;set;}
   public String baseUrl{get;set;}
   public String assettype{get;set;}
   public List<Asset__c> assetHeirarchyRecs{get;set;}
   public List<Asset__c> assetRefineryHeirarchy{get;set;}
   public List<Asset__c> assetRUHeirarchy{get;set;}
   public List<Asset__c> assetTankHeirarchy{get;set;}
   public List<Asset__c> assetRackHeirarchy{get;set;}
   public String tbcolor{get{if(null==tbcolor)
                         {HCVC_Customsettings__c tc = HCVC_Customsettings__c.getInstance();
                         tbcolor = tc.HCVC_tabId__c;}return tbcolor;}set;}
   public static Id crudeRecordTypeId{get{if(null==crudeRecordTypeId) crudeRecordTypeId=HCVC_Utility.crudeRecordTypeId; return crudeRecordTypeId;}set;} 
   public static Id overviewRecordTypeId{get{if(null==overviewRecordTypeId) overviewRecordTypeId=HCVC_Utility.overviewRecordTypeId; return overviewRecordTypeId;}set;} 
   public static Id refineryTypeId{get{if(null==refineryTypeId) refineryTypeId=HCVC_Utility.refineryTypeId; return refineryTypeId;}set;} 
   public static Id refineryUnitTypeId{get{if(null==refineryUnitTypeId) refineryUnitTypeId=HCVC_Utility.refineryUnitTypeId; return refineryUnitTypeId;}set;} 
   public static Id refineryRacksTypeId{get{if(null==refineryRacksTypeId) refineryRacksTypeId=HCVC_Utility.refineryRacksTypeId; return refineryRacksTypeId;}set;} 
   public static Id tankTypeId{get{if(null==tankTypeId) tankTypeId=HCVC_Utility.tankTypeId; return tankTypeId;}set;}
   public static Id geojsonTypeId{get{if(null==geojsonTypeId) geojsonTypeId=HCVC_Utility.geojsonTypeId; return geojsonTypeId;}set;}  
   public Map<ID, String> recordTypeNames{get;set;}


     /**
      * This Constructor load perticular ASSET Record using assetId and loads all assets using Parent RefineryID.
      * Sort all assets by Asset types(Refinery,Refinery Unit,Tank,Rack).
      */

      public HCVC_AssetDetailControllerX(ApexPages.StandardController controller) {
           Boolean aflag;
           userId=UserInfo.getUserId();
           assetId=Apexpages.currentPage().getParameters().get('id');

           Asset__c assettypedata=[SELECT Id,Name,RecordTypeId FROM Asset__c WHERE Id=:assetId LIMIT 1];
           System.debug('assetId  is  '+assetId);
           baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
           
           // Set up the map for recordTypeNames map
           recordTypeNames = new Map<ID, String>();
           recordTypeNames.put(crudeRecordTypeId, 'Crude Pipeline');
           recordTypeNames.put(overviewRecordTypeId, 'Overview');
           recordTypeNames.put(refineryTypeId, 'Refinery');
           recordTypeNames.put(refineryUnitTypeId, 'Refinery Unit');
           recordTypeNames.put(refineryRacksTypeId, 'Refinery Racks');
           recordTypeNames.put(tankTypeId, 'Tank');
           recordTypeNames.put(geojsonTypeId, 'Geojson');
           
           try{
                if(String.isNotEmpty(HCVC_Utility.communitySiteUrl)){ 
                 baseUrl=baseUrl+HCVC_Utility.communitySiteUrl;
               }
    
           }catch(Exception e){
                System.debug('Exception is '+e);
           }

           try{
           aflag=Boolean.valueOf(Apexpages.currentPage().getParameters().get('aflag'));
           }catch(Exception e){
             aflag=false;
           }

           try{
             assettype=Apexpages.currentPage().getParameters().get('assettype');
             }catch(Exception e){
             }

           if(null==assettype){
             assettype=assettypedata.RecordTypeId;
           }

           System.debug('Inside catch assettype is  '+assettypedata.RecordTypeId);
           System.debug('assettype is  '+assettype);

           if(assettype==tankTypeId){
           isasset=false;
           }else{
             isasset=true;
           }

           System.debug('assetId initialy is  '+assetId);

           asset=[SELECT Id,Name,Parent_Refinery_Asset__c,Parent_Refinery_Asset__r.Name,
                      Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Id,
                      Operating_Conditions_Process__c,Process_Description__c,
                      Product_Details__c,Unit_FeedStocks__c,Unit_History__c,
                      Catalyst_and_Chemical_Usage__c,Address__c,Blend_indices__c,
                      Blending_Flow_Diagram__c,Blend_specifications__c, Geojson_Name__c, AssetType__c,
                      Future_outlook__c,Rack_Layout_Operations__c,Image__c,Pipeline_Network_Image__c,
                      Rail_car_loading_operations__c,Splash_Inline_blending__c, Overview__c,
                      Start_up_Year__c,Name_Plate_Capacity__c,LTRS_INCH__c,PFD_small_image__c,
                      Tank_Number__c,LTRS_FOOT__c,BBLS_INCH__c,BBLS_FOOT__c,
                      HEEL_BBLS__c,HEEL_LTRS__c,Product__c,Sales_Tanks__c,Dimensions__c,
                      MAXIMUM_FILL_FEET__c,MAX_VOLUME_BBL__c,MAX_VOLUME_M3__c,Notes__c, BC_Light_Crude__c, Crude_Supply__c, Boundary_Lake_Crude__c, 
                      Refinery_Crude_Figures__c, Refinery_Crude_Receipts__c
                      FROM Asset__c
                      where Id=:assetid  limit 1];

           assettype=asset.RecordTypeId;

           assetHeirarchyRecs=[SELECT Id,Name,RecordTypeId,Tank_Number__c,order__c,Details__c,Parent_Refinery_Asset__c,Sales_Tanks__c FROM Asset__c
                                WHERE Id=:asset.Parent_Refinery_Asset__c OR
                                Parent_Refinery_Asset__c=:asset.Parent_Refinery_Asset__c OR
                                Parent_Refinery_Asset__c=:asset.Id
                                ORDER BY RecordTypeId,order__c];
                                System.debug(assetHeirarchyRecs);

          assetRefineryHeirarchy=new List<Asset__c>();
          assetRUHeirarchy=new List<Asset__c>();
          assetTankHeirarchy=new List<Asset__c>();
          assetRackHeirarchy=new List<Asset__c>();

                      if(null!=assetHeirarchyRecs){

                      for(Asset__c assetTemp:assetHeirarchyRecs){
                        if(assetTemp.RecordTypeId==refineryTypeId){
                         // Only add the current refinery to this list if this is a top level heiracrhy view
						 if(assetTemp.Name == asset.Name && asset.RecordTypeId == refineryTypeId) {
                         	assetRefineryHeirarchy.add(assetTemp);
						 } else if(asset.RecordTypeId != refineryTypeId){
						 	assetRefineryHeirarchy.add(assetTemp);
						 }

                        }else if(assetTemp.RecordTypeId==refineryUnitTypeId){

                           assetRUHeirarchy.add(assetTemp);

                        }else if(assetTemp.RecordTypeId==tankTypeId){

                          assetTankHeirarchy.add(assetTemp);

                        }else if(assetTemp.RecordTypeId==refineryRacksTypeId){

                          assetRackHeirarchy.add(assetTemp);

                        }
                      }

          }

            System.debug('asset before rackasset is  '+assettype);

            Asset__c assettemp=[SELECT Id,Name,Parent_Refinery_Asset__c from Asset__c where Id=:assetid limit 1];
            try{
                rackasset=[SELECT Id,Name,Parent_Refinery_Asset__c,Parent_Refinery_Asset__r.Name,
                      Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Id,
                      Operating_Conditions_Process__c,Process_Description__c,
                      Product_Details__c,Unit_FeedStocks__c,Unit_History__c,
                      Catalyst_and_Chemical_Usage__c,Address__c,Blend_indices__c,
                      Blending_Flow_Diagram__c,Blend_specifications__c,PFD_small_image__c,Pipeline_Network_Image__c,
                      Future_outlook__c,Rack_Layout_Operations__c,Image__c,
                      Rail_car_loading_operations__c,Splash_Inline_blending__c,
                      Start_up_Year__c,Name_Plate_Capacity__c,LTRS_INCH__c,
                      Tank_Number__c,LTRS_FOOT__c,BBLS_INCH__c,BBLS_FOOT__c,
                      HEEL_BBLS__c,HEEL_LTRS__c,Product__c,Sales_Tanks__c,Dimensions__c,
                      MAXIMUM_FILL_FEET__c,MAX_VOLUME_BBL__c,MAX_VOLUME_M3__c,Notes__c FROM Asset__c
                      where Parent_Refinery_Asset__c=:assettemp.Parent_Refinery_Asset__c and RecordTypeId=:refineryRacksTypeId order by Id desc limit 1];

                      }catch(Exception e){
                         System.debug('Exception    '+e);
                      }

               if(null==rackasset){
                rackasset=new Asset__c();
               }

              if(null!=aflag){
                 EditFlag=aflag;
              }else{
              EditFlag=false;
              }

              System.debug('asset after rackassset  is '+asset);

      }


     /**
      * This method sets the Edit flag to true and returns to HCVC_AssetDetails.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

       public PageReference setEditFlag(){


           PageReference reference=new PageReference(baseUrl+'/apex/HCVC_AssetDetails?id='+assetId+'&aflag=true');

            reference.setRedirect(true);
            return reference;

       }


     /**
      * This method update Asset after Edit and returns to HCVC_AssetDetails.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference updateAsset(){

      update asset;

      PageReference reference=new PageReference(baseUrl+'/apex/HCVC_AssetDetails?id='+assetId+'&aflag=false');

      reference.setRedirect(true);
      return reference;

     }


     /**
      * This method sets the Edit flag to false and returns to HCVC_AssetDetails.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference setViewFlag(){

      PageReference reference=new PageReference(baseUrl+'/apex/HCVC_AssetDetails?id='+assetId+'&aflag=false');

      reference.setRedirect(true);
      return reference;

     }


     /**
      * This method  returns to main HCVC_MapPage.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

     public PageReference goBackToMap(){

      PageReference reference;
	  // Make sure their exists childern assets for this refinery, else redirect to main page
	  Id parentAssetId;
	  if(null==asset.Parent_Refinery_Asset__c) {
	  	parentAssetId = asset.Id;
	  } else {
	  	parentAssetId = asset.Parent_Refinery_Asset__r.Id;
	  }
	  // Grab the list of childern for this refinery
	  List<Asset__c> childList = [SELECT id FROM Asset__c WHERE Parent_Refinery_Asset__r.Id =: parentAssetId];
	  
      if(childList.size() > 0) {
         reference=new PageReference(baseUrl+'/apex/HCVC_MapPage?id='+parentAssetId+'&sfdc.tabName='+tbcolor);
      }else{
         reference=new PageReference(baseUrl+'/apex/HCVC_MapPage?&sfdc.tabName='+tbcolor);
      }

      reference.setRedirect(true);
      return reference;

     }

    /**
    * This method loads Default map and redirects to the page HCVC_MapPage. Or the previous asset page if in history
    * Incoming Arguments - nothing
    * Returns - PageReference
    */

    public PageReference loadDefaultMap(){
      PageReference reference=new PageReference(baseUrl+'/apex/HCVC_MapPage?isRefinery=true&isPipeline=true&isRetail=false&isTransport=false&sfdc.tabName='+tbcolor);
      reference.setRedirect(true);
      return reference;
    }


     /**
      * This method  returns HCVC_RacksDetails or HCVC_AssetDetails depends upon the Assettype and
      * uses the get parameters.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference redirectToAssetDetails(){

        PageReference reference;
        assetHtype=System.currentPageReference().getParameters().get('assettypename');
        assetHId=System.currentPageReference().getParameters().get('assetidname');

        if(assetHtype==refineryUnitTypeId || assetHtype==tankTypeId){
         reference=new PageReference(baseUrl+'/apex/HCVC_AssetDetails?id='+assetHId+'&assettype='+assetHtype+'&aflag=false');
        }else if(assetHtype==refineryRacksTypeId){
         reference=new PageReference(baseUrl+'/apex/HCVC_RacksDetails?id='+assetHId+'&aflag=false');
        }

        reference.setRedirect(true);
        return reference;

      }


     /**
      * This method  returns toHCVC_RacksDetails or HCVC_PipelineDetails depends upon the Assettype.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference redirectToDetails(){

        PageReference reference=null;

        if(assettype==refineryRacksTypeId){
           reference=new PageReference(baseUrl+'/apex/HCVC_RacksDetails?id='+assetId+'&aflag=false');
           reference.setRedirect(true);
         }
         
        return reference;

     }
 }