/**
 * Test class for testing triggers for HOG Rigless App
 */
public class TestRiglessTriggers {

    static testMethod void riglessTest() {
        
      test.startTest();

/*        
      // ------------------------------------------------------------
      // Test SetExternalId trigger
      Route__c testRoute = new Route__c(Name = '999');
      insert testRoute;  
        
      // ------------------------------------------------------------
      // Test updateFields trigger
      
      // Setup production engineer
//      Contact prodEng = new Contact(FirstName = 'Test',
//                                    Email = 'test@test.user.com',
//                                    LastName = 'User',
//                                    Phone = '9999999', 
//                                    MobilePhone = '9999999',
//                                    Contact_Role__c = 'Production Engineer');
//      insert prodEng;
      
      
      
      // Setup location
      Location__c loc = new Location__c(
//                                        Production_Engineer__c = prodEng.Id,
//                                        Production_Engineer_Bus__c = null,
//                                        Production_Engineer_Mobile__c = null,
                                        Route__c = testRoute.Id);
      
      insert loc;

*/

//
        //
        Business_Unit__c businessUnit = new Business_Unit__c
            (           
                Name = 'Test Business Unit'
            );            
        insert businessUnit;

        Business_Department__c businessDepartment = new Business_Department__c
            (           
                Name = 'Test Business Department'
            );            
        insert businessDepartment;

        Operating_District__c operatingDistrict = new Operating_District__c
            (           
                Name = 'Test Field',
                Business_Department__c = businessDepartment.Id,
                Business_Unit__c = businessUnit.Id
            );            
        insert operatingDistrict;

        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        Field__c field = new Field__c
            (           
                Name = 'Test Field Control Centre',
                Operating_District__c = operatingDistrict.Id,
                RecordTypeId = recordTypeId
            );            
        insert field;
        
        Route__c route = new Route__c
              (
                  Name = '999'
              );     
        insert route;                  
                              
        Location__c loc = 
            new Location__c
                (
                    Name = 'Test Location' ,
                    Route__c = route.id, 
                    Operating_Field_AMU__c = field.id
                );
        insert loc;        
//
      
      loc = [SELECT Name
//                    Production_Engineer_Bus__c, 
//                    Production_Engineer_Mobile__c
                    FROM Location__c WHERE Id = :loc.Id];
      
//      System.debug('------------  Testing updateFields -----------');      
//      System.assertEquals(prodEng.Phone, loc.Production_Engineer_Bus__c);
//      System.assertEquals(prodEng.MobilePhone, 
//                          loc.Production_Engineer_Mobile__c);
      
      
      // --------------------------------------------------------------
      // Test checkLocationName

      // Change the location name
      loc.Name = 'Changed';
      
      try {
        update loc;
      }
      catch (DmlException e) {
        System.debug('------- Testing checkLocationNameEdit ---------');
        System.assert(
            e.getMessage()
             .contains('You cannot change the name of the location'));
        System.assertEquals(Location__c.Name, e.getDmlFields(0)[0]);
        System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', 
                            e.getDmlStatusCode(0));  
      }

      // --------------------------------------------------------------
      // Test AddUserToRiglessServiceHopper
      
      // Setup user
      Profile standardProfile = [SELECT id FROM profile WHERE name='Standard User'];
      User riglessHopperUser = 
          new User(alias = 'standt', email='standarduser@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = standardProfile.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='blahthisshouldbeunique@huskyenergy.com.huskyprod');
      insert riglessHopperUser;
      
      update riglessHopperUser;
      
      // --------------------------------------------------------------
      // Test createNewRiglessServicingForms
      Service_Request_Form__c serviceReq = new Service_Request_Form__c();
      serviceReq.Name = '1234568';
      serviceReq.Location__c = loc.Id;
      serviceReq.Route_if_different__c = '001';
      serviceReq.Service_Specifics__c = 'Insert Pump Change';
      serviceReq.Well_Down_on__c = Date.today();
      insert serviceReq;
      
      Rigless_Servicing_Form__c newRigless = 
          [SELECT Id, RecordTypeId, Service_Status__c, Name
                  FROM Rigless_Servicing_Form__c 
                  WHERE Service_Request_Form__c = :serviceReq.Id];
      System.assertEquals(newRigless.name, serviceReq.Name);
     
      update serviceReq;
      
      // --------------------------------------------------------------
      // Test checkDuplicateTicket
      
      // Set up a dummy account
      Account dummyAcct = new Account(Name = 'Dummy Account');
      insert dummyAcct;

      // Set up duplicate vendor/personnel
      Vendor_Personnel__c vp1 = new Vendor_Personnel__c();
      vp1.Vendor__c = dummyAcct.Id;
      vp1.Rigless_Servicing_Form__c = newRigless.Id;
      insert vp1;
      
      try {
        Vendor_Personnel__c vp2 = new Vendor_Personnel__c();
        vp2.Vendor__c = dummyAcct.Id;
        vp2.Rigless_Servicing_Form__c = newRigless.Id;
        insert vp2;
      }
      catch (DmlException repeatVp){
        System.debug('------- Testing checkDuplicateTicket ---------');
        System.assert(
            repeatVp.getMessage()
             .contains('Vendor already has this ticket assigned'));
      }
      
      // --------------------------------------------------------------
      // Test SetDayofTheServiceAndDay1RigOnLocation
      Service_Time__c serviceTime = new Service_Time__c();
      serviceTime.Day__c = 'Day 1';
      serviceTime.Rig_On_Location__c = 
        Datetime.valueOf('2013-02-12 12:00:00');
      serviceTime.Rig_Off_Location__c =
        Datetime.valueOf('2013-02-13 12:00:00');
      serviceTime.Rigless_Servicing_Form__c = newRigless.Id;
      insert serviceTime;
      
      /*
      System.assertEquals(serviceTime.Rig_On_Location__c, 
             [SELECT Day_1_Rig_On_Location__c
                     FROM Rigless_Servicing_Form__c 
                     WHERE Id = :newRigless.Id].Day_1_Rig_On_Location__c );
      */
                     
      update serviceTime;
      
      serviceTime.Day__c = null;
      serviceTime.Rig_On_Location__c = 
        Datetime.valueOf('2013-02-14 12:00:00');
      serviceTime.Rig_Off_Location__c =
        Datetime.valueOf('2013-02-15 12:00:00');
      update serviceTime;  
      
      /*
      System.assertEquals('Day 2', 
                           [SELECT Day__c 
                                   FROM Service_Time__c 
                                   WHERE Id = :serviceTime.Id].Day__c);
      */
      
      // --------------------------------------------------------------
      // Test RiglessServiceingFormUpdated
      newRigless.Service_Status__c = 'In Progress';
//      prodEng.Email = null;
//      update prodEng;
      for (RecordType riglessRecordType : 
           [SELECT Name, Id 
                   FROM RecordType 
                   WHERE SobjectType = 'Rigless_Servicing_Form__c']) {
        newRigless.RecordTypeId = riglessRecordType.Id;
        update newRigless;
      }
      System.debug(newRigless); 
      
      // --------------------------------------------------------------
      // Test deleteRiglessServiceForm
      delete serviceReq;
      System.assertEquals(0, [SELECT Id 
                                     FROM Rigless_Servicing_Form__c 
                                     WHERE Id = :newRigless.Id].size()); 
           
      
      test.stopTest();
      
    }
}