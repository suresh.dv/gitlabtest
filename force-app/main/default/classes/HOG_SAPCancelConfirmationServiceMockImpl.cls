@isTest
public class HOG_SAPCancelConfirmationServiceMockImpl implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp resp = new HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp();
        resp.Confirmation_Resp = new List<HOG_SAPCancelConfirmationsService.Confirmation_Resp_element>();

        //Create response for all confirmations in request
        HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation[] confirmationRequestList = ((HOG_SAPCancelConfirmationsService.DT_SFDC_CancelConfirmation) request).Confirmation;
        for(Integer i=0; i < confirmationRequestList.size(); i++) {
          HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation confirmation = confirmationRequestList[i];
          HOG_SAPCancelConfirmationsService.Confirmation_Resp_element respElement = new HOG_SAPCancelConfirmationsService.Confirmation_Resp_element();
          respElement.Conf_No = confirmation.Order_Number + '-' + confirmation.Operation_Number + i;
          respElement.OBJNR = null;
          respElement.Status = 'E -' + ' ' + confirmation.Order_Number + ' not found.';
          resp.Confirmation_Resp.add(respElement);
        }

        response.put('response_x', resp);
   }
}