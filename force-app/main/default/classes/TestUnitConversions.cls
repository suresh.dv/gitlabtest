/*
* Tests the Unit Conversion Utility class. This won't be an exhaustive test.
*/
@isTest
public class TestUnitConversions {

	static TestMethod void SameUnitConversionTest() {
		Decimal conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Tonne', 1.00);
		System.assertEquals(conversionRatio, 1);
	}
		
	static TestMethod void MassConversionTests() {
		Decimal conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Kilogram', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Tonne', 'US Ton', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Kilogram', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.UsTonPerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Tonne', 1.00);
		System.assertEquals(conversionRatio, 1/UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'Tonne', 1.00);
		System.assertEquals(conversionRatio, 1/UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'US Ton', 1.00);
		System.assertEquals(conversionRatio, 1/UnitConversions.UsTonPerKilogram);
	}

	static TestMethod void VolumeConversionTests() {
		Decimal conversionRatio = UnitConversions.getConversionRatio('Liter', 'Drum', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Liter', 'Tote', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Liter', 'Meter 3', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Liter', 'US Gallon', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'Liter', 1.00);
		System.assertEquals(conversionRatio, 1/UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'Tote', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerTote / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'Meter 3', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerCubicMeter / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'US Gallon', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerUsGalon / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'Liter', 1.00);
		System.assertEquals(conversionRatio, 1 / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'Drum', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerDrum / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'Meter 3', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerCubicMeter / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'US Gallon', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerUsGalon / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'Liter', 1.00);
		System.assertEquals(conversionRatio, 1 / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'Drum', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerDrum / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'Tote', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerTote / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'US Gallon', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerUsGalon / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Liter', 1.00);
		System.assertEquals(conversionRatio, 1/UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Drum', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerDrum / UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Meter 3', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerTote / UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Tote', 1.00);
		System.assertEquals(conversionRatio, UnitConversions.LitersPerCubicMeter / UnitConversions.LitersPerUsGalon);
	}

	static TestMethod void MassToVolumeConversionTests() {
		Decimal density = 1.00;
		Decimal conversionRatio = UnitConversions.getConversionRatio('Liter', 'Tonne', density);
		System.assertEquals(conversionRatio, 1000 / density);
		conversionRatio = UnitConversions.getConversionRatio('Liter', 'Kilogram', density);
		System.assertEquals(conversionRatio, (1000 / density) * UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Liter', 'US Ton', density);
		System.assertEquals(conversionRatio, (1000 / density) * UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'Tonne', density);
		System.assertEquals(conversionRatio, (1000 / density) / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'Kilogram', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerKilogram) / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Drum', 'US Ton', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerUsTon) / UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'Tonne', density);
		System.assertEquals(conversionRatio, (1000 / density) / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'Kilogram', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerKilogram) / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tote', 'US Ton', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerUsTon) / UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'Tonne', density);
		System.assertEquals(conversionRatio, (1000 / density) / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'Kilogram', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerKilogram) / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Meter 3', 'US Ton', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerUsTon) / UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Tonne', density);
		System.assertEquals(conversionRatio, (1000 / density) / UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'Kilogram', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerKilogram) / UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('US Gallon', 'US Ton', density);
		System.assertEquals(conversionRatio, ((1000 / density) * UnitConversions.TonnePerUsTon) / UnitConversions.LitersPerUsGalon);
	}

	static TestMethod void VolumeToMassConversionTests() {
		Decimal density = 1.00;
		Decimal conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Liter', density);
		System.assertEquals(conversionRatio, density / 1000);
		conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Drum', density);
		System.assertEquals(conversionRatio, (density / 1000) * UnitConversions.LitersPerDrum);
		conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Tote', density);
		System.assertEquals(conversionRatio, (density / 1000) * UnitConversions.LitersPerTote);
		conversionRatio = UnitConversions.getConversionRatio('Tonne', 'Meter 3', density);
		System.assertEquals(conversionRatio, (density / 1000) * UnitConversions.LitersPerCubicMeter);
		conversionRatio = UnitConversions.getConversionRatio('Tonne', 'US Gallon', density);
		System.assertEquals(conversionRatio, (density / 1000) * UnitConversions.LitersPerUsGalon);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'Liter', density);
		System.assertEquals(conversionRatio, (density / 1000) / UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'Drum', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerDrum) / UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'Tote', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerTote) / UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'Meter 3', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerCubicMeter) / UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('Kilogram', 'US Gallon', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerUsGalon) / UnitConversions.TonnePerKilogram);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Liter', density);
		System.assertEquals(conversionRatio, (density / 1000) / UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Drum', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerDrum) / UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Tote', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerTote) / UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'Meter 3', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerCubicMeter) / UnitConversions.TonnePerUsTon);
		conversionRatio = UnitConversions.getConversionRatio('US Ton', 'US Gallon', density);
		System.assertEquals(conversionRatio, ((density / 1000) * UnitConversions.LitersPerUsGalon) / UnitConversions.TonnePerUsTon);
	}
}