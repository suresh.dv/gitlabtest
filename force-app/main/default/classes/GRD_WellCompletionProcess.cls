global class GRD_WellCompletionProcess{
    
    @InvocableMethod
    public static void updateBottomhole(List<Id> updatedRecords) {
        List<GRD_Well_Test__c> bholeList = new List<GRD_Well_Test__c>();

        try{
            for(Id u : updatedRecords){
                GRD_Well_Test__c test = [SELECT Id,Name,GRD_Borehole__c from GRD_Well_Test__c WHERE Id =: u];
                GRD_Borehole__c bhole = [SELECT Id,Target_Formation__c,Target_Location__c,Well__r.Surface_Location__c from GRD_Borehole__c WHERE UWI_Location__c =: test.GRD_Borehole__c];
                
                //Update GRD well completion record with fromation and surface location from GRD bottom hole object
                test.Target_Formation__c = bhole.Target_Formation__c;
                test.UWI_Location__c = bhole.Id;    
                test.Well__c = bhole.Well__r.Surface_Location__c;            
                
                bholeList.add(test);
            }
            
            update bholeList;
        } catch(Exception exp) {
            System.debug('Exception : ' + exp);
        }            
    }
}