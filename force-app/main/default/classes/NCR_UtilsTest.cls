@isTest(SeeAllData=true)
private class NCR_UtilsTest {

    static testMethod void testIsNCRAdminUser(){
        //Create sys admin
        User ncrUser = UserTestData.createTestsysAdminUser();
        insert ncrUser;
        
        System.AssertNotEquals(ncrUser.Id, Null);
        
        //run as sys admin
        System.runAs(ncrUser) {
            Boolean isAdmin = NCR_Utils.IsNCRAdminUser();
            System.assertEquals(isAdmin, true);
            
            NCR_Utils.logError('Test error');

        }   
        
        
        //repeat the test as ncr user
        User ncrNormalUser = NCRUserTestData.createNCRUser();
        
        System.AssertNotEquals(ncrNormalUser.Id, Null);
        
        //run as normal user
        System.runAs(ncrNormalUser) {
            Boolean isAdmin = NCR_Utils.IsNCRAdminUser();            
            System.assertEquals(isAdmin, false);
            
            
        }          
    }
    
    static testMethod void testNcrCaseShare() {  
        NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
            'State1', 'Case', 'Project', true);     
             
        NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
            'State2', 'Case', 'Project', true);
            
        //Create NCR action
        NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
        
        //Create NCR State action
        NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                state2, '', 'Current User', true); 
                
        System.AssertNotEquals(state1.Id, null);
        System.AssertNotEquals(state2.Id, null);
        System.AssertNotEquals(action1.Id, null);
        System.AssertNotEquals(stateAction1.Id, null);  
        
        PageReference pageRef = Page.NCR_ViewPage;
        Test.setCurrentPageReference(pageRef);  
            
        User ncrU1;
        Account a;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Heavy Oil & Gas'];
            
            ncrU1 = new User(alias = 'ptest', email='test@testorg.com', 
                emailencodingkey='UTF-8', lastname='Smith', 
                languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/Los_Angeles', 
                username='ncrtest1@testorg.com');
            insert ncrU1;
            a = new Account(name='Acme');
            insert a;
            
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', a.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true); 
                                                 
                    
            Boolean isManualShare = NCR_Utils.manualNCRCaseShareReadOwner(ncrCase.id, ncrU1.id); 
            NCR_Utils.manualNCRCaseShareRead(null);  
            NCR_Utils.manualNCRCaseShareRead(ncrCase.id);  
            
            NCR_Utils.manualNCRCARShareRead(null);                          
        }
    }   
    
    
    static testMethod void testNcrCARhare() {  
        NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
            'State1', 'Case', 'Project', true);     
             
        NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
            'State2', 'Case', 'Project', true);
            
        //Create NCR action
        NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
        
        //Create NCR State action
        NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                state2, '', 'Current User', true); 
                
        System.AssertNotEquals(state1.Id, null);
        System.AssertNotEquals(state2.Id, null);
        System.AssertNotEquals(action1.Id, null);
        System.AssertNotEquals(stateAction1.Id, null);  
        
        PageReference pageRef = Page.NCR_ViewPage;
        Test.setCurrentPageReference(pageRef);  
            
        User ncrU2;
        Account a;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Heavy Oil & Gas'];
            
            ncrU2 = new User(alias = 'ptest', email='test@testorg.com', 
                emailencodingkey='UTF-8', lastname='Smith', 
                languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/Los_Angeles', 
                username='ncrtest2@testorg.com');
            insert ncrU2;
            a = new Account(name='Acme');
            insert a;
            
            Date today = Date.today();
            NCR_CAR__c ncrCar = NCR_CAR_TestData.createCorrectiveAction(today, ncrU2.Id, 'Corrective Action Requested', ncrU2.Id, 'Short Description', true);
            
            NCR_Utils.manualNCRCARShareRead(null);                      
        }
    }   
    
    static testMethod void testNcrShare() {  
        User user1 = new User();
        User user2 = new User();
        Profile p = [select id from profile where name='Standard User'];
        
        user1.email = 'NCRUser1' + '@ncr.com';
        user1.Alias = 'NCRUser1' ;
        user1.EmailEncodingKey = 'UTF-8';
        user1.LastName = 'User1';
        user1.LanguageLocaleKey = 'en_US'; 
        user1.LocaleSidKey = 'en_US'; 
        user1.ProfileId = p.Id;
        user1.TimeZoneSidKey = 'America/Los_Angeles'; 
        user1.UserName = 'NCRUser1' + '@ncr.com.unittest';
        
        user2.email = 'NCRUser2' + '@ncr.com';
        user2.Alias = 'NCRUser2' ;
        user2.EmailEncodingKey = 'UTF-8';
        user2.LastName = 'User2';
        user2.LanguageLocaleKey = 'en_US'; 
        user2.LocaleSidKey = 'en_US'; 
        user2.ProfileId = p.Id;
        user2.TimeZoneSidKey = 'America/Los_Angeles'; 
        user2.UserName = 'NCRUser2' + '@ncr.com.unittest';        
        
        insert user1;
        insert user2;
        
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Label =: 'NCR User']; 
        PermissionSetAssignment permissionSetAssignment1 = new PermissionSetAssignment();
        PermissionSetAssignment permissionSetAssignment2 = new PermissionSetAssignment();
        
        permissionSetAssignment1.PermissionSetId = permissionSet.Id;
        permissionSetAssignment1.AssigneeId = user1.Id; 
        
        permissionSetAssignment2.PermissionSetId = permissionSet.Id;
        permissionSetAssignment2.AssigneeId = user2.Id;         
        
        insert permissionSetAssignment1;
        insert permissionSetAssignment2;
        
        System.runAs(user1) {
            Account a = new Account(name='Acme');
            insert a;        
                    
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);  
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);  
                    
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', a.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true); 
            ncrCase.ownerid = user1.id;                                     
                    
            Boolean isManualShare = NCR_Utils.manualNCRCaseShareRead(ncrCase.id, user2.id);      
        }
    }   
    
    
    static testMethod void testCARShare() {  
        User user1 = new User();
        User user2 = new User();
        Profile p = [select id from profile where name='Standard User'];
        
        user1.email = 'NCRUser3' + '@ncr.com';
        user1.Alias = 'NCRUser3' ;
        user1.EmailEncodingKey = 'UTF-8';
        user1.LastName = 'User3';
        user1.LanguageLocaleKey = 'en_US'; 
        user1.LocaleSidKey = 'en_US'; 
        user1.ProfileId = p.Id;
        user1.TimeZoneSidKey = 'America/Los_Angeles'; 
        user1.UserName = 'NCRUser3' + '@ncr.com.unittest';
        
        user2.email = 'NCRUser4' + '@ncr.com';
        user2.Alias = 'NCRUser4' ;
        user2.EmailEncodingKey = 'UTF-8';
        user2.LastName = 'User4';
        user2.LanguageLocaleKey = 'en_US'; 
        user2.LocaleSidKey = 'en_US'; 
        user2.ProfileId = p.Id;
        user2.TimeZoneSidKey = 'America/Los_Angeles'; 
        user2.UserName = 'NCRUser4' + '@ncr.com.unittest';        
        
        insert user1;
        insert user2;
        
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Label =: 'NCR User']; 
        PermissionSetAssignment permissionSetAssignment1 = new PermissionSetAssignment();
        PermissionSetAssignment permissionSetAssignment2 = new PermissionSetAssignment();
        
        permissionSetAssignment1.PermissionSetId = permissionSet.Id;
        permissionSetAssignment1.AssigneeId = user1.Id; 
        
        permissionSetAssignment2.PermissionSetId = permissionSet.Id;
        permissionSetAssignment2.AssigneeId = user2.Id;         
        
        insert permissionSetAssignment1;
        insert permissionSetAssignment2;
        
        System.runAs(user1) {
            Account a = new Account(name='Acme');
            insert a;        
                    
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);  
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);  
                    
            Date today = Date.today();
            NCR_CAR__c ncrCar = NCR_CAR_TestData.createCorrectiveAction(today, user1.Id, 'Corrective Action Requested', user1.Id, 'Short Description', true);
            ncrCar.ownerid = user1.id;                                     

            UserRole role = new UserRole(name = 'TEST ROLE');
            insert role;
            user1.userroleid = role.id;
            
            Group g = [select Id from Group Where type='RoleAndSubordinates' limit 1];  
            GroupMember gm = new GroupMember();     
            gm.GroupId = g.id;   
            gm.UserOrGroupId = user1.id;
     
            NCR_Utils.manualNCRCARShareRead(ncrCar.id); 
            NCR_Utils.manualNCRCARShareRead(ncrCar.id,user2.Id);     

        }
    }      
  
}