/**********************************************************************************
  CreatedBy : Accenture
  Organization: Accenture
  Purpose   :  RaM_Entitlement : To Share Entitlemnt record with Community user
  Test Class:  RaM_CaseTriggerHandlerTest
  Version:1.0.0.1
**********************************************************************************/
public without sharing Class RaM_Entitlement{

 /**************************************
    Method name:    entitlementMethod
    Purpose:        To return entitlment id
    Return:         Id   
    *************************************/
   public static Id entitlementMethod(){
       List<Entitlement> EntitlementRecord= [select id,name from entitlement where name = :RaM_ConstantUtility.SLA_HUSKY_ENERGY_TEXT limit 1];
       if(EntitlementRecord != NULL && EntitlementRecord.size()>0)
         return EntitlementRecord[0].id;
       else
        return null;  
   }
   
}