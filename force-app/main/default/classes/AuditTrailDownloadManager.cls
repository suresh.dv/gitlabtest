/*--------------------------------------------------------------------------------------
Author     : Yen Le
Company    : Thirdwave Consulting
Description: Download Audit Trail and write it to "HR Audit Trail" folder Document 
Test Class : AuditTrailUnitTest

----------------------------------------------------------------------------------------*/ 
global class AuditTrailDownloadManager implements Database.Batchable<sObject>, Database.AllowsCallouts {
	//this is just a batch wrapper workaround to avoid "Callout from scheduled Apex not supported" limitation
	
	global Database.querylocator start(Database.BatchableContext BC) {
        //get a session Id for all the API requests
        String sessionId = AuditTrailUtils.getSessionId();  
        System.debug('sessionId =' + sessionId);
        //===================================================================
        //Step 1: get the URL for the Setup Audit Trail .CSV file.  This is just because we need the CONFIRMATIONTOKEN (screen scrape!)
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/setup/org/orgsetupaudit.jsp');
        req.setMethod('GET');   
          
        req.setHeader('Cookie','sid='+sessionId);
        HTTPResponse res = new HTTPResponse(); 
        
        String viewAuditPage;
        if (Test.isRunningTest()) {
            viewAuditPage = getDummyPage();
        } else {            
            res = http.send(req);
            viewAuditPage = res.getBody();
        }
           
        //parse out just the URL we want from the full HTML source
        List<String> auditPageList = viewAuditPage.split('href="/servlet/servlet.SetupAuditTrail', 2);
        System.debug(auditPageList[1]);
        String csvURL = '/servlet/servlet.SetupAuditTrail' + auditPageList[1].split('"', 2)[0];
        csvURL = csvURL.replace('&amp;', '&');
        
        
        
        //===================================================================
        //Step 2: get the csv file      
        http = new Http();
        req = new HttpRequest();
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+csvURL);
        req.setMethod('GET');
        req.setHeader('Cookie','sid='+sessionId); 

        Blob csvContent; 
        
        if (Test.isRunningTest()) {
            csvContent = Blob.valueOf(getDummyCSV());
        } else {
            res = http.send(req);
            csvContent = res.getBodyAsBlob();
        }
        
        //===================================================================  
        //Step 3: Upload csv file to Document  
        Folder fileFolder = [SELECT Id FROM Folder WHERE Name = 'HR Audit Trail'];
        Document auditTrail = null;
        List<Document> existingAuditTrail = [SELECT Id, Body, Name, FolderId, Type, Description, ContentType 
                                                FROM Document WHERE Name = 'HR_AuditTrail'];
        if (existingAuditTrail.size() == 0)
        {
        	auditTrail = new Document();
        }
        else
            auditTrail =  existingAuditTrail[0];
            
        auditTrail.Name = 'HR_AuditTrail';
        auditTrail.Body = csvContent;  
        auditTrail.FolderId = fileFolder.Id;
        auditTrail.Type = 'csv';
        auditTrail.Description = 'this Salesforce audit trail is downloaded at ' + datetime.now().format();
        auditTrail.ContentType = 'application/vnd.ms-excel';
        try
        {  
        	upsert auditTrail;
        	System.debug('******** Audit Trail Download Successful **********');
        }
        catch(Exception e)
        {
        	System.debug('*********** Audit Trail Download Errors ' + e.getMessage() + ' ***********');
        }
         
        //just a dummy query to avoid "First error: Start did not return a valid iterable object." error 
        return Database.getQueryLocator('select Id from Document limit 1');
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){}   
    
    global void finish(Database.BatchableContext BC) {}   

    //===================================================================
    //dummy http responses for testing
    public static String getDummyPage() {
        return
        '<a href="/servlet/servlet.SetupAuditTrail?id=00Di00000001111&amp;_CONFIRMATIONTOKEN=28907nr89nd2589.2895724758d8937nd9wfushffsdfjk.skjfhsjkfhskdfh8937nxtr389nu.248907n528947n589347n5c9">';       
    }   

    public static String getDummyCSV() {
        return
        'Date,User,Action,Section,Delegate User\n'+
        '"10/5/2013 10:11:56 AM PDT","usergroup@test.com","Changed AuditTrailImporter Apex Class code","Apex Class",""\n'+
        '"10/5/2013 10:11:26 AM PDT","usergroup@test.com","Changed AuditTrailImporter Apex Class code","Apex Class",""\n';
    }

  
}