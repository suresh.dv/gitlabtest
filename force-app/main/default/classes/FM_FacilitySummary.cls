public class FM_FacilitySummary
{
	private List<FacilityDataWrapper> lAllFacilityData;
	private Map<String, FacilityDataWrapper> mapFacilityData;

	private Date dTime;
	private String sTime;

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_FacilitySummary()
	{
		InitializeData();
		BuildFacilityData();
	}

	/////////////////////////////////////
	// * INITIALIZE CONTROLLER DATA * ///
	/////////////////////////////////////

	private void InitializeData()
	{
		lAllFacilityData = new List<FacilityDataWrapper>();
		mapFacilityData = new Map<String, FacilityDataWrapper>();

		this.sTime = (this.sTime == null) ? '' : this.sTime;
		this.dTime = (this.sTime.toLowerCase() != 'yesterday') ? Date.Today() : Date.Today() - 1;
	}

	//////////////////////////////////
	// * BUILD ALL FACILITY DATA * ///
	//////////////////////////////////

	public PageReference BuildFacilityData()
	{
		InitializeData();

		List<FM_Truck_Trip__c> lTripData = [SELECT Id, Product__c, Shift__c, Load_Weight__c, Facility__c, Facility_Lookup__c, Location_Lookup__c 
											FROM FM_Truck_Trip__c 
											WHERE Run_Sheet_Date__c = :dTime 
											AND Facility__c != '' 
											AND Truck_Trip_Status__c IN :FM_Utilities.FACILITY_SUMMARY_STATUS_LIST 
											ORDER BY Facility__c, Product__c LIMIT 5000];
		List<Facility_Capacity__c> lFacilityData = [SELECT Id, Facility__c, Well_Location__c, Facility_Name__c, Total_Capacity_Oil__c, Total_Capacity_Water__c 
													FROM Facility_Capacity__c LIMIT 5000];

		Map<String, Integer> mapFacilityCapacity = new Map<String, Integer>();
		FacilityDataWrapper oOneWrapper;

		String sKey = '';
		Integer iTotalFound = 0;

		for(Facility_Capacity__c oOneFacility : lFacilityData)
		{
			mapFacilityCapacity.put(oOneFacility.Facility_Name__c + '-O', Integer.valueOf(oOneFacility.Total_Capacity_Oil__c));
			mapFacilityCapacity.put(oOneFacility.Facility_Name__c + '-W', Integer.valueOf(oOneFacility.Total_Capacity_Water__c));
		}

		for(FM_Truck_Trip__c oOneTrip : lTripData)
		{
			
			sKey = oOneTrip.Facility__c + '-' + oOneTrip.Product__c;
			iTotalFound = (mapFacilityCapacity.get(sKey) == null) ? 0 : mapFacilityCapacity.get(sKey);

			// No Wrapper Found --> Create
			if(mapFacilityData.get(sKey) == null)
			{
				oOneWrapper = new FacilityDataWrapper((oOneTrip.Facility_Lookup__c != null) ? oOneTrip.Facility_Lookup__c : oOneTrip.Location_Lookup__c, oOneTrip.Facility__c, oOneTrip.Product__c, iTotalFound);
				mapFacilityData.put(sKey, oOneWrapper);
			}
			else
			{
				oOneWrapper = mapFacilityData.get(sKey);
			}

			if(oOneTrip.Shift__c.toLowerCase() == 'night')
			{
				oOneWrapper.IncrementCapacityNight(Integer.valueOf(oOneTrip.Load_Weight__c));
			}
			else if(oOneTrip.Shift__c.toLowerCase() == 'day')
			{
				oOneWrapper.IncrementCapacityDay(Integer.valueOf(oOneTrip.Load_Weight__c));
			}

			oOneWrapper.IncrementLoads(oOneTrip.Shift__c);

			// Update Wrapper
			mapFacilityData.put(sKey, oOneWrapper);
		}

		for(String sOneFacility : mapFacilityData.keySet())
		{
			oOneWrapper = mapFacilityData.get(sOneFacility);
			this.lAllFacilityData.add(oOneWrapper);
		}

		return null;
    }

	//////////////////////////////////////////
	// * WRAPPER CLASS FOR FACILITY DATA * ///
	//////////////////////////////////////////

	public class FacilityDataWrapper
	{
		public String sFacilityId {get; private set;}
		public String sFacilityName {get; private set;}
		public String sProductType {get; private set;}

		public Integer iTotalCapacity {get; private set;}
		public Integer iHalfCapacity {get; private set;}
		public Integer iCurrentCapacityNight {get; private set;}
		public Integer iCurrentCapacityDay {get; private set;}
		public Integer iDay {get; private set;}
		public Integer iNight {get; private set;}

		public FacilityDataWrapper(String sFacilityRecordId, String sFacility, String sProduct, Integer iTotal)
		{
			this.sFacilityId = sFacilityRecordId;
			this.sFacilityName = sFacility;
			this.sProductType = sProduct;

			this.iTotalCapacity = iTotal;
			this.iHalfCapacity = (iTotal != 0) ? Integer.valueOf(iTotal / 2) : 0;
			this.iCurrentCapacityNight = 0;
			this.iCurrentCapacityDay = 0;
			this.iDay = 0;
			this.iNight = 0;
		}

		public void IncrementCapacityNight(Integer iIncrease)
		{
			this.iCurrentCapacityNight += iIncrease;
		}

		public void IncrementCapacityDay(Integer iIncrease)
		{
			this.iCurrentCapacityDay += iIncrease;
		}

		public void IncrementLoads(String sShift)
		{
			if(sShift != null && sShift != '' && sShift.toLowerCase() == 'night')
			{
				this.iNight++;
			}
			else if(sShift != null && sShift != '' && sShift.toLowerCase() == 'day')
			{
				this.iDay++;
			}
		}

		public Boolean getIsAtCapacityNight()
		{
			return (this.iCurrentCapacityNight > this.iHalfCapacity) ? true : false;
		}

		public Boolean getIsAtCapacityDay()
		{
			return (this.iCurrentCapacityDay > this.iHalfCapacity) ? true : false;
		}
	}

	/////////////////////////////////
	// * GETTER + SETTER METHODS * //
	/////////////////////////////////

	public List<FacilityDataWrapper> getFacilitySummary()
	{
		return lAllFacilityData;
	}

	public Integer getFacilityLineCount()
	{
		return lAllFacilityData.size();
	}

	public String getTimeValue()
	{
		return sTime;
	}

	public void setTimeValue(String sNewTime)
	{
		sTime = sNewTime;
	}
}