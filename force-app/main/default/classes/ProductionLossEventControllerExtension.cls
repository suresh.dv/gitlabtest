public with sharing class ProductionLossEventControllerExtension {
    public Boolean isNew {get; set;}
    public Production_Loss_Event__c prodLossEvent {get; set;}
    
    // the values of the selected items
    public string selectedLevel1 {get; set;}
    public string selectedLevel2 {get; set;}
    public string selectedLevel3 {get; set;}
    
    public string selectedNone = '*';
    
    public string selectedEquipmentBySearch {get; set;}
    
    public String searchByTagID{get; set;}
    
    public ProductionLossEventControllerExtension(ApexPages.StandardController stdController) {
        this.prodLossEvent = (Production_Loss_Event__c) stdController.getRecord();
        this.isNew = String.isEmpty(this.prodLossEvent.id);
        
        if(!this.isNew) {
            this.prodLossEvent = [SELECT Id, Name, Operating_District__c, Start_Date_Time__c, End_Date_Time__c, Plant__c, Unit__c, Equipment__c, Production_Loss_Category__c, Production_Loss_Sub_Categories__c, Production_Loss_Code__c, Description__c FROM Production_Loss_Event__c WHERE Id =: prodLossEvent.Id];
        }
        
        /*Populate operating district field only for Sunrise contacts*/
        if(this.isNew){
            Contact[] c = [SELECT id, name from Contact WHERE Name =: UserInfo.getName()];
            
            if(c.size() > 0){
                Shift_Operator__c[] shiftOps = [SELECT Team__c, Team__r.Operating_District__c,Team__r.Operating_District__r.Name FROM Shift_Operator__c WHERE Operator_Contact__c =: c[0].id AND Team__r.Operating_District__c != null LIMIT 1];
                
                if(shiftOps.size() > 0 && shiftOps[0].Team__r.Operating_District__r.Name == 'Sunrise'){
                    this.prodLossEvent.Operating_District__c = shiftOps[0].Team__r.Operating_District__c;
                }              
            }          
        }
        
        system.debug('Production Loss Event Record : ' + this.prodLossEvent);
        
        selectedLevel1 = String.ValueOf(prodLossEvent.get('Plant__c'));
        selectedLevel2 = String.ValueOf(prodLossEvent.get('Unit__c'));
        selectedLevel3 = String.ValueOf(prodLossEvent.get('Equipment__c'));
    }
    
    public void populatePlantUnitEquipment() {
        System.debug('selectedEquipmentBySearch : ' + selectedEquipmentBySearch);
        if(selectedEquipmentBySearch != Null) {
            Equipment__c equipment = [SELECT Id, Name, Tag_Number__c, Unit__c, Unit__r.Plant__c FROM Equipment__c WHERE Id =: selectedEquipmentBySearch];
            System.debug('Selected Equipment : ' + equipment);
            System.debug('equipment.Unit__r.Plant__c : '  + equipment.Unit__r.Plant__c);
            System.debug('equipment.Unit__c : ' + equipment.Unit__c);
            selectedLevel1 = equipment.Unit__r.Plant__c;
            selectedLevel2 = equipment.Unit__c;
            selectedLevel3 = selectedEquipmentBySearch;
        }
    }
    
    public void resetUnitAndEquipment() {
        System.debug('Entered resetUnitAndEquipment');
        System.debug('selectedLevel2 : ' + selectedLevel2);
        System.debug('selectedLevel3 : ' + selectedLevel3);
        System.debug('searchByTagID : ' + searchByTagID);
        
        if(selectedLevel2 != Null) {
            selectedLevel2 = Null;
            selectedLevel3 = Null;      
            selectedEquipmentBySearch = Null;   
        }
    }
    
    public List<selectOption> level1Items {
        get {
            System.debug('Entered level1Items');
            System.debug('level1Items Entered - selectedLevel3 : ' + selectedLevel3);
            List<selectOption> options = new List<selectOption>();
            
            options.add(new SelectOption(selectedNone,'-- Choose a Plant --'));
            
            for (Plant__c plant : [select Id, Name from Plant__c Order By Name limit 999])
                options.add(new SelectOption(plant.Id,plant.Name));
            
            System.debug('level1Items END - selectedLevel1 : ' + selectedLevel1);
            System.debug('level1Items END - selectedLevel3 : ' + selectedLevel3);
            return options;           
        }
        set;
    }
    
    public List<selectOption> level2Items {
        get {
            System.debug('level2Items Entered - selectedLevel3 : ' + selectedLevel3);
            List<selectOption> options = new List<selectOption>();
 
            if (selectedLevel1 != NULL && selectedLevel1 != selectedNone) {
                System.debug('Entered level2Items - selectedLevel1 : ' + selectedLevel1);
                options.add(new SelectOption(selectedNone,'-- Choose a Unit --'));
                for (Unit__c unit : [select Id, Name from Unit__c Where Plant__c = :selectedLevel1 Order By Name limit 999])
                    options.add(new SelectOption(unit.Id,unit.Name));
            }
            System.debug('level2Items END - selectedLevel2 : ' + selectedLevel2);
            System.debug('level2Items END - selectedLevel3 : ' + selectedLevel3);
            return options;            
        }
        set;
    }
    
    public List<selectOption> level3Items {
        get {
           System.debug('level3Items Entered - selectedLevel3 : ' + selectedLevel3);
            List<selectOption> options = new List<selectOption>();
            
            if (selectedLevel2 != NULL && selectedLevel2 != selectedNone) {
                options.add(new SelectOption(selectedNone,'-- Choose an Equipment --'));
                
                if(searchByTagID == Null) {
                    for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c = :selectedLevel2 Order By Tag_Number__c limit 999]) {
                        if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                        }
                        else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50))); 
                        }
                        else if(equipment.Description_of_Equipment__c == Null) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c));   
                        }
                    }
                }
                else if(searchByTagID != Null) {
                    String filterString = '%' + searchByTagID + '%';
                    for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c =:selectedLevel2 AND (Tag_Number__c like :filterString OR Description_of_Equipment__c like :filterString) Order By Tag_Number__c limit 999]) {
                        if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                        }
                        else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50))); 
                        }
                        else if(equipment.Description_of_Equipment__c == Null) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c));   
                        }
                    }
                }
            }
            System.debug('level3Items END - selectedLevel3 : ' + selectedLevel3);
            return options;           
        }
        set;
    }  
    
    public List<selectOption> equipmentSearchList {
        get {
            System.debug('equipmentSearchList Entered - selectedLevel3 : ' + selectedLevel3);
            selectedEquipmentBySearch = Null;
            List<selectOption> options = new List<selectOption>();
            
            if(searchByTagID != Null) {
                String filterString = '%' + searchByTagID + '%';
                for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c != Null AND Unit__r.Plant__c != Null AND (Tag_Number__c like :filterString OR Description_of_Equipment__c like :filterString) Order By Tag_Number__c Limit 1000]) {
                    if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                        options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                    }
                    else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50)));  
                    }
                    else if(equipment.Description_of_Equipment__c == Null) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c));    
                    }
                }                   
            }
            else if(searchByTagID == Null) {
                for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c WHERE Tag_Number__c != Null AND Unit__c != Null AND Unit__r.Plant__c != Null Order By Tag_Number__c Limit 1000]) {
                    if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                        options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                    }
                    else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50)));  
                    }
                    else if(equipment.Description_of_Equipment__c == Null) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c));    
                    }
                }                   
            }
            System.debug('equipmentSearchList End - selectedLevel3 : ' + selectedLevel3);
            return options;  
        }
        set;
    } 
    
    public PageReference save() {
        prodLossEvent.Plant__c = selectedLevel1;
        
        if(selectedLevel2 != selectedNone) {
            prodLossEvent.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                prodLossEvent.Equipment__c = selectedLevel3;
            }
            else {
                prodLossEvent.Equipment__c = null;
            }
        }
        else {
            prodLossEvent.Unit__c = null;
            prodLossEvent.Equipment__c = null;
        }
        
        if(prodLossEvent.End_Date_Time__c == null){
            prodLossEvent.End_Date_Time__c = prodLossEvent.Start_Date_Time__c.addDays(7);
        }
        
        return new ApexPages.StandardController(this.prodLossEvent).save();
    }
    
    public PageReference saveAndNew() {     
        prodLossEvent.Plant__c = selectedLevel1;
        
        if(selectedLevel2 != selectedNone) {
            prodLossEvent.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                prodLossEvent.Equipment__c = selectedLevel3;
            }
            else {
                prodLossEvent.Equipment__c = null;
            }
        }
        else {
            prodLossEvent.Unit__c = null;
            prodLossEvent.Equipment__c = null;
        }
        
        if(prodLossEvent.End_Date_Time__c == null){
            prodLossEvent.End_Date_Time__c = prodLossEvent.Start_Date_Time__c.addDays(7);
        }        
        
        // Save the Operational Event to the database        
        new ApexPages.StandardController(this.prodLossEvent).save();

        // Go to the page that adds a new Operational Event
        //return new OperationalEventListController().newOperationalEvent();
        //return null;
        prodLossEvent = prodLossEvent.clone(false);
        return null;
        
    }
}