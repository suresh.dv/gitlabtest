/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = true)
private class ATSCustomEmailControllerTest {

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            // Create a Test Account with the name Asphalt
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
            
            insert account;
            System.AssertNotEquals(account.Id, Null);
        
            Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
            insert cont; 
            
            ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];

            ATS_Tender_Customers__c customer = ATSTestData.createTenderCustomer(account.Id, tender.id, cont.id);
            customer.Won__c = true;
            insert customer; 
            
            // Create Products        
            List<Product2> productList = ProductTestData.createProducts(101);
            insert productList;
            
            // Create Price Book Entries
            /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
            insert priceBookEntryList;*/
            
            Map<String, Id> recordTypeMap = new Map<String, Id>();
    
            for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                      WHERE DeveloperName in ('ATS_Asphalt_Product_Category', 'ATS_Emulsion_Product_Category', 'ATS_Residual_Product_Category') AND 
                                      SObjectType = 'Opportunity']) {
                recordTypeMap.put(recType.DeveloperName, recType.Id);            
            }
            
            List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
            
            Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('ATS_Emulsion_Product_Category'));
            opp.Opportunity_ATS_Product_Category__c = tender.Id;
            opp.StageName = 'Won'; 
            opp.Confirmation_Method__c = 'PO';
            opp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            opp.PO__c = 'PO Test';
            insert opp;
            opp.Pricebook2Id = standardPriceBook[0].Id;
            opp.priceBook2 = standardPriceBook[0];
            update opp;
            
            List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
            priceBookEntryList[0].IsActive = true;
            update priceBookEntryList[0]; 
            OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
            insert oli;
            
            Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
            theOpp.Confirmation_Method__c = 'PO';
            theOpp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            update theOpp;
            System.assertEquals(theOpp.Pricebook2Id, standardPriceBook[0].Id);
            System.assertNotEquals(theOpp.Pricebook2, Null);
            System.assertNotEquals(theOpp.PriceBook2.Name, Null);        
          
          //freight
          ATS_Freight__c  freight = new ATS_Freight__c();
          freight.Husky_Supplier_1__c= 'Husky_Supplier_1';
          freight.Husky_Supplier_2__c= 'Husky_Supplier_2';          
          freight.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
          freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
          freight.ATS_Freight__c  =   theOpp.Id;
          freight.Husky_Supplier_1_Selected__c = true;                       
          insert freight;      

          //Create Controller
          ATSCustomEmailController controller = new ATSCustomEmailController();
          controller.OpportunityID = tender.id;

          ATSCustomEmailController.ATSTenderWrapper  tenderWrapper = controller.getATSTender();
          System.AssertNotEquals(tenderWrapper , null);
          //1 opportunity
          System.AssertEquals(tenderWrapper.Opportunities.size(),1); 
          System.AssertEquals(tenderWrapper.Destination,'Test Destination');     
            
          ATSCustomEmailController.ATSOpportunityWrapper oppWrapper =  tenderWrapper.Opportunities[0];
          //1 product          
          System.AssertEquals(oppWrapper.Products .size(),1); 
          //3 suppliers
          System.AssertEquals(oppWrapper.Suppliers.size(),3);
        }
    }
}