/*
Called by the Downstream Portal CMS Calendar page. Returns all events to be displayed on that page.
*/
global with sharing class DSP_CMS_CalendarService implements cms.ServiceInterface
{
    // Class for easy JSON conversion
    public class calEvent
    {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String startStringIE {get;set;}
        public String endString {get;set;}
        public String endStringIE {get;set;}
        public String url {get;set;}
        public String location {get;set;}
        public String createdBy {get;set;}
        public String description {get;set;}
    }
    
    private String action;
    private System.JSONGenerator response;
    
    public System.Type getType()
    {
        return DSP_CMS_CalendarService.class;
    }
    
    public list<calEvent> events {get;set;}
    public Id calendarId {get;set;}
    
    //The calendar plugin is expecting dates is a certain format. We can use this string to get it formated correctly
    String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
    // Datetime format for IE 9
    String dtFormatIE = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
    
    public void fetchEventsForCalPage()
    {
        events = new list<calEvent>();
       
        // Get ID of DS Portal Calendar it cannot be queried so we need to hardcode it or put it in a custom setting
        DSP_Settings__c settings = DSP_Settings__c.getInstance();
        calendarId = ID.valueOf(settings.Public_Calendar_ID__c);
        
        //Get my Events
        for(Event evnt: [SELECT Id,
                                Subject,
                                Description,
                                OwnerId,
                                location,
                                IsAllDayEvent,
                                StartDateTime,
                                EndDateTime,
                                CreatedBy.Name
                         FROM Event
                         WHERE OwnerId =: calendarId
                         ORDER BY StartDateTime ASC])
        {
            DateTime startDT = evnt.StartDateTime;
            DateTime endDT = evnt.EndDateTime;

            calEvent myEvent = new calEvent();
             
            myEvent.title = evnt.Subject;
            myEvent.location = evnt.Location;
            myEvent.createdBy = evnt.createdBy.name;
            if(evnt.description != null && evnt.description.length() > 250)
            {
                myEvent.description = evnt.description.substring(0, 250) + '...';
            }
            else
            {
                myEvent.description = evnt.description;
            }
            myEvent.allDay = evnt.IsAllDayEvent; // Using the non custom event for this all day event
            
            // Keep date/time as GMT if it is an all day event
            if(myEvent.allDay){
                myEvent.startString = startDT.formatGmt(dtFormat);
                myEvent.startStringIE = startDT.formatGmt(dtFormatIE);
                // We add a day to the end time so it includes the next day (IE 2 day all day events)
                myEvent.endString = endDT.formatGmt(dtFormat);
                myEvent.endStringIE = endDT.addDays(1).formatGmt(dtFormatIE);
            } else { // Set time as local time of the salesforce instance
                myEvent.startString = startDT.format(dtFormat);
                myEvent.startStringIE = startDT.format(dtFormatIE);
                myEvent.endString = endDT.format(dtFormat);
                myEvent.endStringIE = endDT.format(dtFormatIE);
            }
                           
            myEvent.url = '/' + evnt.Id;
            events.add(myEvent);            
        }
    }
    
    global String executeRequest(Map<String, String> p)
    {
        action = p.get('action');
        
        if(action == 'CalendarEvents')
        {
            fetchEventsForCalPage();
        }
        
        // Generate JSON
        
        response = System.JSON.createGenerator(false);
        
        response.writeStartObject();
        response.writeFieldName('events');
        response.writeStartArray();
        
        for(calEvent eachEvent : events)
        {
            response.writeObject(eachEvent);
        }
        
        response.close();
        return (response.getAsString());
    }
}