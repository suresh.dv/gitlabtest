/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentTransferFormBulkClose page to close Status of multiple Equipment Transfer Request records

Test Class:    EquipmentTransferFormTest

History:       
			   26-Mar-15	Gangha Kaliyan	To include facilities in Equipment Inventory
			   27-Feb-17    Miro Zelina included reference to Well Event
			   29-Jun-17    Miro Zelina included reference to System and Sub-System
			   27-Jul-17    Miro Zelina - added role check for SAP Support Lead
			   04-Aug-17    Miro Zelina - added profile & PS check for process button
         04.09.17    Marcel Brimus - added FEL and Yard 
------------------------------------------------------------*/

public with sharing class EquipmentTransferFormBulkCloseController { 
    
    public List<Equipment_Transfer_Form__c> selectedList {get; private set;}
    public integer CurrentIndex {get; private set;}
    public Equipment_Transfer_Form__c CurrentForm {get; private set;}
    public EquipmentUtilities.UserPermissions userPermissions;

    public EquipmentTransferFormBulkCloseController(ApexPages.StandardSetController controller)
    {
       userPermissions = new EquipmentUtilities.UserPermissions();
       
       if (!Test.isRunningTest())
       {
           controller.addFields(new List<String>{'From_Location__c', 'To_Location__c', 'Charge_No__c', 'Status__c',
               'Date_of_Physical_Transfer__c', 'Authorized_By__c', 'Reason_for_Transfer_Additional_Details__c',
                'Shipped_Via__c', 'Waybill_No__c', 'Maintenance_Work_Order__c',
               'CreatedById', 'CreatedDate', 'LastModifiedById', 'LastModifiedDate', 'Name', 'Comments__c', 
               'From_Facility__c', 'To_Facility__c', 'From_Well_Event__c', 'To_Well_Event__c',
               'From_System__c', 'To_System__c', 'From_Sub_System__c', 'To_Sub_System__c',
               'From_Functional_Equipment_Level__c', 'To_Functional_Equipment_Level__c', 'From_Yard__c', 'To_Yard__c'
           });
       }
       
       if (!userPermissions.isSystemAdmin && !userPermissions.isSapSupportLead && !userPermissions.isHogAdmin){
           
           selectedList = new List<Equipment_Transfer_Form__c>();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
           return;
       }
       
       else if(controller.getSelected().Size()==0)
       {
           selectedList = new List<Equipment_Transfer_Form__c>();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Equipment Transfer Form'));
           return;
       }
       
       selectedList = controller.getSelected();
       
       CurrentIndex = 1;       
       
       CurrentForm = selectedList[CurrentIndex-1];
    }
    public List<EquipmentItemWrapper> equipmentList
    {
        get{
            if (equipmentList == null)
            {
                equipmentList = new List<EquipmentItemWrapper>();
                if (CurrentForm != null)
                {   List<Equipment_Transfer_Item__c> itemList = [select Id, Equipment__r.Description_of_Equipment__c, Equipment__r.Manufacturer__c, 
                                   Equipment__r.Model_Number__c, Equipment__r.Manufacturer_Serial_No__c, Equipment__r.Tag_Number__c,
                                   Equipment__c, Equipment__r.Equipment_Number__c, Tag_Colour__c,Equipment__r.Location__c, Equipment__r.Facility__c,
                                   Equipment__r.Well_Event__c, Equipment__r.System__c, Equipment__r.Sub_System__c, Equipment__r.Functional_Equipment_Level__c, Equipment__r.Yard__c,
                                   (select Id, Name, ContentType from Attachments)
                                   from Equipment_Transfer_Item__c where Equipment_Transfer_Form__c =: CurrentForm.Id 
                                   order by Equipment__r.Description_of_Equipment__c];
                    for(Equipment_Transfer_Item__c item : itemList)
                    {
                       equipmentList.add(new EquipmentItemWrapper(item));  
                    }          
                }
            }
            return equipmentList;
        }
        set;
    }
    public boolean getHasPreviousForm()
    {
        return (CurrentIndex > 1);
    }   
   
    public boolean getHasNextForm()
    {
        return (CurrentIndex < selectedList.size());
    }
    
    public PageReference NextForm()
    {
        if(getHasNextForm())
        { 
            CurrentIndex = CurrentIndex + 1 ;
            CurrentForm = selectedList[CurrentIndex-1];   
            equipmentList = null;
            system.debug(CurrentIndex); 
        }
       return null; 
    }
   
    public PageReference PreviousForm()
    { 
       if(getHasPreviousForm())
       { 
           system.debug(CurrentIndex);       
           CurrentIndex = CurrentIndex - 1 ;
           CurrentForm = selectedList[CurrentIndex-1];     
           equipmentList = null;  
           system.debug(CurrentIndex);
       }
       return null;
    }    
    
    public PageReference CloseForm()
    {
       Equipment_Transfer_Form__c obj = CurrentForm;  

       if(obj.Status__c != 'Closed')
       {
           try
           {
            obj.Status__c = 'Closed';
            update obj;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
            return null;
            
           }
           catch(Exception ex)    
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
               return null;
           }               
       }   
  
       return null;   
   
    }
}