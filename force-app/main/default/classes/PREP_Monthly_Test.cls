@isTest
private class PREP_Monthly_Test {

    static testMethod void myUnitTest() 
    {       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c();
        cat.Name = 'Category';
        cat.Active__c = true;
        cat.Discipline__c = disp.Id;
        
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
        sub.Name = 'SubCat';
        sub.Category__c = cat.Id;
        sub.Category_Manager__c = u.Id;
        sub.Category_Specialist__c = u.Id;
        sub.Active__c = true;
        
        insert sub;
        
        PREP_Initiative__c ini = new PREP_Initiative__c();
        ini.Initiative_Name__c = 'Test ini';
        ini.Discipline__c = disp.Id;
        ini.Category__c = cat.Id;
        ini.Sub_Category__c = sub.Id;
        ini.Status__c = 'Active';
        ini.Type__c = 'Category';
        ini.Risk_Level__c = 'High';
        ini.HSSM_Package__c = 'Full';
        ini.Workbook__c = 'Yes';
        ini.SRPM_Package__c = 'As Needed';
        ini.Aboriginal__c = 'No';
        ini.Global_Sourcing__c = 'Yes';
        ini.OEM__c = 'Yes';
        ini.Rate_Validation__c = 'Yes';
        ini.Reverse_Auction__c = 'No';
        ini.Rebate__c = 'Multiple Area Award';
        ini.Rebate_Frequency__c = 'Annually';
        ini.SCM_Manager__c = u.Id;
        ini.Category_Manager__c = u.Id;
        ini.Category_Specialist__c = u.Id;
        
        insert ini; 
        
        Id bvsRecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('Cat-Project Business Value Submission').getRecordTypeId();

        
        PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
        BVS.Category__c = cat.Id;
        BVS.Discipline__c = disp.Id;
        BVS.Sub_Category__c = sub.Id;
        BVS.Initiative_Id__c = ini.Id;
        BVS.SCM_Manager__c = u.Id;
        BVS.Specialist_Rep__c = u.Id;
        BVS.Contract_Purchase_Approve_Date__c = Date.newInstance(2015, 4, 10);
        //BVS.Local_Sourcing__c = 10;
        //BVS.Global_Sourcing__c = 90;
        BVS.Short_Term_Interests_Rate__c = 1.50;
        BVS.RecordTypeId = bvsRecordTypeId;
        BVS.Contract_Start_Date__c = Date.newInstance(2015, 4, 10);
        BVS.Contract_End_Date__c = Date.newInstance(2015, 12, 10);
        BVS.Awarded_Spend__c = 100000000;
        BVS.Rate_Reduction_Increase__c = 100000000;
        
        insert BVS;
        
        Id scheRecordTypeId = Schema.SObjectType.PREP_Business_Value_Spend_Schedule__c.getRecordTypeInfosByName().get('Monthly Business Value Savings Schedule').getRecordTypeId();
        
        PREP_Business_Value_Spend_Schedule__c sche = new PREP_Business_Value_Spend_Schedule__c();
        
        sche.Spend_Allocated__c = 30000;
        sche.Savings_To_Schedule__c = 0;
        sche.Start_Month__c = Date.newInstance(2015, 5, 10);
        sche.End_Month__c = Date.newInstance(2015, 10, 10);
        sche.Current_Value_Classification__c = 'Cost Avoidance';
        sche.Value_Source__c = 'Rate Reduction';
        sche.RecordTypeId = scheRecordTypeId;
        sche.Business_Value_Submission_Id__c = BVS.Id;
        
        insert sche;
        
        PREP_Business_Value_Spend_Schedule__c sche2 = new PREP_Business_Value_Spend_Schedule__c();
        
        sche2.Spend_Allocated__c = 30000;
        sche2.Savings_To_Schedule__c = 0;
        sche2.Start_Month__c = Date.newInstance(2015, 12, 10);
        sche2.End_Month__c = Date.newInstance(2015, 12, 10);
        sche2.Current_Value_Classification__c = 'Cost Avoidance';
        sche2.Value_Source__c = 'Rate Reduction';
        sche2.RecordTypeId = scheRecordTypeId;
        sche2.Business_Value_Submission_Id__c = BVS.Id;
        
        insert sche2;
        
        PREP_Business_Value_Spend_Schedule__c sche3 = new PREP_Business_Value_Spend_Schedule__c();
        
        sche3.Spend_Allocated__c = 30000;
        sche3.Savings_To_Schedule__c = 0;
        sche3.Start_Month__c = Date.newInstance(2015, 11, 10);
        sche3.End_Month__c = Date.newInstance(2015, 11, 10);
        sche3.Current_Value_Classification__c = 'Cost Avoidance';
        sche3.Value_Source__c = 'Rate Reduction';
        sche3.RecordTypeId = scheRecordTypeId;
        sche3.Business_Value_Submission_Id__c = BVS.Id;
        
        insert sche3;
        
     }     
}