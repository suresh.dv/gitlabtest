global class BoreholeProductionXML
{
    public class JSONresult
    {
        public Integer Row;
        public Id SFID;
        public String GUID;
        public String Status;
        public String Error;
    }
    
    public BoreholeProductionXML(BoreholeProductionController controller)
    {}
    
    @RemoteAction
    global static Id createWellProduction(Id      boreholeId,
                                          String  calMonth,
                                          String  prodType,
                                          Decimal overrideValue)
    {
        
        GRD_Borehole__c borehole = [SELECT Id, GRD_GUID__c FROM GRD_BOREHOLE__c WHERE Id =: boreholeId];
        
        String monthString = calMonth.substring(0,3);
        
system.debug('mikep calMonth '+calMonth);
        
        Integer year = Integer.valueOf('20'+calMonth.substring(4,6));
        
system.debug('mikep year '+year);
        
        Integer month = 1;
        if(monthString == 'Jan') month = 1;
        if(monthString == 'Feb') month = 2;
        if(monthString == 'Mar') month = 3;
        if(monthString == 'Apr') month = 4;
        if(monthString == 'May') month = 5;
        if(monthString == 'Jun') month = 6;
        if(monthString == 'Jul') month = 7;
        if(monthString == 'Aug') month = 8;
        if(monthString == 'Sep') month = 9;
        if(monthString == 'Oct') month = 10;
        if(monthString == 'Nov') month = 11;
        if(monthString == 'Dec') month = 12;
        
        Date calDate = Date.newInstance(year,month,1);
        
system.debug('mikep calDate '+calDate);
        
        Well_Production__c wp = new Well_Production__c();
        if(prodType == 'OIL')
            wp.Oil_Override_bbl__c = overrideValue;
        else if(prodType == 'GAS')
            wp.Gas_Override_mcf__c = overrideValue;
        else if(prodType == 'NGL')
            wp.NGL_Override_bbl__c = overrideValue;
        
        monthString = String.valueOf(calDate.Month());
        if(monthString.length() == 1)
            monthString = '0'+monthString;
        String YYYYMM = String.valueOf(year) + monthString;
        wp.YYYYMM__c = YYYYMM;
        wp.Borehole__c = boreholeId;
        wp.External_Id__c = borehole.GRD_GUID__c+'-'+YYYYMM;
system.debug('mikep wp.External_Id__c '+wp.External_Id__c);
        
        try
        {
            insert wp;
        }
        catch (Exception e1)
        {
            // maybe the record already exists?
            try
            {
                List<Well_Production__c> existingProduction = [SELECT Id,
                                                                      Oil_Override_bbl__c,
                                                                      Gas_Override_mcf__c,
                                                                      NGL_Override_bbl__c
                                                               FROM Well_Production__c
                                                               WHERE External_Id__c =: wp.External_Id__c];
                
                if(prodType == 'OIL')
                    existingProduction[0].Oil_Override_bbl__c = overrideValue;
                else if(prodType == 'GAS')
                    existingProduction[0].Gas_Override_mcf__c = overrideValue;
                else if(prodType == 'NGL')
                    existingProduction[0].NGL_Override_bbl__c = overrideValue;
                
                update existingProduction;
                return existingProduction[0].Id;
            }
            catch (Exception e2)
            {
                throw e1;
            }
        }
        return wp.Id;
    }
    
    @RemoteAction
    global static void updateWellProduction(Id      boreholeId,
                                            Id      SFID,
                                            String  prodType,
                                            Decimal overrideValue)
    {
        Well_Production__c wp = new Well_Production__c(Id = SFID);
        
        if(prodType == 'OIL')
            wp.Oil_Override_bbl__c = overrideValue;
        else if(prodType == 'GAS')
            wp.Gas_Override_mcf__c = overrideValue;
        else if(prodType == 'NGL')
            wp.NGL_Override_bbl__c = overrideValue;
        
        update wp;
    }
    
    // Implement remote action to do a roundtrip call to mulesoft by generating XML and sending it to mulesoft
    @RemoteAction
    global static Map<String,List<List<String>>> XMLWellProduction(String SFID,
                                              String action,
                                              String calMonth,
                                              String productionType,
                                              String manualOverride,
                                              String GUID,
                                              String userEmail,
                                              String schemaName)
    {
        String monthString = calMonth.substring(0,3);
        String year = '20'+calMonth.substring(4,6);
        String month = '';
        if(monthString == 'Jan') month = '01';
        if(monthString == 'Feb') month = '02';
        if(monthString == 'Mar') month = '03';
        if(monthString == 'Apr') month = '04';
        if(monthString == 'May') month = '05';
        if(monthString == 'Jun') month = '06';
        if(monthString == 'Jul') month = '07';
        if(monthString == 'Aug') month = '08';
        if(monthString == 'Sep') month = '09';
        if(monthString == 'Oct') month = '10';
        if(monthString == 'Nov') month = '11';
        if(monthString == 'Dec') month = '12';
        
        String YYYYMM = year + month;
        
        Dom.Document doc = generateXML(SFID, action, YYYYMM, productionType, manualOverride, GUID, userEmail);

        Map<String,List<List<String>>> result = sendXMLtoMule(doc, schemaName, SFID);
        
        return result;
    }

    public static Dom.Document generateXML(String SFID,
                                           String action,
                                           String YYYYMM,
                                           String productionType,
                                           String manualOverride,
                                           String GUID,
                                           String userEmail)
    {
        // Xml created using DOM Xml
        // Create the request envelope
        DOM.Document doc = new DOM.Document();
        
        // Set up the URL to the namespace
        String namespace    = 'http://wcp.huskyenergy.com/wellproduction';
        
        /* Start well_message block */
        // Create the root element wellMessage and add attributes needed
        dom.XmlNode wellMessage = doc.createRootElement('well_message', namespace, 'wcp');
        wellMessage.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        wellMessage.setAttribute('xsi:schemaLocation', 'http://wcp.huskyenergy.com/wellproduction http://wcp.huskyenergy.com/wellproduction');

        /* Start header information for well_message block */
        
        // session_id
        // This is the email of the user that sent the create action concat. to the current timestamp
        wellMessage.addChildElement('session_id', namespace, 'wcp').addTextNode(userEmail+'@'+string.valueof(System.now()).replace(' ', 'T'));
        
        // message_dttm
        // This is the date/time for the message
        wellMessage.addChildElement('message_dttm', namespace, 'wcp').addTextNode(string.valueof(System.now()).replace(' ', 'T'));
        
        // user_email
        // This is the email of the user that sent the create action
        wellMessage.addChildElement('user_email', namespace, 'wcp').addTextNode(UserInfo.getUserEmail());
        
        // user_SAMID
        // This is the name of the user that sent the create action
        wellMessage.addChildElement('user_SAMID', namespace, 'wcp').addTextNode(UserInfo.getName());
        
        // messageAction
        // This is one of CREATE, UPDATE, or DELETE.
        wellMessage.addChildElement('action', namespace, 'wcp').addTextNode(action);
        
        // GUID
        // This is the wellbore GUID
        wellMessage.addChildElement('GUID', namespace, 'wcp').addTextNode(GUID);
        
        // SFID
        // This is the Well Production Salesforce Id
        wellMessage.addChildElement('SFID', namespace, 'wcp').addTextNode(SFID);
        
        // yearmonth
        // This is the YYYYMM field
        wellMessage.addChildElement('yearmonth', namespace, 'wcp').addTextNode(YYYYMM);
        
        // production_type
        // Either Oil, Gas, NGL
        wellMessage.addChildElement('production_type', namespace, 'wcp').addTextNode(productionType);
        
        // override
        // The override value being set
        if(action != 'DELETE')
            wellMessage.addChildElement('override', namespace, 'wcp').addTextNode(manualOverride);
        
        /* End well_message block */

        // Return the XML Doc
        return doc;
    }

    // Sends an XML string to Mulesoft to be passed to a WCP database
    public static Map<String,List<List<String>>> sendXMLtoMule(DOM.Document doc, String schemaName, String SFID)
    {
        // Get certificate name
        USCP_Settings__c mc = USCP_Settings__c.getOrgDefaults();
        String WS_CertificateName = mc.HEI_WS_CertificateName__c;
        String muleHost = getMuleHost();
        
        // Where we're sending the XML to
        //String endpointUrl = 'https://salesforce-wcp.huskyenergy.com:8001/biPortalGrd/'+schemaName+'/production';
        String endpointUrl = 'https://salesforce-wcp' + muleHost + '.huskyenergy.com:8001/biPortalGrd/'+schemaName+'/production';
        
        // Instantiate a new http object
        Http h = new Http();    
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        
        // The body is the XML string
        req.setBody(doc.toXmlString());
        
system.debug(doc.toXmlString());
        
        // Tell the http request where to go
        req.setEndpoint(endpointUrl);
        
        // Specify certificate
        if(WS_CertificateName != null) req.setClientCertificateName(WS_CertificateName);
        
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml');
        
        // This is the response we'll get back
        HttpResponse res;
        
        
        
        Map<String,List<List<String>>> results = new Map<String,List<List<String>>>();
//        return results;
        
        
        
        
        
        // Send the request, and return a response
        if(!Test.isRunningTest())
        {
            res = h.send(req);
        
System.debug('XML to Mule reponse is '+res);
System.debug('res.getBody() '+res.getBody());
System.debug(res.getStatusCode()+' : '+res.getStatus());
        }
        
        results.put('Success',new List<List<String>>());
        results.put('Fail',new List<List<String>>());
        
        if(!Test.isRunningTest())
        {
            if(res.getStatusCode() != 200)
            {
                List<String> error = new List<String>();
                error.add(SFID);
                error.add('Error: Could not connect to database.');
                results.get('Fail').add(error);
                return results;
            }
            else
            {
                List<JSONresult> resultsList = (List<JSONresult>)JSON.deserialize(res.getBody(), List<JSONresult>.class);
                for(JSONresult result : resultsList)
                {
                    if(result.Status == 'SUCCESS')
                    {
                        List<String> error = new List<String>();
                        error.add(result.SFID);
                        error.add('');
                        results.get('Success').add(error);
                    }
                    else
                    {
                        List<String> error = new List<String>();
                        error.add(result.SFID);
                        error.add(result.Error);
                        results.get('Fail').add(error);
                    }
                }
                return results;
            }
        }
        return results;
    }
    
    private static String getMuleHost(){
        String hostName = URL.getSalesforceBaseUrl().getHost().split('\\.')[0].toLowerCase();
        
        //TODO : Use enum ??
        if(hostName.containsIgnoreCase('--dev')){
            return '-dv';
        }
        
        if(hostName.containsIgnoreCase('--test')){
            return '-qa';
        }  
        
        if(hostName.containsIgnoreCase('--ps')){
            return '-ps';
        }                 
        
        return '';
    }    
}