public class SwiftCurrentController
{
    public Boolean hasChatterAccess {get;set;}
    
    public String spotfireURL {get;set;}
    
    public String SwiftCurrentTeamChatterGroup {get;set;}
    
    public List<AssetWrapper> assets {get;set;}
    
    public class AssetWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String icon {get;set;}
        public Boolean isActive {get;set;}
        
        public AssetWrapper(String URL, String name, String icon, Boolean active)
        {
            this.name = name;
            this.URL = URL;
            this.icon = icon;
            isActive = active;
        }
    }
    
    public List<TeamWrapper> teams {get;set;}
    
    public class TeamWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String pic  {get;set;}
        
        public TeamWrapper(String URL, String name, String pic)
        {
            this.name = name;
            this.URL = URL;
            this.pic = pic;
        }
    }
    
    public SwiftCurrentController()
    {
        AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
        
        spotfireURL = spotfireURLs.SwiftCurrentSpotfireSession__c;
        
        List<String> chatterNames = new List<String>{'Swift Current/Estevan','Swift Current Reservoir','Swift Current Development','Swift Current D&C','Swift Current Production Ops','Swift Current Finance','Swift Current G&G','Swift Current BPI'};
        
        Map<String,Id> chatterName2Id = new Map<String,Id>();
        
        for(CollaborationGroup cg : [SELECT Id,Name FROM CollaborationGroup WHERE Name IN: chatterNames])
        {
            chatterName2Id.put(cg.Name,cg.Id);
        }
        
        SwiftCurrentTeamChatterGroup = '';
        if(chatterName2Id.containsKey('Swift Current/Estevan'))
            SwiftCurrentTeamChatterGroup = chatterName2Id.get('Swift Current/Estevan');
        
        // Does the current user have access to the Swift Current Chatter Group?
        hasChatterAccess = false;
        List<CollaborationGroupMember> SwiftCurrentGroupMembers = [SELECT Id FROM CollaborationGroupMember WHERE CollaborationGroupId =: SwiftCurrentTeamChatterGroup AND MemberId =: UserInfo.getUserId()];
        if(SwiftCurrentGroupMembers.size() > 0)
            hasChatterAccess = true;
        
        assets = new List<AssetWrapper>();
        assets.add(new AssetWrapper('/apex/WCPWells?show=Western Canada Production', 'Wells',  'wells.png', true));
        assets.add(new AssetWrapper('#','Routes / Pipelines', 'pipelines.png', false));
        assets.add(new AssetWrapper('#','Facilities', 'facilities.png', false));
        
        teams = new List<TeamWrapper>();
        
        if(chatterName2Id.containsKey('Swift Current Reservoir'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current Reservoir'),'Reservoir','Reservoir.png'));
        else
            teams.add(new TeamWrapper('#','Reservoir','Reservoir.png'));
        
        if(chatterName2Id.containsKey('Swift Current G&G'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current G&G'),'G & G','GandG.jpg'));
        else
            teams.add(new TeamWrapper('#','G & G','GandG.jpg'));
        
        if(chatterName2Id.containsKey('Swift Current Development'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current Development'),'Development','Development.jpg'));
        else
            teams.add(new TeamWrapper('#','Development','Development.jpg'));
        
        if(chatterName2Id.containsKey('Swift Current Production Ops'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current Production Ops'),'Production Ops','Production.jpg'));
        else
            teams.add(new TeamWrapper('#','Production Ops','Production.jpg'));
        
        if(chatterName2Id.containsKey('Swift Current BPI'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current BPI'),'BPI','BPI.jpg'));
        else
            teams.add(new TeamWrapper('#','BPI','BPI.jpg'));
        
        if(chatterName2Id.containsKey('Swift Current Finance'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current Finance'),'Finance','Finance.jpg'));
        else
            teams.add(new TeamWrapper('#','Finance','Finance.jpg'));
        
        if(chatterName2Id.containsKey('Swift Current D&C'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Swift Current D&C'),'D & C','DandC.png'));
        else
            teams.add(new TeamWrapper('#','D & C','DandC.png'));
    }
}