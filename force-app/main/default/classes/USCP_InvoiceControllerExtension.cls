/*
this is a controller extension class for USCP_Invoice VF page
I am going to read pdf blob from husky web service as a byte array and save it in the personal document folder, then display it to the user
the reason why I have to save pdf as a document first is because I didn't find a way to properly display pdf on VF page, 
so far the only work around would be to save it as a document in personal documents folder, extract url for this document and then redirect user to this url
*/
public class USCP_InvoiceControllerExtension {

    private final USCP_Invoice__c invoice;
    
    public string pdfDocumentID {get; private set;}
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public USCP_InvoiceControllerExtension (ApexPages.StandardController stdController) {
        List<string> fields = new List<String>{'Name','Account__r.Customer_Number__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    
    
        this.invoice = (USCP_Invoice__c)stdController.getRecord();
    }
    
    public PageReference DownloadPdf()
    {
    
            try
            {
            
                USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
                String WS_Endpoint = mc.HEI_WS_Endpoint__c; 
                String WS_CertificateName = mc.HEI_WS_CertificateName__c;              
                String endpointUrl= WS_Endpoint.replace('{0}',invoice.Account__r.Customer_Number__c).replace('{1}',invoice.Name ); 
                //'https://salesforce-uscp-dv.huskyenergy.com:8001/uscp-invoice-file/invoice/?customer=' + invoice.Account__r.Customer_Number__c + '&invoice=' + invoice.Name ;
            
                
                // Instantiate a new http object
                Http h = new Http();
                // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
                HttpRequest req = new HttpRequest();
                System.debug(endpointUrl);
                System.debug(WS_CertificateName);
                req.setEndpoint(endpointUrl);
                if(WS_CertificateName != null) req.setClientCertificateName(WS_CertificateName);
                req.setTimeout(60000);
                System.debug('req.getBody()>>>>>>'+req.getBody());
                req.setMethod('GET');
                // Send the request, and return a response
                HttpResponse res = h.send(req);
                System.debug('res is>>>>>>'+res);
                System.debug('res.getBody()>>>>>>'+res.tostring());
                    
                
        if(res.getStatusCode()>299) {
          System.debug('ERROR: '+res.getStatusCode()+': '+res.getStatus());
          System.debug(res.getBody());
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR! ' + res.getStatusCode()+': '+res.getStatus()));  
          
          String errorMessage = '';
          if(USCP_Utils.IsAdminUser())
          {
             errorMessage = 'Web service call to retrieve invoice PDF failed with status: ' + res.getStatusCode()+' '+res.getStatus() ;
          }
          else
          {
            errorMessage ='Invoice PDFs are currently unavailable.  Please try again shortly.';    
          }

          USCP_Utils.logError(errorMessage);
        } else {                
                System.debug('res.getBody()>>>>>>'+res.getBody());
                //Blob pdfContent= Blob.valueOf(res.getBody());
                Blob pdfContent= res.getBodyAsBlob();
    
            
                //System.debug('pdfContent>>>>'+pdfContent);
                
                //let see if Invoice document with name 'invoice.pdf' already there
                Document d;
                List<Document> doclist = [select id, name from Document where folderid = :UserInfo.getUserId() and name = 'invoice.pdf'];
                if(doclist.size() == 0 )
                {
                    d = new Document();
                    d.name = 'invoice.pdf';
                    d.body = pdfContent;
                    d.folderId = UserInfo.getUserId();
                    insert d;
                }
                else
                {
                    d = doclist[0];
                    d.body = pdfContent;
                    update d;
                } 
                system.debug(d.id); 
                pdfDocumentID = d.id;
                
                String docUrl = '/servlet/servlet.FileDownload?file='+ d.Id + '&id=' + Math.random();
                PageReference page = new PageReference(docUrl);  
                page.setRedirect(true);
                return page;
            }
        }
        catch (Exception e) 
            {  
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+ e));        
                USCP_Utils.logError(e);
            } 
                
        return null;
            
    }    
    
}