public with sharing class PCR_BypassController {

    //public ID changeId = System.currentPagereference().getParameters().get('id');
    public ID changeId = ApexPages.currentPage().getParameters().get('Change_Request');
    public Flow.Interview.PCR_Bypass_Review bypassReview{get;set;}
    private Change_Request__c c;
    
    public ID returnId = changeId;
    
    public PCR_BypassController(ApexPages.StandardController controller){
        this.c = (Change_Request__c)controller.getRecord();
        
    }
    
    public String getChangeRequest(){
        System.debug('getChangeRequest.changeId = ' + changeId); 
        return changeId; 
    }
    
    public PageReference getfinishlocation(){
        System.debug('changeId = ' + changeId);
        Change_Request__c c = [select id,Bypass_Review__c from Change_Request__c where id =: changeId];
        
        if(c != null){
            if(c.Bypass_Review__c){
                //bypass review
                PageReference send = new PageReference('apex/Change_Request_View?id=' + returnId);
                send.setRedirect(true);
                return send;
            } else {
                //do not bypass review
                return null;
            }
        } else {  
            PageReference send = new PageReference('/'); 
            send.setRedirect(true);         
            return send;
        } 
        
        return null;
    }
}