@isTest
public class TestPurgeVolumeWeeklyBatchSchedule
{
        @isTest 
    public static void testScheduledBatchJob() 
    {
        // First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));

        Account acct = new Account(Name = 'Account 1');
        insert acct;

        Volume_Transaction__c vts = new Volume_Transaction__c(
            Account__c = acct.Id,
            Card_Type__c = 'RC',
            Product_Type__c = '0131F',
            Profit_CPL__c = 100,
            PYTD_Volume__c = 200,
            Total_Profit__c = 300,
            Total_PYTD_Profit__c = 400,
            Total_PYTD_Profit_CPL__c = 500,
            Total_YTD_Profit__c = 600,
            Volume_Transaction__c = 700,
            YTD_Profit_CPL__c = 800,
            YTD_Volume__c = 900,
            Transaction_Week_Ending_Date__c = lastSundayDate
        );
        insert vts;

        final Id cronTriggerId;
 
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'BatchApex']);

        for (CronTrigger cronTrigger : [SELECT Id FROM CronTrigger where state != 'EXECUTING']) System.abortJob(cronTrigger.Id);
        
        Test.startTest();
        // 1: second, 2: minute, 3: hour, 4: day of month, 5: month, 6: day of week, and optional 7: year
        // 0 0 1 ? * TUE means every Tuesday at 1am.  Second = 0, Minute = 0, Day of Month is not specified, Month is every month.
        cronTriggerId = System.schedule('PurgeVolumeWeeklyBatchSchedule', '0 0 1 ? * TUE', new PurgeVolumeWeeklyBatchSchedule());
        Test.stopTest();

        // This proves that the Scheduled Job has kicked off the Batch Job.
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'BatchApex']);
                                
        // Both jobs' status should be Queued
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'ScheduledApex'].Status);
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeWeeklyBatchSchedule') AND JobType = 'BatchApex'].Status);
    }

    private static void createTestData(Integer pWeekInWeekly, Integer pNumMissingWeeks)
    {
        // First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));

        Account acct = new Account(Name = 'Account 1');
        insert acct;

        // create pWeekInWeekly records in the weekly object and (pWeekInWeekly - pNumMissingWeeks) records in summary object.
        // After the batch is run, there should be pWeekInWeekly records left in the weekly object.
        Date sundayDateIndex = lastSundayDate;
        List<Volume_Transaction__c> vtList = new List<Volume_Transaction__c>();
        for (Integer i = 0; i < pWeekInWeekly; i++)
        {
            Volume_Transaction__c vt = new Volume_Transaction__c(
                Account__c = acct.Id,
                Card_Type__c = 'RC',
                Product_Type__c = '0131F',
                Profit_CPL__c = 100,
                PYTD_Volume__c = 200,
                Total_Profit__c = 300,
                Total_PYTD_Profit__c = 400,
                Total_PYTD_Profit_CPL__c = 500,
                Total_YTD_Profit__c = 600,
                Volume_Transaction__c = 700,
                YTD_Profit_CPL__c = 800,
                YTD_Volume__c = 900,
                Transaction_Week_Ending_Date__c = sundayDateIndex
            );
            vtList.add(vt);
            sundayDateIndex = sundayDateIndex.addDays(-7);
        }   
        insert vtList;
        
        // now insert records into the summary object
        sundayDateIndex = lastSundayDate;
        List<Volume_Transaction_Summary__c> vtsList = new List<Volume_Transaction_Summary__c>();
        for (Integer i = 0 ; i < pWeekInWeekly - pNumMissingWeeks ; i++)
        {
            Volume_Transaction_Summary__c vts = new Volume_Transaction_Summary__c(
                Account__c = acct.Id,
                Total_Profit__c = 100, 
                Total_Profit_PYTD__c = 200,
                Total_Profit_YTD__c = 300,
                Volume__c = 400,
                Volume_PYTD__c = 500,
                Volume_YTD__c = 600,
                Transaction_Week_Ending_Date__c = sundayDateIndex
            );
            vtsList.add(vts);
            sundayDateIndex = sundayDateIndex.addDays(-7);
        }
        insert vtsList;
    }
    
    @isTest
    public static void testKeep3InWeeklyNoMissingWeeks()
    {
        // No missing week so there won't be an extra email sent to system admins.  The email invocations will be zero.
        // (The one in Finish notifying the system email the process has completed, is not accoutned for in test methods.)
        Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
        csSettings.Num_Weeks_Of_Weekly_Data_To_Keep__c = 3;
        insert csSettings;
        
        createTestData(30, 0);
        
        system.assertEquals(30, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction__c][0].get('Counter'));
        system.assertEquals(30, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter'));
        system.assertEquals(false, PurgeVolumeWeeklyBatchSchedule.isMissingWeekEMailsSent);
        system.assertEquals(false, PurgeVolumeWeeklyBatchSchedule.isPurgeSuccessEMailSent);
        Test.startTest();
        Database.executeBatch(new PurgeVolumeWeeklyBatchSchedule());
        Test.stopTest();
        system.assertEquals(false, PurgeVolumeWeeklyBatchSchedule.isMissingWeekEMailsSent);
        system.assertEquals(true, PurgeVolumeWeeklyBatchSchedule.isPurgeSuccessEMailSent);
        system.assertEquals(3, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction__c][0].get('Counter'));
        system.assertEquals(30, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter'));
    }
    
    @isTest
    public static void testKeep6InWeekly20InSummaryWith5MissingWeeks()
    {
        // this should cause some email code to execute.  We send out email in the constructor so we'll be able to detect
        // email invocations.
        // If there are 5 missing weeks (i.e. 5 week of data that are not in
        // summary), then those 5 weeks of data are not removed.  And then, 6 weeks of summarized data to keep, so altogether
        // there are 11 weeks of data in the Weekly object.
        Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
        csSettings.Num_Weeks_Of_Weekly_Data_To_Keep__c = 6;
        insert csSettings;
        
        createTestData(40, 5);
        system.assertEquals(40, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction__c][0].get('Counter'));
        system.assertEquals(35, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter'));
        system.assertEquals(false, PurgeVolumeWeeklyBatchSchedule.isMissingWeekEMailsSent);
        system.assertEquals(false, PurgeVolumeWeeklyBatchSchedule.isPurgeSuccessEMailSent);
        Test.startTest();
        Database.executeBatch(new PurgeVolumeWeeklyBatchSchedule());
        Test.stopTest();
        system.assertEquals(true, PurgeVolumeWeeklyBatchSchedule.isMissingWeekEMailsSent);
        system.assertEquals(true, PurgeVolumeWeeklyBatchSchedule.isPurgeSuccessEMailSent);
        system.assertEquals(11, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction__c][0].get('Counter'));
        system.assertEquals(35, (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter'));
    }
}