@isTest
public class TestPurgeVolumeSummaryBatchSchedule
{
    
    @isTest 
    public static void testScheduledBatchJob() 
    {
        // First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));

        Account acct = new Account(Name = 'Account 1');
        insert acct;

        Volume_Transaction_Summary__c vts = new Volume_Transaction_Summary__c(
            Account__c = acct.Id,
            Total_Profit__c = 100, 
            Total_Profit_PYTD__c = 200,
            Total_Profit_YTD__c = 300,
            Volume__c = 400,
            Volume_PYTD__c = 500,
            Volume_YTD__c = 600,
            Transaction_Week_Ending_Date__c = lastSundayDate
        );
        insert vts;

        final Id cronTriggerId;
 
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'BatchApex']);

        for (CronTrigger cronTrigger : [SELECT Id FROM CronTrigger where state != 'EXECUTING']) System.abortJob(cronTrigger.Id);
        
        
        
        Test.startTest();
        // 1: second, 2: minute, 3: hour, 4: day of month, 5: month, 6: day of week, and optional 7: year
        // 0 0 1 ? * TUE means every Tuesday at 1am.  Second = 0, Minute = 0, Day of Month is not specified, Month is every month.
        cronTriggerId = System.schedule('PurgeVolumeSummaryBatchSchedule', '0 0 1 ? * TUE', new PurgeVolumeSummaryBatchSchedule());
        Test.stopTest();

        // This proves that the Scheduled Job has kicked off the Batch Job.
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'BatchApex']);
                                
        // Both jobs' status should be Queued
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'ScheduledApex'].Status);
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'PurgeVolumeSummaryBatchSchedule') AND JobType = 'BatchApex'].Status);
                                 
    }

    private static void createTestData(Integer pMode)
    {
        // if pMode = 1, then create exactly 2 years of data
        // if pMode = 2, then create just more than 2 years of data
        // if pMode = 3, then create 3 years of data.
        // for pMode = 1, the before and after record count in volume_transaction_summary__c should be identical
        // for pMode = 2, the record count diff should be 1.
        // for pMode = 3, the record count should be diff by at least 51, and the farthest transaction date should be within 2 years from the nearest transaction date.

        // First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
        Date twoYearsPriorToLastSunday = lastSundayDate.addYears(-2);
        Date batchStartSunday;
        
        if (pMode == 1)
        {
            // Note that this date may not be a Sunday.  TO test exactly two years, find the following Sunday.
            batchStartSunday = twoYearsPriorToLastSunday.addDays(Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
        }
        else if (pMode == 2)
        {
            batchStartSunday = twoYearsPriorToLastSunday.addDays(-1 * Math.abs(Math.mod(twoYearsPriorToLastSunday.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
        }
        else if (pMode == 3)
        {
            batchStartSunday = lastSundayDate.addYears(-3);
        }
        // Now we'll add records to summary file from batchStartSunday to lastSundayDate.  Running the purge job against this data set should not
        // end up deleting any records.

        Account acct = new Account(Name = 'Account 1');
        insert acct;

        List<Volume_Transaction_Summary__c> vtslist = new List<Volume_Transaction_Summary__c>();
        for (Date i = batchStartSunday; i <= lastSundayDate; i = i.addDays(7))
        {
            Volume_Transaction_Summary__c vts = new Volume_Transaction_Summary__c(
                Account__c = acct.Id,
                Transaction_Week_Ending_Date__c = i
            );
            vtslist.add(vts);   
        }
        insert vtslist;
    }
    
    @isTest
    public static void testExactly2YearsOfData()
    {
        // The batch always looks at the most current transaction date, subtract 2 years from it, and then trim any records whose transaction date is
        // strictly before that date.
        // So if the most current transaction date is 2013-08-25, and we have data from 2011-08-28 to 2013-08-25, then the number of records in
        // volume summary object before and after remain the same.
        
        createTestData(1);
        
        Integer beforeCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        Test.startTest();
        Database.executeBatch(new PurgeVolumeSummaryBatchSchedule());
        Test.stopTest();
        Integer afterCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assertEquals(beforeCount, afterCount);
    }

    @isTest
    public static void testJustOver2YearsOfData()
    {
        // The batch always looks at the most current transaction date, subtract 2 years from it, and then trim any records whose transaction date is
        // strictly before that date.
        // So if the most current transaction date is 2013-08-25, and we have data from 2011-08-21 to 2013-08-25, then the number of records in
        // volume summary object before and after should be off by one.
        
        createTestData(2);
        
        Integer beforeCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        Test.startTest();
        Database.executeBatch(new PurgeVolumeSummaryBatchSchedule());
        Test.stopTest();
        Integer afterCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assert(beforeCount == (afterCount + 1));
    }

    @isTest
    public static void test3YearsOfData()
    {
        // The batch always looks at the most current transaction date, subtract 2 years from it, and then trim any records whose transaction date is
        // strictly before that date.
        // So if the most current transaction date is 2013-08-25, and we have data from for 3 years, then the number of records in
        // volume summary object before and after should be off by at least 51.  Will also check the range of dates in Volume_Transaction_Sunmmary__c
        // is less than 2 years.
        
        createTestData(3);
        
        Integer beforeCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        Test.startTest();
        Database.executeBatch(new PurgeVolumeSummaryBatchSchedule());
        Test.stopTest();
        Integer afterCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        Date maxDate = (Date)[Select Max(Transaction_Week_Ending_Date__c) batchDate FROM Volume_Transaction_Summary__c][0].get('batchDate');
        Date minDate = (Date)[Select Min(Transaction_Week_Ending_Date__c) batchDate FROM Volume_Transaction_Summary__c][0].get('batchDate');
        system.assert(beforeCount > (afterCount + 51));
        system.assert(maxDate.daysBetween(minDate) < 730);
    }   
    
    private static void createManualRunTestData()
    {
        Account acct = new Account(Name = 'Account 1');
        insert acct;

        // in this test, we use 2012 as an example.  There are 53 Sundays in 2012.  After specifying a specific Sunday,
        // there should be 52 records left in the summary file.  If specifying a non-Sunday, there should be 53 records left.
        // NOte that 2012, 1st Jan is a Sunday, and 2012, 30th December is a Sunday.
        
        Date batchStartSunday = Date.newInstance(2012, 1, 1);
        Date lastSundayDate = Date.newInstance(2012, 12, 30);
        
        List<Volume_Transaction_Summary__c> vtslist = new List<Volume_Transaction_Summary__c>();
        for (Date i = batchStartSunday; i <= lastSundayDate; i = i.addDays(7))
        {
            Volume_Transaction_Summary__c vts = new Volume_Transaction_Summary__c(
                Account__c = acct.Id,
                Transaction_Week_Ending_Date__c = i
            );
            vtslist.add(vts);   
        }
        insert vtslist;
    }
    
    @isTest
    public static void testManualRunOnASunday()
    {
        // this test tests a manual run, and assume the user has provided an actual Sunday.  If date is found, the data
        // for that week is gone.
        createManualRunTestData();
        
        Integer beforeCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assertEquals(53, beforeCount);
        Test.startTest();
        // 9 Sep 2012 is a Sunday
        Database.executeBatch(new PurgeVolumeSummaryBatchSchedule(Date.newInstance(2012, 9, 9)));
        Test.stopTest();
        Integer afterCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assertEquals(52, afterCount);
    }

    @isTest
    public static void testManualRunOnANonSunday()
    {
        // this test tests a manual run, and assume the user has provided a non Sunday.  If date is not found, 
        // no data is purged.
        createManualRunTestData();
        
        Integer beforeCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assertEquals(53, beforeCount);
        Test.startTest();
        // 8 Sep 2012 is NOT a Sunday, so no records should have been purged.
        Database.executeBatch(new PurgeVolumeSummaryBatchSchedule(Date.newInstance(2012, 9, 8)));
        Test.stopTest();
        Integer afterCount = (Integer)[SELECT Count(Id) Counter FROM Volume_Transaction_Summary__c][0].get('Counter');
        system.assertEquals(53, afterCount);
    }

}