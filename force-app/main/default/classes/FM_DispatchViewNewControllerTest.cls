@isTest
private class FM_DispatchViewNewControllerTest {

    static testMethod void DispatchViewTest()
    {
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        FM_Run_Sheet__c oMasterRunSheet = [SELECT Id, Date__c, Well__r.Route__r.Name FROM FM_Run_Sheet__c LIMIT 1];
        List<FM_Truck_Trip__c> lTrips = [SELECT Id, Name, Facility_Type__c,
                                            Load_Request__r.Source_Facility__c,
                                            Load_Request__r.Source_Facility__r.Facility_Name_For_Fluid__c,
                                            Load_Request__r.Source_Location__c,
                                            Load_Request__r.Source_Location__r.Well_Type__c,
                                            Load_Request__r.Source_Location__r.Fluid_Location_Ind__c,
                                            Location_Lookup__c,
                                            Run_Sheet_Lookup__r.Facility__c,
                                            Run_Sheet_Lookup__r.Well__c,
                                            Run_Sheet_Lookup__r.Well__r.Well_Type__c,
                                            Run_Sheet_Lookup__r.Well__r.Fluid_Location_Ind__c,
                                            Product__c,
                                            Facility__c,
                                            Shift__c,
                                            Load_Weight__c,
                                            Truck_Trip_Status__c,
                                            Shift_Day__c
                                         FROM FM_Truck_Trip__c];
        Account oAccount = [SELECT Id FROM Account LIMIT 1];
        Carrier__c oCarrier = [SELECT Id, Carrier_Name_For_Fluid__c, Enabled__c FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id,  Location_Name_For_Fluid__c, Fluid_Location_Ind__c  FROM Location__c LIMIT 1];
        
        // Misc Variables for Assignment
        List<SelectOption> lSelect;
        List<FM_DispatchViewNewController.TruckTripWrapper> lWrapper;
        List<Carrier__c> lCarriers = new List<Carrier__c>();
        lCarriers.add(oCarrier);

        FM_DispatchViewNewController.TruckTripWrapper oWrapper = new FM_DispatchViewNewController.TruckTripWrapper(lTrips[0], true, FM_Utilities.GetFacilityCapacitySummaryMap(lTrips));
        
        oWrapper.destinationConfirmationPopupState = new FM_DispatchViewNewController.PopupState();
        oWrapper.destinationConfirmationPopupState.confirmed = true;

        PageReference oPage;
        Date dToday = Date.today();
        Date dPrevious = dToday.addDays(-7);
        String sFormat = oMasterRunSheet.Date__c.format();
        String sTest = '';
        Boolean bTest;

        //Create Fake Search Filter
        FM_DispatchViewNewController.FilterWrapper testFilter = new FM_DispatchViewNewController.FilterWrapper();
        testFilter.runSheetDate = dPrevious;
        testFilter.searchRoutes = new List<String>{'999'};
        String testFilterString = JSON.serialize(testFilter);

        // Assign Search Variables
        ApexPages.currentPage().getParameters().put('filter', testFilterString);

        //////////////////
        /// START TEST ///
        //////////////////

        Test.startTest();

        // Create Controller Object
        FM_DispatchViewNewController oController = new FM_DispatchViewNewController();
        oController.selectAllCheckbox = true;

        // Wrapper Methods -> Null
        oPage = oWrapper.SplitFacilityAndLocation();

        // Reset Search Parameters
        oController.currentPageFilter.clear();
        testFilter.runSheetDate = null;
        testFilter.searchRoutes = null;
        testFilterString = JSON.serialize(testFilter);
        ApexPages.currentPage().getParameters().put('filter', testFilterString);

        // Wrapper Facility settings SHOULD be blank
        System.debug('ERR' + oWrapper.aTruckTrip.Facility_Type__c);
        System.assert(oWrapper.aTruckTrip.Facility_Type__c == null);
        System.assert(oWrapper.aTruckTrip.Facility_Lookup__c == null);
        System.assert(oWrapper.aTruckTrip.Location_Lookup__c == null);

        // Build Query with no filters (Truck Trip Instance Defaults to Today's Date)
        oController.BuildTruckTripSOQLQuery();
        System.assert(oController.currentPageFilter.runSheetDate == dToday);

        // Try Search with blank values -> SHOULD FAIL but return TRUE (pre-loaded search already set)
        oPage = oController.SearchTruckTrips();
        System.assert(oController.searchOccurred == false);

        // Standard Search
        oController.currentPageFilter.searchRoutes.add('999');
        oController.BuildTruckTripSOQLQuery();

        // Build Query with NULL Date and other Test Criteria
        oController.currentPageFilter.runSheetDate = null;
        oController.currentPageFilter.axle = 'test';
        oController.currentPageFilter.product = 'O';
        oController.currentPageFilter.shift = 'Night';
        oController.currentPageFilter.truckTripStatus = 'New';
        oController.currentPageFilter.searchRoutes.add('999');
        oController.BuildTruckTripSOQLQuery();
        sTest = oController.getResultsStats();

        // Stats String SHOULD indicate that NO loads were found (missing date)
        System.assert(String.isBlank(sTest));

        // Build Query using Test Location
        oWrapper.aFacilityPicklistValue = oLocation.Id + '|Location|' + oLocation.Location_Name_For_Fluid__c;
        oController.BuildTruckTripSOQLQuery();
        sTest = oController.getResultsStats();

        // Wrapper Methods -> Facility
        oPage = oWrapper.SplitFacilityAndLocation();

        lSelect = oWrapper.lLoadTypes;
        lSelect = oWrapper.facilitySelectOptions;

        //bTest = oWrapper.getWellDisabled();
        bTest = oWrapper.isDestinationFacilityAtCapacity;

        oPage = oWrapper.ClearFacility();
        oPage = oWrapper.CutomizeDestinationFavoritesRedirect();
        
        // Sort Toggle -> Sort Direction SHOULD change to Ascending
        oController.sortOptions.lastSortField = 'axle';
        oController.SortToggle();
        System.assert(oController.sortOptions.sortDirection == 'asc NULLS LAST');

        // Get "Dipsatch" View Search Title
        sTest = oController.getDipsatchViewSearchTitle();

        // Check Sort Field -> SHOULD be blank which defaults it to Shift
        sTest = oController.sortOptions.sortField;
        System.assertEquals(sTest, 'Shift__c');

        // Get Selected Value -> SHOULD be NULL
        sTest = oController.valueSelected;
        System.assertEquals(sTest, null);

        // Save Truck Trips
        oPage = oController.SaveTruckTrips();

        // Page Navigation
        oPage = oController.GoNext();
        oPage = oController.GoPrevious();
        oPage = oController.GoFirst();
        oPage = oController.GoLast();

        // Select All Truck Trips -> always returns NULL
        oPage = oController.ToggleAllTruckTripCheckboxes();
        System.assert(oPage == null);

        // Get Option Lists
        lSelect = oController.routeSelectOptionList;
        lSelect = oController.productSelectOptionList;
        lSelect = oController.axleSelectOptionList;
        lSelect = oController.facilitySelectOptionList;
        lSelect = oController.shiftSelectOptionList;

        lSelect = oController.statusSelectOptionListForSearch;
        lSelect = oController.statusSelectOptionListForEntry;
        lSelect = oController.statusSelectOptionListForEntry;
        lSelect = oController.carrierUnitSelectOptionsWithEmptyCarriers;


        //Test wrapper options
        Carrier__c enabledCarrier = [Select Id From Carrier__c Where Enabled__c = true];
        Carrier__c disabledCarrier = [Select Id From Carrier__c Where Enabled__c = false];
        Carrier_Unit__c disabledUnit = [Select Id From Carrier_Unit__c Where Enabled__c = false];
        oController.currentPageFilter.clear();
        oController.currentPageFilter.runSheetDate = Date.today();
        oController.currentPageFilter.searchRoutes = new List<String>{'999'};
        oController.searchTruckTrips();
        List<FM_DispatchViewNewController.TruckTripWrapper> wrapperList = oController.getCurrentTruckTripList();
        wrapperList.get(0).aTruckTrip.Carrier__c = enabledCarrier.Id;
        wrapperList.get(0).aTruckTrip.Unit__c = disabledUnit.Id;
        oController.SaveTruckTrips();
        for(Integer i=0; i < wrapperList.size(); i++) {
            FM_DispatchViewNewController.TruckTripWrapper wrapper = wrapperList.get(i);
            if(i < 2)System.assertNotEquals(null, wrapper.carrierUnit);
            if(i >= 2) System.assertNotEquals(null, wrapper.carrierUnitReadOnly);
        }

        //Test Cancel Reason and Pop-up
        wrapperList = oController.getCurrentTruckTripList();
        String originalStatus = wrapperList[0].aTruckTrip.Truck_Trip_Status__c;

        ApexPages.currentPage().getParameters().put('truckTripId', wrapperList[0].aTruckTrip.Id);
        wrapperList[0].aTruckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER;
        oController.openCancelPopup();
        System.assertNotEquals(originalStatus, wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        System.assertEquals(false, oController.showCancelPopup);
        System.assertEquals(null, oController.truckTripCancel);
        System.assertEquals(false, oController.isCancelError);
        System.assert(String.isBlank(oController.truckTripOldStatus));
        System.assert(String.isBlank(oController.cancelError));
        
        wrapperList[0].aTruckTrip.Truck_Trip_Status__c = originalStatus;

        ApexPages.currentPage().getParameters().put('truckTripId', wrapperList[0].aTruckTrip.Id);
        ApexPages.currentPage().getParameters().put('oldTruckTripStatus', wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        wrapperList[0].aTruckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
        oController.openCancelPopup();
        oController.closeCancelPopup();
        System.assertEquals(originalStatus, wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        System.assertEquals(false, oController.showCancelPopup);
        System.assertEquals(null, oController.truckTripCancel);
        System.assertEquals(false, oController.isCancelError);
        System.assert(String.isBlank(oController.truckTripOldStatus));
        System.assert(String.isBlank(oController.cancelError));

        ApexPages.currentPage().getParameters().put('truckTripId', wrapperList[0].aTruckTrip.Id);
        ApexPages.currentPage().getParameters().put('oldTruckTripStatus', wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        System.assertEquals(originalStatus, wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        wrapperList[0].aTruckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
        oController.openCancelPopup();
        oController.truckTripCancel.Load_Request_Cancel_Reason__c = 'Other';
        oController.confirmCancelPopup();
        System.assert(oController.isCancelError);
        System.assertEquals('Cancel Comments cannot be blank when Cancel Reason is "Other"', oController.cancelError);
        
        
        oController.truckTripCancel.Load_Request_Cancel_Reason__c = 'Something else';
        oController.truckTripCancel.Load_Request_Cancel_Comments__c = 'Other reason';
        oController.handleCancelPopupReasonChange();
        System.assert(String.isBlank(oController.truckTripCancel.Load_Request_Cancel_Comments__c));

        oController.handleCancelReasonChange();
        System.assert(String.isBlank(oController.truckTripCancel.Load_Request_Cancel_Comments__c));
        oController.truckTripCancel.Load_Request_Cancel_Reason__c = 'Other';
        oController.truckTripCancel.Load_Request_Cancel_Comments__c = 'Other reason';
        oController.confirmCancelPopup();
        System.assertNotEquals(originalStatus, wrapperList[0].aTruckTrip.Truck_Trip_Status__c);
        System.assertEquals(false, oController.showCancelPopup);
        System.assertEquals(null, oController.truckTripCancel);
        System.assertEquals(false, oController.isCancelError);
        System.assert(String.isBlank(oController.truckTripOldStatus));
        System.assert(String.isBlank(oController.cancelError));
        //


        //Check Redirect to Add Load Request Page
        System.assertEquals(Page.FM_AddLoadRequest.getUrl(), oController.AddLoadRequestsRedirect().getUrl());

        /////////////////

        /// STOP TEST ///
        /////////////////

        Test.stopTest();
        List<SelectOption> testVarList1 = oController.shiftSelectOptionForFilter;
        Boolean testVarBoo = oController.getDisplayWellColumn();
        testVarBoo = oController.getIsExportedTruckTrips();
        testVarList1  = oController.getLoadRequestCancelReasons();
        testVarBoo = wrapperList[0].isCleanLoad;
        testVarBoo = wrapperList[0].isDestinationFacilityPopulated;
    }

    static testMethod void MassUpdateStandardTest() {
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Carrier__c oCarrier = [SELECT Id FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        // BUILD MASS UPDATE DATA
        Map<String, String> mapMassUpdate = new Map<String, String>();
        mapMassUpdate.put('shift', 'Night');
        mapMassUpdate.put('status', 'Booked');
        mapMassUpdate.put('unit', oCarrier.Id + '-' + oCarrierUnit.Id);


        //////////////////
        /// START TEST ///
        //////////////////

        Test.startTest();

        FM_Utilities.executeTriggerCode = false;

        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();

        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();

        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();
        oUpdateController.selectAllCheckbox = true;
        oPage = oUpdateController.ToggleAllTruckTripCheckboxes();

        // MASS UPDATE DATA -> Run Through Values AND verify that ALL Wrappers have the applied value
        for(String sOneApply : mapMassUpdate.keySet())
        {
            oUpdateController.applyItem = sOneApply;
            oUpdateController.valueSelected = mapMassUpdate.get(sOneApply);

            oPage = oUpdateController.UpdateDependentFilter();
            oPage = oUpdateController.MassUpdateData();

            for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
                if(oUpdateController.applyItem == 'shift') {
                    System.assertEquals(wrapper.aTruckTrip.Shift__c, mapMassUpdate.get('shift'));
                }

                if(oUpdateController.applyItem == 'status') {
                    System.assertEquals(wrapper.aTruckTrip.Truck_Trip_Status__c, mapMassUpdate.get('status'));
                }

                if(oUpdateController.applyItem == 'unit') {
                    System.assertEquals(wrapper.aTruckTrip.Carrier__c, mapMassUpdate.get('unit').split('-')[0]);
                    System.assertEquals(wrapper.aTruckTrip.Unit__c,mapMassUpdate.get('unit').split('-')[1]);
                }
            }
        }

        //Check clear all
        oUpdateController.clearAllTruckTripCheckboxes();
        for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
            System.assertEquals(wrapper.aCheckBox, false);
        }
		oUpdateController.clearAllSelectCheckbox();
        /////////////////

        /// STOP TEST ///
        /////////////////

        Test.stopTest();
        
    }

    static testMethod void MassUpdateUnitTest(){
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Carrier__c oCarrier = [SELECT Id FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        // BUILD MASS UPDATE DATA
        Map<String, String> mapMassUpdate = new Map<String, String>();
        mapMassUpdate.put('unit', null);


        //////////////////
        /// START TEST ///
        //////////////////

        Test.startTest();

        FM_Utilities.executeTriggerCode = false;

        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();

        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();

        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();
        oUpdateController.selectAllCheckbox = true;
        oPage = oUpdateController.ToggleAllTruckTripCheckboxes();

        // MASS UPDATE DATA -> Run Through Values AND verify that ALL Wrappers have the applied value
        for(String sOneApply : mapMassUpdate.keySet()) {
            oUpdateController.applyItem = sOneApply;
            oUpdateController.valueSelected = mapMassUpdate.get(sOneApply);

            oPage = oUpdateController.MassUpdateData();
            oPage = oUpdateController.UpdateDependentFilter();

            for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
               if(oUpdateController.applyItem == 'unit') {
                    System.assertEquals(wrapper.aTruckTrip.Carrier__c, mapMassUpdate.get('unit').split('-')[0]);
                    System.assertEquals(wrapper.aTruckTrip.Unit__c,mapMassUpdate.get('unit').split('-')[1]);
                }
            }
        }

        // BUILD MASS UPDATE DATA
        Map<String, String> mapMassUpdateWithValue = new Map<String, String>();
        mapMassUpdateWithValue.put('unit', oCarrier.Id + '-' + oCarrierUnit.Id);

          // MASS UPDATE DATA -> Run Through Values AND verify that ALL Wrappers have the applied value
        for(String sOneApply : mapMassUpdateWithValue.keySet()) {
            oUpdateController.applyItem = sOneApply;
            oUpdateController.valueSelected = mapMassUpdateWithValue.get(sOneApply);

            oPage = oUpdateController.MassUpdateData();
            oPage = oUpdateController.UpdateDependentFilter();

            for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
               if(oUpdateController.applyItem == 'unit') {
                    System.assertEquals(wrapper.aTruckTrip.Carrier__c, mapMassUpdateWithValue.get('unit').split('-')[0]);
                    System.assertEquals(wrapper.aTruckTrip.Unit__c,mapMassUpdateWithValue.get('unit').split('-')[1]);
                }
            }
        }

        /////////////////
         /// STOP TEST ///
        /////////////////

        Test.stopTest();
    }

    static testMethod void MassUpdateStatusTest (){
         // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Carrier__c oCarrier = [SELECT Id FROM Carrier__c LIMIT 1];
        Carrier_Unit__c oCarrierUnit = [SELECT Id FROM Carrier_Unit__c LIMIT 1];
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        // BUILD MASS UPDATE DATA
        Map<String, String> mapMassUpdate = new Map<String, String>();
        mapMassUpdate.put('status', FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);
       

        //////////////////
        /// START TEST ///
        //////////////////

        Test.startTest();

        FM_Utilities.executeTriggerCode = false;

        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();

        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();

        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();
        oUpdateController.selectAllCheckbox = true;
        oPage = oUpdateController.ToggleAllTruckTripCheckboxes();

        // MASS UPDATE DATA -> Run Through Values AND verify that ALL Wrappers have the applied value
        for(String sOneApply : mapMassUpdate.keySet())
        {
            oUpdateController.applyItem = sOneApply;
            oUpdateController.valueSelected = mapMassUpdate.get(sOneApply);

            oPage = oUpdateController.MassUpdateData();
            oPage = oUpdateController.UpdateDependentFilter();

            for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
              
                if(oUpdateController.applyItem == 'status') {
                    System.assertEquals(wrapper.aTruckTrip.Truck_Trip_Status__c, mapMassUpdate.get('status'));
                }

            }
        }

        /////////////////
        /// STOP TEST ///
        /////////////////

        Test.stopTest();
        
    }

    static testMethod void MassUpdateFacilityTest()
    {
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        // BUILD MASS UPDATE DATA
        Map<String, String> mapMassUpdate = new Map<String, String>();
        mapMassUpdate.put('facility_null', '');
        mapMassUpdate.put('facility_facility', oFacility.Id + '|Facility|' + oFacility.Facility_Name_For_Fluid__c);
        mapMassUpdate.put('facility_location', oLocation.Id + '|Location|' + oLocation.Location_Name_For_Fluid__c);

        //////////////////
        /// START TEST ///
        //////////////////

        Test.startTest();

        //Setup records on page
        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();
        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();
        oUpdateController.selectAllCheckbox = true;
        oPage = oUpdateController.ToggleAllTruckTripCheckboxes();

        // MASS UPDATE DATA -> Run Through Values AND verify that ALL Wrappers have the applied value
        for(String sOneApply : mapMassUpdate.keySet()) {
            System.debug('TESTING VALUE IN APEX TEST ' + mapMassUpdate.get(sOneApply));
            oUpdateController.applyItem = 'facility';
            oUpdateController.valueSelected = mapMassUpdate.get(sOneApply);

            oPage = oUpdateController.MassUpdateData();
            oPage = oUpdateController.UpdateDependentFilter();

        }

        /////////////////
        /// STOP TEST ///
        /////////////////

        Test.stopTest();
    }

    static testMethod void testPopulateLocationLookup(){
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        //Setup records on page
        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();
        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();

        Test.startTest();
        
        for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
           wrapper.aTruckTrip.Well__c = oLocation.Id;
        }
  
        for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
            wrapper.populateLocationLookup();
            System.assertEquals(wrapper.aFacilityPicklistValue, null);
            System.assertEquals(wrapper.aTruckTrip.Facility_Type__c, 'Location');
        }

        for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
           wrapper.aTruckTrip.Well__c = null;
        }
  
        for(FM_DispatchViewNewController.TruckTripWrapper wrapper : lTestWrapper) {
            wrapper.populateLocationLookup();
            System.assertEquals(wrapper.aTruckTrip.Location_Lookup__c, null);
            System.assertEquals(wrapper.aFacilityPicklistValue, null);
        }

        /////////////////
        /// STOP TEST ///
        /////////////////
        Test.stopTest();

    }

    static testMethod void tstResultStats(){
        // Create Test Data - Various Objects used for Testing
        CreateTestData();

        // Retrieve Test Data Values
        Facility__c oFacility = [SELECT Id, Facility_Name_For_Fluid__c FROM Facility__c LIMIT 1];
        Location__c oLocation = [SELECT Id, Location_Name_For_Fluid__c FROM Location__c LIMIT 1];

        // Misc Variables for Assignment
        List<FM_DispatchViewNewController.TruckTripWrapper> lTestWrapper = new List<FM_DispatchViewNewController.TruckTripWrapper>();
        PageReference oPage;
        Date dToday = Date.today();

        //Setup records on page
        FM_DispatchViewNewController oUpdateController = new FM_DispatchViewNewController();
        oUpdateController.currentPageFilter.searchRoutes = new List<String>();
        oUpdateController.currentPageFilter.searchRoutes.add('999');
        oUpdateController.currentPageFilter.runSheetDate = Date.today();
        oPage = oUpdateController.searchTruckTrips();
        lTestWrapper = oUpdateController.getCurrentTruckTripList();

        Test.startTest();
        
        oUpdateController.getResultsStats();
        System.debug('Amount of records '+oUpdateController.getResultsStats());
        System.assertNotEquals(oUpdateController.getResultsStats(), '');

        // Go to last page
        oUpdateController.GoLast();
        oUpdateController.getResultsStats();

        // Add more specific filter
        oUpdateController.currentPageFilter.axle = 'T5X';
        oPage = oUpdateController.searchTruckTrips();
        oUpdateController.getResultsStats();

        // Check static vars
        System.assertEquals(FM_DispatchViewNewController.TRUCKTRIP_STATUS_REDISPATCH,FM_Utilities.TRUCKTRIP_STATUS_REDISPATCH);
        System.assertEquals(FM_DispatchViewNewController.TRUCKTRIP_STATUS_EXPORTED,FM_Utilities.TRUCKTRIP_STATUS_EXPORTED);
        System.assertEquals(FM_DispatchViewNewController.TRUCKTRIP_STATUS_GIVENBACK,FM_Utilities.TRUCKTRIP_STATUS_GIVENBACK);

        /////////////////
        /// STOP TEST ///
        /////////////////
        Test.stopTest();
    }

    private static void CreateTestData()
    {
        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Carrier Account';
        insert oAccount;

        // Test Carrier
        Carrier__c oCarrier = new Carrier__c();
        oCarrier.Carrier__c = oAccount.Id;
        oCarrier.Carrier_Name_For_Fluid__c = 'Test Carrier';
        insert oCarrier;

        // Test Carrier Unit
        Carrier_Unit__c oCarrierUnit = new Carrier_Unit__c();
        oCarrierUnit.Carrier__c = oCarrier.Id;
        oCarrierUnit.Unit_Email__c = 'testing@testing.com';
        insert oCarrierUnit;

        // Test Disabled Unit
        Carrier_Unit__c oDisabledUnit = new Carrier_Unit__c();
        oDisabledUnit.Carrier__c = oCarrier.Id;
        oDisabledUnit.Unit_Email__c = 'disabled@testing.com';
        oDisabledUnit.Enabled__c = false;
        insert oDisabledUnit;

        // Test Disable Carrier
        Carrier__c oDisabledCarrier = new Carrier__c();
        oDisabledCarrier.Carrier__c = oAccount.Id;
        oDisabledCarrier.Carrier_Name_For_Fluid__c = 'Test Disabled Carrier';
        oDisabledCarrier.Enabled__c = false;
        insert oDisabledCarrier;

        // Test Business Department
        Business_Department__c oBusiness = new Business_Department__c();
        oBusiness.Name = 'Husky Business';
        insert oBusiness;

        // Test Operating District
        Operating_District__c oDistrict = new Operating_District__c();
        oDistrict.Name = 'District 1234';
        oDistrict.Business_Department__c = oBusiness.Id;
        insert oDistrict;

        // Test AMU Field
        Field__c oField = new Field__c();
        oField.Name = 'AMU Field';
        oField.Operating_District__c = oDistrict.Id;
        insert oField;

        // Test Route
        Route__c oRoute = new Route__c();
        oRoute.Fluid_Management__c = true;
        oRoute.Name = '999';
        oRoute.Route_Number__c = '999';
        insert oRoute;

        // Test Location
        Location__c oLocation = new Location__c();
        oLocation.Name = 'Husky Well';
        oLocation.Fluid_Location_Ind__c = true;
        oLocation.Location_Name_for_Fluid__c = 'Husky Well Fluid';
        oLocation.Operating_Field_AMU__c = oField.Id;
        oLocation.Route__c = oRoute.Id;
        oLocation.Functional_Location_Category__c = 4;
        insert oLocation;

        // Test Facility
        Facility__c oFacility = new Facility__c();
        oFacility.Fluid_Facility_Ind__c = true;
        oFacility.Facility_Name_for_Fluid__c = 'Fluid Facility B';
        oFacility.Name = 'Facility B';
        insert oFacility;

        // Test Run Sheet
        FM_Run_Sheet__c oRunSheet = new FM_Run_Sheet__c();
        oRunSheet.Date__c = Date.today();
        oRunSheet.Well__c = oLocation.Id;
        oRunSheet.Act_Tank_Level__c = 40;
        insert oRunSheet;

        // Test Load Requests
        List<FM_Load_Request__c> loadRequests = new List<FM_Load_Request__c>();
        loadRequests.addAll(FM_TestData.createLoadRequests(oRunSheet.Id, 'Standard Load', 
            'O', 'Night', oCarrier.Id, 'T5X', 'Primary', null, 
            'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
            'Tank 1', oRunSheet.Act_Tank_Level__c, 20, 120, 4, 5, 3));
        loadRequests.addAll(FM_TestData.createLoadRequests(oRunSheet.Id, 'Standard Load', 
            'W', 'Night', oCarrier.Id, 'T5X', 'Primary', null, 
            'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
            'Tank 1', oRunSheet.Act_Tank_Level__c, 20, 120, 4, 5, 4));
        loadRequests.addAll(FM_TestData.createLoadRequests(oRunSheet.Id, 'Standard Load', 
            'O', 'Night', oDisabledCarrier.Id, 'T5X', 'Primary', null, 
            'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
            'Tank 1', oRunSheet.Act_Tank_Level__c, 60, 120, 4, 5, 1));
        loadRequests.addAll(FM_TestData.createLoadRequests(oRunSheet.Id, 'Standard Load', 
            'W', 'Night', oDisabledCarrier.Id, 'T5X', 'Primary', null, 
            'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
            'Tank 1', oRunSheet.Act_Tank_Level__c, 20, 120, 4, 5, 2));

        loadRequests.addAll(FM_TestData.createLoadRequests(oRunSheet.Id, 'Standard Load', 
            'W', 'Night', oDisabledCarrier.Id, 'T6X', 'Primary', null, 
            'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
            'Tank 1', oRunSheet.Act_Tank_Level__c, 20, 120, 4, 5, 50));
    }

}