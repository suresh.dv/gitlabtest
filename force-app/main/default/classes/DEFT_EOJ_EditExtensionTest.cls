@isTest
public class DEFT_EOJ_EditExtensionTest {
    
    @isTest
    static void testEOJEditPage() {
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

    
        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
    
        System.runAs(runningUser) 
        {
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
    
          system.debug ( serviceRigProgram );
    
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            

            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
            }
        
            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
        
            }
    
            System.runAs(runningUser) 
            {
                HOG_EOJ__c eoj = new HOG_EOJ__c();
                eoj.Service_Rig_Program__c = serviceRigProgram.Id;
                eoj.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
                eoj.Service_Completed__c = Date.today();
                eoj.Final_Cost__c = 5000;
                eoj.Service_General__c  = 'Downhole Suspension - BD13';
                eoj.Service_Detail__c = 'Bridge Plug - CPB4';
                eoj.Status__c = 'New';

                insert eoj;
          
              
              ApexPages.StandardController std = new ApexPages.StandardController(eoj);
              DEFT_EOJ_EditExtension extension = new DEFT_EOJ_EditExtension(std);
              
              System.assert(extension.isSubmitted() == null);
          
              eoj.Status__c = 'Submitted';
              update eoj;
              
              std = new ApexPages.StandardController(eoj);
              extension = new DEFT_EOJ_EditExtension(std);
              
              System.assert(extension.isSubmitted() != null);   
				
          }
    }
    
    @isTest
    static void testEOJNewPage() {
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
    
        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
    
        System.runAs(runningUser) 
        {
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
    
          system.debug ( serviceRigProgram );
    
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            

            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
            }
        
            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
        
            }
    
            System.runAs(runningUser) 
            {
                HOG_EOJ__c eojPrev = new HOG_EOJ__c();
                eojPrev.Service_Rig_Program__c = serviceRigProgram.Id;
                eojPrev.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
                eojPrev.Service_Completed__c = Date.today();
                eojPrev.Final_Cost__c = 5000;
                eojPrev.Service_General__c  = 'Downhole Suspension - BD13';
                eojPrev.Service_Detail__c = 'Bridge Plug - CPB4';
                eojPrev.Status__c = 'New';
                eojPrev.Pump_Elastomer1__c = 'pumptest';
                insert eojPrev;
                
                HOG_EOJ__c eoj = new HOG_EOJ__c();
                eoj.Service_Rig_Program__c = serviceRigProgram.Id;
          
              ApexPages.StandardController std = new ApexPages.StandardController(eoj);
              DEFT_EOJ_EditExtension extension = new DEFT_EOJ_EditExtension(std);
              
              System.assert(eojPrev.Pump_Elastomer2__c == eoj.Pump_Elastomer1__c);
              
          }
    
    }
    
    @isTest
    static void testRigCompanyDependentPicklist() {
        
                    

        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        
        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
    
        System.runAs(runningUser) 
        {
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
    
          system.debug ( serviceRigProgram );
    
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            

            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
            }
        
            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
        
            }
        	
        	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        	Vendor_Unit_Company__c vuc;
        	HOG_Rig__c rig;
            System.runAs ( thisUser ) {
                Account acc = DEFT_TestData.createTestAccount();
                vuc = DEFT_TestData.createVUC(acc.Id,'Rig Company');
                rig = DEFT_TestData.createRig(vuc.Id);
            }    		
        
            System.runAs(runningUser) 
            {
                HOG_EOJ__c eojPrev = new HOG_EOJ__c();
                eojPrev.Service_Rig_Program__c = serviceRigProgram.Id;
                eojPrev.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
                eojPrev.Service_Completed__c = Date.today();
                eojPrev.Final_Cost__c = 5000;
                eojPrev.Service_General__c  = 'Downhole Suspension - BD13';
                eojPrev.Service_Detail__c = 'Bridge Plug - CPB4';
                eojPrev.Status__c = 'New';
                eojPrev.Pump_Elastomer1__c = 'pumptest';
				eojPrev.Rig_Company__c = vuc.Id;
                eojPrev.Rig__c = rig.Id;
                insert eojPrev;
                
                HOG_EOJ__c eoj = new HOG_EOJ__c();
                eoj.Service_Rig_Program__c = serviceRigProgram.Id;
          
              ApexPages.StandardController std = new ApexPages.StandardController(eoj);
              DEFT_EOJ_EditExtension extension = new DEFT_EOJ_EditExtension(std);
              	
                extension.companyId = vuc.Id;
                extension.rigId = rig.Id;
                
                List<SelectOption> companiesSO = extension.getRigCompanies();
                system.assertEquals(2, companiesSO.size());
                
                List<SelectOption> rigsSO = extension.getRigs();
                system.assertEquals(2, rigsSO.size());
                
                extension.populateCompanyId();
                extension.populateRigId();
                system.assertEquals(false,extension.getPicklistDisabled());
                extension.saveRecord();
                extension.companyId = '000000000000000000';
                extension.saveRecord();
                
          }
    }
}