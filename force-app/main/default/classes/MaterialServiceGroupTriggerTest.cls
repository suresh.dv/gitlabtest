@isTest
private class MaterialServiceGroupTriggerTest{
    private testmethod static void testCategoryCheck(){
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = UserInfo.getUserId() );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true,
                                                        Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id, Active__c = true,
                                                                Category_Manager__c = UserInfo.getUserId(),
                                                                Category_Specialist__c = UserInfo.getUserId() );
        insert sub;
        
        PREP_Discipline__c dispNew = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                                SCM_Manager__c = UserInfo.getUserId() );
        insert dispNew;
        
        PREP_Category__c catNew = new PREP_Category__c( Name = 'Category', Active__c = true,
                                                        Discipline__c = dispNew.Id );
        insert catNew;
        
        PREP_Sub_Category__c subNew = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = catNew.Id, Active__c = true,
                                                                Category_Manager__c = UserInfo.getUserId(),
                                                                Category_Specialist__c = UserInfo.getUserId() );
        insert subNew;
        
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',PDE_Phase__c='1',Phase_4_Start_Date__c=Date.newInstance(2017, 7, 7),Phase_4_Completion_Date__c=Date.newInstance(2018, 7, 7),
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = UserInfo.getUserId(), SCM_Team__c='E&C - Equipment',
                                Category_Manager__c = UserInfo.getUserId(), Category_Specialist__c = UserInfo.getUserId(), RecordTypeId ='01216000001QPTv' );
                                
        insert ini;
        
        PREP_Initiative__c ini1 = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'no', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',Global_Sourcing_Exception_Reason__c='none',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',PDE_Phase__c='1',Phase_4_Start_Date__c=Date.newInstance(2017, 7, 7),Phase_4_Completion_Date__c=Date.newInstance(2018, 7, 7),
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = UserInfo.getUserId(), SCM_Team__c='E&C - Equipment',
                                Category_Manager__c = UserInfo.getUserId(), Category_Specialist__c = UserInfo.getUserId(), RecordTypeId ='01216000001QPTu' );
                                
        insert ini1;
        
        PREP_Baseline__c baseline = new PREP_Baseline__c( Baseline_Spend_Dollar_Target__c = 10000, 
                                                            Local_Baseline_Spend_Percent__c = 0.10,
                                                            Local_Baseline_Savings_Percent__c = 0.10,
                                                            Global_Baseline_Savings_Percent__c = 0.10,
                                                            XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10),
                                                            RecordTypeId = '01216000001QPTt', Initiative_Id__c = ini.Id );
        insert baseline;
        
        PREP_Baseline__c baseline1 = new PREP_Baseline__c( Baseline_Spend_Dollar_Target__c = 10000,
                                                            Local_Baseline_Spend_Percent__c = 0.10,
                                                            Local_Baseline_Savings_Percent__c = 0.10,
                                                            Global_Baseline_Savings_Percent__c = 0.10,
                                                            Validation_Rule_Override__c = true,
                                                            XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10),
                                                            RecordTypeId = '01216000001QPTt', Initiative_Id__c = ini1.Id );
        insert baseline1;
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c( Name = 'MS Name', 
                                                Material_Service_Group_Name__c = 'MS', Sub_Category__c = sub.Id,
                                                Category_Manager__c = UserInfo.getUserId(), SAP_Short_Text_Name__c = 'MS',
                                                Active__c = true, Type__c = 'Material', Includes__c = 'MS',
                                                Category_Specialist__c = UserInfo.getUserId(), Does_Not_Include__c = 'MS',
                                                Category_Link__c = cat.Id, Discipline_Link__c = disp.Id );
        insert MS;
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA = new PREP_Mat_Serv_Group_Baseline_Allocation__c( Baseline_Id__c = baseline.Id,
                                                            Material_Service_Group__c = MS.Id,
                                                            RecordTypeId = '01216000001QPTw', Sub_Category__c = sub.Id );
        insert MSA;
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA1 = new PREP_Mat_Serv_Group_Baseline_Allocation__c( Baseline_Id__c = baseline1.Id,
                                                            Material_Service_Group__c = MS.Id, RecordTypeId = '01216000001QPTx',
                                                            Sub_Category__c = sub.Id );
        insert MSA1;
        
        test.startTest();
            MS.Category_Link__c = catNew.Id;
            MS.Sub_Category__c = subNew.Id;
            MS.Discipline_Link__c = dispNew.Id;
            
            update MS;
            
            List<PREP_Initiative__c> iniList = [ Select Category__c, Discipline__c, Sub_Category__c from PREP_Initiative__c ];
            system.assertEquals( iniList[0].Category__c, catNew.Id );
            system.assertEquals( iniList[1].Category__c, catNew.Id );
            system.assertEquals( iniList[0].Sub_Category__c, subNew.Id );
            system.assertEquals( iniList[1].Sub_Category__c, subNew.Id );
            system.assertEquals( iniList[0].Discipline__c, dispNew.Id );
            system.assertEquals( iniList[1].Discipline__c, dispNew.Id );
        test.stopTest();
    }
}