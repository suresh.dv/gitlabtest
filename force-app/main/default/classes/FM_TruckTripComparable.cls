global class FM_TruckTripComparable implements Comparable
{
    public Id truckTripId;
    public String truckTripPriority;
    
    // Constructor
    public FM_TruckTripComparable(Id pTruckTripId, String pTruckTripPriority)
    {
        truckTripId = pTruckTripId;
        truckTripPriority = pTruckTripPriority;
    }
    
    // Implement the compareTo() method
    global Integer compareTo(Object pCompareToObject)
    {
        FM_TruckTripComparable compareToTruckTrip = (FM_TruckTripComparable)pCompareToObject;
        
        if (truckTripPriority == compareToTruckTrip.truckTripPriority)
        {
            return 0;
        }
        if (truckTripPriority > compareToTruckTrip.truckTripPriority)
        {
            return 1;
        }
        return -1;
    }
}