public with sharing class HOG_EquipmentFactoryUtils {
	public class EquipmentFactory {
		public Equipment__c getEquipment(HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM equipmentPayloadEntry) {
			return getEquipmentRecord(equipmentPayloadEntry);
		}
	}

	/************************************* Utility  Functions ***************************************/
	private static Equipment__c getEquipmentRecord(HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM equipmentPayloadEntry) {
		Equipment__c equipment = new Equipment__c();
		HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST input = equipmentPayloadEntry.RIHEQUI_LIST;
		equipment.Model_Number__c = String.isBlank(input.TYPBZ) ? 
			'' : input.TYPBZ;
		equipment.Functional_Location__c = input.TPLNR;
		equipment.Construct_Year__c = String.isBlank(input.BAUJJ) ?
			'' : input.BAUJJ;
		equipment.Cost_Center__c = String.isBlank(input.KOSTL) ?
          	'' : input.KOSTL.substring(input.KOSTL.indexOfAny('123456789'), 
          	input.KOSTL.length());
        equipment.Equipment_Category__c = String.isBlank(input.EQTYP) ?
        	'' : input.EQTYP;
        equipment.Maintenance_Plant__c = String.isBlank(input.SWERK) ?
        	'' : input.SWERK;
        equipment.Main_Work_Centre__c = String.isBlank(input.GEWRK) ?
        	'' : input.GEWRK;
        equipment.Manufacturer__c = String.isBlank(input.HERST) ?
        	'' : input.HERST;
        equipment.Manufacturer_Serial_No__c = String.isBlank(input.SERGE) ?
        	'' : input.SERGE;
        equipment.Object_Type__c = String.isBlank(input.EQART) ?
        	'' : input.EQART;
        equipment.Planner_Group__c = String.isBlank(input.INGRP) ?
        	'' : input.INGRP;
        equipment.Status__c = String.isBlank(input.USTXT) ?
        	'' : input.USTXT;
        equipment.Tag_Number__c = String.isBlank(input.EQFNR) ?
        	'' : input.EQFNR;
        equipment.Manufacturer_Part_Number__c = String.isBlank(input.MAPAR) ?
        	'' : input.MAPAR;
        equipment.Serial_Number__c = String.isBlank(input.SERNR) ?
        	'' : input.SERNR;
        equipment.Name = input.EQUNR.substring(input.EQUNR.indexOfAny('123456789'), 
          	input.EQUNR.length());
        equipment.Equipment_Number__c = input.EQUNR.substring(input.EQUNR.indexOfAny('123456789'), 
          	input.EQUNR.length());
        equipment.Warranty_Date_Start__c = input.GWLDT_L == '00000000' ? null : 
        	HOG_SAPApiUtils.parseDate(input.GWLDT_L);
        equipment.Warranty_Date_End__c = input.GWLEN_L == '00000000' ? null :
        	HOG_SAPApiUtils.parseDate(input.GWLEN_L);
        equipment.ABC_Indicator__c = String.isBlank(input.ABCKZ) ?
        	'' : input.ABCKZ;
        equipment.Description_of_Equipment__c = input.EQKTX;
        equipment.Catalogue_Code__c = String.isBlank(input.RBNR) ?
        	'' : input.RBNR;
        equipment.DLFL__c = !String.isBlank(input.ZZ_RECTYP) && 
        	input.ZZ_RECTYP.startsWith('D');
        equipment.Catalogue_Description__c = input.RBNRX;

        //Superior Equipment
        if(!String.isBlank(input.HEQUI)) {
        	equipment.Superior_Equipment__r = new Equipment__c(
        		Equipment_Number__c = input.HEQUI.substring(input.HEQUI.indexOfAny('123456789'), 
          			input.HEQUI.length())
        	);
        }

        return equipment;
	}
	/************************************************************************************************/
}