@isTest
public class DEFT_EOJ_SubmitButtonExtensionTest {

    @isTest
    static void testEOJSubmitButtonErrorScenario() {
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
    
        System.runAs(runningUser) 
        {
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
    
          system.debug ( serviceRigProgram );
    
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            

            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
            }
        
            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
        
            }
    
            System.runAs(runningUser) 
            {
                HOG_EOJ__c eoj = new HOG_EOJ__c();
                eoj.Service_Rig_Program__c = serviceRigProgram.Id;
                eoj.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
                eoj.Service_Completed__c = Date.today();
                eoj.Final_Cost__c = 5000;
                eoj.Service_General__c  = 'Downhole Suspension - BD13';
                eoj.Service_Detail__c = 'Bridge Plug - CPB4';
                eoj.Status__c = 'New';
                eoj.Rotor_Condition1__c = '';
                eoj.Rotor_Condition2__c = '';
                eoj.Rod_Type1__c = '';
        eoj.Rod_Failure__c = '';
                eoj.Rod_Grade2__c = '';
                eoj.Rod_Grade1__c = '';
                eoj.Rod_Size2__c = '';
                eoj.Rod_Size1__c = ''; 
                eoj.Rod_Type2__c = ''; 
                eoj.Rotor_Condition2__c = '';
                eoj.Rotor_Tag_Type1__c = '';
                eoj.Scope_Joint2__c = '';
                eoj.Scope_Joint1__c = '';
                eoj.Shear_Type2__c = '';
                eoj.Shear_Type1__c = '';
                eoj.Stator_Condition2__c = '';
                eoj.Stator_Condition1__c = '';
                eoj.Tubing_Failure__c = '';
                eoj.Tubing_Size2__c = '';
                eoj.Tubing_Size1__c = '';
                eoj.Bail_Well__c = '';
               
                insert eoj;
           
                ApexPages.StandardController std = new ApexPages.StandardController(eoj);
                DEFT_EOJ_SubmitButtonExtension extension = new DEFT_EOJ_SubmitButtonExtension(std);
                
                system.assertEquals(true, extension.renderButtons);
                system.assertEquals(null, extension.submit());
                system.assertEquals(false, extension.renderButtons);

            }
    }
    
    @isTest
    static void testEOJSubmitButtonSuccessScenario() {
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
    
        System.runAs(runningUser) 
        {
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
          serviceRigProgram.Status__c = 'Started';
          update serviceRigProgram;
    
          system.debug ( serviceRigProgram );
    
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            

            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
            }
        
            System.runAs(serviceRigPlanner) 
            {
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
        
            }
        
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            Vendor_Unit_Company__c vuc;
            HOG_Rig__c rig;
            System.runAs ( thisUser ) {
                Account acc = DEFT_TestData.createTestAccount();
                vuc = DEFT_TestData.createVUC(acc.Id,'Rig Company');
                rig = DEFT_TestData.createRig(vuc.Id);
            }  
        
            System.runAs(runningUser) 
            {
                HOG_EOJ__c eoj = new HOG_EOJ__c();
                eoj.Service_Rig_Program__c = serviceRigProgram.Id;
                eoj.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
                eoj.Service_Completed__c = Date.today();
                eoj.Final_Cost__c = 5000;
                eoj.Service_General__c  = 'Downhole Suspension - BD13';
                eoj.Service_Detail__c = 'Bridge Plug - CPB4';
                eoj.Status__c = 'New';
                
                eoj.Bail_Well__c = '123';
                eoj.Budget__c = 123;
                eoj.Coil_Kit2__c = 'Yes';
                eoj.Coil_Kit1__c = 'Yes';
                eoj.Drain_Type2__c = '123';
                eoj.Comments2__c = '123';
                eoj.Comments1__c = '123';
                eoj.Drain_Placement2__c = '123';
                eoj.Drain_Placement1__c = '123';
                    eoj.Drain_Type1__c = '123';
                    eoj.EOJ_Details__c = '123';
                    eoj.Final_Tag_Depth__c = '123';
                    eoj.Fluid_Inj__c = '123';
                    eoj.Fluid_Recovered__c = '123';
                    eoj.Initial_Tag_Depth__c = '123';
                    eoj.Intake_Depth2__c = '123';
                    eoj.Intake_Depth1__c = '123';
                    eoj.Job_Cost_Comments__c = '123';
                    eoj.Length_Off_Tag2__c = '123';
                    eoj.Length_Off_Tag1__c = '123';
                    eoj.Next_Location__c = '123';
                    eoj.NTT_Type2__c = 'NTT';
                    eoj.NTT_Type1__c = 'NTT';
                    eoj.PBTD__c = '123';
                    eoj.Perf_Interval__c = '123';
                    eoj.Pin_Size2__c = '123';
                    eoj.Pin_Size1__c = '123';
                    eoj.Polished_Rod_Length2__c = 'Other';
                    eoj.Polished_Rod_Length1__c = 'Other';
                    eoj.Previous_Failure_Detail__c = '123';
                    eoj.Previous_Service_Date__c = Date.today();
                    eoj.Producing_Zone__c = '123';
                    eoj.Pull_Out_of_Sand__c = '123';
                    eoj.Pump_Displacement_Size2__c = '123';
                    eoj.Pump_Displacement_Size1__c = '123';
                    eoj.Pump_Elastomer2__c = '123';
                    eoj.Pump_Elastomer1__c = '123';
                    eoj.Pump_Lift2__c = '4';
                    eoj.Pump_Lift1__c = '4';
                    eoj.Pump_Type2__c = 'PC Pump';
                    eoj.Pump_Type1__c = 'PC Pump';
                    eoj.Pump_Vendor2__c = 'Apex';
                    eoj.Pump_Vendor1__c = 'Apex';
                    eoj.Rig_Hours__c = 123;
                    eoj.Rig__c = rig.Id;
                    eoj.Rig_Company__c = vuc.Id;
                    eoj.Rod_Comments2__c = '123';
                    eoj.Rod_Comments1__c = '123';
                    eoj.Rod_Condition2__c = '123';
                    eoj.Rod_Condition1__c = '123';
                    eoj.Rod_Failure__c = '123';
                    eoj.Rod_Grade2__c = '620C';
                    eoj.Rod_Grade1__c = '620C';
                    eoj.Rod_Size2__c = 'Other';
                    eoj.Rod_Size1__c = 'Other';
                    eoj.Rod_Type2__c = 'None';
                    eoj.Rod_Type1__c = 'None';
                    eoj.Rotor_Condition2__c = 'New';
                    eoj.Rotor_Condition1__c = 'New';
                    eoj.Rotor_Length2__c = '123';
                    eoj.Rotor_Length1__c = '123';
                    eoj.Rotor_Tag_Type2__c = 'Other';
                    eoj.Rotor_Tag_Type1__c = 'Other';
                    eoj.Scope_Joint2__c = 'Yes';
                    eoj.Scope_Joint1__c = 'Yes';
                    eoj.Service_Completed__c = Date.today();
                    eoj.Service_Detail__c = '123';
                    eoj.Service_General__c = '123';
                    eoj.Service_Started__c = Date.today();
                    eoj.Shear_Placement2__c = '123';
                    eoj.Shear_Placement1__c = '123';
                    eoj.Shear_Type2__c = 'None';
                    eoj.Shear_Type1__c = 'None';
                    eoj.Stator_Condition2__c = 'New';
                    eoj.Stator_Condition1__c = 'New';
                    eoj.Status__c = '123';
                    eoj.Test_Efficiency2__c = '123';
                    eoj.Test_Efficiency1__c = '123';
                    eoj.Tubing_Failure__c = '123';
                    eoj.Tubing_JIH2__c = '123';
                    eoj.Tubing_JIH1__c = '123';
                    eoj.Tubing_Size2__c = '123';
                    eoj.Tubing_Size1__c = '123';

                insert eoj;
                
                Attachment att = new Attachment(parentId=eoj.Id,name='123',body=Blob.valueOf('123'));
                insert att;
           
                ApexPages.StandardController std = new ApexPages.StandardController(eoj);
                DEFT_EOJ_SubmitButtonExtension extension = new DEFT_EOJ_SubmitButtonExtension(std);
                
                system.assertEquals(true, extension.renderButtons);
                system.assertEquals(null, extension.submit());
                system.assertEquals(false, extension.renderButtons);

            }
    }
}