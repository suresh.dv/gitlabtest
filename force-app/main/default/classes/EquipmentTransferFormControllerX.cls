/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentTransferToFormEdit/EquipmentTransferFormView page

Test Class:    EquipmentTransferFormTest
History: 	   
			   26-Mar-15 Gangha Kaliyan  To include facilities in Equipment Transfer Request
			   27-Feb-17 Miro Zelina - included Well Events into Equipment Transfer Request
			   29-Jun-17 Miro Zelina - included System & Sub-System into Equipment Transfer Request
               31.8.17 Mbrimus  - included FEL and Yard into Equipment Transfer Request
               26-Oct-2017 Miro Zelina - added check for user who can process/close equipment form
------------------------------------------------------------*/

public with sharing class EquipmentTransferFormControllerX{
     
    public Equipment_Transfer_Form__c form {get;set;} 
    public String formId {get;set;}

    public String equipIds {get;set;} 
    public String toUnselect {get; set;}
    public String toSelectIds {get;set;}
    public String destinationObj {get;set;}
    public EquipmentUtilities.UserPermissions userPermissions;
    private Equipment_Transfer_Item__c[] forDeletion = new Equipment_Transfer_Item__c[]{};

    
    public EquipmentTransferFormControllerX(ApexPages.StandardController std)
    {
        if (!Test.isRunningTest())
        {
            std.addFields(new List<String>{'From_Location__c', 'To_Location__c', 'Charge_No__c', 'Status__c',
               'Date_of_Physical_Transfer__c', 'Authorized_By__c', 'Reason_for_Transfer_Additional_Details__c',
               'Shipped_Via__c', 'Waybill_No__c', 'CreatedById', 
               'CreatedDate', 'LastModifiedById', 'LastModifiedDate', 'Name', 'Comments__c', 'Maintenance_Work_Order__c',
               'From_Facility__c', 'To_Facility__c', 'From_Well_Event__c', 'To_Well_Event__c', 
               'From_System__c', 'To_System__c', 'From_Sub_System__c', 'To_Sub_System__c',
               'From_Functional_Equipment_Level__c', 'To_Functional_Equipment_Level__c', 'From_Yard__c', 'To_Yard__c'});
        }
        form = (Equipment_Transfer_Form__c) std.getRecord();
        userPermissions = new EquipmentUtilities.UserPermissions();
       	
        if (form.Id == null)    
            setDefault();
			
		//set destinationObj picklist value 
		if(form.To_Location__c != null)
			destinationObj = 'Location';
		else if(form.To_Well_Event__c != null)
			destinationObj = 'Well Event';	
		else if(form.To_Facility__c != null)
			destinationObj = 'Facility';
	    else if(form.To_System__c != null)
			destinationObj = 'System';
	    else if(form.To_Sub_System__c != null)
			destinationObj = 'Sub-System'; 
        else if(form.To_Functional_Equipment_Level__c != null)
            destinationObj = 'Functional Equipment Level';  
        else if(form.To_Yard__c != null)
            destinationObj = 'Yard';   
        else
            destinationObj = 'Location';
        
        System.debug('destinationObj value: ' + destinationObj);	
    }
    private void setDefault()
    {
    	//if location Ids are supplied in Url, get them
        String fromLocId = ApexPages.currentPage().getParameters().get('fromLocId'); 
        String toLocId = ApexPages.currentPage().getParameters().get('toLocId'); 
        
        //if facility Ids are supplied in Url, get them
        String fromFacId = ApexPages.currentPage().getParameters().get('fromFacId'); 
        String toFacId = ApexPages.currentPage().getParameters().get('toFacId'); 
        
        //if well event Ids are supplied in Url, get them
        String fromEventId = ApexPages.currentPage().getParameters().get('fromEventId'); 
        String toEventId = ApexPages.currentPage().getParameters().get('toEventId');
        
        //if system Ids are supplied in Url, get them
        String fromSystemId = ApexPages.currentPage().getParameters().get('fromSystemId'); 
        String toSystemId = ApexPages.currentPage().getParameters().get('toSystemId');
        
        //if sub-system Ids are supplied in Url, get them
        String fromSubSysId = ApexPages.currentPage().getParameters().get('fromSubSysId'); 
        String toSubSysId = ApexPages.currentPage().getParameters().get('toSubSysId');

        //if Yard Ids are supplied in Url, get them
        String fromYardId = ApexPages.currentPage().getParameters().get('fromYardId');
        String toYardId = ApexPages.currentPage().getParameters().get('toYardId'); 

        //if FEL Ids are supplied in Url, get them
        String fromFELId = ApexPages.currentPage().getParameters().get('fromFELId'); 
        String toFELId = ApexPages.currentPage().getParameters().get('toFELId');
        
        String tranferType = ApexPages.currentPage().getParameters().get('type'); 
        
        if (fromLocId != null)
            form.From_Location__c = fromLocId;
        
        if (toLocId != null)
            form.To_Location__c = toLocId;
            
		if(fromFacId != null)
			form.From_Facility__c = fromFacId;
		
		if(toFacId != null)
			form.To_Facility__c = toFacId;  
		
		//new FLOCs	
		if(fromEventId != null)
			form.From_Well_Event__c = fromEventId;
		
		if(toEventId != null)
			form.To_Well_Event__c = toEventId;
			
		if(fromSystemId != null)
			form.From_System__c = fromSystemId;
		
		if(toSystemId != null)
			form.To_System__c = toSystemId;
			
	    if(fromSubSysId != null)
			form.From_Sub_System__c = fromSubSysId;
		
		if(toSubSysId != null)
			form.To_Sub_System__c = toSubSysId;  

        if(fromYardId != null)
            form.From_Yard__c = fromYardId;
        
        if(toYardId != null)
            form.To_Yard__c = toYardId;
            
        if(fromFELId != null)
            form.From_Functional_Equipment_Level__c = fromFELId;
        
        if(toFELId != null)
            form.To_Functional_Equipment_Level__c = toFELId;  

         
            
        form.Date_of_Physical_Transfer__c = date.today();    
        form.Status__c = 'Open';
    }
    public List<EquipmentItemWrapper> equipmentList
    {
        get{
            if (equipmentList == null)
            {
                equipmentList = new List<EquipmentItemWrapper>();
                equipIds = '';
       
                if (form.Id != null)
                {   List<Equipment_Transfer_Item__c> itemList = 
                                 [select Id, Equipment__r.Description_of_Equipment__c, Equipment__r.Manufacturer__c, 
                                       Equipment__r.Model_Number__c, Equipment__r.Manufacturer_Serial_No__c, Equipment__r.Tag_Number__c,
                                       Equipment__c, Equipment__r.Equipment_Number__c, Tag_Colour__c, Equipment__r.Location__c, Equipment__r.Facility__c, Equipment__r.Well_Event__c,
                                       Equipment__r.System__c, Equipment__r.Sub_System__c, Equipment__r.Functional_Equipment_Level__c, Equipment__r.Yard__c, 
                                       (select Id, Name, ContentType from Attachments)
                                   from Equipment_Transfer_Item__c where Equipment_Transfer_Form__c =: form.Id 
                                   order by Equipment__r.Description_of_Equipment__c];
                    for(Equipment_Transfer_Item__c item : itemList)
                    {
                       equipIds += item.Equipment__c + ',';   
                       equipmentList.add(new EquipmentItemWrapper(item));  
                    }          
                }
            }
            return equipmentList;
        }
        set;
    }
    
    public List<SelectOption> getDestinationObjects()
    {
    	List<SelectOption> options = new List<SelectOption>();
    	options.add(new SelectOption('Location','Location'));
    	options.add(new SelectOption('Facility','Facility'));
    	options.add(new SelectOption('Well Event','Well Event'));
    	options.add(new SelectOption('System','System'));
    	options.add(new SelectOption('Sub-System','Sub-System'));
        options.add(new SelectOption('Functional Equipment Level','Functional Equipment Level'));
        options.add(new SelectOption('Yard','Yard'));
    	return options;
    }
    
    public void onSelectDestination()
    {
    	
    } 
    
    public Boolean getLocOutputFieldFlag()
    {
    	//this flag sets if location output field should be rendered
    	//if((form.Status__c == 'Closed' || form.From_Location__c == null) && form.From_Facility__c == null && form.To_Facility__c == null)
    	if((form.Status__c == 'Closed' || form.From_Location__c == null) && 
    	    form.From_Facility__c == null && 
    	    form.To_Facility__c == null && 
    	    form.From_Well_Event__c == null && 
    	    form.To_Well_Event__c == null &&
    	    form.From_System__c == null &&
    	    form.To_System__c == null &&
    	    form.From_Sub_System__c == null &&
    	    form.To_Sub_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
    		return true;
    	else 
    		return false;
    }
    
    public Boolean getFacOutputFieldFlag()
    {
    	//this flag sets if facility output field should be rendered
    	//if((form.Status__c == 'Closed' || form.From_Facility__c == null) && form.From_Location__c == null && form.To_Location__c == null)
    	if((form.Status__c == 'Closed' || form.From_Facility__c == null) && 
    	    form.From_Location__c == null && 
    	    form.To_Location__c == null && 
    	    form.From_Well_Event__c == null && 
    	    form.To_Well_Event__c == null &&
    	    form.From_System__c == null &&
    	    form.To_System__c == null &&
    	    form.From_Sub_System__c == null &&
    	    form.To_Sub_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
    		return true;
    	else 
    		return false;    	
    }
    
    public Boolean getEventOutputFieldFlag()
    {
    	//this flag sets if well event output field should be rendered
    	if((form.Status__c == 'Closed' || form.From_Well_Event__c == null) && 
    	    form.From_Facility__c == null && 
    	    form.To_Facility__c == null && 
    	    form.From_Location__c == null && 
    	    form.To_Location__c == null &&
    	    form.From_System__c == null &&
    	    form.To_System__c == null &&
    	    form.From_Sub_System__c == null &&
    	    form.To_Sub_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
    		return true;
    	else 
    		return false;    	
    }
    
    public Boolean getSystemOutputFieldFlag()
    {
    	//this flag sets if System output field should be rendered
    	if((form.Status__c == 'Closed' || form.From_System__c == null) && 
    	    form.From_Facility__c == null && 
    	    form.To_Facility__c == null && 
    	    form.From_Location__c == null && 
    	    form.To_Location__c == null &&
    	    form.From_Well_Event__c == null &&
    	    form.To_Well_Event__c == null &&
    	    form.From_Sub_System__c == null &&
    	    form.To_Sub_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
    		return true;
    	else 
    		return false;    	
    }
    
    public Boolean getSubSystemOutputFieldFlag()
    {
    	//this flag sets if SubSystem output field should be rendered
    	if((form.Status__c == 'Closed' || form.From_Sub_System__c == null) && 
    	    form.From_Facility__c == null && 
    	    form.To_Facility__c == null && 
    	    form.From_Location__c == null && 
    	    form.To_Location__c == null &&
    	    form.From_Well_Event__c == null &&
    	    form.To_Well_Event__c == null &&
    	    form.From_System__c == null &&
    	    form.To_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
    		return true;
    	else 
    		return false;    	
    }

    public Boolean getFELOutputFieldFlag()
    {
        //this flag sets if FEL output field should be rendered
        if((form.Status__c == 'Closed' || form.From_Functional_Equipment_Level__c == null) && 
            form.From_Facility__c == null && 
            form.To_Facility__c == null && 
            form.From_Location__c == null && 
            form.To_Location__c == null &&
            form.From_Well_Event__c == null &&
            form.To_Well_Event__c == null &&
            form.From_System__c == null &&
            form.To_System__c == null &&
            form.From_Sub_System__c == null &&
            form.To_Sub_System__c == null &&
            form.From_Yard__c == null &&
            form.To_Yard__c == null)
            return true;
        else 
            return false;       
    }

    public Boolean getYardOutputFieldFlag()
    {
        //this flag sets if Yard output field should be rendered
        if((form.Status__c == 'Closed' || form.From_Yard__c == null) && 
            form.From_Facility__c == null && 
            form.To_Facility__c == null && 
            form.From_Location__c == null && 
            form.To_Location__c == null &&
            form.From_Well_Event__c == null &&
            form.To_Well_Event__c == null &&
            form.From_System__c == null &&
            form.To_System__c == null &&
            form.From_Sub_System__c == null &&
            form.To_Sub_System__c == null &&
            form.From_Functional_Equipment_Level__c == null &&
            form.To_Functional_Equipment_Level__c == null)
            return true;
        else 
            return false;       
    }

    public Boolean getDestInputFieldFlag()
    {
    	//if(form.Status__c != 'Closed' && (form.From_Location__c != null || form.From_Facility__c != null))
    	if(form.Status__c != 'Closed' && 
    	    (form.From_Location__c != null || 
    	     form.From_Facility__c != null || 
    	     form.From_Well_Event__c != null ||
    	     form.From_System__c != null ||
    	     form.From_Sub_System__c != null ||
             form.From_Functional_Equipment_Level__c != null ||
             form.From_Yard__c != null))
    		return true;
    	else 
    		return false;
    	
    }
       
    public void addToEquipmentList()
    {
       
        List<String> ids = toSelectIds.split(',');
        
        for(Equipment__c e : [select Id, Equipment_Number__c, Description_of_Equipment__c, 
                                     Location__c, Facility__c, Well_Event__c, System__c, Sub_System__c, Functional_Equipment_Level__c, Yard__c,
                                     Manufacturer__c, Model_Number__c, Manufacturer_Serial_No__c, Tag_Number__c 
                              from Equipment__c where Id in: ids])
        { 
            equipmentList.add(new EquipmentItemWrapper(
                                    new Equipment_Transfer_Item__c( Equipment__c = e.Id,
                                                                    Equipment__r = e)));
                                                                    
            equipIds += e.Id + ',';                                       
        }    
    }
    public PageReference removeFromEquipmentList() {    
        // This function runs when a user hits "remove" on an item in the "Equipment List" section 
        System.debug('before equipIds = ' + equipIds + ' toUnselect =' + toUnselect);
           
        if (equipIds.contains(toUnselect))
            equipIds = equipIds.replace(toUnselect, '');
        System.debug('after remove    equipIds = ' + toUnselect); 
        Integer count = 0;
    
        for(EquipmentItemWrapper d : equipmentList)
        {
            if((String)d.equipment.Equipment__c == toUnselect){
            
                if(d.equipment.Id != null)
                    forDeletion.add(d.equipment);
            
                equipmentList.remove(count);
                break;
            }
            count++;
        }
        return null;
    }
    public void DeletePhoto()
    {
        try
        {
            String equipId = ApexPages.currentPage().getParameters().get('equipId');
            for(EquipmentItemWrapper d : equipmentList)
            {
                if (d.equipment.Equipment__c == equipId)
                {
                    delete d.photo;
                    d.photo.body = null;
                    d.photo = new Attachment();
                }  
            } 
        }
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
        } 
    }
    //reset the Equipment list when FromLocation is updated
    public void resetEquipmentList()
    {
        System.debug('resetEquipmentList form.From_Location__c =' + form.From_Location__c);
        for(EquipmentItemWrapper d : equipmentList)
        {
            if (d.equipment.Id != null)
               forDeletion.add(d.equipment);
        }
        equipmentList = new List<EquipmentItemWrapper>();
        equipIds = '';
    }
    private void clearViewState()
    {
        if (equipmentList.size() > 0)
        {
            for(EquipmentItemWrapper item : equipmentList)
            {
                item.photo.Body = null;
                item.photo = new Attachment();
            }
        }
    }
    public PageReference Save()
    {
        if (form.Reason_for_Transfer_Additional_Details__c == null)
        {
            clearViewState();
            form.Reason_for_Transfer_Additional_Details__c.addError('You must enter a value');
            return null;
        }
        if (form.Date_of_Physical_Transfer__c == null)
        {
            clearViewState();
            form.Date_of_Physical_Transfer__c.addError('You must enter a value');
            return null;
        }
        if (form.Authorized_By__c == null)
        {
            clearViewState();
            form.Authorized_By__c.addError('You must enter a value');
            return null;
        }
        
        /*if (form.From_Location__c != null && form.To_Location__c == null && form.To_Facility__c == null)
        {
            clearViewState();
            form.To_Location__c.addError('You must enter a value for To(Destination)');
            return null;
        }*/
        
        //if either from location or facility is not null, then to destination should have a value
        if ((form.From_Location__c != null || 
             form.From_Facility__c != null || 
             form.From_Well_Event__c != null || 
             form.From_System__c != null || 
             form.From_Sub_System__c != null || 
             form.From_Yard__c != null || 
             form.From_Functional_Equipment_Level__c != null
            ) && 
             form.To_Facility__c == null && 
             form.To_Location__c == null && 
             form.To_Well_Event__c == null &&
             form.To_System__c == null &&
             form.To_Sub_System__c == null &&
             form.To_Functional_Equipment_Level__c == null &&
             form.To_Yard__c == null
           )
        {
            clearViewState();
            form.addError('You must enter a value for To(Destination)');
            return null;
        }
                
        if (equipmentList == null || equipmentList.size() == 0)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select at least one Equipment'));
            return null;
        } 
           
        if (form.From_Location__c == form.To_Location__c && form.From_Location__c != null && form.To_Location__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Location has to be different from To Location'));
            return null;
        }
        
        if (form.From_Facility__c == form.To_Facility__c && form.From_Facility__c != null && form.To_Facility__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Facility has to be different from To Facility'));
            return null;
        }  
        
        if (form.From_Well_Event__c == form.To_Well_Event__c && form.From_Well_Event__c != null && form.To_Well_Event__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Well Event has to be different from To Well Event'));
            return null;
        }
        
        if (form.From_System__c == form.To_System__c && form.From_System__c != null && form.To_System__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From System has to be different from To System'));
            return null;
        }
        
        if (form.From_Sub_System__c == form.To_Sub_System__c && form.From_Sub_System__c != null && form.To_Sub_System__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Sub-System has to be different from To Sub-System'));
            return null;
        }

        if (form.From_Functional_Equipment_Level__c == form.To_Functional_Equipment_Level__c && form.From_Functional_Equipment_Level__c != null && form.To_Functional_Equipment_Level__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Functional Equipment Level has to be different from To Functional Equipment Level'));
            return null;
        }

        if (form.From_Yard__c == form.To_Yard__c && form.From_Yard__c != null && form.To_Yard__c != null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'From Yard has to be different from To Yard'));
            return null;
        }
        
        System.SavePoint savePoint = Database.setSavepoint();
        // Previously selected products may have new quantities and amounts, and we may have new Equipment listed, so we use upsert here
        try{
            
            // If previously selected products are now removed, we need to delete them
            if(forDeletion.size()>0)
                delete(forDeletion);
            System.debug('upsert form');
            upsert form;    
            if(equipmentList.size()>0)
            {
                Map<Id, Equipment_Transfer_Item__c> upsertList = new Map<Id, Equipment_Transfer_Item__c>();
                for(EquipmentItemWrapper item : equipmentList)
                {
                    if (item.equipment.Tag_Colour__c == null)
                    {
                        clearViewState();
                        item.equipment.Tag_Colour__c.addError('You must enter a value');
                        return null;
                    }
                    item.equipment.Equipment_Transfer_Form__c = form.Id;
                    upsertList.put(item.equipment.Equipment__c, item.equipment);
                    System.debug('item =' + item);
                }
                system.debug('upsert equipment');
                upsert(upsertList.values());
                
                List<Attachment> photoList = new List<Attachment>();
                for(EquipmentItemWrapper item : equipmentList)
                {
                    //create new instance of uploaded file
                    Attachment photo = item.photo.clone(false, true, false, false);
                    if (photo.Id == null && photo.Body != null)
                    {
                        System.debug(upsertList.get(item.equipment.Equipment__c));
                        photo.ParentId = upsertList.get(item.equipment.Equipment__c).Id;    
                        System.debug('photo =' + photo);
                        
                        //clear body of uploaded file to remove from view state limit error
                        item.photo.Body = null;
                        photoList.add(photo);
                    }
                }
                insert photoList;
            }
        }
        catch(Exception e){
            Database.rollback(savePoint);
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }  
        if (ApexPages.currentPage().getParameters().get('retURL') != null)
        {
           System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
           PageReference nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
           nextPage.setRedirect(true);
           return nextPage;
        }
        return new PageReference('/' + form.Id);
    }
    public PageReference CloseRequest()
    {
        try
        {
            
            if (!userPermissions.isSystemAdmin && !userPermissions.isSapSupportLead && !userPermissions.isHogAdmin){
           
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
               return null;
            } 
           
            else {   
                
                form.Status__c = 'Closed';
                update form;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
                return null;
            }
        
        }
        catch(Exception ex)    
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
           return null;
        }     
        
    }
}