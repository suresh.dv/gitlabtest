/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGTasksEmailNotifications.cls
History:        jschn 05.02.2018 - Created.
**************************************************************************************************/
@isTest
private class HOG_VGTasksEmailNotificationsTest {
	
	@isTest static void testEmailSending() {
		Test.startTest();
		Database.executeBatch(new HOG_VGTasksEmailNotifications(
			HOG_VGTasksEmailNotifications.NOTIFICATIONTYPE_NEW_TASKS));
		
		Integer dmlRowsBfr = Limits.getDMLRows();
		Integer dmlStatementsBfr = Limits.getDMLStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		Integer dmlRowsAftr = Limits.getDMLRows();
		Integer dmlStatementsAftr = Limits.getDMLStatements();
		Integer queriesAftr = Limits.getQueries();

		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(10,dmlRowsAftr);
		System.assertEquals(10,dmlStatementsAftr);
		//System.assertEquals(3,queriesAftr);
	}
	
	@testSetup static void createData() {
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User user1 = HOG_VentGas_TestData.createUser('Bruce', 'Wayne', 'Batman');
		User user2 = HOG_VentGas_TestData.createUser('Nick', 'Grayson', 'Robin');

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		'TestDescription', 
																		loc.Id,
																		user1.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_Vent_Gas_Alert_Task__c t = HOG_VentGas_TestData.createTask(alert.Id, 
																	user1.Id, 
																	user2.Id, 
																	'TestTask', 
																	alert.Priority__c, 
																	true, 
																	HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		t = HOG_VentGas_TestData.createTask(alert.Id, 
											user1.Id, 
											null, 
											'TestTask', 
											alert.Priority__c, 
											false, 
											HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
											HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
	}

}