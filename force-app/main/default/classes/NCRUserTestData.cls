@isTest
public class NCRUserTestData {

    private static User createUser(){
        User user = new User();
        
        Profile p = [select id from profile where name='Standard User'];
        
        user.email = 'NCRUser' + '@ncr.com';
        user.Alias = 'NCRUser' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US'; 
        user.LocaleSidKey = 'en_US'; 
        user.ProfileId = p.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.UserName = 'NCRUser' + '@ncr.com.unittest';
        
        insert user;
        
        return user;    
    }
    
    private static User assignPermissionSet(User user, String userPermissionSetlabel){
    
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Label =: userPermissionSetlabel]; 
        PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
        
        permissionSetAssignment.PermissionSetId = permissionSet.Id;
        permissionSetAssignment.AssigneeId = user.Id; 
        
        insert permissionSetAssignment;
            
        return user;
    }

    public static User createNCRUser() {
        User user = createUser();
        user = assignPermissionSet(user,'NCR User');
        
        return user;
    }
    
    public static User createNCRQALead() {
        User user = createUser();
        user = assignPermissionSet(user,'NCR QA Lead');
        
        return user;
    }    
    
    public static User createNCRAdminUser() {
        User user = createUser();
        user = assignPermissionSet(user,'NCR Admin');
        
        return user;
    }  
    
    public static User createNCRAdmin() {
        User user = createUser();
        
        return user;
    }      
    
    public static User createNCRPortalUser() {
        
        User user = createUser();
        user = assignPermissionSet(user,'NCR Community User');
        
        Profile p = [SELECT Id FROM Profile WHERE Name='NCR Community User'];
        user.ProfileId = p.id;
        
        return user;
    }    
    
    public static User createNCRPortalQALead() {
        User user = new User();
        user = assignPermissionSet(user,'NCR Portal QA Lead');
        
        return user;
    }   
    
    public static User getPortalUser(Account a){        
        Contact c = new Contact(firstName='TestFirstName', lastName='TestLastName', email='test@testorg.com');
        insert c;
            
        User u = new User(alias = 'standt', email='test@testorg.com',
            emailencodingkey='UTF-8', lastname='lastname', languagelocalekey='en_US',
            localesidkey='en_US', contactId=c.id,
            timezonesidkey='America/Los_Angeles', username='test@testorg.com');
        
        return u;
    }       
}