@isTest
private class ChangeRequestTriggerTest{
    static testmethod void pcnNumberCheck()
    {
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id );
        Change_Request__c chq1 = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id );
        Change_Request__c chq2 = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id );
                                                        
        test.startTest();
            insert chq;
            
            chq = [ Select Id, PCN_Number__c from Change_Request__c ];
            system.assertEquals( chq.PCN_Number__c, 'Doc 1 - PCN - 001' );
            
            insert chq1;
            chq1 = [ Select Id, PCN_Number__c from Change_Request__c where Id = :chq1.Id ];
            system.assertEquals( chq1.PCN_Number__c, 'Doc 1 - PCN - 002' );
            
            insert chq2;
            chq2 = [ Select Id, PCN_Number__c from Change_Request__c where Id = :chq2.Id ];
            system.assertEquals( chq2.PCN_Number__c, 'Doc 1 - PCN - 003' );
        test.stopTest();
    }
    
    static testmethod void pcnNumberCheckDefault()
    {
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Title__c = 'Test', Project_Area__c = proj.Id );
        test.startTest();
            insert chq;
            
            chq = [ Select Id, PCN_Number__c from Change_Request__c ];
            system.assertEquals( chq.PCN_Number__c, 'PCN - 000' );
        test.stopTest();
    }
}