@isTest
private class HOG_PVRUpdateTest {

    //Test variables
    private static Id wellIdRecordTypeId;
    private static Id wellEventRecordTypeId;
    private static Business_Unit__c businessUnit;
    private static Business_Department__c businessDepartment;
    private static Operating_District__c operatingDistrict;
    private static Field__c field;
    private static Route__c route;

    
    @isTest static void Test_HOG_PVRUpdateBatch_PVRTest() {
        SetupPVRTestData();

        //Conduct Test
        Test.startTest();
            HOG_PVRUpdateBatch pvrUpdateBatch = new HOG_PVRUpdateBatch();
            Database.executeBatch(pvrUpdateBatch);
        Test.stopTest();

        //Test Results
        List<Location__c> wellIDLocations = [Select Id, Name, RecordTypeId, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c, 
                                                    PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c, 
                                                    PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,
                                                    PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,
                                                    PVR_AVGVOL_5D_PROD__c, PVR_AVGVOL_30D_PROD__c, PVR_Hours_On_Prod__c, PVR_Month_To_Day_Generated__c,
                                                    PVR_Produced_Cond__c, PVR_Produced_Gas__c, PVR_Produced_Oil__c, PVR_Produced_Record_Count__c,
                                                    PVR_Produced_Sand__c, PVR_Produced_Water__c, PVR_GOR_Factor__c, 
                                                    PVR_Fuel_Consumption__c, Measured_Vent_Rate__c
                                             From Location__c
                                             Where RecordTypeId =: wellIdRecordTypeId];
        System.assert(wellIDLocations != null && !wellIDLocations.isEmpty() && wellIDLocations.size() == 1);
        Location__c wellIDLocation = wellIDLocations[0];
        List<AggregateResult> results = [Select     Well_ID__c,
                                                    SUM(PVR_AVGVOL_30D_COND__c) TOTAL_PVR_AVGVOL_30D_COND, SUM(PVR_AVGVOL_30D_GAS__c) TOTAL_PVR_AVGVOL_30D_GAS, 
                                                    SUM(PVR_AVGVOL_30D_OIL__c) TOTAL_PVR_AVGVOL_30D_OIL, SUM(PVR_AVGVOL_30D_SAND__c) TOTAL_PVR_AVGVOL_30D_SAND, 
                                                    SUM(PVR_AVGVOL_30D_WATER__c) TOTAL_PVR_AVGVOL_30D_WATER, 
                                                    SUM(PVR_AVGVOL_5D_COND__c) TOTAL_PVR_AVGVOL_5D_COND, SUM(PVR_AVGVOL_5D_GAS__c) TOTAL_PVR_AVGVOL_5D_GAS, 
                                                    SUM(PVR_AVGVOL_5D_OIL__c) TOTAL_PVR_AVGVOL_5D_OIL, SUM(PVR_AVGVOL_5D_SAND__c) TOTAL_PVR_AVGVOL_5D_SAND, 
                                                    SUM(PVR_AVGVOL_5D_WATER__c) TOTAL_PVR_AVGVOL_5D_WATER,
                                                    SUM(PVR_Hours_On_Prod__c) TOTAL_PVR_HOURS_ON_PROD, SUM(PVR_Produced_Cond__c) TOTAL_PVR_PRODUCED_COND, 
                                                    SUM(PVR_Produced_Gas__c) TOTAL_PVR_PRODUCED_GAS, SUM(PVR_Produced_Oil__c) TOTAL_PVR_PRODUCED_OIL, 
                                                    SUM(PVR_Produced_Record_Count__c) TOTAL_PVR_PRODUCED_RECORD_COUNT,
                                                    SUM(PVR_Produced_Sand__c) TOTAL_PVR_PRODUCED_SAND, SUM(PVR_Produced_Water__c) TOTAL_PVR_PRODUCED_WATER,
                                                    SUM(PVR_GOR_Factor__c) TOTAL_PVR_GOR_FACTOR, SUM(PVR_Fuel_Consumption__c) TOTAL_PVR_FUEL_CONSUMPTION, 
                                                    SUM(Measured_Vent_Rate__c) TOTAL_MEASURED_VENT_RATE
                                        From Well_Event__c
                                        Where Well_ID__c =: wellIDLocation.Id
                                        And Status__c = 'PROD'
                                        Group By Well_ID__c];
        System.assert(results != null && !results.isEmpty() && results.size() == 1);
        System.assertEquals(wellIDLocation.PVR_AVGVOL_30D_COND__c, results[0].get('TOTAL_PVR_AVGVOL_30D_COND'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_30D_GAS__c, results[0].get('TOTAL_PVR_AVGVOL_30D_GAS'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_30D_OIL__c, results[0].get('TOTAL_PVR_AVGVOL_30D_OIL'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_30D_SAND__c, results[0].get('TOTAL_PVR_AVGVOL_30D_SAND'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_30D_WATER__c, results[0].get('TOTAL_PVR_AVGVOL_30D_WATER'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_5D_COND__c, results[0].get('TOTAL_PVR_AVGVOL_5D_COND'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_5D_GAS__c, results[0].get('TOTAL_PVR_AVGVOL_5D_GAS'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_5D_OIL__c, results[0].get('TOTAL_PVR_AVGVOL_5D_OIL'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_5D_SAND__c, results[0].get('TOTAL_PVR_AVGVOL_5D_SAND'));
        System.assertEquals(wellIDLocation.PVR_AVGVOL_5D_WATER__c, results[0].get('TOTAL_PVR_AVGVOL_5D_WATER'));

        System.assertEquals(wellIDLocation.PVR_Hours_On_Prod__c, results[0].get('TOTAL_PVR_HOURS_ON_PROD'));
        System.assertEquals(wellIDLocation.PVR_Produced_Cond__c, results[0].get('TOTAL_PVR_PRODUCED_COND'));
        System.assertEquals(wellIDLocation.PVR_Produced_Gas__c, results[0].get('TOTAL_PVR_PRODUCED_GAS'));
        System.assertEquals(wellIDLocation.PVR_Produced_Oil__c, results[0].get('TOTAL_PVR_PRODUCED_OIL'));
        System.assertEquals(wellIDLocation.PVR_Produced_Record_Count__c, results[0].get('TOTAL_PVR_PRODUCED_RECORD_COUNT'));
        System.assertEquals(wellIDLocation.PVR_Produced_Sand__c, results[0].get('TOTAL_PVR_PRODUCED_SAND'));
        System.assertEquals(wellIDLocation.PVR_Produced_Water__c, results[0].get('TOTAL_PVR_PRODUCED_WATER'));
        
        System.assertEquals(wellIDLocation.PVR_GOR_Factor__c, results[0].get('TOTAL_PVR_GOR_FACTOR'));
        System.assertEquals(wellIDLocation.PVR_Fuel_Consumption__c, results[0].get('TOTAL_PVR_FUEL_CONSUMPTION'));
        System.assertEquals(wellIDLocation.Measured_Vent_Rate__c, results[0].get('TOTAL_MEASURED_VENT_RATE'));
    }

    @isTest static void Test_HOG_PVRUpdateBatch_ProductStrategy_PROD_Test() {
        Setup_ProductStrategy_PROD_Test_Data();

        //Conduct Test
        Test.startTest();
            HOG_PVRUpdateBatch pvrUpdateBatch = new HOG_PVRUpdateBatch();
            Database.executeBatch(pvrUpdateBatch);
        Test.stopTest();

        //Test Results
        List<Location__c> wellIDLocations = [Select Id, Name, RecordTypeId, Product_Strategy__c
                                             From Location__c
                                             Where RecordTypeId =: wellIdRecordTypeId];
        System.assertEquals(wellIDLocations[0].Product_Strategy__c, 'CHOPS');
    }

    @isTest static void Test_HOG_PVRUpdateBatch_ProductStrategy_SHUT_Test() {
        Setup_ProductStrategy_SHUT_Test_Data();

        //Conduct Test
        Test.startTest();
            HOG_PVRUpdateBatch pvrUpdateBatch = new HOG_PVRUpdateBatch();
            Database.executeBatch(pvrUpdateBatch);
        Test.stopTest();

        //Test Results
        List<Location__c> wellIDLocations = [Select Id, Name, RecordTypeId, Product_Strategy__c
                                             From Location__c
                                             Where RecordTypeId =: wellIdRecordTypeId];
        System.assertEquals(wellIDLocations[0].Product_Strategy__c, 'GAS');
    }

    @isTest static void Test_HOG_PVRUpdateBatch_ProductStrategy_SUSP_Test() {
        Setup_ProductStrategy_SUSP_Test_Data();

        //Conduct Test
        Test.startTest();
            HOG_PVRUpdateBatch pvrUpdateBatch = new HOG_PVRUpdateBatch();
            Database.executeBatch(pvrUpdateBatch);
        Test.stopTest();

        //Test Results
        List<Location__c> wellIDLocations = [Select Id, Name, RecordTypeId, Product_Strategy__c
                                             From Location__c
                                             Where RecordTypeId =: wellIdRecordTypeId];
        System.assertEquals(wellIDLocations[0].Product_Strategy__c, 'THERMAL');
    }
    
    @isTest static void Test_HOG_PVRUpdateSchedulable() {
        SetupPVRTestData();

        //Conduct Test
        Test.startTest();
        String jobId = HOG_PVRUpdateBatchSchedulable.scheduleJob();

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
                                 NextFireTime
                          FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(HOG_PVRUpdateBatchSchedulable.cronString, ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }

    private static void setUpFlocData() {
        businessUnit = new Business_Unit__c
            (           
                Name = 'Test Business Unit'
            );            
        insert businessUnit;

        businessDepartment = new Business_Department__c
            (           
                Name = 'Test Business Department'
            );            
        insert businessDepartment;

        operatingDistrict = new Operating_District__c
            (           
                Name = 'Test Field',
                Business_Department__c = businessDepartment.Id,
                Business_Unit__c = businessUnit.Id
            );            
        insert operatingDistrict;

        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        field = new Field__c
            (           
                Name = 'Test Field Control Centre',
                Operating_District__c = operatingDistrict.Id,
                RecordTypeId = recordTypeId
            );            
        insert field;
        
        route = new Route__c
              (
                  Name = '999'
              );     
        insert route;
    }

    private static void SetupPVRTestData() {
        setUpFlocData();

        //Create WellId Location
        wellIdRecordTypeId = [Select Id, Name, DeveloperName
                                 From RecordType
                                 Where DeveloperName = 'Well_ID'][0].Id;
        Location__c wellIDLocation = new Location__c (
                Name = 'Test Well Id Location',
                RecordTypeId = wellIdRecordTypeId,
                Route__c = route.id, 
                Operating_Field_AMU__c = field.id
            );
        insert wellIDLocation;

        //Create Well Events
        List<Well_Event__c> wellEvents = new List<Well_Event__c>();
        Well_Event__c wellEvent1 = new Well_Event__c (
                Name = 'Test Well Event 1',
                Route__c = route.id,
                Single_Well_Battery__c = true,
                PVR_AVGVOL_30D_COND__c = 10,
                PVR_AVGVOL_30D_GAS__c = 10,
                PVR_AVGVOL_30D_OIL__c= 10,
                PVR_AVGVOL_30D_SAND__c = 10,
                PVR_AVGVOL_30D_WATER__c = 10,
                PVR_AVGVOL_5D_COND__c = 10,
                PVR_AVGVOL_5D_GAS__c = 10,
                PVR_AVGVOL_5D_OIL__c = 10,
                PVR_AVGVOL_5D_SAND__c = 10,
                PVR_AVGVOL_5D_WATER__c = 10,
                PVR_Hours_On_Prod__c = 10,
                PVR_Produced_Cond__c = 10,
                PVR_Produced_Gas__c = 10,
                PVR_Produced_Oil__c = 10,
                PVR_Produced_Record_Count__c = 10,
                PVR_Produced_Sand__c = 10,
                PVR_Produced_Water__c = 10,
                PVR_GOR_Factor__c = 10,
                PVR_Fuel_Consumption__c = 10,
                Measured_Vent_Rate__c = 10,
                Well_ID__c = wellIDLocation.Id,
                Status__c = 'PROD'
            );

        Well_Event__c wellEvent2 = new Well_Event__c (
                Name = 'Test Well Event 2',
                Route__c = route.id,
                Single_Well_Battery__c = true,
                PVR_AVGVOL_30D_COND__c = 10,
                PVR_AVGVOL_30D_GAS__c = 10,
                PVR_AVGVOL_30D_OIL__c= 10,
                PVR_AVGVOL_30D_SAND__c = 10,
                PVR_AVGVOL_30D_WATER__c = 10,
                PVR_AVGVOL_5D_COND__c = 10,
                PVR_AVGVOL_5D_GAS__c = 10,
                PVR_AVGVOL_5D_OIL__c = 10,
                PVR_AVGVOL_5D_SAND__c = 10,
                PVR_AVGVOL_5D_WATER__c = 10,
                PVR_Hours_On_Prod__c = 10,
                PVR_Produced_Cond__c = 10,
                PVR_Produced_Gas__c = 10,
                PVR_Produced_Oil__c = 10,
                PVR_Produced_Record_Count__c = 10,
                PVR_Produced_Sand__c = 10,
                PVR_Produced_Water__c = 10,
                PVR_GOR_Factor__c = 10,
                PVR_Fuel_Consumption__c = 10,
                Measured_Vent_Rate__c = 10,
                Well_ID__c = wellIDLocation.Id,
                Status__c = 'SHUT'
            );
        wellEvents.add(wellEvent1);
        wellEvents.add(wellEvent2);
        insert wellEvents;
    }

    private static void Setup_ProductStrategy_PROD_Test_Data() {
        setUpFlocData();

        //Create WellId Location
        wellIdRecordTypeId = [Select Id, Name, DeveloperName
                                 From RecordType
                                 Where DeveloperName = 'Well_ID'][0].Id;
        Location__c wellIDLocation = new Location__c (
                Name = 'Test Well Id Location',
                RecordTypeId = wellIdRecordTypeId,
                Route__c = route.id, 
                Operating_Field_AMU__c = field.id
            );
        insert wellIDLocation;

        List<Well_Event__c> wellEvents = new List<Well_Event__c>();
        Well_Event__c wellEvent1 = new Well_Event__c (
            Name = 'Test Well Event 1',
            Route__c = route.id,
            Product_Strategy__c = 'CHOPS',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'PROD'
        );
        Well_Event__c wellEvent2 = new Well_Event__c (
            Name = 'Test Well Event 2',
            Route__c = route.id,
            Product_Strategy__c = 'GAS',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SHUT'
        );
        Well_Event__c wellEvent3 = new Well_Event__c (
            Name = 'Test Well Event 3',
            Route__c = route.id,
            Product_Strategy__c = 'THERMAL',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SUSP'
        );
        wellEvents.addAll(new List<Well_Event__c>{wellEvent1, wellEvent2, wellEvent3});
        insert wellEvents;
    }

    private static void Setup_ProductStrategy_SHUT_Test_Data() {
        setUpFlocData();

        //Create WellId Location
        wellIdRecordTypeId = [Select Id, Name, DeveloperName
                                 From RecordType
                                 Where DeveloperName = 'Well_ID'][0].Id;
        Location__c wellIDLocation = new Location__c (
                Name = 'Test Well Id Location',
                RecordTypeId = wellIdRecordTypeId,
                Route__c = route.id, 
                Operating_Field_AMU__c = field.id
            );
        insert wellIDLocation;

        List<Well_Event__c> wellEvents = new List<Well_Event__c>();
        Well_Event__c wellEvent1 = new Well_Event__c (
            Name = 'Test Well Event 1',
            Route__c = route.id,
            Product_Strategy__c = 'GAS',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SHUT'
        );
        Well_Event__c wellEvent2 = new Well_Event__c (
            Name = 'Test Well Event 2',
            Route__c = route.id,
            Product_Strategy__c = 'THERMAL',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SUSP'
        );
        Well_Event__c wellEvent3 = new Well_Event__c (
            Name = 'Test Well Event 3',
            Route__c = route.id,
            Product_Strategy__c = 'THERMAL',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SUSP'
        );
        wellEvents.addAll(new List<Well_Event__c>{wellEvent1, wellEvent2, wellEvent3});
        insert wellEvents;
    }

    private static void Setup_ProductStrategy_SUSP_Test_Data() {
        setUpFlocData();

        //Create WellId Location
        wellIdRecordTypeId = [Select Id, Name, DeveloperName
                                 From RecordType
                                 Where DeveloperName = 'Well_ID'][0].Id;
        Location__c wellIDLocation = new Location__c (
                Name = 'Test Well Id Location',
                RecordTypeId = wellIdRecordTypeId,
                Route__c = route.id, 
                Operating_Field_AMU__c = field.id
            );
        insert wellIDLocation;

        List<Well_Event__c> wellEvents = new List<Well_Event__c>();
        Well_Event__c wellEvent1 = new Well_Event__c (
            Name = 'Test Well Event 1',
            Route__c = route.id,
            Product_Strategy__c = 'GAS',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SUSP'
        );
        Well_Event__c wellEvent2 = new Well_Event__c (
            Name = 'Test Well Event 2',
            Route__c = route.id,
            Product_Strategy__c = 'THERMAL',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'ABAN'
        );
        Well_Event__c wellEvent3 = new Well_Event__c (
            Name = 'Test Well Event 3',
            Route__c = route.id,
            Product_Strategy__c = 'THERMAL',
            Well_ID__c = wellIDLocation.Id,
            Status__c = 'SUSP'
        );
        wellEvents.addAll(new List<Well_Event__c>{wellEvent1, wellEvent2, wellEvent3});
        insert wellEvents;
    }
    
}