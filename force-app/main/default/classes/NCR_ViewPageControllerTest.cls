@isTest(SeeAllData=true)
private class NCR_ViewPageControllerTest{
    static testMethod void testNCR_ViewPageControllerTest(){
        //Create NCR user
        User ncrAdminUser = NCRUserTestData.createNCRAdminUser();
        
        System.runAs(ncrAdminUser){
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            System.AssertNotEquals(ncrAdminUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);  
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
        
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true);  
            
            System.AssertNotEquals(ncrCase.Id, null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);
            System.AssertNotEquals((sObject)standardController.getRecord(), null);
                        
            NCR_ViewPageControllerX controller = new NCR_ViewPageControllerX(standardController); 
            
            System.AssertNotEquals(controller.exit(), null); 
            System.AssertEquals(controller.NCR_Edit(), null);
            System.AssertEquals(controller.NCR_CancelEdit(), null);
            
            System.AssertNotEquals(controller.getViewScreenStateList(), null); 
            System.AssertNotEquals(controller.getViewScreenStateMap(), null); 
            
            System.AssertEquals(controller.NCR_GenericAction(0), null);  
            System.AssertEquals(controller.NCR_Action1(), null);                                 
        }    
    }
    
    static testMethod void testNCR_ViewPageControllerNCRSave(){
        //Create NCR user
        User ncrAdminUser = NCRUserTestData.createNCRAdminUser();
        
        System.runAs(ncrAdminUser){
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            System.AssertNotEquals(ncrAdminUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);  
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
        
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true);  
            
            System.AssertNotEquals(ncrCase.Id, null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);
            System.AssertNotEquals((sObject)standardController.getRecord(), null);
                        
            NCR_ViewPageControllerX controller = new NCR_ViewPageControllerX(standardController);
            
            ncrCase.Short_Description_of_Non_Conformance__c = 'Updated Short Description';   
            
            //pageRef = controller.NCR_Save();        
            System.AssertNotEquals(pageRef, null);                                                 
        }    
    }  
    
}