@isTest (seeAllData=true)
private class TestEmulsion
{
	static Opportunity createEmulsionOpportunityTestData()
	{
		// Create Account
        Account acc = new Account(Name = 'Test Account');
        Database.saveResult accountResult = Database.insert(acc);
        
        // Make ATS Tender
        ATS_Parent_Opportunity__c atstender = new ATS_Parent_Opportunity__c(Sales_Type__c                = 'Tender',
                                                                            Sales_Status__c              = 'Active',
                                                                            Trade_Class__c               = 'Construction',
                                                                            Country__c                   = 'Canada',
                                                                            Stage__c                     = 'Bid Initiation - Marketer',
                                                                            Destination_City_Province__c = 'Destination',
                                                                            For_Season__c                = '2014',
                                                                            Marketer__c                  = UserInfo.getUserId(),
                                                                            Province_State__c            = 'British Columbia',
                                                                            Region__c                    = 'BC Kootenay',
                                                                            Closest_City__c              = 'Kamloops',
                                                                            Bid_Due_Date_Time__c         = Datetime.now(),
                                                                            Freight_Due_Date_Time__c     = Datetime.now(),
                                                                            Product_Category__c          = 'Emulsion',
                                                                            Bid_Description__c           = 'Description');
        Database.saveResult atstenderResult = Database.insert(atstender);
        
        // Get Opportunity record type
       Map<String, Schema.Recordtypeinfo> recTypesByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
        
        // Create Asphalt Opportunity
        Opportunity opp = new Opportunity(Name                                = 'Test Emulsion',
                                          StageName                           = 'Opportunity Initiation - Marketer',
                                          CloseDate                           = Date.newInstance(2014,1,1),
                                          RecordTypeId                        = recTypesByName.get('ATS: Emulsion Product Category').getRecordTypeId(),
                                          Opportunity_ATS_Product_Category__c = atstenderResult.getId(),
                                          AccountId                           = accountResult.getId(),
                                          Pay_out_Handling_Fees__c            = 'No');
        Database.saveResult opportunityResult = Database.insert(opp);
        return opp;
	}
	static PricebookEntry createEmulsionPricebook()
	{
		// Get Product2 record type
        Map<String, Schema.Recordtypeinfo> recTypesByName = Product2.SObjectType.getDescribe().getRecordTypeInfosByName();
        
        // Create Product
        Product2 prod = new Product2(Name         = 'Test Product',
                                     Density__c   = 2,
                                     RecordTypeId = recTypesByName.get('Emulsion').getRecordTypeId(),
                                     IsActive = true);
        Database.saveResult productResult = Database.insert(prod);
		 // Fetch price book entry
        return [SELECT Id
                    FROM PriceBookEntry
                    WHERE Product2Id      =: productResult.getId() AND
                          Pricebook2.Name =: 'Emulsion Price Book'
                    LIMIT 1];
        
	}
    static testMethod void testEmulsion()
    {
        Opportunity opp = createEmulsionOpportunityTestData();
        PriceBookEntry pricebook = createEmulsionPricebook();

        // Start test
        test.startTest();
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opp.Id,
                                                          PricebookEntryId = pricebook.Id);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
        // Because we've inserted an Asphalt opportunity line item, the trigger 'SetNetback' should have fired to calculate it's netbacks.
        // Everything is null so the netbacks should be too.
        // Fetch the oli from the database
        OpportunityLineItem newOli = [SELECT Id,
                                             Axle_8_Price__c,
                                             Axle_7_Price__c,
                                             Axle_6_Price__c,
                                             Axle_5_Price__c,
                                             Emulsion_Margin_CAD__c
                                      FROM OpportunityLineItem
                                      WHERE Id =: opportunityLineItemResult.getId()];
        
        // Ensure netbacks are null.
        System.assertEquals(null, newOli.Axle_8_Price__c);
        System.assertEquals(null, newOli.Axle_7_Price__c);
        System.assertEquals(null, newOli.Axle_6_Price__c);
        System.assertEquals(null, newOli.Axle_5_Price__c);
        
        // Now we'll update Emulsion Margin (CAD) fields to fire the trigger again.
        newOli.Emulsion_Margin_CAD__c = 100;
        
        // Commit changes to database
        update newOli;
        
        // Re-fetch the Opportunity Line Item and check the netbacks.
        OpportunityLineItem newOli2 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be all 100
        System.assertEquals(100, newOli2.Axle_8_Price__c);
        System.assertEquals(100, newOli2.Axle_7_Price__c);
        System.assertEquals(100, newOli2.Axle_6_Price__c);
        System.assertEquals(100, newOli2.Axle_5_Price__c);
        
        // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter');
        Database.saveResult freightResult = Database.insert(freight);
        
        // The ATS Freight trigger 'SetOliNetbackFromFreight' should have fired, updating nothing since Prices FOB = 'Origin'
        // Make sure this is true.
        OpportunityLineItem newOli3 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c,
                                              Emulsion_Cost_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be all 100
        System.assertEquals(100, newOli3.Axle_8_Price__c);
        System.assertEquals(100, newOli3.Axle_7_Price__c);
        System.assertEquals(100, newOli3.Axle_6_Price__c);
        System.assertEquals(100, newOli3.Axle_5_Price__c);
        
        // Now set all Opportunity Line Item fields. It will use Axle 8 Price and new fields.
        newOli3.Emulsion_Cost_CAD__c = 30;
        newOli3.Handling_Fees_CAD__c = 10;
        update newOli3;
        
        // Make sure that worked.
        OpportunityLineItem newOli4 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c, 
                                              Currency__c 
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be all 130
        System.assertEquals(130, newOli4.Axle_8_Price__c);
        System.assertEquals(130, newOli4.Axle_7_Price__c);
        System.assertEquals(130, newOli4.Axle_6_Price__c);
        System.assertEquals(130, newOli4.Axle_5_Price__c);
        
        newOli4.Currency__c = 'US';
        newOli4.Exchange_Rate_to_CAD__c = 2;
        update newOli4;
        test.StopTest();
    }
    static testMethod void testSetCompetitorMargin()
    {
    	Opportunity opp = createEmulsionOpportunityTestData();
        PriceBookEntry pricebook = createEmulsionPricebook();
        
        // Start test
        test.startTest();
         // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter',
                                                    Emulsion_Rate8_Competitor1__c = 10,
                                                    Emulsion_Rate8_Competitor2__c = 20,
                                                    Emulsion_Axle8_Rate_Competitor3__c = 30,
                                                    Emulsion_Axle8_Rate_Competitor4__c = 40);
        Database.saveResult freightResult = Database.insert(freight);
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opp.Id,
                                                          PricebookEntryId = pricebook.Id);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
        //test trigger OpportunityProduct_SetCompetitorMargin
        
        
        oli.Competitor_1__c = 'Cenex-Mandan';
        oli.Competitor_2__c = 'Colasphalt-Acheson (COAC)';
        oli.Competitor_3__c = 'Chevron-Vancouver';
        oli.Competitor_4__c = 'Esso-Winnipeg (Exchange) (EX: IOEE)';
        oli.Competitor_1_Bid_Price__c = 100;
        oli.Competitor_2_Bid_Price__c = 200;
        oli.Competitor_3_Bid_Price__c = 300;
        oli.Competitor_4_Bid_Price__c = 400;
        update oli;
        
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Competitor_1_Emulsion_Margin_CAD__c,
                                              Competitor_2_Emulsion_Margin_CAD__c,
                                              Competitor_3_Emulsion_Margin_CAD__c,
                                              Competitor_4_Emulsion_Margin_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        
        //100 - ( 10 * 1)                               
        System.assertEquals(90, newOli5.Competitor_1_Emulsion_Margin_CAD__c);
        //200 - (20 * 1)
        System.assertEquals(180, newOli5.Competitor_2_Emulsion_Margin_CAD__c);
        //300 - 30
        System.assertEquals(270, newOli5.Competitor_3_Emulsion_Margin_CAD__c);
        //400-40
        System.assertEquals(360, newOli5.Competitor_4_Emulsion_Margin_CAD__c);                               
        // Finish test
        test.stopTest();
    }
    static testMethod void testEmulsion2()
    {
        Opportunity opp = createEmulsionOpportunityTestData();
        PriceBookEntry pricebook = createEmulsionPricebook();
        
        // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Destination',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter');
        Database.saveResult freightResult = Database.insert(freight);
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity               = 1,
                                                          Unit__c                = 'Tonne',
                                                          Currency__c            = 'CAD',
                                                          TotalPrice             = 10,
                                                          OpportunityId          = opp.Id,
                                                          PricebookEntryId       = pricebook.Id);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
        //populate Margin and Cost to calculate Axle prices
        oli.Emulsion_Margin_CAD__c = 100;
        oli.Emulsion_Cost_CAD__c = 30;
        update oli;
        
        // Start test
        test.startTest();
        
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be all 130
        System.assertEquals(130, newOli5.Axle_8_Price__c);
        System.assertEquals(130, newOli5.Axle_7_Price__c);
        System.assertEquals(130, newOli5.Axle_6_Price__c);
        System.assertEquals(130, newOli5.Axle_5_Price__c);
        
        // Now set the freight axle values to something. The trigger will fire and update the oli.
        ATS_Freight__c freight3 = new ATS_Freight__c(Id                           = freightResult.getId(),
                                                     Emulsion_Rate8_Supplier1__c  = 11,
                                                     Emulsion_Rate8_Supplier2__c  = 9,
                                                     Emulsion_Rate6_Supplier_1__c = 21,
                                                     Emulsion_Rate6_Supplier2__c  = 13,
                                                     Emulsion_Rate7_Supplier1__c = 70,
                                                     Emulsion_Rate7_Supplier2__c = 70,
                                                     Emulsion_Rate5_Supplier1__c = 50,
                                                     Emulsion_Rate5_Supplier2__c = 50);
        update freight3;
        
        // Make sure that worked.
        OpportunityLineItem newOli6 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be 100 + 30 + 11
        System.assertEquals(141, newOli6.Axle_8_Price__c);
        // Should be 100 + 30 + 70
        System.assertEquals(200, newOli6.Axle_7_Price__c);
        // Should be 100 + 30 + 21
        System.assertEquals(151, newOli6.Axle_6_Price__c);
        // Should be 100 + 30 + 50
        System.assertEquals(180, newOli6.Axle_5_Price__c);
        
        // Now switch the Freight supplier to #2. This will use a value of 9 in Liters.
        // Conversion from tonnes to liters (density = 2) is .002
        ATS_Freight__c freight4 = new ATS_Freight__c(Id                           = freightResult.getId(),
                                                     Husky_Supplier_1_Selected__c = false,
                                                     Husky_Supplier_2_Selected__c = true);
        update freight4;
        
        // Make sure that worked.
        OpportunityLineItem newOli7 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c,
                                              Currency__c,
                                              Exchange_Rate_to_CAD__c,
                                              Emulsion_Margin_CAD__c,
                                              Emulsion_Cost_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be 100 + 30 + [ convertLiterToTone(9) = 1000/2 *  9)
        System.assertEquals(4630, newOli7.Axle_8_Price__c);
        // Should be 100 + 30 + [ convertLiterToTone(70) = 1000/2 *  70)
        System.assertEquals(35130, newOli7.Axle_7_Price__c);
        // Should be 100 + 30 + [ convertLiterToTone(13) = 1000/2 *  13)
        System.assertEquals(6630, newOli7.Axle_6_Price__c);
        // Should be 100 + 30 + [ convertLiterToTone(50) = 1000/2 *  50)
        System.assertEquals(25130, newOli7.Axle_5_Price__c);
        
        // Now change the currency and exchange rate on the oli
        newOli7.Currency__c = 'US';
        newOli7.Exchange_Rate_to_CAD__c = null;
        update newOli7;
        
        // The trigger should have fired... Make sure that worked.
        OpportunityLineItem newOli8 = [SELECT Id,
                                              Axle_8_Price__c,
                                              Axle_7_Price__c,
                                              Axle_6_Price__c,
                                              Axle_5_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
  /*      // Should be (100 + 30)/2 + (1000/2 * 9)
        System.assertEquals(4565, newOli8.Axle_8_Price__c);
        // Should be (100 + 30) / 2
        System.assertEquals(65, newOli8.Axle_7_Price__c);
        // Should be (100 + 30)/2 +  (1000/2 * 13)
        System.assertEquals(6565, newOli8.Axle_6_Price__c);
        // Should be (100 + 30) / 2
        System.assertEquals(65, newOli8.Axle_5_Price__c);  */
        
        delete freight4;
        
        // Finish test
        test.stopTest();
    }
}