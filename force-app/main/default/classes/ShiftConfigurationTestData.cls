public with sharing class ShiftConfigurationTestData {
	
	public static Shift_Configuration__c createShiftConfiguration(String name, String startTime, String endTime, Boolean Sun, Boolean Mon, Boolean Tue, Boolean Wed, Boolean Thu, Boolean Fri, Boolean Sat, Boolean enabled) {
		Shift_Configuration__c shiftConfig = new Shift_Configuration__c();
		
		shiftConfig.Name = name;
		shiftConfig.Start_Time__c = startTime;
		shiftConfig.End_Time__c = endTime;
		shiftConfig.Sunday__c = Sun;
		shiftConfig.Monday__c = Mon;
		shiftConfig.Tuesday__c = Tue;
		shiftConfig.Wednesday__c = Wed;
		shiftConfig.Thursday__c = Thu;
		shiftConfig.Friday__c = Fri;
		shiftConfig.Saturday__c = Sat;
		shiftConfig.Enabled__c = enabled;
		
		return shiftConfig;
	}
}