@isTest
global with sharing class SAPHOGWorkOrderResponseMock implements WebServiceMock {
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        
        SAPHOGWorkOrderServices.CreateWorkOrderResponse responseElement = new SAPHOGWorkOrderServices.CreateWorkOrderResponse();
        
        responseElement.WorkOrder = new SAPHOGWorkOrderServices.WorkOrder();

        responseElement.WorkOrder.WorkOrderNumber = '24173534';
        responseElement.WorkOrder.FunctionalLocationObjectId = null;
        responseElement.WorkOrder.EquipmentNumber = null;
        responseElement.WorkOrder.MaintenancePlanningPlant = null;
        responseElement.WorkOrder.PlannerGroup = null;
        responseElement.WorkOrder.PlantSection = null;
		responseElement.WorkOrder.MainDescription = null;
		responseElement.WorkOrder.MainLongDescription = null;
        responseElement.WorkOrder.Priority = null;
        responseElement.WorkOrder.Revision = null;
        responseElement.WorkOrder.OrderType = null;
        responseElement.WorkOrder.SortField = null;
        responseElement.WorkOrder.UserStatus = null;
        responseElement.WorkOrder.SystemStatus = null;
        responseElement.WorkOrder.SystemCondition = null;
        responseElement.WorkOrder.ABCIndicator = null;
        responseElement.WorkOrder.MainWorkCenter = null;
        responseElement.WorkOrder.MaintenanceActivityType = null;
        responseElement.WorkOrder.ReferenceNotificationNumber = null;
        responseElement.WorkOrder.TECODateCompleted = null;
        responseElement.WorkOrder.StartDate = null;
        responseElement.WorkOrder.FinishDate = null;
        responseElement.WorkOrder.EnteredDate = null;
        responseElement.WorkOrder.EnteredBy = null;
        responseElement.WorkOrder.ChangedDate = null;
        responseElement.WorkOrder.ChangedBy = null;
        responseElement.WorkOrder.Operations = null;
		responseElement.type_x = True;
                
        response.put('response_x', responseElement);
    }
}