public class OpportunityLineItemTestData {

    public static OpportunityLineItem createOpportunityLineItem(Opportunity opp, PriceBookEntry priceBookEntry) {
        OpportunityLineItem oppLi = new OpportunityLineItem();
        oppLi.OpportunityId = opp.Id;
        oppLi.Opportunity = opp;
        oppLi.PricebookEntryId = priceBookEntry.Id;
        oppLi.Quantity = 100;
        oppLi.TotalPrice = 1000;
        
        return oppLi;
    }
}