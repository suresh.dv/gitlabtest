@isTest
public with sharing class FM_TruckTripExportControllerTest {

	@isTest
	static void testExportWithMultipleTTs(){
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.currentPageReference().getParameters().put('trucktrips', getTruckTripIds(8));
		System.runAs(runningUser) {
			Test.startTest();
				FM_TruckTripExportController controller = new FM_TruckTripExportController();
				System.assert(String.isNotBlank(controller.header));
				System.assert(String.isBlank(controller.carrier));
				System.assertEquals(0, controller.wrappedTruckTrips.size());
				controller.exportTrucktripsToCSV();
				System.assert(String.isNotBlank(controller.carrier));
				System.assertEquals(8, controller.wrappedTruckTrips.size());
			Test.stopTest();
		}
	}

	@isTest
	static void testExportWithSingleTT(){
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		String ttId = getTruckTripIds(1);
		System.currentPageReference().getParameters().put('trucktrips', ttId);
		System.runAs(runningUser) {
			Test.startTest();
				FM_TruckTripExportController controller = new FM_TruckTripExportController();
				System.assert(String.isNotBlank(controller.header));
				System.assert(String.isBlank(controller.carrier));
				System.assertEquals(0, controller.wrappedTruckTrips.size());
				controller.exportTrucktripsToCSV();
				System.assert(String.isNotBlank(controller.carrier));
				System.assertEquals(1, controller.wrappedTruckTrips.size());
			Test.stopTest();

			for(FM_Truck_Trip__c tt : [Select Id, Truck_Trip_Status__c
									   From FM_Truck_Trip__c
									   Where Id =: ttId])
				System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_EXPORTED, tt.Truck_Trip_Status__c);
		}
	}

	@isTest
	static void testExportWithoutTTs(){
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.currentPageReference().getParameters().put('trucktrips', '');
		System.runAs(runningUser) {
			Test.startTest();
				FM_TruckTripExportController controller = new FM_TruckTripExportController();
				System.assert(String.isNotBlank(controller.header));
				System.assert(String.isBlank(controller.carrier));
				System.assertEquals(0, controller.wrappedTruckTrips.size());
				controller.exportTrucktripsToCSV();
				System.assert(String.isBlank(controller.carrier));
				System.assertEquals(0, controller.wrappedTruckTrips.size());
			Test.stopTest();

			//Query Exported trucktrips and check status
			Set<Id> truckTripIds = new Set<Id>();
			for(FM_TruckTripExportController.TruckTripWrapper tt : controller.wrappedTruckTrips) {
				truckTripIds.add(tt.id);
			}
			for(FM_Truck_Trip__c tt : [Select Id, Truck_Trip_Status__c
									   From FM_Truck_Trip__c
									   Where Id In :truckTripIds]) {
				System.assertNotEquals(tt.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_EXPORTED);
			}
		}
	}

	private static String getTruckTripIds(Integer queryLimit) {
		String ids = '';
		for (FM_Truck_Trip__c tt : [SELECT Id FROM FM_Truck_Trip__c LIMIT :queryLimit]) {
			if (ids != '') ids += ',';
			ids += tt.Id;
		}
		return ids;
	}

	@testSetup
	static void createTestData() {
		Account acc = new Account();
		acc.Name = 'MI6 Account';
		insert acc;

		Carrier__c carrier = new Carrier__c();
        carrier.Carrier__c = acc.Id;
        carrier.Carrier_Name_For_Fluid__c = 'James Bond';
        insert carrier;

        Carrier_Unit__c carrierUnit = new Carrier_Unit__c();
        carrierUnit.Carrier__c = carrier.Id;
        carrierUnit.Unit_Email__c = 'bond@jamesbond.com';
        insert carrierUnit;

		Business_Department__c business = new Business_Department__c();
        business.Name = 'Husky Business';
        insert business;

        Operating_District__c district = new Operating_District__c();
        district.Name = 'District 9';
        district.Business_Department__c = business.Id;
        insert district;

        Field__c field = new Field__c();
        field.Name = 'AMU Field';
        field.Operating_District__c = district.Id;
        insert field;

		Route__c route = new Route__c();
        route.Fluid_Management__c = true;
        route.Name = '007';
        route.Route_Number__c = '007';
        insert route;

		Location__c loc = new Location__c();
        loc.Name = 'London';
        loc.Fluid_Location_Ind__c = true;
        loc.Location_Name_for_Fluid__c = 'London HQ';
        loc.Operating_Field_AMU__c = field.Id;
        loc.Route__c = route.Id;
		loc.Functional_Location_Category__c	= 4;
        insert loc;

		Equipment__c eq = EquipmentTestData.createEquipment(loc);
		eq.Equipment_Category__c = 'D';
		eq.Object_Type__c = 'VESS_ATMOS';
		update eq;

		Equipment_Tank__c eqt = new Equipment_Tank__c();
		eqt.Equipment__c = eq.id;
		eqt.Low_Level__c = 10;
 		eqt.Tank_Size_m3__c = 30;
		eqt.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt;

		//create run sheet for runSheet test
		FM_Run_Sheet__c rs = new FM_Run_Sheet__c();
        rs.Date__c = Date.today();
        rs.Well__c = loc.Id;
        rs.Act_Tank_Level__c = 40;
		rs.Standing_Comments__c = 'Test comment';
		rs.Axle__c = '6';
		rs.Load_Weight__c = 'Primary';
		rs.Act_Flow_Rate__c = 5;
		rs.Flowline_Volume__c = 10;
        rs.Tomorrow_Oil__c = 1;
        rs.Tomorrow_Water__c = 1;
        rs.Tonight_Oil__c = 1;
        rs.Tonight_Water__c = 1;
        insert rs;

		List<FM_Load_Request__c> rlList = new List<FM_Load_Request__c>();

		FM_Load_Request__c lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		Location__c loc2 = new Location__c();
        loc2.Name = 'Brezno';
        loc2.Fluid_Location_Ind__c = true;
        loc2.Location_Name_for_Fluid__c = 'Brezno HQ';
        loc2.Operating_Field_AMU__c = field.Id;
        loc2.Route__c = route.Id;
		loc2.Functional_Location_Category__c = 4;
        insert loc2;

		//Create load request for nonRunSheet test
		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		Facility__c fac = new Facility__c();
		fac.Name = 'Test fac';
		fac.Facility_Name_for_Fluid__c = 'Tst Facilty';
		fac.Fluid_Facility_Ind__c = true;
		fac.Fluid_Facility_Ind_Origin__c = true;
        fac.Operating_Field_AMU__c = field.Id;
		fac.Functional_Location_Category__c	= 3;
		fac.Plant_Section__c = route.id;

		insert fac;

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		insert rlList;
	}

	private static User createUser() {
		User user = new User();
        Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

        Double random = Math.Random();

        user.Email = 'TestUser' +  random + '@hog.com';
        user.Alias = 'TestUser' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.ProfileId = p.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.UserName = 'TestUser' + random + '@hog.com.unittest';

        insert user;
        return user;
	}

	private static User assignPermissionSet(User user, String userPermissionSetName){
         PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];
         PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();

         permissionSetAssignment.PermissionSetId = permissionSet.Id;
         permissionSetAssignment.AssigneeId = user.Id;

         insert permissionSetAssignment;

         return user;
     }

    private static User assignProfile (User user, String profileName){
        Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
        user.ProfileId = profile.Id;
        update user;
        return user;
    }

}