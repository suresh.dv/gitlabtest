public with sharing class VTT_IntegrationUtilities {
	public static final String STATUS_ERROR = 'Error';
	public static final String STATUS_SUCCESS = 'Success';
    public static final String TAG = 'VTT_IntegrationUtilities';
    public static final String RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR = 'Update_Activity_Status_Error';

    public static final Map<String, String> validStatusMap = 
    	new Map<String, String>{
    		VTT_Utilities.ACTIVITY_STATUS_NEW => '0NSC',
    		VTT_Utilities.ACTIVITY_STATUS_STARTED => '3BEX',
    		VTT_Utilities.ACTIVITY_STATUS_STARTEDATSITE => '3BEX',
    		VTT_Utilities.ACTIVITY_STATUS_FINISHEDATSITE => '3BEX',
    		VTT_Utilities.ACTIVITY_STATUS_FINISHEDFORTHEDAY => '3BEX',
    		VTT_Utilities.ACTIVITY_STATUS_ONHOLD => 'HOLD',
    		VTT_Utilities.ACTIVITY_STATUS_COMPLETED => '4COM'
    	};

    //Trigger Flag
    public static Boolean executeTriggerCode = true;

    @future(callout=true)
    public static void SAP_UpdateWorkOrderActivityStatuses(Set<Id> activityIdSet) {
        //Make callout and handle response
        UpdateActivitiesCallout(activityIdSet);
    }

    @Future(callout=true)
    public static void SAP_UpdateWorkOrderStatus(ID pWorkOrderID, String pNewUserStatus){
        SAPHOGWorkOrderServices.WorkOrderPort  workOrderService = new SAPHOGWorkOrderServices.WorkOrderPort();
        SAPHOGWorkOrderServices.UpdateWorkOrderResponse response;

        //requestList.add(request);
        system.debug('UpdateWorkOrder Callout...');
        system.debug('Work Order ID:' + pWorkOrderID);
        system.debug('Work Order User Status:' + pNewUserStatus);
        HOG_Maintenance_Servicing_Form__c pWorkOrder = [select id, name, Work_Order_Number__c, 
        System_Status_Code__c,
        User_Status_Code__c,
        SAP_Changed_Date__c,
        SAP_Changed_By__c
        from HOG_Maintenance_Servicing_Form__c where id = :pWorkOrderID limit 1][0];
        try
        {
            response = workOrderService.UpdateWorkOrder(pWorkOrder.Work_Order_Number__c,null,null,null,null,null,null,pNewUserStatus,pWorkOrder.SAP_Changed_Date__c,pWorkOrder.SAP_Changed_By__c,null);
            
            system.debug('Response:');
            system.debug(response);
            //return true;
        }
        catch(Exception e)
        {
            response = null;                                
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }       
    }

    @Future(callout=true)
    public static void SAP_UpdateNotification(String pActivityRecordID) {
        system.debug('SAP_UpdateNotification executing...');

        //sm 01/22/16 pass mal fun starte and mal func end date

        Work_Order_Activity__c activityRecord = [select id, name, 
        Maintenance_Work_Order__c,
        Maintenance_Work_Order__r.On_Hold__c,
        Maintenance_Work_Order__r.Equipment__c,
        Maintenance_Work_Order__r.Equipment__r.Catalogue_Code__c,
        Maintenance_Work_Order__r.Notification_Number__c,
        Maintenance_Work_Order__r.Main_Work_Centre__c,
        Operation_Number__c,
        Part_Key__c,
        Cause__c,
        Cause__r.Cause_Code__c,
        Cause__r.Code_Group__c,
        Cause_Text__c,
        Work_Details__c,
        Part__r.Catalogue_Code__c,
        Part__r.Part_Code__c,
        Part__r.Part_Description__c,
        Damage__c,
        Damage__r.Damage_Code__c,
        Damage_Text__c,
        Assigned_Vendor__c, Status__c, Status_Reason__c, User_Status__c,
        Maintenance_Work_Order__r.HOG_Service_Request_Notification_Form__r.Malfunction_Start_Date__c ,
        Maintenance_Work_Order__r.VTT_Activities_Count__c,
        Maintenance_Work_Order__r.VTT_Activities_Completed_Count__c
        from Work_Order_Activity__c where id=:pActivityRecordID][0];

        system.debug(activityRecord);

        DateTime malfunctionStartDate = activityRecord.Maintenance_Work_Order__r.HOG_Service_Request_Notification_Form__r.Malfunction_Start_Date__c;

        //sm if all activities are complete pass today as mal fun end date        
        DateTime malfunctionEndDate = activityRecord.Maintenance_Work_Order__r.VTT_Activities_Count__c == activityRecord.Maintenance_Work_Order__r.VTT_Activities_Completed_Count__c ?
        DateTime.Now() : null ;

        String returnPartKey = null;
        If (activityRecord != null 
            && activityRecord.Maintenance_Work_Order__r.Notification_Number__c !=null
            && integer.valueof(activityRecord.Maintenance_Work_Order__r.Notification_Number__c) !=0)
        {

            HOG_Cause__c causeRecord = 
            MaintenanceServicingGlobal.GetCauseCodeRecord(String.isBlank(activityRecord.Cause__c) 
                ? MaintenanceServicingUtilities.settingsHOG.Maintenance_WO_Default_Cause_Code__c
                : activityRecord.Cause__r.Cause_Code__c);

            String causeCode = causeRecord.Cause_Code__c;
            String causeCodeGroup = causeRecord.Code_Group__c;

            system.debug(causeRecord);


            String notificationNumber = activityRecord.Maintenance_Work_Order__r.Notification_Number__c;            
            String partCodeGroup = activityRecord.Part__r.Catalogue_Code__c;
            String partCode = activityRecord.Part__r.Part_Code__c;

            //SM 01.07.16
            //Today we found out that glorious MuleSof always create partKey = 0001 when we pass null as a partkey parameter
            //so we can't pass null or multiple activities as a part key
            //We decided to use Operation_Number__c instead
            String partKey = activityRecord.Operation_Number__c; //activityRecord.Operation_Number__c; //Part_Key__c;

            String damageCodeGroup = activityRecord.Part__r.Part_Code__c;
            String damageCode = activityRecord.Damage__r.Damage_Code__c;
            String damageDescription = activityRecord.Damage_Text__c;
            String newDamageLongDescription = null;
            //String causeCodeGroup = notificationFormRecord.Cause__r.Code_Group__c;
            //String causeCode = notificationFormRecord.Cause__r.Cause_Code__c;
            String causeDescription = activityRecord.Cause_Text__c;
            String newCauseLongDescription = activityRecord.Work_Details__c;
            String mainDescription = null; /*activityRecord.Work_Details__c;*/
            String mainLongDescription = null;

            System.debug('SAP_UpdateNotification->mainDescription: ' + mainDescription);
            
            // rbo 04.20.15 set to null values, no need to update these in SAP
            String coding = null;
            String codingGroup = null;
            //
            
            String mainWorkCenter = activityRecord.Maintenance_Work_Order__r.Main_Work_Centre__c;
            DateTime lastChangedDate = null;
            String lastChangedBy = null;
            String priority = null;

            SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();

            system.debug('partKey:' + partKey);

            try{
                SAPHOGNotificationServices.UpdateNotificationResponse notificationResponse = notification.UpdateNotification
                (
                    notificationNumber,
                    causeCodeGroup,
                    causeCode,
                    causeDescription,
                    newCauseLongDescription,
                    damageCodeGroup,
                    damageCode,
                    damageDescription,
                    newDamageLongDescription,
                    partCodeGroup,
                    partCode,
                    partKey,
                    mainDescription,
                    mainLongDescription,
                    coding,
                    codingGroup,
                    mainWorkCenter,
                    priority,
                    lastChangedDate,
                    lastChangedBy,
                        null, // equipment number, passing null value means retaining the current one
                        malfunctionStartDate, // malfunction start date, passing null value means retaining the current one
                        malfunctionEndDate,
                        null
                        );

                if (notificationResponse != null)
                {
                    //returnPartKey = notificationResponse.type_x ? notificationResponse.Notification.Items.NotificationItem[0].ItemKey : null;
                    //SM 1/27/16 we need last element of items
                    Integer itemIndex = notificationResponse.Notification.Items.NotificationItem.size() - 1 ; //get last element index
                    returnPartKey = notificationResponse.type_x ? notificationResponse.Notification.Items.NotificationItem[itemIndex].ItemKey : null;
                    activityRecord.Part_Key__c = returnPartKey;
                    update activityRecord; 
                }

            }
            catch(Exception e){
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }                                                                                                          
        }

        system.debug('PartKey: ' + returnPartKey);
    }

   /**
    * Make callout to SAP and change multiple activities
    * @param activities [Activities with changed statuses]
    * @return response  [Response from SAP]
    */
    public static SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse SAP_UpdateWorkOrderActivityStatuses(List<Work_Order_Activity__c> activities) {
        List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest> updateRequestList = new List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest>();

        //Initialize Service
        SAPHOGWorkOrderServices.WorkOrderPort  workOrderService = new SAPHOGWorkOrderServices.WorkOrderPort();

        //Create Request List
        for(Work_Order_Activity__c activity : activities) {
            SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest request = new SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest();
            request.ActivityNumber = activity.Operation_Number__c;
            request.ActivitySubNumber = activity.Sub_Operation_Number__c;
            request.WorkOrderNumber = activity.Maintenance_Work_Order__r.Work_Order_Number__c;
            request.UserStatus = activity.User_Status__c;
            request.MainDescription = activity.Description__c;
            updateRequestList.add(request);
        }

        
        SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse response = workOrderService.UpdateWorkOrderActivities(updateRequestList);
        system.debug('UpdateWorkOrderActivities Callout...');
        system.debug('Response:');
        system.debug(response);
        return response;
    }

    /**
    * Function to handle callout response
    * @param response response from webservice
    * @param activityList activities that were updated
    * @return activityList  [Response from SAP]
    */
    public static void handleActivityUpdateResponse(SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse response,
            List<Work_Order_Activity__c> activityList) {
        List<VTT_Integration_Error__c> integrationErrorList = new List<VTT_Integration_Error__c>();
        List<Work_Order_Activity__c> activitiesUpdatedSuccessfully = new List<Work_Order_Activity__c>();
        if(response.type_x == false) {
            integrationErrorList.addAll(VTT_IntegrationUtilities.createIntegrationErrors(activityList, 'SAP Returned the following error: ' + response.Message));
        } else {
            if(response.WorkOrderActivityResponseList == null || response.WorkOrderActivityResponseList.isEmpty()) {
                integrationErrorList.addAll(VTT_IntegrationUtilities.createIntegrationErrors(activityList, 'No response retured for actities.'));
            } else {
                for(Integer i=0; i < response.WorkOrderActivityResponseList.size(); i++) {
                    SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse activityResponse =
                        response.WorkOrderActivityResponseList[i];
                    if(activityResponse.type_x == false) {
                        System.debug('activityResponse.type_x: ' + activityResponse.type_x);
                        System.debug('activityResponse.message: ' + activityResponse.message);
                        integrationErrorList.add(VTT_IntegrationUtilities.createIntegrationError(activityList[i], activityResponse));
                    } else {
                        activityList[i].User_Status__c = 
                            VTT_IntegrationUtilities.validStatusMap.get(activityList[0].Status__c);
                        activitiesUpdatedSuccessfully.add(activityList[i]);
                    }
                }
            }
        }

        //DML
        upsert integrationErrorList;
        VTT_IntegrationUtilities.InvalidateIntegrationErrors(activitiesUpdatedSuccessfully);
    }

    /**
     * Deletes/Invalidates integration errors so they are not picked up on next schedule run
     * @param activityList [List of activities with updated User Status]
     */
    public static void InvalidateIntegrationErrors(List<Work_Order_Activity__c> activityList){
    	Set<Id> activityIdSet = new Set<Id>();
    	List<VTT_Integration_Error__c> errorsToUpdate = new List<VTT_Integration_Error__c>();

    	//Get Activity Ids with matching User Status and Status
    	for(Work_Order_Activity__c activity : activityList) {
    		if(validStatusMap.containsKey(activity.Status__c) && activity.User_Status__c.contains(
    			validStatusMap.get(activity.Status__c)))
    			activityIdSet.add(activity.Id);
    	}

        //Get All Integration Errors associated with activities and update
        for(VTT_Integration_Error__c error : [Select Id, Name, Status__c, Error_log__c
        									  From VTT_Integration_Error__c
        									  Where Work_Order_Activity__c In :activityIdSet
        									  And Status__c = :STATUS_ERROR
                                              AND RecordType.DeveloperName =: RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR]) {
        	error.Status__c = STATUS_SUCCESS;
        	addEntryToErrorLog(error, 
        		String.format('{0} User status updated from SAP', new List<String>{String.valueof(Datetime.now())}));
        	errorsToUpdate.add(error);
        }

        update errorsToUpdate;
    }

    public static void UpdateActivitiesCallout(Set<Id> activityIdSet) {
    	List<Work_Order_activity__c> activityList = [Select Id, Name, Operation_Number__c, Sub_Operation_Number__c,
                                                            Maintenance_Work_Order__r.Work_Order_Number__c, Description__c, 
                                                            Work_Details__c, Status__c, User_Status__c,
                                                           (Select Id, Name, Status__c, Status_Icon__c, Error_Log__c
                                                            From VTT_Integration_Error__r
                                                            Order By SystemModStamp Desc) 
                                                     From Work_Order_Activity__c where Id in :activityIdSet];
        List<Work_Order_Activity__c> activitiesToUpdate = new List<Work_Order_Activity__c>();
    	List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest> updateRequestList = new List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest>();
    	List<VTT_Integration_Error__c> integrationErrorList = new List<VTT_Integration_Error__c>();

    	//Check if status update needed
    	for(Work_Order_Activity__c activity : activityList) {
    		if(activity.User_Status__c == null || !activity.User_Status__c.contains(validStatusMap.get(activity.Status__c)))
    			activitiesToUpdate.add(activity);
    	}

    	//Create Request List
        for(Work_Order_Activity__c activity : activitiesToUpdate) {
            SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest request = new SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest();
            request.ActivityNumber = activity.Operation_Number__c;
            request.ActivitySubNumber = activity.Sub_Operation_Number__c;
            request.WorkOrderNumber = activity.Maintenance_Work_Order__r.Work_Order_Number__c;
            request.UserStatus = validStatusMap.get(activity.Status__c);
            request.MainDescription = activity.Description__c;
            //request.MainLongDescription = activity.Work_Details__c;
            updateRequestList.add(request);
        }

    	//Replace logErrors with error objects on activities to be retried
        try {
        	//Initialize Service
        	SAPHOGWorkOrderServices.WorkOrderPort  workOrderService = new SAPHOGWorkOrderServices.WorkOrderPort();
            SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse response = workOrderService.UpdateWorkOrderActivities(updateRequestList);
            if(response.type_x == false) {
                integrationErrorList.addAll(createIntegrationErrors(activitiesToUpdate, 'SAP Returned the following error: ' + response.Message));
            } else {
                if(response.WorkOrderActivityResponseList == null || response.WorkOrderActivityResponseList.isEmpty()) {
                    integrationErrorList.addAll(createIntegrationErrors(activitiesToUpdate, 'No response retured for actities.'));
                } else {
                    for(Integer i=0; i < response.WorkOrderActivityResponseList.size(); i++) {
                    	SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse activityResponse =
                    		response.WorkOrderActivityResponseList[i];
                        if(activityResponse.type_x == false) {
                        	System.debug('activityResponse.type_x: ' + activityResponse.type_x);
                        	System.debug('activityResponse.message: ' + activityResponse.message);
							integrationErrorList.add(createIntegrationError(activitiesToUpdate[i], activityResponse));
                        }
                    }
                }
            }
        } catch (Exception ex) {
            integrationErrorList.addAll(createIntegrationErrors(activitiesToUpdate, 
            	'Service is not available because of the following error: ' + ex.getMessage()));
        }

        //DML
        upsert integrationErrorList;
    }

    public static List<VTT_Integration_Error__c> createIntegrationErrors(List<Work_Order_Activity__c> activities, 
        String errorMsg){
        Id updateAcStatusRecordTypeId = [Select Id From RecordType Where DeveloperName =: RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR].Id;
        List<VTT_Integration_Error__c> errors = new List<VTT_Integration_Error__c>();
        for(Work_Order_Activity__c ac: activities){
        	VTT_Integration_Error__c error = (ac.VTT_Integration_Error__r == null || ac.VTT_Integration_Error__r.isEmpty()) ?
        		new VTT_Integration_Error__c(RecordTypeId = updateAcStatusRecordTypeId) : ac.VTT_Integration_Error__r[0];
        	error.Work_Order_Activity__c = ac.Id;
        	error.Status__c = STATUS_ERROR;
        	if(String.isBlank(error.Error_Log__c))
	            error.Error_Log__c = errorMsg;
	        else 
	            addEntryToErrorLog(error,errorMsg);
            errors.add(error);
        }
        return errors;
    }

    public static VTT_Integration_Error__c createIntegrationError(Work_Order_Activity__c activity, SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse activityResponse){
        String errMsg = String.format('{0} SAP Returned: ' + activityResponse.Message, new List<String>{String.valueof(Datetime.now())});
        Id updateAcStatusRecordTypeId = [Select Id From RecordType Where DeveloperName =: RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR].Id;
        VTT_Integration_Error__c error = (activity.VTT_Integration_Error__r == null || activity.VTT_Integration_Error__r.isEmpty()) ?
        		new VTT_Integration_Error__c(RecordTypeId = updateAcStatusRecordTypeId) : activity.VTT_Integration_Error__r[0];
        
		//Populate error
		error.Work_Order_Activity__c = activity.Id;
		error.Status__c = STATUS_ERROR;                 
        if(String.isBlank(error.Error_Log__c))
            error.Error_Log__c = errMsg;
        else 
            addEntryToErrorLog(error,errMsg);
        
        return error;
    }

    /**
     * Helper method to get all activities stuck in error state
     * @return [Work order Activities]
     */
    public List<Work_Order_Activity__c> getActivitiesForRetry(){
        List<VTT_Integration_Error__c> errors = new List<VTT_Integration_Error__c>([SELECT Work_Order_Activity__c 
                                                                                    FROM VTT_Integration_Error__c
                                                                                    Where Status__c = :STATUS_ERROR
                                                                                    AND RecordType.DeveloperName =: RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR]);
        Set<Id> woaIds = new Set<Id>();
        for(VTT_Integration_Error__c error: errors){
            woaIds.add(error.Work_Order_Activity__c);
        }
        return [SELECT id FROM Work_Order_Activity__c WHERE ID IN:woaIds];
    }

    //Use this function to add response to log
	public static void addEntryToErrorLog(VTT_Integration_Error__c error, String entry) {
		String newLog = entry + '\n' + error.Error_log__c;
		while(newLog.length() > Schema.SObjectType.VTT_Integration_Error__c.fields.Error_log__c.getLength())
			newLog = newLog.substringBeforeLast('\n');
		error.Error_log__c = newLog;
	}
}