public class PREPDeliveryPlanController 
{
	public String iniMonth {get;set;}
	public integer iniYear {get;set;}

    public String selectedDisc {get;set;}


    public List<DeliveryPlan> plans {get;set;}

    
    public PREPDeliveryPlanController() 
    {
       
    }
	
	public List<SelectOption> getDisciplineOptions()
    {
    	List<SelectOption> disc = new List<selectOption>();

    	disc.add(new selectOption('%', 'All'));

    	for(PREP_Discipline__c d : [SELECT Name FROM PREP_Discipline__c order by Name])
    	{
    		disc.add(new selectOption(d.Name, d.Name));
    	}
		return disc;
    }
    public void runSearch()
	{
		plans = new List<DeliveryPlan>();
		plans.addAll(findCategoryInitatives());
		plans.addAll(findProjectInitatives());
		
	}
	private List<DeliveryPlan> findCategoryInitatives()
	{
		//key = Initative Id
        Map<Id, DeliveryPlan> catPlanMap = new Map<Id, DeliveryPlan>();
		Map<Id, PREP_Baseline__c> baselineListCat ;
		
		if(iniMonth == 'All')
        {
        	baselineListCat = new Map<Id, PREP_Baseline__c>([SELECT Name, Planned_Award_Year__c, Initiative_Category_Manager__c, Initiative_Category_Specialist__c, Initiative_Category_Technician__c, 
					Initiative_Business_Analyst__c, Initiative_Id__c, Discipline__c, Ini_Name__c, RecordTypeId, Baseline_Spend_Dollar_Target__c,
					Local_Baseline_Savings_Dollar__c, Local_Baseline_Savings_Percent__c, Global_Baseline_Savings_Dollar__c, Global_Baseline_Savings_Percent__c,
					XCAC1_CAC_Strategy_Approved__c, X05_Going_To_Market__c, X07_Contract_Start_Up__c, XCAC2_CAC_Award_Approval__c,
					Global_Baseline_Spend_Dollar__c, Local_Baseline_Spend_Dollar__c,
					(SELECT Name, Local_Forecast_Savings_Dollar__c, Forecast_Spend_Dollar_Total__c, Local_Forecast_Spend_Dollar__c, Local_Forecast_Spend_Percent__c, Global_Forecast_Spend_Dollar__c, 
					Global_Forecast_Spend_Percent__c, Forecast_Savings_Total_Percent__c, Local_Forecast_Savings_Percent__c, Global_Forecast_Savings_Percent__c, 
					Forecast_Date_CAC1__c, Global_Forecast_Savings_Dollar__c, Actual_Date_CAC1__c, Forecast_Date_05__c, Actual_Date_05__c, Forecast_Date_CAC2__c, Actual_Date_CAC2__c, Forecast_Date_07__c, Actual_Date_07__c
					FROM Material_Group_Status_Summaries__r WHERE Complete__c = true
					ORDER BY Update_Date__c DESC, LastModifiedDate DESC LIMIT 1) 
				FROM PREP_Baseline__c 
				WHERE CALENDAR_YEAR(XCAC2_CAC_Award_Approval__c) = :iniYear
						AND	Discipline__c like: selectedDisc
						AND RecordType.DeveloperName != 'Locked_Projects_Baseline' AND RecordType.DeveloperName != 'Projects_Baseline'
				ORDER BY XCAC2_CAC_Award_Approval__c ASC]);
        }

        else if(iniMonth != 'All')
        {
			baselineListCat = new Map<Id, PREP_Baseline__c>([SELECT Name, Planned_Award_Year__c, Initiative_Category_Manager__c, Initiative_Category_Specialist__c, Initiative_Category_Technician__c, 
					Initiative_Business_Analyst__c, Initiative_Id__c, Discipline__c, Ini_Name__c, RecordTypeId, Baseline_Spend_Dollar_Target__c,
					Local_Baseline_Savings_Dollar__c, Local_Baseline_Savings_Percent__c, Global_Baseline_Savings_Dollar__c, Global_Baseline_Savings_Percent__c,
					XCAC1_CAC_Strategy_Approved__c, X05_Going_To_Market__c, X07_Contract_Start_Up__c, XCAC2_CAC_Award_Approval__c,
					Global_Baseline_Spend_Dollar__c, Local_Baseline_Spend_Dollar__c,
						(SELECT Name, Local_Forecast_Savings_Dollar__c, Forecast_Spend_Dollar_Total__c, Local_Forecast_Spend_Dollar__c, Local_Forecast_Spend_Percent__c, Global_Forecast_Spend_Dollar__c, 
						Global_Forecast_Spend_Percent__c, Forecast_Savings_Total_Percent__c, Local_Forecast_Savings_Percent__c, Global_Forecast_Savings_Percent__c, 
						Forecast_Date_CAC1__c, Global_Forecast_Savings_Dollar__c, Actual_Date_CAC1__c, Forecast_Date_05__c, Actual_Date_05__c, Forecast_Date_CAC2__c, Actual_Date_CAC2__c, Forecast_Date_07__c, Actual_Date_07__c
						FROM Material_Group_Status_Summaries__r WHERE Complete__c = true
						ORDER BY Update_Date__c DESC LIMIT 1) 
				FROM PREP_Baseline__c 
				WHERE Discipline__c like: selectedDisc 
						AND CALENDAR_YEAR(XCAC2_CAC_Award_Approval__c) = :iniYear
						AND CALENDAR_MONTH(XCAC2_CAC_Award_Approval__c) = : System.Integer.valueOf(iniMonth)
						AND RecordType.DeveloperName != 'Locked_Projects_Baseline' AND RecordType.DeveloperName != 'Projects_Baseline'
				ORDER BY XCAC2_CAC_Award_Approval__c ASC]);
        }
        
        for(PREP_Baseline__c baseline : baselineListCat.values())
        {
        	
        	DeliveryPlan catPlan = new DeliveryPlan(baseline, 'Category');
        	
        	for(PREP_Initiative_Status_Update__c statusUpdate : baseline.Material_Group_Status_Summaries__r)
        	{
        		catPlan.forecastSpend = statusUpdate.Forecast_Spend_Dollar_Total__c == null ? 0 : statusUpdate.Forecast_Spend_Dollar_Total__c;
        		catPlan.forecastLocalNegSavingsDollar = statusUpdate.Local_Forecast_Savings_Dollar__c == null ? 0 : statusUpdate.Local_Forecast_Savings_Dollar__c;
        		catPlan.forecastLocalNegSavingsPer = statusUpdate.Local_Forecast_Savings_Percent__c == null ? 0 : statusUpdate.Local_Forecast_Savings_Percent__c;
        		catPlan.forecastGlobalNegSavingsDollar = statusUpdate.Global_Forecast_Savings_Dollar__c == null ? 0 : statusUpdate.Global_Forecast_Savings_Dollar__c;
        		catPlan.forecastGlobalNegSavingsPer = statusUpdate.Global_Forecast_Savings_Percent__c == null ? 0 : statusUpdate.Global_Forecast_Savings_Percent__c;
        		catPlan.forecastLocalSpend = statusUpdate.Local_Forecast_Spend_Dollar__c;
        		catPlan.forecastGlobalSpend = statusUpdate.Global_Forecast_Spend_Dollar__c;
        	
        		catPlan.forecastStrategy = statusUpdate.Forecast_Date_CAC1__c;
        		catPlan.actualStrategy = statusUpdate.Actual_Date_CAC1__c;
        		
        		catPlan.forecastRFX = statusUpdate.Forecast_Date_05__c;
        		catPlan.actualRFX = statusUpdate.Actual_Date_05__c;
        		catPlan.forecastAward = statusUpdate.Forecast_Date_CAC2__c;
        		catPlan.actualAward = statusUpdate.Actual_Date_CAC2__c;
        		catPlan.forecastContractStartUp = statusUpdate.Forecast_Date_07__c;
        		catPlan.actualContractStartUp = statusUpdate.Actual_Date_07__c;
        	}
        	catPlanMap.put(baseline.Initiative_Id__c, catPlan);
        }
      
        for(PREP_Business_Value_Submission__c submission : [SELECT Id, Local_Savings_Amount__c, Initiative_Id__c, Local_Awarded_Spend__c, Global_Savings_Amount__c,
        															Global_Awarded_Spend__c, Awarded_Spend__c 
        													FROM PREP_Business_Value_Submission__c 
        													WHERE Initiative_Id__c in :catPlanMap.keySet() AND Status__c = 'Approved'])
        {
        	DeliveryPlan catPlan = catPlanMap.get(submission.Initiative_Id__c);
        	catPlan.actualSpend +=  submission.Awarded_Spend__c == null ? 0 : submission.Awarded_Spend__c;
        	catPlan.actualLocalNegSavingsDollar += submission.Local_Savings_Amount__c == null ? 0 : submission.Local_Savings_Amount__c;
        	catPlan.actualLocalSpend += submission.Local_Awarded_Spend__c == null ? 0 : submission.Local_Awarded_Spend__c;
        	catPlan.actualGlobalNegSavingsDollar += submission.Global_Savings_Amount__c == null ? 0 : submission.Global_Savings_Amount__c;
        	catPlan.actualGlobalSpend += submission.Global_Awarded_Spend__c == null ? 0 : submission.Global_Awarded_Spend__c;
        	
        }
        
        for(DeliveryPlan catPlan : catPlanMap.values())
        {
        	catPlan.actualLocalNegSavingsPer = catPlan.actualLocalSpend == 0 ? 0 : catPlan.actualLocalNegSavingsDollar/catPlan.actualLocalSpend * 100;
        	catPlan.actualGlobalNegSavingsPer = catPlan.actualGlobalSpend == 0 ? 0 : catPlan.actualGlobalNegSavingsDollar/catPlan.actualGlobalSpend * 100;
        }
       
       	return catPlanMap.values();
       
	}
	private List<DeliveryPlan> findProjectInitatives()
	{
		Map<Id, PREP_Baseline__c> baselineListProj ;
		List<DeliveryPlan> projPlans = new List<DeliveryPlan>();
		if(iniMonth == 'All')
        {
			baselineListProj = new Map<Id, PREP_Baseline__c>(
								[SELECT Id, Initiative_Category_Specialist__c, Initiative_Category_Technician__c, Initiative_Business_Analyst__c,
									Initiative_Phase_4_Start_Date__c, Initiative_Id__r.Strategy_Approved__c, Initiative_SCM_Team__c, 
									Baseline_Spend_To_Market_Dollar__c, Initiative_Procurement_Rep__c, Initiative_Contract_Administrator__c, 
						            Planned_Award_Year__c, Discipline__c, Ini_Name__c, Initiative_Id__c, RecordTypeId, 
						            Local_Baseline_Savings_Dollar__c, Local_Baseline_Savings_Percent__c, X07_Contract_Start_Up__c,
						            Local_Baseline_Spend_Dollar__c, Global_Baseline_Savings_Dollar__c, Global_Baseline_Savings_Percent__c, 
						            X05_Going_To_Market__c, XCAC2_CAC_Award_Approval__c, Global_Baseline_Spend_Dollar__c, 
									(SELECT Id, Global_Sourcing__c, Planned_Spend_Allocation_Dollar__c, Planned_Savings_Allocation_Percent__c, 
						            Planned_Savings_Allocation_Dollar__c 
						            FROM Material_Group_Baselines__r)
								FROM PREP_Baseline__c 
								WHERE CALENDAR_YEAR(XCAC2_CAC_Award_Approval__c) = :iniYear
					            AND	Discipline__c like: selectedDisc
								AND (RecordType.DeveloperName = 'Locked_Projects_Baseline' OR RecordType.DeveloperName = 'Projects_Baseline')]
															);
        }

        else if(iniMonth != 'All')
        {
            baselineListProj = new Map<Id, PREP_Baseline__c>(
					            	[SELECT Id, Initiative_Category_Specialist__c, Initiative_Category_Technician__c, Initiative_Business_Analyst__c,
					            		Initiative_Phase_4_Start_Date__c, Initiative_Id__r.Strategy_Approved__c, Initiative_SCM_Team__c, 
					            		Baseline_Spend_To_Market_Dollar__c, Initiative_Procurement_Rep__c, Initiative_Contract_Administrator__c, 
							            Planned_Award_Year__c, Discipline__c, Ini_Name__c, Initiative_Id__c, RecordTypeId, Local_Baseline_Savings_Dollar__c, 
							            Local_Baseline_Savings_Percent__c, X07_Contract_Start_Up__c, Global_Baseline_Spend_Dollar__c, 
							            Local_Baseline_Spend_Dollar__c, Global_Baseline_Savings_Dollar__c, Global_Baseline_Savings_Percent__c, X05_Going_To_Market__c, XCAC2_CAC_Award_Approval__c,
							            (SELECT Id, Global_Sourcing__c, Planned_Spend_Allocation_Dollar__c, Planned_Savings_Allocation_Percent__c, 
							            Planned_Savings_Allocation_Dollar__c 
							            FROM Material_Group_Baselines__r)
						            FROM PREP_Baseline__c 
						            WHERE Discipline__c like:selectedDisc AND CALENDAR_YEAR(XCAC2_CAC_Award_Approval__c) = :iniYear
						            AND CALENDAR_MONTH(XCAC2_CAC_Award_Approval__c) =: Integer.valueOf(iniMonth)
						            AND (RecordType.DeveloperName = 'Locked_Projects_Baseline' OR RecordType.DeveloperName = 'Projects_Baseline')]
	            											);
        }
       
        //get associated BVS with Package
        //Map key = baseline id
        Map<Id, List<PREP_Business_Value_Submission__c>> bvsListByBaseline = new Map<Id, List<PREP_Business_Value_Submission__c>>();
        for(PREP_Business_Value_Submission__c bvs : [SELECT Id, Global_Savings_Amount__c, Global_Savings_Percent__c, Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c,
        												Mat_Serv_Group_Baseline_Allocation_Id__c, Local_Awarded_Spend__c, Local_Savings_Amount__c, Local_Savings_Percent__c,
        												Awarded_Spend__c, Global_Awarded_Spend__c 
									        			FROM PREP_Business_Value_Submission__c 
									        			WHERE Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in :baselineListProj.keySet()
									        				AND Status__c = 'Approved'])
		{
			if (bvsListByBaseline.containsKey(bvs.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c))
			{
				bvsListByBaseline.get(bvs.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c).add(bvs);
			}
			else
			{
				bvsListByBaseline.put(bvs.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c, new List<PREP_Business_Value_Submission__c>{bvs});
			}
		}
		//get associated Schedule
        //Map key = Baseline id
        Map<Id, List<PREP_Mat_Serv_Group_Planned_Schedule__c>> scheListByBaseline = new Map<Id, List<PREP_Mat_Serv_Group_Planned_Schedule__c>>();
        for(PREP_Mat_Serv_Group_Planned_Schedule__c sche :  [SELECT Id, Mat_Serv_Group_Baseline_Allocation_Id__c, Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c,
									        					(SELECT Id, Forecast_P_O_Issued__c, Actual_P_O_Issued__c, Planned_Bid_Request_Issued_RFQ__c,
									        							Planned_Decision_Summary_Approved__c, Planned_P_O_Issued__c, Planned_RFQ_Issued__c,
									        							Actual_Decision_Summary_Approved__c, Forecast_Decision_Summary_Approved_Mat__c, 
									        							Forecast_Decision_Summary_Approved_Serv__c, Forecast_Strategy_Approval__c, 
									        							Status_Delivery_Variance_Days_Mat__c, Status_Delivery_Variance_Days_Serv__c,
									        							Delivery_Variance_Days_Mat__c, Delivery_Variance_Days_Serv__c, Float__c,
									        							Actual_Strategy_Approval__c,  Status_Strategy_Approval__c, Status_RFQ_Issued__c,
									        							Status_Decision_Summary_Approved__c, Status_Contract_Executed_LOA_Effective__c,
									        							Forecast_Bid_Request_Issued_RFQ__c, Actual_Bid_Request_Issued_RFQ__c,Status_P_O_Issued__c,
									        							Actual_RFQ_Issued__c, Status_Bid_Request_Issued_RFQ__c, Actual_Contract_Executed_LOA_Effective__c,
									        							RecordType.DeveloperName, Forecast_RFQ_Issued__c, Planned_Contract_Executed_LOA_Effective__c 
									        						FROM Mat_Serv_Group_Updates__r WHERE Completed__c = true ORDER BY LastModifiedDate DESC LIMIT 1) 
										        			FROM PREP_Mat_Serv_Group_Planned_Schedule__c 
										        			WHERE Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in :baselineListProj.keySet()])
        {
        	if (scheListByBaseline.containsKey(sche.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c))
			{
				scheListByBaseline.get(sche.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c).add(sche);
			}
			else
			{
				scheListByBaseline.put(sche.Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c, new List<PREP_Mat_Serv_Group_Planned_Schedule__c>{sche});
			}
        }			

        for(PREP_Baseline__c blObjProj : baselineListProj.values())
        {
            DeliveryPlan projPlan = new DeliveryPlan(blObjProj, 'Project');
           
            for(PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA : blObjProj.Material_Group_Baselines__r)
            {
                projPlan.forecastSpend += MSGBA.Planned_Spend_Allocation_Dollar__c == null ? 0 : MSGBA.Planned_Spend_Allocation_Dollar__c;
               
                if(MSGBA.Global_Sourcing__c == 'No')
                {
                    projPlan.forecastLocalNegSavingsDollar += MSGBA.Planned_Savings_Allocation_Dollar__c == null ? 0 : MSGBA.Planned_Savings_Allocation_Dollar__c;
					projPlan.forecastLocalSpend += MSGBA.Planned_Spend_Allocation_Dollar__c == null ? 0 : MSGBA.Planned_Spend_Allocation_Dollar__c;
				}
				else if(MSGBA.Global_Sourcing__c == 'Yes') 
                {
                    projPlan.forecastGlobalNegSavingsDollar += MSGBA.Planned_Savings_Allocation_Dollar__c == null ? 0 : MSGBA.Planned_Savings_Allocation_Dollar__c;
                    projPlan.forecastGlobalSpend += MSGBA.Planned_Spend_Allocation_Dollar__c == null ? 0 : MSGBA.Planned_Spend_Allocation_Dollar__c;
                }
            }
            projPlan.forecastLocalNegSavingsPer = projPlan.forecastLocalSpend != 0 ? projPlan.forecastLocalNegSavingsDollar/projPlan.forecastLocalSpend * 100 : 0;
            projPlan.forecastGlobalNegSavingsPer = projPlan.forecastGlobalSpend != 0 ? projPlan.forecastGlobalNegSavingsDollar/projPlan.forecastGlobalSpend * 100 : 0;
            
         
            //caculate fields from Business Value Submission
            if (bvsListByBaseline.containsKey(blObjProj.Id))
            {
            	List<PREP_Business_Value_Submission__c> bvsList = bvsListByBaseline.get(blObjProj.Id);
            	for(PREP_Business_Value_Submission__c bvs : bvsList)
            	{
            		projPlan.actualSpend += bvs.Awarded_Spend__c == null ? 0 : bvs.Awarded_Spend__c;
            		projPlan.actualLocalNegSavingsDollar += bvs.Local_Savings_Amount__c == null ? 0 : bvs.Local_Savings_Amount__c;
            		projPlan.actualGlobalNegSavingsDollar += bvs.Global_Savings_Amount__c == null ? 0 : bvs.Global_Savings_Amount__c;
            		projPlan.actualLocalSpend += bvs.Local_Awarded_Spend__c == null ? 0 : bvs.Local_Awarded_Spend__c;
            		projPlan.actualGlobalSpend += bvs.Global_Awarded_Spend__c == null ? 0 : bvs.Global_Awarded_Spend__c;
            	}
            	projPlan.actualLocalNegSavingsPer = projPlan.actualLocalSpend != 0 ? projPlan.actualLocalNegSavingsDollar/projPlan.actualLocalSpend * 100 : 0;
            	projPlan.actualGlobalNegSavingsPer = projPlan.actualGlobalSpend != 0 ? projPlan.actualGlobalNegSavingsDollar/projPlan.actualGlobalSpend * 100 : 0;
            	
            }
           
             
            if(scheListByBaseline.containsKey(blObjProj.Id))
            {
            	List<PREP_Mat_Serv_Group_Planned_Schedule__c> scheList = scheListByBaseline.get(blObjProj.Id);
            	System.debug('scheList size =' + scheList);
           		
            	for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : scheList)
            	{
            		if (sche.Mat_Serv_Group_Updates__r.size() > 0)
            		{
            			for(PREP_Mat_Serv_Group_Update__c groupUpdate : sche.Mat_Serv_Group_Updates__r)
            			{
            				if (groupUpdate.Actual_Strategy_Approval__c != null)
            					projPlan.noActualStrategy ++;
            				else
            					projPlan.noForecastStrategy ++;	
            				if (groupUpdate.Status_Strategy_Approval__c.contains('green.png'))
            					projPlan.noGreenStrategy ++;
            				
            				Date actualRFX = groupUpdate.RecordType.DeveloperName.contains('Mat_Group_Update') 
            								? groupUpdate.Actual_Bid_Request_Issued_RFQ__c : groupUpdate.Actual_RFQ_Issued__c;
            								
            				String statusRFX = groupUpdate.RecordType.DeveloperName.contains('Mat_Group_Update') 
            								? groupUpdate.Status_Bid_Request_Issued_RFQ__c : groupUpdate.Status_RFQ_Issued__c;
            								
            				if (actualRFX != null)
        						projPlan.noActualRFX ++;
            				else
            					projPlan.noForecastRFX ++;	
            				if (statusRFX.contains('green.png'))
            					projPlan.noGreenRFX ++;
	            				
            				System.debug('status RFX =' + statusRFX);
            				
            				if (groupUpdate.Actual_Decision_Summary_Approved__c != null)
	            					projPlan.noActualAward ++;
            				else
            					projPlan.noForecastAward ++;	
            				if (groupUpdate.Status_Decision_Summary_Approved__c.contains('green.png'))
            					projPlan.noGreenAward ++;
            				
            				
            				Date actualContractStartup = groupUpdate.RecordType.DeveloperName.contains('Mat_Group_Update') 
            								? groupUpdate.Actual_P_O_Issued__c : groupUpdate.Actual_Contract_Executed_LOA_Effective__c;
            								
            				String statusContractStartup = groupUpdate.RecordType.DeveloperName.contains('Mat_Group_Update') 
            								? groupUpdate.Status_P_O_Issued__c : groupUpdate.Status_Contract_Executed_LOA_Effective__c;
            								
            				if (actualContractStartup != null)
        						projPlan.noActualContractStartup ++;
            				else
            					projPlan.noForecastContractStartup ++;	
            				if (statusContractStartup.contains('green.png'))
            					projPlan.noGreenContractStartup ++;
            			} 
            		}
            	}
            	
            }
            
            projPlans.add(projPlan);	
        }
        return projPlans; 
	}
	


    public class DeliveryPlan
    {   
    	public Id initId {get;set;}
        public String iniName {get;set;}
        public String discByGroup {get;set;}
        public String plannedAwardYear {get;set;}
        public String iniType {get;set;}
        
        public String catManager {get;set;}
        public String catSpecialist {get;set;}
        public String catTechnician {get;set;}
        public String businessAnalyst {get;set;}
        
        public Decimal plannedLocalSpend {get;set;}
        public Decimal forecastLocalSpend {get;set;}
        public Decimal actualLocalSpend {get;set;}
        
        public Decimal plannedGlobalSpend {get;set;}
        public Decimal forecastGlobalSpend {get;set;}
        public Decimal actualGlobalSpend {get;set;}
        
        public Decimal plannedSpend {get;set;}
        public Decimal forecastSpend {get;set;}
        public Decimal actualSpend {get;set;}
        
        public Decimal plannedLocalNegSavingsDollar {get;set;}
        public Decimal forecastLocalNegSavingsDollar {get;set;}
        public Decimal actualLocalNegSavingsDollar {get;set;}
        
        public Decimal plannedLocalNegSavingsPer {get;set;}
        public Decimal forecastLocalNegSavingsPer {get;set;}
        public Decimal actualLocalNegSavingsPer {get;set;}
        
        public Decimal plannedGlobalNegSavingsDollar {get;set;}
        public Decimal forecastGlobalNegSavingsDollar {get;set;}
        public Decimal actualGlobalNegSavingsDollar {get;set;}
        
        public Decimal plannedGlobalNegSavingsPer {get;set;}
        public Decimal forecastGlobalNegSavingsPer {get;set;}
        public Decimal actualGlobalNegSavingsPer {get;set;}
        
        public Date plannedStrategy {get;set;}
        public Date forecastStrategy {get;set;}
        public Date actualStrategy {get;set;}
        
		
		public Date plannedRFX {get;set;}
		public Date forecastRFX {get;set;}
		public Date actualRFX {get;set;}

		public Date plannedAward {get;set;}
		public Date forecastAward {get;set;}
		public Date actualAward {get;set;}

		public Date plannedContractStartUp {get;set;}
		public Date forecastContractStartUp {get;set;}
		public Date actualContractStartUp {get;set;}
		
		public integer noForecastStrategy {get;set;}
		public integer noActualStrategy {get;set;}
		public integer noGreenStrategy {get;set;}
		
		public integer noForecastRFX {get;set;}
		public integer noActualRFX {get;set;}
		public integer noGreenRFX {get;set;}
		
		public integer noForecastAward {get;set;}
		public integer noActualAward {get;set;}
		public integer noGreenAward {get;set;}
		
		public integer noForecastContractStartup {get;set;}
		public integer noActualContractStartup {get;set;}
		public integer noGreenContractStartup {get;set;}
		
		public DeliveryPlan(PREP_Baseline__c baseline, String strType)
		{
			initId = baseline.Initiative_Id__c;
			iniName = baseline.Ini_Name__c;
			discByGroup = baseline.Discipline__c;
			plannedAwardYear = baseline.Planned_Award_Year__c;
			iniType = strType;
			
			
			catSpecialist = baseline.Initiative_Category_Specialist__c;
			catTechnician = baseline.Initiative_Category_Technician__c;
			businessAnalyst = baseline.Initiative_Business_Analyst__c;
			
			plannedLocalSpend = baseline.Local_Baseline_Spend_Dollar__c == null ? 0 : baseline.Local_Baseline_Spend_Dollar__c;
			plannedGlobalSpend = baseline.Global_Baseline_Spend_Dollar__c == null ? 0 : baseline.Global_Baseline_Spend_Dollar__c;
			
			plannedLocalNegSavingsDollar = baseline.Local_Baseline_Savings_Dollar__c == null ? 0 : baseline.Local_Baseline_Savings_Dollar__c;
			plannedLocalNegSavingsPer = baseline.Local_Baseline_Savings_Percent__c == null ? 0 : baseline.Local_Baseline_Savings_Percent__c;
			plannedGlobalNegSavingsDollar = baseline.Global_Baseline_Savings_Dollar__c == null ? 0 : baseline.Global_Baseline_Savings_Dollar__c;
			plannedGlobalNegSavingsPer = baseline.Global_Baseline_Savings_Percent__c == null ? 0 : baseline.Global_Baseline_Savings_Percent__c;
			
			
			if (strType == 'Category')
			{
				catManager = baseline.Initiative_Category_Manager__c;
				plannedSpend = baseline.Baseline_Spend_Dollar_Target__c == null ? 0 : baseline.Baseline_Spend_Dollar_Target__c;
				plannedStrategy = baseline.XCAC1_CAC_Strategy_Approved__c ;
				
			}
			else if (strType == 'Project')
			{
				catManager = baseline.Initiative_SCM_Team__c;
				plannedSpend = baseline.Baseline_Spend_To_Market_Dollar__c == null ? 0 : baseline.Baseline_Spend_To_Market_Dollar__c;
				plannedStrategy = baseline.Initiative_Id__r.Strategy_Approved__c;
			}
			plannedRFX = baseline.X05_Going_To_Market__c;
			plannedAward = baseline.XCAC2_CAC_Award_Approval__c;
        	plannedContractStartUp = baseline.X07_Contract_Start_Up__c;
        	
			forecastSpend = 0;
	        actualSpend = 0;
	        actualLocalSpend = 0;
	        actualGlobalSpend = 0;
	        forecastLocalSpend = 0;
	        forecastGlobalSpend = 0;
	        actualLocalSpend = 0;
	        actualGlobalSpend = 0;
	         
	        forecastLocalNegSavingsDollar = 0 ;
	        actualLocalNegSavingsDollar = 0;
	        forecastLocalNegSavingsPer = 0;
	        actualLocalNegSavingsPer = 0;
	     	forecastGlobalNegSavingsDollar = 0;
	        actualGlobalNegSavingsDollar = 0;
	      	forecastGlobalNegSavingsPer = 0;
	        actualGlobalNegSavingsPer = 0;
	        
	        noForecastStrategy = 0;
			noActualStrategy  = 0;
		    noGreenStrategy  = 0;
		
		 	noForecastRFX  = 0;
		 	noActualRFX  = 0;
		 	noGreenRFX  = 0;
		
		 	noForecastAward  = 0;
		 	noActualAward  = 0;
		 	noGreenAward  = 0;
		
		 	noForecastContractStartup  = 0;
		 	noActualContractStartup  = 0;
		 	noGreenContractStartup  = 0;
		}
	}

}