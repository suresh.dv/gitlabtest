@isTest
private class FM_CarryoverTruckTripTest {
	@isTest static void testFMUtilitiesMethod() {
		List<FM_Truck_Trip__c> truckTrips = [Select Id, Name, Truck_Trip_Status__c, Run_Sheet_Date__c
											 From FM_Truck_Trip__c
											 Where Load_Request__c <> null
											 And Run_Sheet_Date__c = YESTERDAY];
		FM_Utilities.UpdateCarryoverTruckTripsToToday(truckTrips);
		String truckTripId = truckTrips.get(0).Id;
		truckTrips = [Select Id, Name, Truck_Trip_Status__c, Run_Sheet_Date__c
					  From FM_Truck_Trip__c
					  Where Id = :truckTripId];
		System.assertEquals(truckTrips.get(0).Run_Sheet_Date__c, Date.today());
	}
	
	@isTest static void testBatch() {
		Test.startTest();
		 	FM_CarryoverTrucktripBatch carryoverBatch = new FM_CarryoverTrucktripBatch();
		 	Database.executeBatch(carryoverBatch);
		Test.stopTest();

		//Test Data
		List<FM_Truck_Trip__c> truckTripList = [Select Id, Name, Truck_Trip_Status__c, Run_Sheet_Date__c
												From FM_Truck_Trip__c];
		for(FM_Truck_Trip__c truckTrip : truckTripList) {
			System.debug('truckTrip->Truck_Trip_Status__c: ' + truckTrip.Truck_Trip_Status__c);
			if(truckTrip.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER) 
				System.assertEquals(truckTrip.Run_Sheet_Date__c, Date.today());
		}
	}

	@isTest static void testSchedulable() {
		Test.startTest();
		 	//Schedule the test job
		 	String jobId = FM_CarryoverTrucktripSchedulable.scheduleJob();

		 	// Get the information from the CronTrigger API object
	      	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
	         						 NextFireTime
	         				  FROM CronTrigger WHERE id = :jobId];

			// Verify the expressions are the same
			System.assertEquals(FM_CarryoverTrucktripSchedulable.cronString, ct.CronExpression);

			// Verify the job has not run
			System.assertEquals(0, ct.TimesTriggered);
		Test.stopTest();

		//Test Data
		//List<FM_Truck_Trip__c> truckTripList = [Select Id, Name, Truck_Trip_Status__c, Run_Sheet_Date__c
		//										From FM_Truck_Trip__c];
		//for(FM_Truck_Trip__c truckTrip : truckTripList) {
		//	if(truckTrip.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER) 
		//		System.assertEquals(truckTrip.Run_Sheet_Date__c, Date.today());
		//}
	}

	@testSetup static void setup() {
		//Create Test Route
		Route__c route = RouteTestData.createRoute('999');
		route.Fluid_Management__c = true;
		insert route;

		//Create Test Location
		Location__c location = LocationTestData.createLocation();
		location.Functional_Location_Category__c = 4;
		location.Route__c = route.Id;
		update location;

		//Create Load Request - 1
		FM_Load_Request__c loadRequest1 = FM_TestData.createLoadRequest(null, 'Standard Load', 
			'O', 'night', null, 'T5X', 'Primary', null, 
			'Load Request created from Runsheet', null, location.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
			'Tank 1', 100, 60, 120, 4, 5);

		//Create Load Request - 2
		FM_Load_Request__c loadRequest2 = FM_TestData.createLoadRequest(null, 'Standard Load', 
			'W', 'night', null, 'T5X', 'Primary', null, 
			'Load Request created from Runsheet', null, location.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
			'Tank 2', 100, 60, 120, 4, 5);

		//Create Trucktrip for Load Request - 1 and set status as carryover
		FM_Utilities.executeTriggerCode = false;
		FM_Truck_Trip__c truckTrip1 = [Select Id, Truck_Trip_Status__c, Run_Sheet_Date__c From FM_Truck_Trip__c].get(0);
		truckTrip1.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER;
		truckTrip1.Run_Sheet_Date__c = Date.today().addDays(-1);
		update truckTrip1;
		FM_Utilities.executeTriggerCode = true;
	}
}