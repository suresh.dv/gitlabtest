@isTest(SeeAllData=true)
private class DSP_CMS_IndexControllerTest {

    static testMethod void myUnitTest() {     
        // Create an id for the "calendar" to get the events from
        DSP_Settings__c settings = DSP_Settings__c.getOrgDefaults();
        String calendarTestId = settings.Public_Calendar_ID__c;
        
        // Init the controller for the page
        DSP_CMS_IndexController controller = new DSP_CMS_IndexController();
        DSP_CMS_IndexController controller2 = new DSP_CMS_IndexController(null);
        
        DSP_Settings__c weatherSettings = [SELECT id, Weather_Code_Forecast__c, Weather_Code_Today__c, Weather_Code_Tommorow__c, 
                                            Weather_Code_Tonight__c, Weather_Day_Tommorow__c, Weather_Last_Updated__c, 
                                            Weather_Temp_Forecast__c, Weather_Temp_Today__c, Weather_Temp_Tommorow__c, Weather_Temp_Tonight__c, 
                                            Weather_Wind__c
                                            FROM DSP_Settings__c];
        
        weatherSettings.Weather_Last_Updated__c = DateTime.now();
        update weatherSettings;
        
        DSP_CMS_IndexController controller3 = new DSP_CMS_IndexController();
        DSP_CMS_IndexController controller4 = new DSP_CMS_IndexController(null);
        
        // Add some calendar events
        
        Event newEvent = new Event();
        newEvent.Subject = 'Testing';
        newEvent.description = 'Testing';
        newEvent.OwnerId = calendarTestId;
        newEvent.location = 'Testing';
        newEvent.IsAllDayEvent = true;
        newEvent.StartDateTime = Date.Today()+5;
        newEvent.EndDateTime = Date.Today()+6;
        insert newEvent;

        Event newEvent2 = new Event();
        newEvent2.Subject = 'Testing';
        newEvent2.description = 'Testing';
        newEvent2.OwnerId = calendarTestId;
        newEvent2.location = 'Testing';
        newEvent2.IsAllDayEvent = false;
        newEvent2.StartDateTime = Date.Today()+8;
        newEvent2.EndDateTime = Date.Today()+9;
        insert newEvent2;
        
        // Add an idea
        
        Community community = [ SELECT Id FROM Community WHERE Name =: settings.Idea_Zone_Name__c ];
        
        Idea i = new Idea(Title = 'test',Body='test',CommunityId=community.Id);
        insert i;
        
        // Test ajax service
        
        Map<String,String> ajaxRequest = new Map<String,String>();
        ajaxRequest.put('action','home');
        
        DSP_CMS_AjaxService ajaxService = new DSP_CMS_AjaxService();
        ajaxService.executeRequest(ajaxRequest);
    }
    
    static testMethod void myUnitTest2() {     
        // Create an id for the "calendar" to get the events from
        DSP_Settings__c settings = DSP_Settings__c.getOrgDefaults();
        String calendarTestId = settings.Public_Calendar_ID__c;
        
        Event newEvent = new Event();
        newEvent.Subject = 'Testing';
        newEvent.description = 'Testing';
        newEvent.OwnerId = calendarTestId;
        newEvent.location = 'Testing';
        newEvent.IsAllDayEvent = true;
        newEvent.StartDateTime = Date.Today();
        newEvent.EndDateTime = Date.Today();
        insert newEvent;

        Event newEvent2 = new Event();
        newEvent2.Subject = 'Testing';
        newEvent2.description = 'Testing';
        newEvent2.OwnerId = calendarTestId;
        newEvent2.location = 'Testing';
        newEvent2.IsAllDayEvent = false;
        newEvent2.StartDateTime = Date.Today();
        newEvent2.EndDateTime = Date.Today();
        insert newEvent2;

        // Init the controller for the page
        DSP_CMS_CalendarController controller = new DSP_CMS_CalendarController(null);
        
        Map<String,String> p = new Map<String,String>();
        p.put('action','CalendarEvents');
        
        DSP_CMS_CalendarService calService = new DSP_CMS_CalendarService();
        calService.executeRequest(p);
        
        DSP_CMS_UserService uService = new DSP_CMS_UserService();
        uService.getType();
        uService.executeRequest(p);
    }
    
    static testMethod void myUnitTest3() {
        
        DSP_Settings__c AOMSettings = [SELECT Id,
                                              Alberta_Oil_Magazine_Article_1__c,
                                              Alberta_Oil_Magazine_Article_2__c,
                                              Alberta_Oil_Magazine_Article_3__c,
                                              Alberta_Oil_Magazine_Article_4__c
                                       FROM DSP_Settings__c];
        
        AOMSettings.Alberta_Oil_Magazine_Article_1__c = 'http%3A%2F%2Fwww.albertaoilmagazine.com%2F2016%2F02%2Fenergy-ink-podcast-ides-march%2F%0AThe+Energy+Ink+Podcast%3A+The+Ides+of+March+Edition%0AMon%2C+29+Feb+2016+16%3A12%3A39+%2B0000';
        AOMSettings.Alberta_Oil_Magazine_Article_2__c = 'http%3A%2F%2Fwww.albertaoilmagazine.com%2F2016%2F02%2Foil-services-firm-cbw-engineering-is-flourishing%2F%0ASalt+Cavern+Storage+Is+Flourishing+Despite+The+Energy+Downturn%0AWed%2C+24+Feb+2016+08%3A00%3A19+%2B0000';
        AOMSettings.Alberta_Oil_Magazine_Article_3__c = 'http%3A%2F%2Fwww.albertaoilmagazine.com%2F2016%2F02%2Fwhere-is-canada-in-a-future-unified-market-for-natural-gas%2F%0AWhere+Is+Canada+In+A+Future+Unified+Market+For+Natural+Gas%3F%0ATue%2C+23+Feb+2016+08%3A00%3A58+%2B0000';
        AOMSettings.Alberta_Oil_Magazine_Article_4__c = 'http%3A%2F%2Fwww.albertaoilmagazine.com%2F2016%2F02%2Fsuncor-wraps-up-cos-takeover%2F%0ASuncor+Wraps+up+COS+Takeover%0ATue%2C+23+Feb+2016+07%3A06%3A52+%2B0000';
        
        update AOMSettings;
        
        Map<String,String> ajaxRequest = new Map<String,String>();
        ajaxRequest.put('action','news');
        
        DSP_CMS_AjaxService ajaxService = new DSP_CMS_AjaxService();
        ajaxService.executeRequest(ajaxRequest);
    }
}