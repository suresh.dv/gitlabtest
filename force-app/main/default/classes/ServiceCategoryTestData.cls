/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Category__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class ServiceCategoryTestData
{
    public static HOG_Service_Category__c createServiceCategory(String name)
    {                
        HOG_Service_Category__c results = new HOG_Service_Category__c
            (
                Name = name 
            );
                
        return results;
    }
}