/*
 * Calculates the Refinery Netback and Terminal Netback for an Opportunity Product
 */
public class calculateNetback
{
    /*
     * Calculates Refinery Netback field for Asphalt Opportunity Products.
     * 
     * Formula:
     *   If "Destination" is selected, then the formula to be used should be:
     *     Refinery Netback = Price ? Freight ? Handling Fees ? Additive ? Terminal Freight ? AC Premium
     *   If "Origin" or "Origin + Freight" are selected, then the formula to be used should be:
     *     Refinery Netback = Price ? Handling Fees ? Additive ? Terminal Freight ? AC Premium
     *
     * 
     */
    public static Decimal calculateAsphaltRefineryNetback(OpportunityLineItem oli, ATS_Freight__c f, Product2 p)
    {
        // Make sure at least one supplier is checked on freight record. Choose which supplier to use.
        
        Integer selectedHuskySupplier;
        
        if     (f == null)                      selectedHuskySupplier = null;
        else if(f.Husky_Supplier_1_Selected__c) selectedHuskySupplier = 1;
        else if(f.Husky_Supplier_2_Selected__c) selectedHuskySupplier = 2;
        
        Decimal exchangeRate = 1.0;
        if(oli.Currency__c != 'CAD')
        {
            if(oli.Exchange_Rate_to_CAD__c == null) return null;
            else                                 exchangeRate = oli.Exchange_Rate_to_CAD__c;
        }
        
        Decimal axelPrice;
        Decimal supplierAxelPrice = 0;
        
        if(f != null && f.Prices_F_O_B__c == 'Destination')
        {
            if(oli.Axle_8_Price__c != null)
            {
                axelPrice = oli.Axle_8_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate8_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate8_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate8_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate8_Supplier2__c;
                }
            }
            else if(oli.Axle_5_Price__c != null)
            {
                axelPrice = oli.Axle_5_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate5_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate5_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate5_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate5_Supplier2__c;
                }
            }
            else if(oli.Axle_6_Price__c != null)
            {
                axelPrice = oli.Axle_6_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate6_Supplier_1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate6_Supplier_1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate6_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate6_Supplier2__c;
                }
            }
            else if(oli.Axle_7_Price__c != null)
            {
                axelPrice = oli.Axle_7_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate7_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate7_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate7_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate7_Supplier2__c;
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            if     (oli.Axle_8_Price__c != null) axelPrice = oli.Axle_8_Price__c * exchangeRate;
            else if(oli.Axle_5_Price__c != null) axelPrice = oli.Axle_5_Price__c * exchangeRate;
            else if(oli.Axle_6_Price__c != null) axelPrice = oli.Axle_6_Price__c * exchangeRate;
            else if(oli.Axle_7_Price__c != null) axelPrice = oli.Axle_7_Price__c * exchangeRate;
            else                                 return null;
        }
        
        // Calculate refinery netback amount
        
        Decimal refineryNetback = axelPrice;
        
        refineryNetback -= (supplierAxelPrice * exchangeRate);
        
        if(oli.Handling_Fees_CAD__c    != null) refineryNetback -= oli.Handling_Fees_CAD__c;
        if(oli.Additives_CAD__c        != null) refineryNetback -= oli.Additives_CAD__c;
        if(oli.Terminal_Freight_CAD__c != null) refineryNetback -= oli.Terminal_Freight_CAD__c;
        if(oli.AC_Premium_CAD__c       != null) refineryNetback -= oli.AC_Premium_CAD__c;
        
        return refineryNetback;
    }
    
    public static Decimal calculateAsphaltTerminalNetback(OpportunityLineItem oli, ATS_Freight__c f, Product2 p)
    {
        // Make sure at least one supplier is checked on freight record. Choose which supplier to use.
        
        Integer selectedHuskySupplier;
        
        if     (f == null)                      selectedHuskySupplier = null;
        else if(f.Husky_Supplier_1_Selected__c) selectedHuskySupplier = 1;
        else if(f.Husky_Supplier_2_Selected__c) selectedHuskySupplier = 2;
        
        Decimal exchangeRate = 1.0;
        if(oli.Currency__c != 'CAD')
        {
            if(oli.Exchange_Rate_to_CAD__c == null) return null;
            else                                 exchangeRate = oli.Exchange_Rate_to_CAD__c;
        }
        
        Decimal axelPrice;
        Decimal supplierAxelPrice = 0;
        
        if(f != null && f.Prices_F_O_B__c == 'Destination')
        {
            if(oli.Axle_8_Price__c != null)
            {
                axelPrice = oli.Axle_8_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate8_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate8_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate8_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate8_Supplier2__c;
                }
            }
            else if(oli.Axle_5_Price__c != null)
            {
                axelPrice = oli.Axle_5_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate5_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate5_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate5_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate5_Supplier2__c;
                }
            }
            else if(oli.Axle_6_Price__c != null)
            {
                axelPrice = oli.Axle_6_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate6_Supplier_1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate6_Supplier_1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate6_Supplier2__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate6_Supplier2__c;
                }
            }
            else if(oli.Axle_7_Price__c != null)
            {
                axelPrice = oli.Axle_7_Price__c * exchangeRate;
                if(selectedHuskySupplier == 1 && f.Emulsion_Rate7_Supplier1__c != null && p.Density__c != null)
                {
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate7_Supplier1__c;
                }
                else if(selectedHuskySupplier == 2 && f.Emulsion_Rate7_Supplier2__c != null && p.Density__c != null)
                {
                    
                    Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                    if(conversion != null) supplierAxelPrice = conversion * f.Emulsion_Rate7_Supplier2__c;
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            if     (oli.Axle_8_Price__c != null) axelPrice = oli.Axle_8_Price__c * exchangeRate;
            else if(oli.Axle_5_Price__c != null) axelPrice = oli.Axle_5_Price__c * exchangeRate;
            else if(oli.Axle_6_Price__c != null) axelPrice = oli.Axle_6_Price__c * exchangeRate;
            else if(oli.Axle_7_Price__c != null) axelPrice = oli.Axle_7_Price__c * exchangeRate;
            else return null;
        }
        
        // Calculate terminal netback amount
        
        Decimal terminalNetback = axelPrice;
        
        terminalNetback -= (supplierAxelPrice * exchangeRate);
        
        if(oli.Handling_Fees_CAD__c    != null) terminalNetback -= oli.Handling_Fees_CAD__c;
        if(oli.Additives_CAD__c        != null) terminalNetback -= oli.Additives_CAD__c;
        if(oli.AC_Premium_CAD__c       != null) terminalNetback -= oli.AC_Premium_CAD__c;
        
        return terminalNetback;
    }
    
    //
    // 
    //
    public static Map<Integer,Decimal> calculateEmulsionAxlePrices(OpportunityLineItem oli, ATS_Freight__c f, Product2 p)
    {
        // Make sure at least one supplier is checked on freight record. Choose which supplier to use.
        
        Integer selectedHuskySupplier;
        
        if     (f == null)                      selectedHuskySupplier = null;
        else if(f.Husky_Supplier_1_Selected__c) selectedHuskySupplier = 1;
        else if(f.Husky_Supplier_2_Selected__c) selectedHuskySupplier = 2;
        
        Decimal axelPrice = 0.0;
        
        if(oli.Emulsion_Cost_CAD__c   != null) axelPrice += oli.Emulsion_Cost_CAD__c;
        if(oli.Emulsion_Margin_CAD__c != null) axelPrice += oli.Emulsion_Margin_CAD__c;
        
        Map<Integer,Decimal> axelPrices = new Map<Integer,Decimal>{5 => axelPrice,
                                                                   6 => axelPrice,
                                                                   7 => axelPrice,
                                                                   8 => axelPrice};
        
        if(f != null && f.Prices_F_O_B__c == 'Destination')
        {
            Decimal supplierAxel5Price = 0;
            Decimal supplierAxel6Price = 0;
            Decimal supplierAxel7Price = 0;
            Decimal supplierAxel8Price = 0;
            
            if(selectedHuskySupplier == 1 && f.Emulsion_Rate8_Supplier1__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel8Price = conversion * f.Emulsion_Rate8_Supplier1__c;
            }
            else if(selectedHuskySupplier == 2 && f.Emulsion_Rate8_Supplier2__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel8Price = conversion * f.Emulsion_Rate8_Supplier2__c;
            }
            
            if(oli.Currency__c == 'CAD')
                axelPrices.put(8, axelPrice + supplierAxel8Price);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null)
                    axelPrices.put(8, null);
                else
                    axelPrices.put(8, (axelPrice / oli.Exchange_Rate_to_CAD__c) + supplierAxel8Price);
            }
            
            if(selectedHuskySupplier == 1 && f.Emulsion_Rate7_Supplier1__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel7Price = conversion * f.Emulsion_Rate7_Supplier1__c;
            }
            else if(selectedHuskySupplier == 2 && f.Emulsion_Rate7_Supplier2__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel7Price = conversion * f.Emulsion_Rate7_Supplier2__c;
            }
            
            if(oli.Currency__c == 'CAD')
                axelPrices.put(7, axelPrice + supplierAxel7Price);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null)
                    axelPrices.put(7, null);
                else
                    axelPrices.put(7, (axelPrice / oli.Exchange_Rate_to_CAD__c) + supplierAxel7Price);
            }
            
            if(selectedHuskySupplier == 1 && f.Emulsion_Rate6_Supplier_1__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel6Price = conversion * f.Emulsion_Rate6_Supplier_1__c;
            }
            else if(selectedHuskySupplier == 2 && f.Emulsion_Rate6_Supplier2__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel6Price = conversion * f.Emulsion_Rate6_Supplier2__c;
            }
            
            if(oli.Currency__c == 'CAD')
                axelPrices.put(6, axelPrice + supplierAxel6Price);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null)
                    axelPrices.put(6, null);
                else
                    axelPrices.put(6, (axelPrice / oli.Exchange_Rate_to_CAD__c) + supplierAxel6Price);
            }
            
            if(selectedHuskySupplier == 1 && f.Emulsion_Rate5_Supplier1__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_1_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel5Price = conversion * f.Emulsion_Rate5_Supplier1__c;
            }
            else if(selectedHuskySupplier == 2 && f.Emulsion_Rate5_Supplier2__c != null && p.Density__c != null)
            {
                Decimal conversion = UnitConversions.getConversionRatio(f.Supplier_2_Unit__c,oli.Unit__c,p.Density__c);
                if(conversion != null) supplierAxel5Price = conversion * f.Emulsion_Rate5_Supplier2__c;
            }
            
            if(oli.Currency__c == 'CAD')
                axelPrices.put(5, axelPrice + supplierAxel5Price);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null)
                    axelPrices.put(5, null);
                else
                    axelPrices.put(5, (axelPrice / oli.Exchange_Rate_to_CAD__c) + supplierAxel5Price);
            }
        }
        else
        {
            if(oli.Currency__c == 'CAD')
                axelPrices.put(8, axelPrice);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null) axelPrices.put(8, null);
                else                                 axelPrices.put(8, (axelPrice / oli.Exchange_Rate_to_CAD__c));
            }
            if(oli.Currency__c == 'CAD')
                axelPrices.put(7, axelPrice);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null) axelPrices.put(7, null);
                else                                 axelPrices.put(7, (axelPrice / oli.Exchange_Rate_to_CAD__c));
            }
            if(oli.Currency__c == 'CAD')
                axelPrices.put(6, axelPrice);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null) axelPrices.put(6, null);
                else                                 axelPrices.put(6, (axelPrice / oli.Exchange_Rate_to_CAD__c));
            }
            if(oli.Currency__c == 'CAD')
                axelPrices.put(5, axelPrice);
            else
            {
                if(oli.Exchange_Rate_to_CAD__c == null) axelPrices.put(5, null);
                else                                 axelPrices.put(5, (axelPrice / oli.Exchange_Rate_to_CAD__c));
            }
        }
        return axelPrices;
    }
    
    //
    // Competitor Terminal Netback = Competitor Bid Price ? Competitor Freight ? Handling Fees ? Additive ? AC Premium
    //
    public static Decimal calculateCompetitorTerminalNetback(Integer competitor, OpportunityLineItem oli, ATS_Freight__c f)
    {
        Decimal terminalNetback = (Decimal) oli.get('Competitor_'+competitor+'_Bid_Price__c');
        
        if(terminalNetback == null) return terminalNetback;
        
        Decimal exchangeRate = 1.0;
        if(oli.Currency__c != 'CAD')
        {
            if(oli.Exchange_Rate_to_CAD__c == null) return null;
            else                                 exchangeRate = oli.Exchange_Rate_to_CAD__c;
        }

        if(f != null)
        {
Product2 p = null;
if (oli.Product2Id != null)
  p = [SELECT Id, Density__c FROM Product2 WHERE Id = :oli.Product2Id];
if (p == null)
  return null;
          
Decimal conversion = UnitConversions.getConversionRatio('Tonne', oli.Unit__c, p.Density__c);

if (conversion != null)
{        
            if(competitor == 1)
            {
                if(f.get('Emulsion_Rate8_Competitor1__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor1__c');
                else if(f.get('Emulsion_Rate5_Competitor1__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor1__c');
                else if(f.get('Emulsion_Rate6_Competitor1__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor1__c');
                else if(f.get('Emulsion_Rate7_Competitor1__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor1__c');
            }
            else if(competitor == 2)
            {
                if(f.get('Emulsion_Rate8_Competitor2__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor2__c');
                else if(f.get('Emulsion_Rate5_Competitor2__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor2__c');
                else if(f.get('Emulsion_Rate6_Competitor2__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor2__c');
                else if(f.get('Emulsion_Rate7_Competitor2__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor2__c');
            }
            else if(competitor == 3)
            {
                if(f.get('Emulsion_Axle8_Rate_Competitor3__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor3__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor3__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor3__c');
                else if(f.get('Emulsion_Rate7_Competitor3__c')      != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor3__c');
            }
            else
            {
                if(f.get('Emulsion_Axle8_Rate_Competitor4__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor4__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor4__c') != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor4__c');
                else if(f.get('Emulsion_Rate7_Competitor4__c')      != null) terminalNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor4__c');
            }
}            
        }
        
        terminalNetback *= exchangeRate;
        
        if(oli.Handling_Fees_CAD__c != null) terminalNetback -= oli.Handling_Fees_CAD__c;
        if(oli.Additives_CAD__c     != null) terminalNetback -= oli.Additives_CAD__c;
        if(oli.AC_Premium_CAD__c    != null) terminalNetback -= oli.AC_Premium_CAD__c;
        
        return terminalNetback;
    }
    
    //
    // Competitive Refinery Netback = Competitor Bid Price ? Competitor Freight ? Handling Fees ? Additive ? Competitor Terminal Freight ? AC Premium
    //
    public static Decimal calculateCompetitorRefineryNetback(Integer competitor, OpportunityLineItem oli, ATS_Freight__c f)
    {
        Decimal refineryNetback = (Decimal) oli.get('Competitor_'+competitor+'_Bid_Price__c');
        
        if(refineryNetback == null) return refineryNetback;
        
        Decimal exchangeRate = 1.0;
        if(oli.Currency__c != 'CAD')
        {
            if(oli.Exchange_Rate_to_CAD__c == null) return null;
            else                                 exchangeRate = oli.Exchange_Rate_to_CAD__c;
        }

        if(f != null)
        {
Product2 p = null;
if (oli.Product2Id != null)
  p = [SELECT Id, Density__c FROM Product2 WHERE Id = :oli.Product2Id];
if (p == null)
  return null;
          
Decimal conversion = UnitConversions.getConversionRatio('Tonne', oli.Unit__c, p.Density__c);

if (conversion != null)
{
            if(competitor == 1)
            {
                if     (f.get('Emulsion_Rate8_Competitor1__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor1__c');
                else if(f.get('Emulsion_Rate5_Competitor1__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor1__c');
                else if(f.get('Emulsion_Rate6_Competitor1__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor1__c');
                else if(f.get('Emulsion_Rate7_Competitor1__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor1__c');
            }
            else if(competitor == 2)
            {
                if     (f.get('Emulsion_Rate8_Competitor2__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor2__c');
                else if(f.get('Emulsion_Rate5_Competitor2__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor2__c');
                else if(f.get('Emulsion_Rate6_Competitor2__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor2__c');
                else if(f.get('Emulsion_Rate7_Competitor2__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor2__c');
            }
            else if(competitor == 3)
            {
                if     (f.get('Emulsion_Axle8_Rate_Competitor3__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor3__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor3__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor3__c');
                else if(f.get('Emulsion_Rate7_Competitor3__c')      != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor3__c');
            }
            else
            {
                if     (f.get('Emulsion_Axle8_Rate_Competitor4__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor4__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor4__c') != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor4__c');
                else if(f.get('Emulsion_Rate7_Competitor4__c')      != null) refineryNetback -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor4__c');
            }
}            
        }
        
        refineryNetback *= exchangeRate;
        
        if(oli.Handling_Fees_CAD__c                                 != null) refineryNetback -= oli.Handling_Fees_CAD__c;
        if(oli.Additives_CAD__c                                     != null) refineryNetback -= oli.Additives_CAD__c;
        if(oli.get('Competitor_'+competitor+'_Terminal_Freight_CAD__c') != null) refineryNetback -= ((Decimal)oli.get('Competitor_'+competitor+'_Terminal_Freight_CAD__c'));
        if(oli.AC_Premium_CAD__c                                    != null) refineryNetback -= oli.AC_Premium_CAD__c;
        
        return refineryNetback;
    }
    
    public static Decimal calculateCompetitorMargin(Integer competitor, OpportunityLineItem oli, ATS_Freight__c f)
    {
        Decimal margin = (Decimal) oli.get('Competitor_'+competitor+'_Bid_Price__c');
        
        if(margin == null) return margin;
        
        Decimal exchangeRate = 1.0;
        if(oli.Currency__c != 'CAD')
        {
            if(oli.Exchange_Rate_to_CAD__c == null) return null;
            else                                 exchangeRate = oli.Exchange_Rate_to_CAD__c;
        }

/*        
        if(oli.get('Competitor_'+competitor+'_Emulsion_Cost_CAD__c') != null)
            margin -= (Decimal)oli.get('Competitor_'+competitor+'_Emulsion_Cost_CAD__c');
*/
        
        if(f != null)
        {
Product2 p = null;
if (oli.Product2Id != null)
  p = [SELECT Id, Density__c FROM Product2 WHERE Id = :oli.Product2Id];
if (p == null)
  return null;

Decimal conversion = UnitConversions.getConversionRatio('Tonne', oli.Unit__c, p.Density__c);

if (conversion != null)
{        
        	
            if(competitor == 1)
            {
                if     (f.get('Emulsion_Rate8_Competitor1__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor1__c');
                else if(f.get('Emulsion_Rate5_Competitor1__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor1__c');
                else if(f.get('Emulsion_Rate6_Competitor1__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor1__c');
                else if(f.get('Emulsion_Rate7_Competitor1__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor1__c');
            }
            else if(competitor == 2)
            {
                if     (f.get('Emulsion_Rate8_Competitor2__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate8_Competitor2__c');
                else if(f.get('Emulsion_Rate5_Competitor2__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate5_Competitor2__c');
                else if(f.get('Emulsion_Rate6_Competitor2__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate6_Competitor2__c');
                else if(f.get('Emulsion_Rate7_Competitor2__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor2__c');
            }
            else if(competitor == 3)
            {
                if     (f.get('Emulsion_Axle8_Rate_Competitor3__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor3__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor3__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor3__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor3__c');
                else if(f.get('Emulsion_Rate7_Competitor3__c')      != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor3__c');
            }
            else
            {
                if     (f.get('Emulsion_Axle8_Rate_Competitor4__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle8_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle5_Rate_Competitor4__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle5_Rate_Competitor4__c');
                else if(f.get('Emulsion_Axle6_Rate_Competitor4__c') != null) margin -= conversion * (Decimal)f.get('Emulsion_Axle6_Rate_Competitor4__c');
                else if(f.get('Emulsion_Rate7_Competitor4__c')      != null) margin -= conversion * (Decimal)f.get('Emulsion_Rate7_Competitor4__c');
            }
        }
}        
/*
        return (margin * exchangeRate);
*/

margin *= exchangeRate;
if(oli.get('Competitor_'+competitor+'_Emulsion_Cost_CAD__c') != null)
    margin -= (Decimal)oli.get('Competitor_'+competitor+'_Emulsion_Cost_CAD__c');
return margin;	
    }
}