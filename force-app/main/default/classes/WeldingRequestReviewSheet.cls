global class WeldingRequestReviewSheet {
    WebService Static void GenerateReviewSheetAndSend( List<Id> approvedRequestIds) {
        Map<Id, Set<Id>> requestToRelatedProcedureMap = new Map<Id, Set<Id>>();
        
        Map<Id, String> relatedWeldingMap = new Map<Id, String>();
        
        Map<Id, Welding_Request__c> requestIdToRequestMap = new Map<Id, Welding_Request__c>([SELECT Id, Name, OwnerId, Owner.Name, Owner.Email, CreatedDate 
                                                                                                 FROM Welding_Request__c WHERE Id in :approvedRequestIds]);
        for(Related_Welding_Procedure__c relatedWeldingProcedure : [SELECT Id, Name, Welding_Request__c, Welding_Procedure__c, Welding_Procedure__r.Name 
                                                                    FROM Related_Welding_Procedure__c
                                                                    WHERE Welding_Request__c in :approvedRequestIds]) {
                                                                        
            Set<Id> relatedWeldingProcedureIds = requestToRelatedProcedureMap.get(relatedWeldingProcedure.Welding_Request__c);
            if(relatedWeldingProcedureIds == Null) {
                relatedWeldingProcedureIds = new Set<Id>();
                relatedWeldingProcedureIds.add(relatedWeldingProcedure.Id);
                requestToRelatedProcedureMap.put(relatedWeldingProcedure.Welding_Request__c, relatedWeldingProcedureIds);
            }
            else {
                relatedWeldingProcedureIds.add(relatedWeldingProcedure.Id);
                requestToRelatedProcedureMap.put(relatedWeldingProcedure.Welding_Request__c, relatedWeldingProcedureIds);
            }
            relatedWeldingMap.put(relatedWeldingProcedure.Id, relatedWeldingProcedure.Welding_Procedure__r.Name);
        }
        
        List<Attachment> attachments = new List<Attachment>();
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        for(Id request : requestToRelatedProcedureMap.keySet()) {
            
            List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
            
            for(Id relatedWeldingId : requestToRelatedProcedureMap.get(request)) {
                PageReference weldingReview = new pageReference('/apex/WeldingProcedureReviewTemplate');
                weldingReview.getParameters().put('relatedWeldingProcedureId', relatedWeldingId);
                
                Blob weldingReviewBlob = Test.isRunningTest()? Blob.valueOf('UNIT.TEST') : weldingReview.getContentAsPDF();
                
                Attachment attachment = new Attachment();
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = request;
                attachment.Name = relatedWeldingMap.get(relatedWeldingId) + ' Review Sheet.pdf';
                attachment.IsPrivate = False;
                attachment.Body = weldingReviewBlob;
                attachment.ContentType = 'application/pdf';
                
                attachments.add(attachment);
                
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setFileName(relatedWeldingMap.get(relatedWeldingId) + ' Review Sheet.pdf');
                emailAttachment.setBody(weldingReviewBlob);
                
                emailAttachments.add(emailAttachment);
            }
            
            Welding_Request__c weldingRequest = requestIdToRequestMap.get(request);
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject('Welding Procedure Review Sheet (' + weldingRequest.Name + ')');
            System.debug('weldingRequest.Owner.Email: ' + weldingRequest.Owner.Email);
            
            email.setToAddresses(new String[] {Test.isRunningTest()? 'welding@huskyenergy.com' : weldingRequest.Owner.Email});
            email.setFileAttachments(emailAttachments);
            email.setPlainTextBody('Dear ' + weldingRequest.Owner.Name + ', ' + '\n\nWe have attached the Welding Procedure Review Sheets for the welding request' + 
                                   ' that you have submitted on ' + weldingRequest.CreatedDate + 
                                   '.\n\nThank you,\nTechnical Services-Engineering\nProcurement & Project Mgmt.\nHusky Energy');
            
            email.setReplyTo('welding@huskyenergy.com');
            email.setSenderDisplayName('SE&P Technical Services - Husky Oil Operations Ltd.');
            
            emails.add(email);
        }
        
        System.debug('emails:' + emails);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(emails);
        
        insert attachments;
    }
}