/*-------------------------------------------------------------------------------------------------
Author: 		Marcel Brimus
Company: 		Husky Energy
Description: 	Utility class for common methods used in HOG Schedulable classes

Test Class:		HOG_VGEmailNotificationsSchedulableTest
				VTT_RevertActivityStatusBatchTest
				
History:        mbrimus 14.5.2018 - Created.
---------------------------------------------------------------------------------------------------*/ 
public without sharing class HOG_ScheduleUtilities {
	public static String TAG = 'HOG_ScheduleUtilities';
	
	/**
	 * Aborts Scheduled jobs with name equal to
	 * @param jobName [job name]
	 */
	public static void abortCompletedJobsEqualTo(String jobName) {
		List<CronJobDetail> jobsDetails = getRescheduledJobsDetailsNameEqualsTo(jobName);
		if (jobsDetails != null && jobsDetails.size() > 0) {
			Set<Id> jobsDetailsIds = (new Map<Id, CronJobDetail> (jobsDetails)).keySet();
			List<CronTrigger> jobsToAbort = getCompletedJobsByIds(jobsDetailsIds);
			abortJobs(jobsToAbort);
		}
	}

	/**
	 * Aborts Scheduled jobs with name like to
	 * @param jobName [job name]
	 */
	public static void abortCompletedJobsLike(String jobName) {
		List<CronJobDetail> jobsDetails = getRescheduledJobsDetailsNameLike(jobName);
		if (jobsDetails != null && jobsDetails.size() > 0) {
			Set<Id> jobsDetailsIds = (new Map<Id, CronJobDetail> (jobsDetails)).keySet();
			List<CronTrigger> jobsToAbort = getCompletedJobsByIds(jobsDetailsIds);
			abortJobs(jobsToAbort);
		}
	}

	/**
	 * Aborts all jobs passed in
	 * @param jobsToAbort [Scheduled jobs to abort]
	 */
	private static void abortJobs(List<CronTrigger> jobsToAbort) {
		if(jobsToAbort != null)
			for(CronTrigger ct : jobsToAbort)
				System.abortJob(ct.Id);
	}

	/**
	 * Return Scheduled job details with name equal to
	 * @param  jobName [name of job]
	 * @return         [scheduled jobs details]
	 */
	private static List<CronJobDetail> getRescheduledJobsDetailsNameEqualsTo(String jobName) {
		return [SELECT Id
				FROM CronJobDetail
				WHERE Name =:jobName];
	}

	/**
	 * Return Scheduled job details with name equal to
	 * @param  jobName [name of job]
	 * @return         [scheduled jobs details]
	 */
	private static List<CronJobDetail> getRescheduledJobsDetailsNameLike(String jobName) {
		return [SELECT Id
				FROM CronJobDetail
				WHERE Name LIKE :jobName];
	}

	/**
	 * Get ids of Scheduled job based on JobDetails
	 * @param  jobsDetailsIds [Id of JobDetails]
	 * @return                [List of Sceduled Jobs]
	 */
	private static List<CronTrigger> getCompletedJobsByIds(Set<Id> jobsDetailsIds) {
		return [SELECT Id 
				FROM CronTrigger
				WHERE CronJobDetailId IN : jobsDetailsIds
					AND NextFireTime = null
					AND State IN ('DELETED','COMPLETE')];
	}
}