@isTest
global with sharing class SAPHOGGetWorkOrderResponseMock implements WebServiceMock{

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGWorkOrderServices.GetWorkOrderResponse responseElement = new SAPHOGWorkOrderServices.GetWorkOrderResponse();
   		responseElement.Message = 'SAPHOGWorkOrderServices.GetWorkOrderResponse Test Message';
   		responseElement.type_x = True;
   		responseElement.WorkOrder = new SAPHOGWorkOrderServices.WorkOrder();
   		responseElement.WorkOrder.WorkOrderNumber = '24173534';
   		
   		response.put('response_x', responseElement);
	}
}