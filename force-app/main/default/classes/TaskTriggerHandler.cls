// ====================================================================
// Handler class for all the triggers related to changes in Task object
public without sharing class TaskTriggerHandler {
    
    // ------------------------------------------------------------------
  // On closed task, do following
  //
  // 1) If parent is a case, update the case's Last Activity Date
  //    to now
    public static void onClosedTask(List<Task> activities) {
        Set<Id>closedCases = new Set<Id>();
        
        Map<Id, List<Task>> failedTasks = 
            new Map<Id, List<Task>>();
        
        for (Task activity : activities) {  
            closedCases.add(activity.whatId);
            if (!failedTasks.containsKey(activity.whatId)) {
                failedTasks.put(activity.whatId, new List<Task>());
            }
            failedTasks.get(activity.whatId).add(activity);
        }
        
        System.debug(closedCases);
        
        
        for (List<Case> updatedCases : 
             [SELECT Id, Last_Activity_Date__c 
                     FROM Case
                      WHERE Id IN :closedCases]) {
            for(Case updatedCase: updatedCases) {
                updatedCase.Last_Activity_Date__c = DateTime.now();
            }
            
            System.debug(updatedCases);

            update updatedCases;
  
        }
    }

}