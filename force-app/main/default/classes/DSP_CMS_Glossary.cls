global virtual with sharing class DSP_CMS_Glossary extends DSP_CMS_GlossaryController
{
    global override String getHTML()
    {
        return glossaryHTML();
    }
}