public with sharing class TeamAllocationListController {
    private static Boolean activeAllocationsLoaded = false;
    private static Boolean activeExpiredAllocationsLoaded = false;
    private static  List<Team_Allocation__c> activeAllocations;
    private static  List<Team_Allocation__c> activeExpiredAllocations;
    // Number of results listing page
    private final Integer RESULT_PER_PAGE = 50;
  
    public Boolean disableApproval {get;set;}
    public Team_Allocation__c approverName {get; set;}
    public String selectedTAIds {get;set;}

    public TeamAllocationListController()    
    {
        approverName = new Team_Allocation__c();
        disableApproval = true;
       
    }
    public ApexPages.StandardSetController InActiveTASetCtrl 
    {
        get 
        {
            System.debug('InActiveTASetCtrl');
            if (InActiveTASetCtrl == null)
            {
                //InActiveTASetCtrl = new ApexPages.StandardSetController( TeamAllocationUtilities.getInActiveTeamAllocations()); 
                //SM 3/28/2016 replace list with Database.queryLocator
                InActiveTASetCtrl = new ApexPages.StandardSetController(
                     Database.getQueryLocator([
                        SELECT 
                            id,
                            Status__c,
                            Date__c,
                            Team__c, Team__r.Name,
                            Shift__c, Shift__r.Name,
                            Operating_District__c, Operating_District__r.Name,
                            Team_Allocation_Name__c,
                            Team_Allocation_DetailPage_HLink__c, 
                            LastModifiedDate,
                            LastModifiedById, LastModifiedBy.Name,
                            Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
                        FROM Team_Allocation__c
                        WHERE Status__c = :TeamAllocationUtilities.INACTIVE_STATUS_NAME
                        ORDER BY Date__c DESC, Team_Allocation_Name__c DESC
                       limit 3000
                    ])                
                ); 
               
                System.debug('InActiveTASetCtrl size' + InActiveTASetCtrl.getResultSize());
            }
            return InActiveTASetCtrl;
        }
        set;
    }
    public List<Team_Allocation__c> getInActiveTeamAllocations() {
    
        if (InActiveTASetCtrl != null)
        {
            InActiveTASetCtrl.setpagesize(RESULT_PER_PAGE);
            System.debug('getInActiveTeamAllocations');
            return (List<Team_Allocation__c>) InActiveTASetCtrl.getRecords();
        }
        return null;
    }
 
    public List<Team_Allocation__c> getActiveTeamAllocations() {
        if (!activeAllocationsLoaded) {
            activeAllocations = TeamAllocationUtilities.getActiveTeamAllocations();
            activeAllocationsLoaded = true;
        }
        return activeAllocations;
    }
    
    public List<Team_Allocation__c> getActiveExpiredTeamAllocations() {
        if (!activeExpiredAllocationsLoaded) {
            activeExpiredAllocations = TeamAllocationUtilities.getActiveExpiredTeamAllocations();
            activeExpiredAllocationsLoaded = true;
        }
        return activeExpiredAllocations;
    }
    public ApexPages.StandardSetController UnapprovedTASetCtrl 
    {
        get 
        {
            if (UnapprovedTASetCtrl == null)
            {
                //UnapprovedTASetCtrl = new ApexPages.StandardSetController( TeamAllocationUtilities.getUnapprovedTeamAllocations()); 
                //SM 3/28/2016 replace list with Database.queryLocator
                UnapprovedTASetCtrl = new ApexPages.StandardSetController(
                     Database.getQueryLocator( [
                        SELECT 
                            id,
                            Status__c,
                            Date__c,
                            Team__c, Team__r.Name,
                            Shift__c, Shift__r.Name,
                            Operating_District__c, Operating_District__r.Name,
                            Team_Allocation_Name__c,
                            Team_Allocation_DetailPage_HLink__c, 
                            LastModifiedDate,
                            LastModifiedById, LastModifiedBy.Name,
                            Approver__c, Approver__r.Name, RecordTypeId, RecordType.Name
                        FROM Team_Allocation__c
                        WHERE Approver__c = null 
                        ORDER BY Date__c DESC, Team_Allocation_Name__c DESC
                        limit 3000
                    ])                
                );                 
                
                
               
                System.debug('UnapprovedTASetCtrl size' + UnapprovedTASetCtrl.getResultSize());
            }
            
            return UnapprovedTASetCtrl;
        }
        set;
    }
 
    public List<Team_Allocation__c> getUnapprovedTeamAllocations()
    {
        if (UnapprovedTASetCtrl != null)
        {
             UnapprovedTASetCtrl.setpagesize(RESULT_PER_PAGE);
            return (List<Team_Allocation__c>)UnapprovedTASetCtrl.getRecords();
      }
        return null;
    }
    public Boolean hasInActiveTeamAllocation {
        get {
            return (InActiveTASetCtrl == null) ? false : InActiveTASetCtrl.getResultSize() > 0;
        }
    }
    
    public Boolean hasActiveTeamAllocation {
        get {
            return (getActiveTeamAllocations() == null) ? false : activeAllocations.size() > 0;
        }
    }
    
    public Boolean hasActiveExpiredTeamAllocation {
        get {
            return (getActiveExpiredTeamAllocations() == null) ? false : activeExpiredAllocations.size() > 0;
        }
    }
    
    public Boolean hasUnapprovedTeamAllocation {
        get {
            return (UnapprovedTASetCtrl == null) ? false : UnapprovedTASetCtrl.getResultSize() > 0;
        }
    }

    

    public void setApprovalStatus()
    {
        disableApproval = true;

        if (selectedTAIds != null && selectedTAIds != '')
            disableApproval = false;

        disableApproval = disableApproval || approverName.Approver__c == null;
    }
    //query again selected Team Allocation and update choosen Approver
    public void approveTeamAllocation()
    {       
        System.debug('**********selectedTAIds =' + selectedTAIds);
        try
        {
            List<String> taIDList = new List<String>();
            if (selectedTAIds != null)
                taIDList = selectedTAIds.split(',');
            List<Team_Allocation__c> updateList = [select Id, Approver__c from Team_Allocation__c where Id in: taIDList];
            for(Team_Allocation__c ta : updateList)
            {
                ta.Approver__c = approverName.Approver__c;
            }     
            update updateList;
            refreshData();
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
  
    }
    private void refreshData()
    {
        activeAllocationsLoaded = false;
        activeExpiredAllocationsLoaded = false;
        InActiveTASetCtrl = null;
        UnapprovedTASetCtrl = null;
        disableApproval = true;
        approverName = new Team_Allocation__c();
    }
    //check permission of current user
    public boolean isApprover
    {
        get{
            Id currentUserId = UserInfo.getUserId();
            List<PermissionSetAssignment> userAssigment = 
                          [SELECT Id
                            FROM PermissionSetAssignment
                            WHERE PermissionSetId
                            IN (SELECT ParentId FROM FieldPermissions 
                                 WHERE SObjectType = 'Team_Allocation__c' 
                                     AND Field='Team_Allocation__c.Approver__c' AND PermissionsEdit = true)
                            AND AssigneeId =: currentUserId];
            if (userAssigment.size() > 0)
                 return true;
                 
            return false;     
        }
    }
  
    public String getCurrentUserRecType(){
        List<RecordTypeInfo> infos = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfos();
        String recTypes = null;

        for (RecordTypeInfo i : infos) {
            if(i.getName() != 'Master' && i.isAvailable()){
                recTypes = recTypes + ' ' + i.getName();
            }
        }
        
        return recTypes;
    }  
}