@isTest
private class PREPRetireTaxonomyControllerTest {
    
    static testmethod void defaultMethods(){
        User u = [ Select Id, Name, Email from User where Id = :UserInfo.getUserId() ];
            
        Account acc = new Account( Name = 'Test' );
        insert acc;
        
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true, SCM_Manager__c = u.Id );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id, Category_Manager__c = u.Id,
                                                                Category_Specialist__c = u.Id, Active__c = true );
        insert sub;
        
        PREP_Discipline__c disp1 = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true, SCM_Manager__c = u.Id );
        insert disp1;
        
        PREP_Category__c cat1 = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp1.Id );
        insert cat1;
        
        PREP_Sub_Category__c sub1 = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat1.Id, Category_Manager__c = u.Id,
                                                                Category_Specialist__c = u.Id, Active__c = true );
        insert sub1;
        
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Services',
                                Category_Manager__c = u.Id, Category_Specialist__c = u.Id, RecordTypeId ='01216000001QPTu',
                                Technician__c = u.Id, Business_Analyst__c = u.Id, GSO__c = u.Id );
        
        PREP_Initiative__c ini1 = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually', Category_Manager__c = u.Id, Category_Specialist__c = u.Id,
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Equipment',
                                Commercial_Representative__c = u.Id, Procurement_Representative__c = u.Id, RecordTypeId = '01216000001QPTv',
                                Business_Analyst__c = u.Id, GSO__c = u.Id );
        
        List<PREP_Initiative__c> initList = new List<PREP_Initiative__c>{ ini, ini1 };
        insert initList;
        
        PREP_Material_Service_Group__c materialGroup = new PREP_Material_Service_Group__c( Name = 'MS Name', Material_Service_Group_Name__c = 'MS',
                                                        Sub_Category__c = sub.Id, Category_Manager__c = u.Id, SAP_Short_Text_Name__c = 'MS', 
                                                        Active__c = true, Type__c = 'Material', Category_Specialist__c = u.Id, Includes__c = 'MS',
                                                        Does_Not_Include__c = 'MS', Category_Link__c = cat.Id, Discipline_Link__c = disp.Id );
        insert materialGroup;
        
        test.startTest();
            PREPRetireTaxonomyController controller = new PREPRetireTaxonomyController();
            controller.preInitiative.Category__c = cat.Id;
            controller.taxonomy.Sub_Category__c = sub.Id;
            controller.preInitiative.Discipline__c = disp.Id;
            controller.preInitiative.Category_Manager__c = u.Id;
            controller.preInitiative.Category_Specialist__c = u.Id;
            controller.taxonomy.Material_Service_Group_Name__c = 'MS';
            controller.taxonomy.SAP_Short_Text_Name__c = 'MS';
            
            controller.sortExpression = 'GL_Account__r.Name';
            controller.getSortDirection();
            controller.setSortDirection( 'ASC' );
            
            controller.searchRecords();
            controller.sortRecords();
            
            system.assertEquals( controller.taxonomyWrapperList.size(), 1 );
                        
            controller.taxonomyWrapperList[0].selected = true;
            controller.taxonomyWrapperList[0].taxonomy.SAP_Short_Text_Name__c = 'MS1';
            controller.taxonomyWrapperList[0].taxonomy.Material_Service_Group_Name__c = 'MS1';
            controller.requestTitle = 'Test';
            
            controller.saveChanges();
            controller.saveAndSubmitChanges();
        test.stopTest();
    }
}