/*
* Author: Kishore Chandolu
* Name: HCVC_MapController
* Purpose: This Class action related to the Pages   HCVC_MapPage,HCVC_AssetDetails and HCVC_RacksDetails.
* Created Date: 25th July 2014
*/


global with sharing class HCVC_MapController{



     public List<Asset__c> assets{get;set;}
     public List<Asset__c> assetUnits{get;set;}
     public List<Retail_Location__c> retailLocations{get;set;}
     public String basePathSFDC{get;set;}
     public List<String> crudepipline{get;set;}
     public List<String> crudepiplineName{get;set;}
     public List<String> crudepiplineId{get;set;}
     public List<String> geoJson{get;set;}
     public List<String> geoJsonName{get;set;}
     public List<String> geoJsonResourceName{get;set;}
     public List<String> geoJsonType{get;set;}
     public String TaylorLat{get;set;} 
     public String TaylorLong{get;set;}
     public String KamloopsLat{get;set;}
     public String KamloopsLong{get;set;}
     public String assetidHistory{get;set;}
     public String assetHistory{get{if(null==assetHistory)assetHistory=''; return assetHistory;}set;}
     public String assetOverview{get{if(null==assetOverview)assetOverview=''; return assetOverview;}set;}
     public String assetRetailNetwork{get{if(null==assetRetailNetwork)assetRetailNetwork=''; return assetRetailNetwork;}set;}
     public String refineryHistory{get{if(null==refineryHistory)refineryHistory=''; return refineryHistory;}set;}
     public String refineryOverview{get{if(null==refineryOverview)refineryOverview=''; return refineryOverview;}set;}
     public String refineryRetailnetwork{get{if(null==refineryRetailnetwork)refineryRetailnetwork=''; return refineryRetailnetwork;}set;}
     public Boolean isTankFarm{get{if(null==isTankFarm)isTankFarm=false; return isTankFarm;}set;}
     public String backtoMapExt{get{if(null==backtoMapExt)backtoMapExt='false'; return backtoMapExt;}set;}
     public List<PGtankCoordinates__c> tankPGCoordinates{get;set;}
     public List<PGracksCoordinates__c> racksPGCoordinates{get;set;}
     public String assetid{get;set;}
     public String assetPageID{get;set;}
     public String refineryPageID{get;set;}
     public List<Asset__c> parentRefineries{get;set;}
     public List<Asset__c> topLevelOverviews{get{if(null==topLevelOverviews)topLevelOverviews=new List<Asset__c>(); return topLevelOverviews;}set;}
     public String assettype{get;set;}
     public String assetthoid{get;set;}
     public List<String> pipelineJsonNames{get;set;}
     public List<String> pipelineResourceNames{get;set;}
     public Boolean backmapFlag{get{if(null==backmapFlag)backmapFlag=false; return backmapFlag;}set;}
     public Boolean isTopOverview{get{if(null==isTopOverview)isTopOverview=true; return isTopOverview;}set;}
     public Boolean isRefinery{get{if(null==isRefinery)isRefinery=true; return isRefinery;}set;}
     public Boolean isPipeline{get{if(null==isPipeline)isPipeline=false; return isPipeline;}set;}
     public Boolean isRetail{get{if(null==isRetail)isRetail=false; return isRetail;}set;}
     public Boolean isTransport{get{if(null==isTransport)isTransport=false; return isTransport;}set;}
     public String tbcolor {get{if(null==tbcolor)
                            {HCVC_Customsettings__c tc = HCVC_Customsettings__c.getInstance();
                            tbcolor = tc.HCVC_tabId__c;}return tbcolor;}set;}
     public String topOverviewHistory{get{if(null==topOverviewHistory) topOverviewHistory=''; return topOverviewHistory;}set;}
     public List<latlongwrapper> backmaptankCoordinates{get{if(null==backmaptankCoordinates)
                                                        backmaptankCoordinates=new List<latlongwrapper>();
                                                        return backmaptankCoordinates;}set;}
     public static Id crudeRecordTypeId{get{if(null==crudeRecordTypeId) crudeRecordTypeId=HCVC_Utility.crudeRecordTypeId; return crudeRecordTypeId;}set;} 
     public static Id overviewRecordTypeId{get{if(null==overviewRecordTypeId) overviewRecordTypeId=HCVC_Utility.overviewRecordTypeId; return overviewRecordTypeId;}set;} 
     public static Id refineryTypeId{get{if(null==refineryTypeId) refineryTypeId=HCVC_Utility.refineryTypeId; return refineryTypeId;}set;} 
     public static Id refineryUnitTypeId{get{if(null==refineryUnitTypeId) refineryUnitTypeId=HCVC_Utility.refineryUnitTypeId; return refineryUnitTypeId;}set;} 
     public static Id refineryRacksTypeId{get{if(null==refineryRacksTypeId) refineryRacksTypeId=HCVC_Utility.refineryRacksTypeId; return refineryRacksTypeId;}set;} 
     public static Id tankTypeId{get{if(null==tankTypeId) tankTypeId=HCVC_Utility.tankTypeId; return tankTypeId;}set;}
     public static Id geojsonTypeId{get{if(null==geojsonTypeId) geojsonTypeId=HCVC_Utility.geojsonTypeId; return geojsonTypeId;}set;}                                            



     /**
      * This Constructor loads all KMZ paths from the Custom settings.
      * Loads top level Overview Contents.
      * Create list of TankCoordinates and Rack coordinates for the polygons.
      * Loads all Parent Refineries.
      * Loads all Assets of type Refinery Unit using Parent RefineryID.
      * Loads all Retail Location records.
      * Creates Json object for the Parent Refinery kml paths.
      */

      public HCVC_MapController(){

            System.debug('Inside Constructor HCVC_MapController');
            HCVC_Customsettings__c hcvccs = HCVC_Customsettings__c.getInstance();
            // Init the arrays of strings
      crudepipline = new List<String>();
      crudepiplineName = new List<String>();
      crudepiplineId = new List<String>();
      // Get all the links for the pipeline KMLs and store them in the pipeline KML
        List<Asset__c> pipelineInfo = [SELECT Geojson_Name__c, Resource_Name__c, id, Name FROM Asset__c WHERE RecordTypeId =: HCVC_Utility.crudeRecordTypeId];
        for (Asset__c pipelineObj : pipelineInfo) {
            crudepipline.add(pipelineObj.Geojson_Name__c);
              crudepiplineName.add(pipelineObj.Name);
              crudepiplineId.add(pipelineObj.id);
        }
            // Hardcoded values for taylor and kamloops lat and longs
            TaylorLat=String.valueOf(hcvccs.HCVC_Taylor_Lat__c);
            TaylorLong=String.valueOf(hcvccs.HCVC_Taylor_Long__c);
            KamloopsLat=String.valueOf(hcvccs.HCVC_Kamloops_Lat__c);
            KamloopsLong=String.valueOf(hcvccs.HCVC_Kamloops_Long__c);

			// If an ID is found, it means we are on a refinery level view
            if(null!=ApexPages.currentPage().getParameters().get('id')){
              String parentAssetId=ApexPages.currentPage().getParameters().get('id');

            if(null!=ApexPages.currentPage().getParameters().get('isRefinery')){
            isRefinery=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isRefinery'));
            }
             if(null!=ApexPages.currentPage().getParameters().get('isPipeline')){
            isPipeline=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isPipeline'));
            }
             if(null!=ApexPages.currentPage().getParameters().get('isRetail')){
            isRetail=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isRetail'));
            }
            if(null!=ApexPages.currentPage().getParameters().get('isTransport')){
            isTransport=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isTransport'));
            }

            System.debug('isRefinery  '+isRefinery+'  isPipeline   '+'    isRetail     '+isRetail+'     isTransport      '+isTransport);


            assets=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                  Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Refinery_History__c,
                  Parent_Refinery_Asset__r.Refinery_Overview__c,Parent_Refinery_Asset__r.Name,
                  Address__c,Parent_Refinery_Asset__r.Id,Parent_Refinery_Asset__r.Retail_Overview__c FROM Asset__c
                  where Parent_Refinery_Asset__c=:parentAssetId and RecordTypeId!=: HCVC_Utility.tankTypeId  ORDER BY Name];

                refineryHistory=assets[0].Parent_Refinery_Asset__r.Refinery_History__c;
                refineryOverview=assets[0].Parent_Refinery_Asset__r.Refinery_Overview__c;
                refineryRetailnetwork=assets[0].Parent_Refinery_Asset__r.Retail_Overview__c;
                assetHistory=assets[0].Parent_Refinery_Asset__r.Name+' History';
                assetOverview=assets[0].Parent_Refinery_Asset__r.Name+' Overview';
                assetRetailNetwork=assets[0].Parent_Refinery_Asset__r.Name+' Retail Network';

            parentRefineries=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                  Lat__c,Long__c,RecordTypeId,Details__c,
                  Address__c FROM Asset__c
                  where RecordTypeId=:HCVC_Utility.refineryTypeId];

            assetUnits=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                    Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                    Address__c,Parent_Refinery_Asset__r.Id FROM Asset__c
                    where Parent_Refinery_Asset__c!=null AND RecordTypeId=: HCVC_Utility.refineryUnitTypeId AND Lat__c>0 ORDER BY Parent_Refinery_Asset__c];

             retailLocations=[SELECT Id, Name, Location_Name__c, 
                    Latitude__c, Longitude__c,
                    Address__c FROM Retail_Location__c where Latitude__c > 0];
                    

      if(!retailLocations.isEmpty())
        System.debug(LoggingLevel.INFO, '# of retail Locations retrieved: ' + retailLocations.size());
      else
        System.debug(LoggingLevel.INFO, 'No RetailLocations retrieved.');

              pipelineJsonNames=new List<String>();
              pipelineResourceNames=new List<String>();
        // Get all the links for the pipeline KMLs and store them in the pipeline KML
        List<Asset__c> pipelineKMLURLs = [SELECT Geojson_Name__c, Resource_Name__c FROM Asset__c WHERE RecordTypeId =: HCVC_Utility.crudeRecordTypeId];
        for (Asset__c pipelineObj : pipelineKMLURLs) {
                pipelineJsonNames.add(pipelineObj.Geojson_Name__c);
                pipelineResourceNames.add(pipelineObj.Resource_Name__c);
        }

                    backmapFlag=true;
                    isTopOverview=false;

                   List<Sobject> sobjtankCoordinates=HCVC_MapController.loadTankCoordinates(parentAssetId);
                   if(null!=sobjtankCoordinates){
                    for(Sobject sobj:sobjtankCoordinates){
                      latlongwrapper llw=new latlongwrapper();
                      llw.latitude=sobj.get('Lat__c');
                      llw.longitude=sobj.get('Long__c');
                     backmaptankCoordinates.add(llw);

                    }
                    }

                    System.debug('sobjtankCoordinates   '+backmaptankCoordinates);
                    tankPGCoordinates=PGtankCoordinates__c.getAll().values();
                    racksPGCoordinates=PGracksCoordinates__c.getAll().values();
            }else{ // On top level view or another page

            if(null!=ApexPages.currentPage().getParameters().get('isRefinery')){
            isRefinery=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isRefinery'));
            backtoMapExt='true';

            }
             if(null!=ApexPages.currentPage().getParameters().get('isPipeline')){
            isPipeline=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isPipeline'));
            }
             if(null!=ApexPages.currentPage().getParameters().get('isRetail')){
            isRetail=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isRetail'));
            }
            if(null!=ApexPages.currentPage().getParameters().get('isTransport')){
            isTransport=Boolean.valueOf(ApexPages.currentPage().getParameters().get('isTransport'));
            }

            isTopOverview=true;

            topLevelOverviews=[SELECT Id,Name,Tank_Number__c,order__c,Overview__c,isOverview__c FROM Asset__c
                               WHERE isOverview__c=true AND RecordTypeId =: overviewRecordTypeId ORDER BY order__c ];

            System.debug('topLevelOverviews    '+topLevelOverviews);
            System.debug('isRefinery  '+isRefinery+'  isPipeline   '+'    isRetail     '+isRetail+'     isTransport      '+isTransport);

            assets=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                    Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                    Address__c FROM Asset__c
                    where RecordTypeId=:HCVC_Utility.refineryTypeId  ORDER BY Name];

            assetUnits=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                    Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                    Address__c,Parent_Refinery_Asset__r.Id FROM Asset__c
                    where Parent_Refinery_Asset__c!=null AND RecordTypeId=: HCVC_Utility.refineryUnitTypeId AND Lat__c>0 ORDER BY Parent_Refinery_Asset__c];

            retailLocations=[SELECT Id, Name, Location_Name__c,
                    Latitude__c, Longitude__c, 
                    Address__c FROM Retail_Location__c where Latitude__c > 0];
                    
      if(!retailLocations.isEmpty())
        System.debug(LoggingLevel.INFO, '# of retail Locations retrieved: ' + retailLocations.size());
      else
        System.debug(LoggingLevel.INFO, 'No RetailLocations retrieved.');

              pipelineJsonNames=new List<String>();
              pipelineResourceNames=new List<String>();
        
              List<Asset__c> pipelineKMLURLs = [SELECT Geojson_Name__c, Resource_Name__c FROM Asset__c WHERE RecordTypeId =: HCVC_Utility.crudeRecordTypeId];
        for (Asset__c pipelineObj : pipelineKMLURLs) {
                pipelineJsonNames.add(pipelineObj.Geojson_Name__c);
                pipelineResourceNames.add(pipelineObj.Resource_Name__c);
        }
              tankPGCoordinates=PGtankCoordinates__c.getAll().values();
              racksPGCoordinates=PGracksCoordinates__c.getAll().values();
          }
             basePathSFDC=URL.getSalesforceBaseUrl().toExternalForm();

               try{
                   if(String.isNotEmpty(HCVC_Utility.communitySiteUrl)){

                     basePathSFDC=basePathSFDC+HCVC_Utility.communitySiteUrl;
                   }

               }catch(Exception e){
                    System.debug('Exception is '+e);
              }

              // The below adds all the railway and refinery geojson data too objects that will get loaded at certain times
              List<Asset__c> geoJsonInfo = [SELECT Geojson_Name__c, Resource_Name__c, Name, AssetType__c FROM Asset__c WHERE RecordTypeId =: HCVC_Utility.geojsonTypeId];
              geoJson=new List<String>();
              geoJsonName=new List<String>();
              geoJsonResourceName=new List<String>();
              geoJsonType=new List<String>();
              for (Asset__c geoInfo : geoJsonInfo) {
                // Here we add the geojson file names
                geoJson.add(geoInfo.Geojson_Name__c);
                // Here we add the geojson resource names 
                geoJsonName.add(geoInfo.Name);
                // Here we add the geojson names 
                geoJsonResourceName.add(geoInfo.Resource_Name__c);
                // Here we add the type if it exists else add ''
                if(geoInfo.AssetType__c == null || geoInfo.AssetType__c == '') {
                	geoJsonType.add('');
                } else {
                	geoJsonType.add(geoInfo.AssetType__c);
                }
              }
       }



     /**
      * This method loads the all child Assets(apart from assets of Assettype Tank) using the assetId.
      * Incoming Arguments - String
      * Returns - Asset__c[]
      */

     @RemoteAction
     global static Asset__c[] loadAssets(String assetId){

         System.debug('Asset Id in loadAssets is   '+assetId);

         Asset__c[] tempAssets=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                               Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                               Address__c FROM Asset__c
                               where Parent_Refinery_Asset__c=:assetId and RecordTypeId!=: HCVC_Utility.tankTypeId];

         System.debug('Asset Id is  '+assetId+'    TempAssets are  '+tempAssets);

         return tempAssets;
     }



     /**
      * This method fetches all the TankCoordinates from the Custom settings "TankCoordinatesMap__c" using Parent assetId.
      * Incoming Arguments - String
      * Returns - Sobject[]
      */

     @RemoteAction
      global static Sobject[] loadTankCoordinates(String assetId){

        System.debug('Inside loadTankCoordinates method');

        Sobject[] tankCoordinates;

        try{

        TankCoordinatesMap__c tcm = TankCoordinatesMap__c.getValues(assetId);

         if(tcm.csname__c=='PGtankCoordinates'){
          System.debug('apex name is   '+tcm.csname__c);
          tankCoordinates=PGtankCoordinates__c.getAll().values();
          System.debug('Tank coordinates are      '+tankCoordinates);
          }

        }catch(Exception e){
          System.debug('Exception is     '+e);
        }

         return tankCoordinates;

      }



     /**
      * This method fetches all the TankCoordinates from the Custom settings "PGtankCoordinates__c".
      * Incoming Arguments - nothing
      * Returns - Sobject[]
      */

      @RemoteAction
      global static Sobject[] loadPGTankCoordinates(){

        System.debug('Inside loadTankCoordinates method');

        PGtankCoordinates__c[] tankPGCoordinates;
        tankPGCoordinates=PGtankCoordinates__c.getAll().values();

         return tankPGCoordinates;

      }



     /**
      * This method clears all the Assets and add all the child assets from the arguments.
      * Incoming Arguments - List<Asset__c> childassets
      * Returns - void
      */

       public void loadChildAssets(List<Asset__c> childassets){

         assets.clear();
         assets.addAll(childassets);
         System.debug('asset list is   '+assets);

       }



     /**
      * This method loads all the assets of type "Refinery Unit".
      * Incoming Arguments - nothing
      * Returns - void
      */

      public void getCoordinatesForProcessFlow(){
       // Check if an asset ID was passed in, if so modify the back to map to go back to the asset page it came from. 
       if(null!=ApexPages.currentPage().getParameters().get('assetPageID')){
          assetPageID = ApexPages.currentPage().getParameters().get('assetPageID');
       } else {
          assetPageID = null;
       }
       // Check if an refinery ID was passed in, if so modify the back to map to go back to the refinery page
       if(null!=ApexPages.currentPage().getParameters().get('refineryId')){
          refineryPageID = ApexPages.currentPage().getParameters().get('refineryId');
       } else {
          refineryPageID = null;
       }
       
         assets=[SELECT Id,Name,RecordTypeId,Coordinates_Flow_Map__c FROM Asset__c where RecordTypeId=: HCVC_Utility.refineryUnitTypeId  ORDER BY Name];
         System.debug('Asset list is            '+assets);
      }



     /**
      * This method loads all the Parent Refinery assets of type "Refinery".
      * Incoming Arguments - nothing
      * Returns - Asset__c[]
      */

     @RemoteAction
     global static Asset__c[] loadRefineryAssets(){

         List<Asset__c> tempAssets=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                    Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                    Address__c FROM Asset__c
                    where RecordTypeId=:HCVC_Utility.refineryTypeId  and RecordTypeId!=: HCVC_Utility.tankTypeId];


         System.debug('Inside loadRefineryAssets  TempAssets are   '+tempAssets);


         return tempAssets;
       }



     /**
      * This method loads all Retail_Location__c records.
      * Incoming Arguments - nothing
      * Returns - Retail_Location__c[]
      */

     @RemoteAction
     global static Retail_Location__c[] loadRetailSites(){

         List<Retail_Location__c> retailLocations=[SELECT Id, Name, Location_Name__c, 
                    Latitude__c, Longitude__c, 
                    Address__c FROM Retail_Location__c];


         System.debug(LoggingLevel.INFO, 'Inside loadRetailSites  retailLocations are   '+retailLocations);


         return retailLocations;
       }




     /**
      * This method loads Default map and redirects to the page HCVC_MapPage. Or the previous asset page if in history
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference loadDefaultMap(){

    PageReference reference;
    if(null==assetPageID && null==refineryPageID) {
          reference=new PageReference(basePathSFDC+'/apex/HCVC_MapPage?isRefinery='+isRefinery+'&isPipeline='+isPipeline+'&isRetail='+isRetail+'&isTransport='+isTransport+'&sfdc.tabName='+tbcolor);
    } else if(refineryPageID != null) {
      reference=new PageReference(basePathSFDC+'/apex/HCVC_MapPage?id='+refineryPageID+'&sfdc.tabName='+tbcolor);
    } else {
      reference=new PageReference(basePathSFDC+'/apex/HCVC_AssetDetails?id='+assetPageID+'&assettype='+refineryUnitTypeId+'&aflag=false');
    }

        reference.setRedirect(true);
        return reference;
      }

     /**
      * This method loads Refinery Level Overview and respective Labels.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference setRefineryHistoryOverview(){

        isTopOverview=false;

        Asset__c assethisoverview=[SELECT Id,Name,RecordTypeId,Refinery_History__c,Refinery_Overview__c,Retail_Overview__c
                                   FROM Asset__c WHERE Id=:assetidHistory];

        refineryHistory=assethisoverview.Refinery_History__c;
        refineryOverview=assethisoverview.Refinery_Overview__c;
        refineryRetailnetwork=assethisoverview.Retail_Overview__c;
        assetHistory=assethisoverview.Name+' History';
        assetOverview=assethisoverview.Name+' Overview';
        assetRetailNetwork=assethisoverview.Name+' Retail Network';

        return null;
      }




     /**
      * This method loads perticular Top Level Refinery Overview using refineryID.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference setTopHistoryOverview(){

        Asset__c tophoAsset=[SELECT Id,Name,Overview__c
                                   FROM Asset__c WHERE Id=:assetthoid LIMIT 1];

        topOverviewHistory=tophoAsset.Overview__c;

        return null;
      }



     /**
      * This method sets isTopOverview flag to true.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference setTopHistoryOverviewFlag(){


        isTopOverview=true;
        topLevelOverviews=[SELECT Id,Name,Tank_Number__c,order__c,Overview__c,isOverview__c FROM Asset__c
                           WHERE isOverview__c=true AND RecordTypeId =: overviewRecordTypeId ORDER BY order__c ];
        return null;
      }



     /**
      * This method loads the Placemarkers like Refinery or Refinery Units based on requested Type.
      * Incoming Arguments - nothing
      * Returns - PageReference
      */

      public PageReference loadPlacemarker() {

        if(assettype==String.valueOf(refineryTypeId) || assettype=='Retail'){
           assets=[SELECT Id,Name,Parent_Refinery_Asset__c,Substance__c,Tank_Number__c,
                  Lat__c,Long__c,RecordTypeId,Details__c,Parent_Refinery_Asset__r.Name,
                  Address__c FROM Asset__c
                  where RecordTypeId=:HCVC_Utility.refineryTypeId and RecordTypeId!=: HCVC_Utility.tankTypeId  ORDER BY Name];
                  isTankFarm=false;
        }
        return null;
     }
     
     /**
      * This is the Wrapper Class to hold the Latitude and Longitude of the assets.
      *
      *
      */

      public class latlongwrapper{
        public Object latitude{get;set;}
        public Object longitude{get;set;}


      }
}