/*************************************************************************************************\
Author:         Martin Panak
Company:        Husky Energy
Description:    Controller Extension for Well Tracker Edit Field page
Test class      wellTrackerFieldEditTest
History:        mp 03.04.2018 - Created.
**************************************************************************************************/
public with sharing class WellTrackerFieldEdit {

    public Well_Tracker__c wellTracker      {get;set;}
    public String          wellTrackerId    {get;set;}
    public String          wellTrackerKeyPrefix    {get;set;}

    public WellTrackerFieldEdit(ApexPages.StandardController stdController) {
        wellTrackerId = ApexPages.currentPage().getParameters().get('id');
        wellTracker = [SELECT id, Comments__c FROM Well_Tracker__c WHERE id = :wellTrackerId];
        wellTrackerKeyPrefix = Well_Tracker__c.sobjecttype.getDescribe().getKeyPrefix();
    }

    /**
    * Save the input to Comments__c field.
    */
    public void save() {
        try {
            update wellTracker;
        } catch (Exception e) {
            System.debug('wellTrackerFieldEdit Exception thrown at Save: ' + e);
        }
        
    }

  }