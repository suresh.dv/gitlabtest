/*************************************************************************************************\
Author:         Miro Zelina
Company:        Husky Energy
Description:    Controller Extension Unit Test for HOG_VGAlertCtrlX
History:        mz 04.23.2018 - Created.
**************************************************************************************************/
public with sharing class HOG_VGAlertCtrlX {
    
    private List<HOG_Vent_Gas_Alert_Task__c> ventGasAlertOpenTasks;
    private List<HOG_Vent_Gas_Alert_Task__c> ventGasAlertClosedTasks;
    private HOG_Vent_Gas_Alert__c alert;
    
    public HOG_VGAlertCtrlX(ApexPages.StandardController controller) {
        this.alert= (HOG_Vent_Gas_Alert__c)controller.getRecord();
    }
    
    //fill the list of related open tasks for custom related list on Vent Gas Alert 
    public List<HOG_Vent_Gas_Alert_Task__c> getVGAOpenTasks()
    {
        ventGasAlertOpenTasks = new List<HOG_Vent_Gas_Alert_Task__c>();
        ventGasAlertOpenTasks = [SELECT Id, Name, Assignee1__c, Assignee2__c, Assignee_Type__c, Priority__c, 
                                        Remind__c, Status__c, Subject__c, Vent_Gas_Alert__c, LastModifiedDate 
                                 FROM HOG_Vent_Gas_Alert_Task__c 
                                 WHERE Vent_Gas_Alert__r.Id = :alert.Id
                                 AND Is_Closed__c = false
                                 ORDER BY LastModifiedDate DESC];
                                 
        return ventGasAlertOpenTasks;                         
    }
    
    //fill the list of related closed tasks for custom related list on Vent Gas Alert 
    public List<HOG_Vent_Gas_Alert_Task__c> getVGAClosedTasks()
    {
        
        ventGasAlertClosedTasks = new List<HOG_Vent_Gas_Alert_Task__c>();
        ventGasAlertClosedTasks = [SELECT Id, Name, Assignee1__c, Assignee2__c, Assignee_Type__c, Priority__c, 
                                        Remind__c, Status__c, Subject__c, Vent_Gas_Alert__c, LastModifiedDate 
                                   FROM HOG_Vent_Gas_Alert_Task__c 
                                   WHERE Vent_Gas_Alert__r.Id = :alert.Id
                                   AND Is_Closed__c = true
                                   ORDER BY LastModifiedDate DESC];
                                   
        return ventGasAlertClosedTasks;                           

    }
}