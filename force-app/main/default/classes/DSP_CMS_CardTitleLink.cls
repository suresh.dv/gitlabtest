global virtual with sharing class DSP_CMS_CardTitleLink extends DSP_CMS_CardTitleLinkController
{
    global override String getHTML()
    {
        return linkHTML();
    }
}