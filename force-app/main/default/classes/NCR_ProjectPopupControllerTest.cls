@isTest 
private class NCR_ProjectPopupControllerTest {
     static testMethod void testNCRProjectPopup(){
         User ncrQaLead = NCRUserTestData.createNCRQALead();
         System.AssertNotEquals(ncrQaLead.Id, Null);
         
         System.runAs(ncrQaLead) {
             NCR_ProjectPopupController c = new NCR_ProjectPopupController();
             System.AssertNotEquals(c.listOfProjects, null);
             
             c.searchByProjectName = 'Sun';
             c.searchByBUName = 'Oil';
             
             System.AssertNotEquals(c.searchByProjectName, null);
             System.AssertNotEquals(c.searchByBUName, null);
             
             c.onSearch();
             
             System.AssertEquals(c.query, null);
             System.AssertEquals(c.accounts, null);
              
         }     
     }
}