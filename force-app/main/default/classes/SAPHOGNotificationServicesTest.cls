//SM 01/22/16 added 2 additional parameters - mal fun end date and breakdown indicator
@isTest
private class SAPHOGNotificationServicesTest {

    static testMethod void CreateNotificationResponseTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGCreateNotificationResponseMock());
        
        String functionalLocationObjectId = '?0100000000000034667';
        String maintenancePlanningPlant = 'PP01';
        String mainDescription = 'Test';
        String mainLongDescription = 'Test';
        String priority = '1';
        String notificationType = 'RE';
        String referenceId = null;
        String coding = 'BRK';
        String codingGroup = 'ENHANCE';
 		String reportedBy = 'Test';
 		
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();

        SAPHOGNotificationServices.CreateNotificationResponse response = notification.CreateNotification(
																				    functionalLocationObjectId,
																			        maintenancePlanningPlant,
																			        mainDescription,
																			        mainLongDescription,
																			        priority,
																			        notificationType,
																			        referenceId,
																			        coding,
																			        codingGroup,
																			        reportedBy,
																			        null,
																			        null,
                                                                                    null,
                                                                                    null);        
                                                                                    
        System.assertEquals(response.Message, 'SAPHOGNotificationServices.CreateNotificationResponse Test Message');
        System.assertEquals(response.Notification.NotificationNumber, '7521256');
        System.assertEquals(response.type_x, True);
    }


    static testMethod void CreateNotificationsResponseTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGCreateNotificationsResponseMock());
        
	    SAPHOGNotificationServices.CreateNotificationRequest[] notificationsRequest = new List<SAPHOGNotificationServices.CreateNotificationRequest>();
    	SAPHOGNotificationServices.CreateNotificationRequest notificationRequest = new SAPHOGNotificationServices.CreateNotificationRequest();

        notificationRequest.FunctionalLocationObjectId = '?0100000000000034667';
        notificationRequest.MaintenancePlanningPlant = 'PP01';
        notificationRequest.MainDescription = 'Test Main Description';
        notificationRequest.MainLongDescription = 'Test Main Long Description';
        notificationRequest.Priority = '1';
        notificationRequest.NotificationType = 'RE';                            
        notificationRequest.ReferenceId = null;  
        notificationRequest.Coding = 'BRK';
        notificationRequest.CodingGroup = 'ENHANCE';
        notificationRequest.ReportedBy = 'Recurring Job';                            
        notificationsRequest.add(notificationRequest);
 
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();

        SAPHOGNotificationServices.CreateNotificationsResponse response = notification.CreateNotifications(notificationsRequest);
                                                                                    
        System.assertEquals(response.Message, 'SAPHOGNotificationServices.CreateNotificationsResponse Test Message');
        System.assertEquals(response.type_x, True);
    }
    
    static testMethod void IsAliveTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGIsAliveResponseMock());
        
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
        String response = notification.IsAlive();
        
        System.assertEquals('SAPHOGNotificationServices.IsAliveResponse Test Message', response); 
    }
    
    static testMethod void GetNotificationTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGGetNotificationResponseMock(null, null, null));
        
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
        
        SAPHOGNotificationServices.GetNotificationResponse response = notification.GetNotification('7521256');
        
        System.assertEquals(response.Message, 'SAPHOGNotificationServices.GetNotificationResponse Test Message');
        System.assertEquals(response.type_x, True);
        System.assertEquals(response.Notification.NotificationNumber, '7521256');
        
    }
    
    static testMethod void UpdateNotificationTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGUpdateNotificationResponseMock());
        
 		String notificationNumber = '1234567';
 		String causeCodeGroup = 'I-SUB203';
 		String causeCode = '0001';
 		String causeDescription = 'Test Cause';
 		String newCauseLongDescription = 'Test Cause Long Description';
 		String damageCodeGroup = 'I-SUB204';
 		String damageCode = '0001';
 		String damageDescription = 'Test Damage';
 		String newDamageLongDescription = 'Test Damage Long Description';
 		String partCodeGroup = 'I-SUB201';
 		String partCode = '0004';
 		String partKey = '111';
 		String mainDescription = 'Test Main Description';
 		String newMainLongDescription = 'Test Main Long Description';
 		String coding = 'BRK';
 		String codingGroup = 'ENHANCE';
 		String mainWorkCenter = 'OPER';
 		String priority = '4';
 		DateTime lastChangedDate = System.now();
 		String lastChangedBy = 'MULESOFT';
 
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
  
        SAPHOGNotificationServices.UpdateNotificationResponse response = notification.UpdateNotification(
																						  		notificationNumber,
																						 		causeCodeGroup,
																						 		causeCode,
																						 		causeDescription,
																						 		newCauseLongDescription,
																						 		damageCodeGroup,
																						 		damageCode,
																						 		damageDescription,
																						 		newDamageLongDescription,
																						 		partCodeGroup,
																						 		partCode,
																						 		partKey,
																						 		mainDescription,
																						 		newMainLongDescription,
																						 		coding,
																						 		codingGroup,
																						 		mainWorkCenter,
																						 		priority,
																						 		lastChangedDate,
																						 		lastChangedBy,
																						 		null,
																						 		null,
                                                                                                null,
                                                                                                null);
        
        System.assertEquals(response.Message, 'SAPHOGNotificationServices.UpdateNotificationResponse Test Message');
        System.assertEquals(response.type_x, True);
        System.assertEquals(response.Notification.NotificationNumber, '7521256');
    }
        
    static testMethod void CloseNotificationResponseTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGCloseNotificationResponseMock());

        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
        SAPHOGNotificationServices.CloseNotificationResponse response = notification.CloseNotification('7521256');
                                                                                    
        System.assertEquals(response.Message, 'SAPHOGNotificationServices.CloseNotificationResponse Test Message');
        System.assertEquals(response.Notification.NotificationNumber, '7521256');
        System.assertEquals(response.type_x, True);
    }

    static testMethod void RefreshSalesforceNotificationsResponseTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGRefreshNotificationsResponseMock());

        DateTime dateToRefresh = System.now();
        SAPHOGNotificationServices.ArrayOfNotificationNumbers notificationNumbers;
        
        SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();                
        String response = notification.RefreshSalesforceNotifications(dateToRefresh, notificationNumbers);
                                                                                    
        System.assertEquals(response, 'SAPHOGNotificationServices.RefreshSalesforceNotificationsResponse Test Message');
    }
    
    static testMethod void ArrayOfNotificationNumbers() {
        SAPHOGNotificationServices.ArrayOfNotificationNumbers notificationNumbers = new SAPHOGNotificationServices.ArrayOfNotificationNumbers();
    }    
}