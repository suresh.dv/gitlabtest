@isTest
private class TestServiceCodeMAT
{
    @isTest
    static void controller_serviceCodeMAT()
    {                                        
        //-- Setup Service Code MAT
        //String recordTypeName = [Select Name From RecordType Where SobjectType = 'Rigless_Servicing_Form__c' Limit 1].Name;        
        HOG_Service_Code_MAT__c serviceCodeMAT = new HOG_Service_Code_MAT__c
            (
                Name = 'Test MAT',
                MAT_Code__c = 'TST',
                Record_Type_Name__c = 'Flushby - Production Flush'
            );
        insert serviceCodeMAT;
        //--

        Test.startTest();
        
        PageReference pageRef = Page.ServiceCodeMAT;

        Test.setCurrentPage(pageRef);
 
        ApexPages.StandardController controller = new ApexPages.StandardController(serviceCodeMAT);
     
        serviceCodeMAT serviceCodeMATController = new ServiceCodeMAT(controller);
        
        serviceCodeMATController.serviceCodeMAT = serviceCodeMAT;        
        
        serviceCodeMATController.getRecordTypes();
        
        serviceCodeMATController.getRecordTypeOptions();
                
        serviceCodeMATController.cancel();
                
        serviceCodeMATController.save();
        
        serviceCodeMATController.saveAndNew();
                        
        Test.stopTest();                     
    }   
}