/*------------------------------------------------------------------------------------------------------------
Author     : Miroslav Zelina
Company    : IBM
Description: Unit Test Class for HOG_SRPCycleTimesControllerX
History    : 04.28.17    Class Created            
-------------------------------------------------------------------------------------------------------------*/
@isTest
private class HOG_SRPCycleTimesControllerXTest {
    
    private static HOG_Service_Rig_Program__c serviceRigProgram;
    private static HOG_EOJ__c endOfJob;
    private static HOG_EOJ__c endOfJob2;
    private static HOG_Service_Rig_Program__c testSRP;
    
    @isTest
    static void testCycleTimesCalculations() {
        
        CreateTestData();
        
        Test.startTest();
        
            HOG_SRPCycleTimesControllerX SRPCycleTimesCtrlX = new HOG_SRPCycleTimesControllerX(
                new ApexPages.StandardController(serviceRigProgram));
        
        
            testSRP = [SELECT Id, SRP_Submitted_to_Fully_Approved__c, Well_Down_to_SRP_Submitted__c  
                       FROM  HOG_Service_Rig_Program__c 
                       WHERE Id =: SRPCycleTimesCtrlX.srpId];
        
            System.assertEquals(5, testSRP.SRP_Submitted_to_Fully_Approved__c);
            System.assertEquals(10, testSRP.Well_Down_to_SRP_Submitted__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.eoj.Fully_Approved_to_Service_Started__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.eoj.Serv_Started_to_Serv_Completed__c);
            System.assertEquals(25, SRPCycleTimesCtrlX.eoj.Well_Down_to_Service_Completed__c);
            System.assertEquals(5, SRPCycleTimesCtrlX.ed.Serv_Completed_to_ED_Completed__c);
            
            Set<String> errMsgSet = new Set<String>();
            
            for(ApexPages.Message msg :  ApexPages.getMessages()) {

                errMsgSet.add(msg.getSummary());
            }
            
            System.assertEquals(true, errMsgSet.contains('This SRP has more than one End of Job, Cycle Times are calculated based on the oldest one.'));
            System.assertEquals(true, errMsgSet.contains('This SRP has more than one Engineering Diagnosis form, Cycle Times are calculated based on the oldest one.'));

        Test.stopTest(); 
    }
    
    
    public static void CreateTestData(){
        
        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
        Approval.ProcessResult result;
      
              
        System.runAs(runningUser) {
           
          //SRP  + SNR and all required related objects
          serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
          serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
         
          //Submit the approval request
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(serviceRigProgram.Id);
                req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
                req.setSkipEntryCriteria(true);
                result = Approval.process(req);
            }
            
         //Approve SRP as Service Rig Coordinator
         System.runAs(serviceRigPlanner) {
                
            List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req2);
        }
        
        //Approve SRP as Production Coordinator
        System.runAs(serviceRigPlanner) {
                
            List<Id> newWorkItemIds = result.getNewWorkitemIds();
                
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req2);
        
        }
          
        
        System.runAs(runningUser) {
            
            //EOJ
            endOfJob = DEFT_TestData.createEOJReport(serviceRigProgram);
            endOfJob.Service_Started__c = date.today().addDays(20);
            endOfJob.Service_Completed__c = date.today().addDays(25);
            update endOfJob;
            
            //Create another EOJ for the same SRP - test odd case - disaply info message on vf page
            endOfJob2 = DEFT_TestData.createEOJReport(serviceRigProgram);
            endOfJob2.Service_Started__c = date.today().addDays(21);
            endOfJob2.Service_Completed__c = date.today().addDays(26);
            update endOfJob2;
            
            //ED
            HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();
            engDiag.Service_Rig_Program__c = serviceRigProgram.Id;
            engDiag.End_Of_Job__c = endOfJob.Id;
            engDiag.Diagnosis_Completed_Date__c = date.today().addDays(30);
            insert engDiag;
            
            //Create another ED for the same SRP - test odd case - disaply info message on vf page
            HOG_Engineering_Diagnosis__c engDiag2 = new HOG_Engineering_Diagnosis__c();
            engDiag2.Service_Rig_Program__c = serviceRigProgram.Id;
            engDiag2.End_Of_Job__c = endOfJob.Id;
            engDiag2.Diagnosis_Completed_Date__c = date.today().addDays(31);
            insert engDiag2;
            
            //Update SRP fields used in calculations
            serviceRigProgram.Financial_Approved_Date__c = DateTime.now().addDays(15);
            serviceRigProgram.Technical_Approved_Date__c = DateTime.now().addDays(15);
            serviceRigProgram.SRP_Submitted_date__c = DateTime.now().addDays(10);
            update serviceRigProgram;    
        }
          
    } 

}