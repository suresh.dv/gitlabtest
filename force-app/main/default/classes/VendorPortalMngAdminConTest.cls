@isTest
public class VendorPortalMngAdminConTest {

    @isTest static void testEnableDisableUser() {
    	User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        VendorPortalUtility.portalExecuteTriggerCode = false;
        
        Test.startTest();
		System.runAs(runningUser){
			Account portalAccount = AccountTestData.createAccount('PortalAccount', Null);
	        insert portalAccount;
	        portalAccount.IsPartner = true;
	        update portalAccount;
            
            Account portalAccount2 = AccountTestData.createAccount('PortalAccount', Null);
	        insert portalAccount2;
	        portalAccount2.IsPartner = true;
	        update portalAccount2;
	        
	        //Test tradesmen
			Contact testTradesmen = VTT_TestData.createTradesmanContact('Test', 'Vendor',  portalAccount.id);

			VendorPortalManagementAdminController controller = new VendorPortalManagementAdminController();

 			//Test enable user
 			controller.contactIdToEnablePortal = testTradesmen.id;
 			controller.userType = 'HOG_Vendor_Community_VTT_User';
 			controller.enablePortalUser();
            User u1 = [SELECT Id, ContactId FROM User WHERE ContactId =: testTradesmen.id];
            System.assertNotEquals(null, u1);
            
            //Test disable user
            controller.contactIdToDisablePortal = testTradesmen.id;
            controller.disablePortalUser();
            u1 = [SELECT Id, ContactId, IsActive FROM User WHERE ContactId =: testTradesmen.id];
            System.assertEquals(false, u1.IsActive);
            
            //Test reenable user
            controller.contactIdToDisablePortal = testTradesmen.id;
            controller.userType = 'HOG_Vendor_Community_VTT_User';
            controller.enablePortalUser();
            u1 = [SELECT Id, ContactId, IsActive FROM User WHERE ContactId =: testTradesmen.id];
            System.assertEquals(true, u1.IsActive);
            
            //Test change of select options
            controller.selectedAccountId = portalAccount2.Id;
            controller.getAccountInfo();

            // test pagination methods
            controller.getPortalDisabledVendorSet();
			controller.refreshPageSizePortalDisabled();
            controller.getPortalEnabledVendorSet();
            controller.refreshPageSizePortalEnabled();
                
        }
		Test.stopTest();  
        VendorPortalUtility.portalExecuteTriggerCode = true;
    }
}