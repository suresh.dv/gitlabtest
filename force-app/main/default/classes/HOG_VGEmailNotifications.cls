/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Batch class for sending notifications on Vent Gas Alert.
Test class:     HOG_VGEmailNotificationsTest
History:        sdhond 04.25.2018 - Created.
                mz: 23-May-2018 - sending notifications to Production Engineers on Re-assignment on Alert (generateEmailNotification method)
**************************************************************************************************/
global class HOG_VGEmailNotifications implements Database.Batchable<sObject> {
	
	private static final String CLASS_NAME = 'HOG_VGEmailNotifications';

	public static final String NOTIFICATIONTYPE_INCOMPLETE_ALERTS = 'INCOMPLETE_ALERTS';
	public static final String NOTIFICATIONTYPE_PROD_ENG_REASSIGN = 'PRODUCTION_ENGINEER_REASSIGNMENT_NOTIFICATION';

	private static final Set<String> INCOMPLETE_STATES = new Set<String>{'Not Started', 'In Progress'};
	
	private String NotificationType;
	private String query;
	private String queryInCompleteAlerts = 'SELECT Id, Name, Description__c, Start_Date__c, '
		+ 'Status__c, Type__c, Well_Location__c, Well_Location__r.Name, Production_Engineer__c, '
		+ 'Production_Engineer__r.Name, Priority__c '
		+ 'FROM HOG_Vent_Gas_Alert__c '
		+ 'WHERE Status__c IN :INCOMPLETE_STATES';


	
	global HOG_VGEmailNotifications(String notificationType) {
		this.NotificationType = notificationType;

		if(this.NotificationType == NOTIFICATIONTYPE_INCOMPLETE_ALERTS)
			query = queryInCompleteAlerts;
			
		if(this.NotificationType == NOTIFICATIONTYPE_PROD_ENG_REASSIGN)
			System.debug(CLASS_NAME + ' -> Vent Gas Alert Re-Assign Production Engineer Notification Triggered');
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<HOG_Vent_Gas_Alert__c> ventGasAlertList = (List<HOG_Vent_Gas_Alert__c>) scope;
		Messaging.SingleEmailMessage[] emailList = new Messaging.SingleEmailMessage[] {};

		if(NotificationType == NOTIFICATIONTYPE_INCOMPLETE_ALERTS)
			emailList = GenerateInCompleteVentGasAlertEmails(ventGasAlertList);

		if(Limits.getEmailInvocations() < Limits.getLimitEmailInvocations())
			Messaging.sendEmail(emailList);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private Messaging.SingleEmailMessage[] GenerateInCompleteVentGasAlertEmails(List<HOG_Vent_Gas_Alert__c> ventGasAlertList) {
		Messaging.SingleEmailMessage[] emailList = new Messaging.SingleEmailMessage[] {};
		Map<Id, List<HOG_Vent_Gas_Alert__c>> incompleteAlertsToProductionEngineerMap = new Map<Id, List<HOG_Vent_Gas_Alert__c>>();

		//Populate Map of Vent Gas Alerts
		for(HOG_Vent_Gas_Alert__c alert : ventGasAlertList) {
			if(incompleteAlertsToProductionEngineerMap.containsKey(alert.Production_Engineer__c))
				incompleteAlertsToProductionEngineerMap.get(alert.Production_Engineer__c).add(alert);
			else
				incompleteAlertsToProductionEngineerMap.put(alert.Production_Engineer__c, 
					new List<HOG_Vent_Gas_Alert__c>{alert});
		}

		//Create Email for each production engineer in map
		for(Id prodEngineer : incompleteAlertsToProductionEngineerMap.keySet())
			emailList.add(GenerateInCompleteVentGasAlertEmail(prodEngineer, 
				incompleteAlertsToProductionEngineerMap.get(prodEngineer)));

		return emailList;
	}

	private Messaging.SingleEmailMessage GenerateInCompleteVentGasAlertEmail(Id productionEngineer, 
		List<HOG_Vent_Gas_Alert__c> alertsList) {
		String messageBody_Html = '';
		
		for(HOG_Vent_Gas_Alert__c alert : alertsList) {
			String urlAlert = '<a target="vg_alert" href="https://'+ System.URL.getSalesforceBaseUrl().getHost() + '/apex/HOG_VGAlertView?id=' + alert.id + '">' 
				+ alert.Name + '</a>';
			String urlWellLocation = '<a target="wellLocation" href="https://'+ System.URL.getSalesforceBaseUrl().getHost() + '/' + alert.Well_Location__c + '">' 
				+ alert.Well_Location__r.Name + '</a>';
			messageBody_Html  += '<table>';
			messageBody_Html  += '<tr><td>Alert: </td><td>' + urlAlert + '<td/></tr>';
			messageBody_Html  += '<tr><td>Type: </td><td>' +  alert.Type__c + '<td/></tr>';
			messageBody_Html  += '<tr><td>Description: </td><td>' + (alert.Description__c <> null ? alert.Description__c : '') + '<td/></tr>';					
			messageBody_Html  += '<tr><td>Production Engineer: </td><td>' + alert.Production_Engineer__r.Name + '<td/></tr>';				
			messageBody_Html  += '<tr><td>Well Location: </td><td>' +  urlWellLocation + '<td/></tr>';										
			messageBody_Html  += '<tr><td>Priority: </td><td>' + alert.Priority__c + '<td/></tr>';
			messageBody_Html  += '<tr><td>Start Date:</td><td>' + (alert.Start_Date__c <> null ? alert.Start_Date__c.format() : 'Not Started') + '<td/></tr>';
			messageBody_Html  += '</table>';	
			messageBody_Html  += '<hr/>';
		}

		//Create Email
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setSubject('HOG Vent Gas Alert Daily Summary');
		email.setTargetObjectId(productionEngineer);
		email.setHtmlBody(messageBody_Html);
		email.setSaveAsActivity(false);
		
		if(this.NotificationType == NOTIFICATIONTYPE_PROD_ENG_REASSIGN){
		   email.setSubject('HOG Vent Gas Alert - Production Engineer has been Re-Assigned'); 
		}

		return email;
	}
	
	//mz: 23-May-2018 - Method called from "Vent Gas Alert Flow - Send Notification on Production Engineer Re-assignment" Flow
	@InvocableMethod
    public static void generateEmailNotification(List<List<String>> AlertIdAndProdEngId) {
        
        //check to avoid multiple notifactions sent (ie. for update with DataLoader)
        if (AlertIdAndProdEngId != null && 
            AlertIdAndProdEngId.Size() == 1 && 
            AlertIdAndProdEngId[0].Size() == 2){
            
            //get ID's of Alert and Production Engineer populated in Flow on Production Engineer Re-Assignemnt
            String AlertId = AlertIdAndProdEngId[0][0];
            String ProdEngId = AlertIdAndProdEngId[0][1];
            
            //query for Alert related fields needed in Emial Notification
            List<HOG_Vent_Gas_Alert__c> ventGasSingleAlertList = new List<HOG_Vent_Gas_Alert__c>(
                                                                    [SELECT Id, Name, Description__c, Start_Date__c, Status__c, Type__c, 
                                                                            Well_Location__c, Well_Location__r.Name, Production_Engineer__c,
                                                                            Production_Engineer__r.Name, Priority__c 
                                                                     FROM HOG_Vent_Gas_Alert__c 
                                                                     WHERE Id =: AlertId]);
                                                                     
            //check for result (which should always have only one Alert in the list)
            if (ventGasSingleAlertList != null && ventGasSingleAlertList.Size() == 1){
                
                //prepare email body and email template and send the notification  
                HOG_VGEmailNotifications vgEmailNotifClass = new HOG_VGEmailNotifications(HOG_VGEmailNotifications.NOTIFICATIONTYPE_PROD_ENG_REASSIGN);    
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message = vgEmailNotifClass.GenerateInCompleteVentGasAlertEmail(ProdEngId, ventGasSingleAlertList);
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug(CLASS_NAME + ' -> The email was sent successfully.');
                } 
                else {
                    System.debug(CLASS_NAME + ' -> The email failed to send: ' + results[0].errors[0].message);
                }
            }
        }
    }
	
}