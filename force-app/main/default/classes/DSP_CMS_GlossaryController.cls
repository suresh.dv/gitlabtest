global virtual with sharing class DSP_CMS_GlossaryController extends cms.ContentTemplateController {
    global DSP_CMS_GlossaryController(cms.CreateContentController cc) {
        super(cc);
    }
    
    global DSP_CMS_GlossaryController(cms.GenerateContent cc){
        super(cc);
    }
    
    global DSP_CMS_GlossaryController(){}
    
    global virtual override String getHTML(){return '';}
    
    public String abbreviation {
        get{
            return String.isNotBlank(getProperty('abbreviation')) ?  getProperty('abbreviation') : '';
        }
        set;
    }
    
    public String fullname {
        get{
            return String.isNotBlank(getProperty('fullname')) ?  getProperty('fullname') : '';
        }
        set;
    }
    
    public String definition {
        get{
            return String.isNotBlank(getProperty('definition')) ?  getProperty('definition') : '';
        }
        set;
    }
    
    public String glossaryHTML() {
        String html='';
        
        html += '<tr>';
        html += '<td>'+abbreviation+'</td>';
        html += '<td>'+fullname+'</td>';
        html += '<td>'+definition+'</td>';
        html += '</tr>';
        
        return html;
    }
}