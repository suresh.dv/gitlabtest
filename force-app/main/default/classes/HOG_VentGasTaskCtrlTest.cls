/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VentGasTaskCtrl.cls
History:        jschn 04.27.2018 - Created.
**************************************************************************************************/
@isTest
private class HOG_VentGasTaskCtrlTest {
	
	@isTest static void cancelTest() {
		HOG_Vent_Gas_Alert_Task__c vgTask = [SELECT Id FROM HOG_Vent_Gas_Alert_Task__c].get(0);
		HOG_Vent_Gas_Alert__c vgAlert = [SELECT Id FROM HOG_Vent_Gas_Alert__c].get(0);
		ApexPages.StandardController stdVGTask = new ApexPages.StandardController(vgTask);
		
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_TASK, vgTask.Id);
        HOG_VentGasTaskCtrl controller = new HOG_VentGasTaskCtrl(stdVGTask);
        System.assertEquals(new PageReference('/' + vgTask.Id).getUrl(), controller.cancel().getUrl());

		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_TASK, '');
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_ALERT, vgAlert.Id);
		controller = new HOG_VentGasTaskCtrl(stdVGTask);
        System.assertEquals(new PageReference('/' + vgAlert.Id).getUrl(), controller.cancel().getUrl());

		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_RETURN_URL, '/testReturnUrl');
		controller = new HOG_VentGasTaskCtrl(stdVGTask);
        System.assertEquals(new PageReference('/testReturnUrl').getUrl(), controller.cancel().getUrl());

	}

	@isTest static void editExistingTask() {
		HOG_Vent_Gas_Alert_Task__c vgTask = [SELECT Id FROM HOG_Vent_Gas_Alert_Task__c].get(0);
		ApexPages.StandardController stdVGTask = new ApexPages.StandardController(vgTask);

		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_TASK, vgTask.Id);
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_SAVE_URL, '/saveUrlTest');

		Test.startTest();
        HOG_VentGasTaskCtrl controller = new HOG_VentGasTaskCtrl(stdVGTask);

        controller.ventGasTask.Comments__c += ' EDITED';
        controller.ventGasTask.Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;
        System.assertEquals(new PageReference('/saveUrlTest').getUrl(), controller.save().getUrl());
        System.assertEquals(HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS, [SELECT Status__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Id =: vgTask.Id].get(0).Status__c);
        System.assert(!ApexPages.hasMessages());
        
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_SAVE_URL, '');
        controller = new HOG_VentGasTaskCtrl(stdVGTask);
        controller.ventGasTask.Status__c = HOG_VentGas_Utilities.TASK_STATUS_COMPLETED;
        System.assertEquals(new PageReference('/' + vgTask.Id).getUrl(), controller.save().getUrl());
        System.assertEquals(HOG_VentGas_Utilities.TASK_STATUS_COMPLETED, [SELECT Status__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Id =: vgTask.Id].get(0).Status__c);
        System.assert(!ApexPages.hasMessages());

		Test.stopTest();
	}
	
	@isTest static void createNewTask() {
		HOG_Vent_Gas_Alert_Task__c vgTask = [SELECT Id FROM HOG_Vent_Gas_Alert_Task__c].get(0);
		HOG_Vent_Gas_Alert__c vgAlert = [SELECT Id FROM HOG_Vent_Gas_Alert__c WHERE Status__c =: HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED].get(0);
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'].get(0);
		Integer taskCount = [SELECT Count() FROM HOG_Vent_Gas_Alert_Task__c];

		ApexPages.StandardController stdVGTask = new ApexPages.StandardController(vgTask);

		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_ALERT, vgAlert.Id);

		Test.startTest();
        HOG_VentGasTaskCtrl controller = new HOG_VentGasTaskCtrl(stdVGTask);
        
        controller.ventGasTask.Assignee_Type__c = HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR;
        controller.handleAssigneeTypeChange();
        System.assertEquals(usr.Id, controller.ventGasTask.Assignee1__c);
        controller.ventGasTask.Assignee_Type__c = HOG_VentGas_Utilities.ASSIGNEE_TYPE_OPERATIONS_ENGINEER;
        controller.handleAssigneeTypeChange();
        System.assertNotEquals(usr.Id, controller.ventGasTask.Assignee1__c);

        controller.ventGasTask.Assignee1__c = usr.Id;
        controller.ventGasTask.Comments__c = 'NewTask';
        System.assertEquals(new PageReference('/' + vgAlert.Id).getUrl(), controller.save().getUrl());
        System.assert(!ApexPages.hasMessages());
        Integer newTaskCount = [SELECT Count() FROM HOG_Vent_Gas_Alert_Task__c];
        System.assertNotEquals(taskCount, newTaskCount);
        System.assertEquals(taskCount+1, newTaskCount);
        Test.stopTest();
	}

	@isTest static void testNonAllowedAccess() {
		HOG_Vent_Gas_Alert_Task__c vgTask = [SELECT Id FROM HOG_Vent_Gas_Alert_Task__c].get(0);

		ApexPages.StandardController stdVGTask = new ApexPages.StandardController(vgTask);
        HOG_VentGasTaskCtrl controller = new HOG_VentGasTaskCtrl(stdVGTask);

        System.assertEquals(HOG_VentGasTaskCtrl.ERROR_MSG_MISSING_PARAMS, ApexPages.getMessages().get(0).getDetail());
	}

	@isTest static void testErrors() {
		HOG_Vent_Gas_Alert_Task__c vgTask = [SELECT Id FROM HOG_Vent_Gas_Alert_Task__c].get(0);
		ApexPages.StandardController stdVGTask = new ApexPages.StandardController(vgTask);
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'].get(0);

		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_TASK, vgTask.Id);

		Test.startTest();
		System.assert(!ApexPages.hasMessages());
        HOG_VentGasTaskCtrl controller = new HOG_VentGasTaskCtrl(stdVGTask);
        controller.ventGasTask.Status__c = 'Really Really Wrong Status';
        System.assertEquals(null, controller.save());
        System.assert(ApexPages.hasMessages());
        System.assertEquals(1, ApexPages.getMessages().size());
        controller.ventGasTask.Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;

        controller.ventGasTask.Assignee1__c = null;
        System.assertEquals(null, controller.save());
        System.assert(ApexPages.hasMessages());
        System.assertEquals(2, ApexPages.getMessages().size());
        Boolean foundError = false;
        for (ApexPages.Message msg : ApexPages.getMessages())
        	if(msg.getDetail().contains('You have to set Assignee')) foundError = true;
    	System.assert(foundError);
    	controller.ventGasTask.Assignee1__c = usr.Id;

    	controller.ventGasTask.Assignee_Type__c = null;
        System.assertEquals(null, controller.save());
        System.assert(ApexPages.hasMessages());
        System.assertEquals(3, ApexPages.getMessages().size());
        foundError = false;
        for (ApexPages.Message msg : ApexPages.getMessages())
        	if(msg.getDetail().contains('Assignee Type')) foundError = true;
    	System.assert(foundError);
    	Test.stopTest(); 
	}

	@testSetup static void prepareData() {
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User usr = HOG_VentGas_TestData.createUser();
		Operator_On_Call__c operatorOnCall = HOG_VentGas_TestData.createOperatorOnCall(loc.Route__c, usr.Id);

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		loc.Id, 
																		usr.Id, 
																		true);

		HOG_Vent_Gas_Alert_Task__c t = HOG_VentGas_TestData.createTask(alert, usr.Id, true);

		alert = HOG_VentGas_TestData.createAlert(loc.Id, usr.Id, true);
	}
	
}