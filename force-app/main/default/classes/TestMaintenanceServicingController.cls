/*------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A test class for MaintenanceServicingController
History    : 07.23.15 rbo modified due to Route and Well Tracker modifications
                          refactored the code
--------------------------------------------------------------------------------------------------*/

@isTest
private class TestMaintenanceServicingController 
{
    private static HOG_Service_Request_Notification_Form__c serviceNotificationForm;
    private static HOG_Service_Code_MAT__c serviceCodeMAT;
    private static HOG_Work_Order_Type__c workOrderType;
    private static HOG_Notification_Type_Priority__c notificationTypePriority;
    private static HOG_Service_Category__c serviceCategory;
    private static HOG_Service_Code_Group__c serviceCodeGroup;
    private static HOG_Notification_Type__c notificationType;
    private static HOG_Service_Required__c serviceRequiredList;
	private static HOG_User_Status__c userStatus;
	private static HOG_Service_Priority__c servicePriorityList;
	private static Business_Unit__c businessUnit;
	private static Business_Department__c businessDepartment;
	private static Operating_District__c operatingDistrict;
	private static Field__c field;
	private static Route__c route;
	private static Location__c location;		
	private static Facility__c facility;		
	private static Equipment__c equipment;
	private static HOG_Part__c equipmentPart;
	private static HOG_Damage__c equipmentPartDamage;
	private static HOG_Cause__c equipmentPartDamageCause;
	private static Account account;
	private static Contact contact;
	private static HOG_Maintenance_Servicing_Form__c maintenanceRecord;
	private static HOG_Maintenance_Servicing_Vendor__c vendorRecord;
	private static Id recordTypeId;
    private static String workOrderNumber = 'TEST_WORK';
    private static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

	
    @isTest    
    private static void MaintenanceServicingController_Test()
    {            
        MaintenanceServicingUtilities.executeTriggerCode = false;   // do not to execute trigger      

		SetupTestData();
        
        Test.startTest();

        final MaintenanceServicingController controller = new MaintenanceServicingController();

        final PageReference pageRef = Page.MaintenanceServicing;
        Test.setCurrentPage(pageRef);
                
        final Attachment[] attachment = controller.getMaintenanceAttachments();
        System.assertNotEquals(null, attachment);

        // test attachment
        controller.document = new Attachment();
        controller.document.Name = 'Test Attachment';
        controller.document.Body = Blob.valueOf('Unit Test Attachment Body');
        controller.document.OwnerId = UserInfo.getUserId();
        controller.maintenanceServicingFormId = maintenanceRecord.Id;
        
        final PageReference pageRefUploadFile = controller.uploadFile();
        System.assertEquals(null, pageRefUploadFile);        
        
        final List<Attachment> attachments = [Select Id, Name, OwnerId From Attachment Where Parent.Id = :maintenanceRecord.Id];
        System.assertEquals(1, attachments.size());
        
        controller.selectedAttachmentId = attachments[0].Id;
        
        final PageReference pageRefRemoveFile = controller.removeFile();
        System.assertEquals(null, pageRefRemoveFile);
        //

        final String searchWorkOrder = controller.getSearchWorkOrder();         
        System.assertEquals(null, searchWorkOrder);

        controller.setSearchWorkOrder(workOrderNumber); 
        
        final List<SelectOption> partsOptions = controller.getPartsOptions();
        System.assertNotEquals(null, partsOptions);

        final List<SelectOption> damagesOptions = controller.getDamagesOptions();
        System.assertNotEquals(null, damagesOptions);

        final List<SelectOption> causesOptions = controller.getCausesOptions();
        System.assertNotEquals(null, causesOptions);

        final List<SelectOption> usersOptions = controller.getUsersOptions();
        System.assertNotEquals(null, usersOptions);

        final HOG_Maintenance_Servicing_Vendor__c[] maintenanceVendors = controller.getMaintenanceVendors();
        System.assertNotEquals(null, maintenanceVendors);

        controller.searchWorkOrder = workOrderNumber;
        
        final PageReference pageRefDoSearch = controller.doSearch();
        System.assertEquals(null, pageRefDoSearch);

        final PageReference pageRefCancel = controller.cancel();
        System.assertNotEquals(null, pageRefCancel);

        controller.userItem = contact.Id;
               
        controller.maintenanceVendors[0].Comments__c = 'Changed Comment';

        final PageReference pageRefSave = controller.save();
        System.assertNotEquals(null, pageRefSave);
          
        Test.stopTest();                
    }
    
	private static void SetupTestData()
	{        
		final String serviceCategoryCode = 'MNT';  // Maintenance service category code
	    final String notificationTypeCode = 'WP';
	    final String orderTypeCode = 'WP01';
	    final String matCode = 'TXN';
	    final String notificationNumber = 'TEST_NOTI';
		final String catalogueCode = 'CATCODE';
		final String partCode = 'PARTCODE';

        //-- Begin setup of data needed for Maintenance Servicing --//                
        //-- Setup Service Category
        serviceCategory = new HOG_Service_Category__c
            (
                Name = 'Test Category',
                Service_Category_Code__c = serviceCategoryCode                                
            );
        insert serviceCategory;

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
        notificationType = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategory.Id,
                Notification_Type__c = notificationTypeCode,
                Order_Type__c = orderTypeCode
            );
        insert notificationType;
        //--
  
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', matCode, 'Master'); 	    
        insert serviceCodeMAT;        
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--
                
        //-- Setup Notification Type - Priority Detail
        notificationTypePriority = NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriorityList.Id, false);
        insert notificationTypePriority;        
        //--

        //-- Setup Work Order Type
 	    workOrderType = WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true);
        insert workOrderType;
        //--

        //-- Setup objects for Maintenance Work Order                                 
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;
        
        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);		
        insert location;

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        
        
        equipment = new Equipment__c
        	(
        		Name = 'Equipment Test',
        		Location__c = location.Id,     		
		        Catalogue_Code__c = catalogueCode
			);                
        insert equipment;

		equipmentPart = HOGPartTestData.createHOGPart('Test Part', partCode, 'Test Part', catalogueCode, true);
		insert equipmentPart;		

		equipmentPartDamage = HOGDamageTestData.createHOGDamage('Test Damage', 'DMGECODE', 'Test Damage', partCode, true);
		insert equipmentPartDamage;		

		equipmentPartDamageCause = HOGCauseTestData.createHOGCause('Test Cause', 'DMGECODE', 'Test Cause', 'CDEGROUP', true);
		insert equipmentPartDamageCause;		
		        
		account = AccountTestData.createAccount('Test Account', null);			
        insert account;
        
        contact = new Contact
            (
                AccountId = account.Id,
                LastName = 'Lastname',
                User__c = UserInfo.getUserId()
            );
        insert contact;
        //--

        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        serviceNotificationForm = ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm (
            null,
            workOrderType.Id,
            businessUnit.Id,
            operatingDistrict.Id,
            field.Id,
            facility.Id,
            location.Id,
            equipment.Id,
            notificationTypePriority.Id,
            notificationTypePriority.Id,
            account.Id,
            notificationNumber,
            workOrderNumber,
            'Test Detail',
            Date.today(),
            Date.today().addDays(5)
        );
        insert serviceNotificationForm;
        ServiceRequestNotificationUtilities.executeTriggerCode = true;

        //-- Setup HOG_Maintenance_Servicing_Form__c        
        maintenanceRecord = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = workOrderNumber,
            Notification_Number__c = notificationNumber,
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS',
            Equipment__c = equipment.Id,
            HOG_Service_Request_Notification_Form__c = serviceNotificationForm.Id,
            HOG_Notification_Type__c = notificationType.Id
        );        
        insert maintenanceRecord;

        vendorRecord = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name',
            Maintenance_Servicing_Form__c = maintenanceRecord.Id,
            Activity_Status__c = 'Complete',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment'
        );
        insert vendorRecord;
        //-- End setup of data needed to test Maintenance Servicing --//
	}            
}