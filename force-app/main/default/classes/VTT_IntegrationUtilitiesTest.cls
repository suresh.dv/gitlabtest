@isTest
private class VTT_IntegrationUtilitiesTest {
	
	@isTest static void testSAP_UpdateNotification() {
		User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false;  
            VTT_TestData.SetupRelatedTestData(false);

            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);           

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;
            workOrder1.Notification_Number__c = '9999999';  
            update workOrder1;

            MaintenanceServicingUtilities.executeTriggerCode = true; 

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Operation_Number__c = '0010';
            update woActivity;

    		//Test
	        Test.startTest();
	            Test.setMock(WebServiceMock.class, new HOG_SAPUpdateNotificationMockImpl());
		        
	            VTT_IntegrationUtilities.SAP_UpdateNotification(woActivity.Id);
	          	
		    Test.stopTest();

		    //Validate Result
		  	woActivity = [Select Id, Part_Key__c From Work_Order_Activity__c Where Id =:woActivity.Id];
		  	System.assertEquals(woActivity.Part_Key__c, HOG_SAPUpdateNotificationMockImpl.ITEM_KEY);
        }
	}
	
}