@isTest(SeeAllData=true)
private class NCR_CAR_CreatePageControllerTest{

    static testMethod void testNCRCARCreatePageAsSysAdmin(){
        //Create sys admin
        User ncrUser = UserTestData.createTestsysAdminUser();
        insert ncrUser;
        
        System.AssertNotEquals(ncrUser.Id, Null);
        
        //Execute test as NCR QA lead
        System.runAs(ncrUser) {
            PageReference pageRef = Page.NCR_CreateCARPage;
            Test.setCurrentPageReference(pageRef); 
            
            Date currentDate = Date.Today();
            NCR_CAR__c car = NCR_CAR_TestData.createNCRCAR('short', 'detailed', currentDate, true);
            
            System.AssertNotEquals(car.Id, Null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(car);            
            NCR_CAR_CreatePageControllerX carController = new NCR_CAR_CreatePageControllerX(standardController);
            
            System.AssertNotEquals(carController.getRecordId(),null);
            System.AssertNotEquals(carController.getNCRCAR(),null);
            
            System.AssertNotEquals(carController.getBusinessUnits(), Null);
            System.AssertNotEquals(carController.getProjects(), Null); 
            
            System.AssertEquals(carController.businessUnitName, Null);
            System.AssertEquals(carController.ProjectName, Null);  
            
            System.AssertNotEquals(carController.save(), Null);         
        }
    }
     
}