public class UserTestData {

    public static User createTestCustomerPortalUser(Id contactId) {
        ID ProfileID = [ Select id from Profile where name = 'Customer Portal Manager Custom'].id;
                
        User user = new User();
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
        user.ContactId = contactId;
        user.ProfileId = ProfileID;
        user.UserName = 'test-user' + random + '@fakeemail.com';
        user.alias = 'tu' + String.ValueOf(contactId).right(6);
        user.CommunityNickName = 'tuser1' + random;
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1'; 
        user.LanguageLocaleKey='en_US';
        user.FirstName = 'Test';
        user.LastName = 'User';
        insert user;
        
        return user;
    }
    
    public static User createTestUser(String profileName)
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name=:profileName];
        
        UserRole role = [SELECT Id, name FROM UserRole Where PortalType = 'None' Limit 1];
        
        return createTestUser(profile, role);
    }
    
    public static User createTestUser() {
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        UserRole role = [SELECT Id, name FROM UserRole Where PortalType = 'None' Limit 1];
        
        return createTestUser(profile, role);
    }
    
    public static User createTestsysAdminUser() {
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        UserRole role = [SELECT Id FROM UserRole WHERE Name ='Information Services'  Limit 1];
        
        return createTestUser(profile, role);
    }    


    public static User createAsphalUser()
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name='Asphalt User'];
        
        UserRole role = [SELECT Id FROM UserRole WHERE Name ='Sales Representative'  Limit 1];
        
        return createTestUser(profile, role);
    }

    public static User createTestUser(Profile profile, UserRole role) {
        User user = new User();
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
        user.Alias = 'tu' + String.ValueOf(random).right(6);
        user.Email = 'test-user' + random + '@fakeemail.com';
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US'; 
        user.LocaleSidKey = 'en_US'; 
        user.ProfileId = profile.Id;
        user.UserRoleId = role.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.UserName = 'test-user' + random + '@fakeemail.com';

        return user;
    }
    

    public static List<User> createSunriseTestUser(Integer numberOfUsers) {     
        return createSunriseUser('Change Management - User', numberOfUsers);          
    }
    

    public static List<User> createSunriseUser(String permissionSetName, Integer numberOfUsers ) {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard Oilsands User'];
        

        UserRole role = [SELECT Id FROM UserRole WHERE Name = 'Sunrise Operations'];
        

        List<User> userList = new List<User>();
        

        for(Integer i = 0; i < numberOfUsers; i++) {        
            User user = createTestUser(profile, role);
            userList.add(user);
        }       

        insert userList;        

        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Label =: permissionSetName];              

        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();        

        for(User user : userList) {
            PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
            permissionSetAssignment.PermissionSetId = permissionSet.Id;
            permissionSetAssignment.AssigneeId = user.Id;            

            assignmentList.add(permissionSetAssignment);
        }
        
        insert assignmentList;
        return userList;
    }    

    public static List<User> createSunriseTestAdminUser(Integer numberOfUsers) {
        return createSunriseUser('Change Management - Admin', numberOfUsers);         
    }
}