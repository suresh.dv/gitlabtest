/*
* Author: Kishore Chandolu
* Name: HCVC_TankMapController
* Purpose: This Class loads the Tank Asset Records and Redirects the Page to main HCVC_MapPage.
* Created Date: 8th August 2014
*/

public class HCVC_TankMapController{


   public List<Asset__c> tankAssets{get;set;}
   public String baseUrl{get;set;}
   public Asset__c asset{get;set;}
   public String tbcolor{get{if(null==tbcolor)
                         {HCVC_Customsettings__c tc = HCVC_Customsettings__c.getInstance();
                          tbcolor = tc.HCVC_tabId__c;}return tbcolor;}set;}



   /**
    * This Constructor loads all Tank Assetrecords Using Parent RefineryID.
    */

     public HCVC_TankMapController(){

      String assetId=Apexpages.currentPage().getParameters().get('id');

      System.debug('assetId in Class -  HCVC_TankMapController  Method - HCVC_TankMapController  '+assetId);

      asset=[SELECT Id,Parent_Refinery_Asset__r.Id FROM Asset__c WHERE Id=:assetId LIMIT 1];

      tankAssets=[SELECT Id,Name,Details__c,Volume_BB__c,
                    CEPA_Tank__c,Material__c,SHAPE_Area__c,SHAPE_Length__c,
                    Substance__c,Tank_Number__c,order__c,Temperatur__c,UN_CAS_Number__c,
                    BBLS_FOOT__c, BBLS_INCH__c, Dimensions__c, HEEL_BBLS__c,RecordTypeId,
                    HEEL_LTRS__c, LTRS_FOOT__c, LTRS_INCH__c, MAXIMUM_FILL_FEET__c,
                    MAX_VOLUME_BBL__c, MAX_VOLUME_M3__c, Notes__c, Product__c, Sales_Tanks__c
                  FROM Asset__c
                  WHERE Parent_Refinery_Asset__r.Id=:asset.Parent_Refinery_Asset__r.Id AND RecordTypeId=:HCVC_Utility.tankTypeId ORDER BY order__c];
                  
                  baseUrl=System.URL.getSalesforceBaseUrl().toExternalForm();

                   try{
                        if(String.isNotEmpty(HCVC_Utility.communitySiteUrl)){ 
                         baseUrl=baseUrl+HCVC_Utility.communitySiteUrl;
                       }
                
                   }catch(Exception e){
                        System.debug('Exception is '+e);
                  }
                                

      }


   /**
    * This method will returns to the main HCVC_MapPage.
    * Incoming Arguments - nothing
    * Returns - PageReference
    */

     public PageReference goBackToMap(){


        PageReference reference=new PageReference(baseUrl+'/apex/HCVC_MapPage?id='+asset.Parent_Refinery_Asset__r.Id+'&sfdc.tabName='+tbcolor);

        reference.setRedirect(true);
        return reference;

       }



}