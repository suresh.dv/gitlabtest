// ********************************************************************
// Test cases for Case Comment Trigger Handler class
@isTest
private class TestCaseCommentTriggerHandler {

  // ------------------------------------------------------------------
  // Prevent creation of the case comment when
  // case's record type is Tech_Support_Incident and Status is 'closed'
  // unless current user is sys admin
  static testMethod void preventCaseCommentCreation() {
  	Id profileId = TestUtility.getFakeId(Profile.SobjectType, 1);
    
    Recordtype techSupport = 
        [SELECT Id FROM RecordType WHERE Name = 'Tech Support Incident'];
    
    Id caseId = TestUtility.getFakeId(Case.SobjectType, 1);
    
    Case testCase = 
        new Case(Status = 'Closed', 
                 RecordTypeId = techSupport.Id, 
                 Id = caseId);
                 
    CaseComment testComment = 
        new CaseComment(CommentBody = 'test', Parent = testCase);
    
    Boolean errorFound = false;
    
    
    try {
    	CaseCommentTriggerHandler.preventCreation(
    	    new List<CaseComment>{testComment}, profileId);
    }
    catch (Exception ex) {
    	System.assert(
    	    ex.getMessage().contains(
    	        'Can\'t insert comment for closed case')); 
    	errorFound = true;
    }
    
    System.assert(errorFound);
    
  }
  
  
  // ------------------------------------------------------------------
  // Prevent the user from updating published flag of the case comment
  // unless current user is sys admin
  static testMethod void preventPublishedFlag() {
  	Id profileId = TestUtility.getFakeId(Profile.SobjectType, 1);
    
    Id commentId = TestUtility.getFakeId(CaseComment.SobjectType, 1);
                 
    CaseComment oldComment = 
        new CaseComment(
            CommentBody = 'test', isPublished = false);
        
    CaseComment newComment = 
        new CaseComment(
            Id = commentId, CommentBody = 'test', isPublished = true);
    
    Boolean errorFound = false;
    
    
    try {
    	CaseCommentTriggerHandler.preventPublished(
    	    new List<CaseComment>{newComment}, 
    	    new Map<Id, CaseComment>{commentId => oldComment},
    	    profileId);
    }
    catch (Exception ex) {
    	System.assert(
    	    ex.getMessage().contains(
    	        'Can\'t change public flag of a comment')); 
    	errorFound = true;
    }
    
    System.assert(errorFound);
    
  }
  
  
  static testMethod void dummyTest() {
  	CaseComment comment = new CaseComment();
  	try {
  		insert comment;
  	}
  	catch(Exception ex) {
  		
  	}
  }

}