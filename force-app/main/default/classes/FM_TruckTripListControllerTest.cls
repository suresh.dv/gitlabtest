@isTest
private class FM_TruckTripListControllerTest {

	@isTest static void trucktripsVendorExportSelection() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.runAs(runningUser) {
			FM_TruckTripListController controller = new FM_TruckTripListController();
			System.assert(controller.isVendor);
			System.assert(!controller.isSelection);
			System.assert(controller.filter != null);
			System.assertEquals(1, controller.filter.carrierOptions.size());
			System.assert(controller.trucktrips != null);
			System.assert(controller.sortWrapper != null);
			System.assert(String.isBlank(controller.linkForExport));
			System.assert(!controller.getAbleToExport());
			System.assert(!controller.getTableHasRecords());
			controller.filter.routes.add(controller.filter.routeOptions.get(0).getValue());
			controller.filter.carrier = controller.filter.carrierOptions.get(0).getValue();

			Test.startTest();
				controller.search();
				System.assert(String.isNotBlank(controller.linkForExport));
				System.assertEquals(8, controller.truckTrips.size());
				controller.truckTrips.get(0).marked = true;
				System.assert(controller.isSelection);
				System.assert(controller.getAbleToExport());
				System.assert(controller.getTableHasRecords());
				controller.exportSelection();
			Test.stopTest();
		}
	}

	@isTest static void trucktripsVendorExportAll() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.runAs(runningUser) {
			FM_TruckTripListController controller = new FM_TruckTripListController();
			System.assert(controller.isVendor);
			System.assert(!controller.isSelection);
			System.assert(controller.filter != null);
			System.assertEquals(1, controller.filter.carrierOptions.size());
			System.assert(controller.trucktrips != null);
			System.assert(controller.sortWrapper != null);
			System.assert(String.isBlank(controller.linkForExport));
			System.assert(!controller.getAbleToExport());
			System.assert(!controller.getTableHasRecords());
			controller.filter.routes.add(controller.filter.routeOptions.get(0).getValue());
			controller.filter.carrier = controller.filter.carrierOptions.get(0).getValue();

			Test.startTest();
				controller.search();
				System.assert(String.isNotBlank(controller.linkForExport));
				System.assertEquals(8, controller.truckTrips.size());
				controller.export();
			Test.stopTest();
		}
	}

	@isTest static void truckTripErrors() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		FM_TruckTripListController controller;
		System.runAs(runningUser) {
			controller = new FM_TruckTripListController();
			controller.export();
			System.assert(ApexPages.hasMessages());
			controller = new FM_TruckTripListController();
			controller.exportSelection();
			System.assert(ApexPages.hasMessages());
			controller = new FM_TruckTripListController();
			controller.filter.carrier = null;
			controller.search();
			System.assert(ApexPages.hasMessages());
			System.assertEquals(3, ApexPages.getMessages().size());
		}
	}

	@isTest static void truckTripSorting() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.runAs(runningUser) {
			FM_TruckTripListController controller = new FM_TruckTripListController();
			//don't know what to assert...
			controller.filter.routes.add(controller.filter.routeOptions.get(0).getValue());
			controller.filter.carrier = controller.filter.carrierOptions.get(0).getValue();
			controller.search();
			//NOTE as they are on same route, created on same date, with same status. I don't do assert
			//NOTE it could be on well name and status (after exportSelection on first but don't know how to check others.)
			controller.sortWrapper.SORT_BY = controller.sortWrapper.SORT_BY_ROUTE;
			controller.SortToggle();
			controller.SortToggle();
			controller.sortWrapper.SORT_BY = controller.sortWrapper.SORT_BY_CREATED_DATE;
			controller.SortToggle();
			controller.SortToggle();
			controller.sortWrapper.SORT_BY = controller.sortWrapper.SORT_BY_WELL_NAME;
			controller.SortToggle();
			controller.SortToggle();
			controller.sortWrapper.SORT_BY = controller.sortWrapper.SORT_BY_STATUS;
			controller.SortToggle();
			controller.SortToggle();
		}
	}

	@isTest static void truckTripNonVendor() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.runAs(runningUser) {
			FM_TruckTripListController controller = new FM_TruckTripListController();
			System.assert(!controller.isVendor);
			System.assertEquals(2, controller.filter.carrierOptions.size());
		}
	}

	// @isTest static void truckTripImport() {
	// 	User runningUser = createUser();
	// 	runningUser = assignPermissionset(runningUser, 'HOG_FM_Dispatcher_Vendor');
	// 	runningUser = assignProfile(runningUser, 'Standard HOG - General User');
	// 	System.runAs(runningUser) {
	// 		FM_TruckTripListController controller = new FM_TruckTripListController();
	// 		System.assertEquals(null, controller.file);
	// 		System.assertEquals(null, controller.import());
	// 		controller.filter.routes.add(controller.filter.routeOptions.get(0).getValue());
	// 		controller.filter.carrier = controller.filter.carrierOptions.get(0).getValue();
	// 		controller.search();
	// 		controller.truckTrips.get(0).marked = true;
	// 		controller.truckTrips.get(1).marked = true;
	// 		System.assert(controller.isSelection);
	// 		System.assert(controller.getAbleToExport());
	// 		System.assert(controller.getTableHasRecords());
	// 		controller.exportSelection();
	// 		controller.file = generateFileForImport(new FM_Truck_Trip__c[]{controller.truckTrips.get(0).truckTrip,
	// 																		controller.truckTrips.get(1).truckTrip,
	// 																		controller.truckTrips.get(2).truckTrip});
	// 		controller.import();
	// 		List<FM_Load_Confirmation__c> lcs = [SELECT Id, Truck_Trip__c, Truck_Trip__r.Truck_Trip_Status__c, Truck_Trip__r.Load_Request__r.Status__c FROM FM_Load_Confirmation__c];
	// 		System.assertEquals(2, lcs.size());
	// 		for (FM_Load_Confirmation__c lc : lcs) {
	// 			System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_COMPLETED, lc.Truck_Trip__r.Truck_Trip_Status__c);
	// 			System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED, lc.Truck_Trip__r.Load_Request__r.Status__c);
	// 		}
	// 		System.assert(ApexPages.hasMessages());
	// 		System.assertEquals(4, ApexPages.getMessages().size());
	// 	}
	// }
	//
	// private static Blob generateFileForImport(List<FM_Truck_Trip__c> truckTrips) {
	// 	String csvFile = 'Truck Trip, Load Request, Route, Date, Well Name, Tank Size, '
	// 					+'Tank Low Level, Act Tank Level, Act Flow Rate, Shift, Product, '
	// 					+'Standing Comments, Unit Configuration, Sour, Surface Location,'
	// 					+'Ticket number,Scale ticket number,truck unit number,Production date,'
	// 					+'From Location,Destination,Fluid volume,Ticket Product,'
	// 					+'Truck configuration,Stuck Time in,stuckTime out\n';
	// 	Integer ticketNumber = 123456;
	// 	for (FM_Truck_Trip__c tt : truckTrips) {
	// 		csvFile += tt.Name + ','+tt.Load_Request__r.Name+',' + tt.Route__c + ','
	// 					+'2017-08-02,Doesnt matter,1,1,1,1,Day,'
	// 					+'O,Doesnt matter,Doesnt matter,Doesnt matter,Doesnt matter,' + ticketNumber + ',' + ticketNumber + ','
	// 					+ 'Bond1,2017-jul-20,London,Tst Facilty,28.5,O,T6X,,'
	// 					+'\n';
	// 		ticketNumber += 1;
	// 	}
	// 	csvFile += ' ,s,1,'
	// 				+'2017-08-02,Doesnt matter,1,1,1,1,Day,'
	// 				+'O,Doesnt matter,Doesnt matter,Doesnt matter,Doesnt matter,' + ticketNumber + ',' + ticketNumber + ','
	// 				+ 'Bond1,2017-jul-20,Brezno,Tst Facilty,28.5,O,T6X,,'
	// 				+'\n';
	// 	csvFile +=  truckTrips.get(0).Name + ', ,1,'
	// 				+'2017-08-02,Doesnt matter,1,1,1,1,Day,'
	// 				+'O,Doesnt matter,Doesnt matter,Doesnt matter,Doesnt matter,' + ticketNumber + ',' + ticketNumber + ','
	// 				+ 'Bond1,2017-jul-20,Brezno,London,28.5,O,T6X,,'
	// 				+'\n';
	// 	csvFile += 'asd,asd,asd';
	// 	return Blob.valueOf(csvFile);
	// }

	@testSetup
	static void createTestData() {
		Account acc = new Account();
		acc.Name = 'MI6 Account';
		insert acc;

		Carrier__c carrier = new Carrier__c();
		carrier.Carrier__c = acc.Id;
		carrier.Carrier_Name_For_Fluid__c = 'Husky Bond';
		insert carrier;

		carrier = new Carrier__c();
		carrier.Carrier__c = acc.Id;
		carrier.Carrier_Name_For_Fluid__c = 'James Bond';
		insert carrier;

		Carrier_Unit__c carrierUnit = new Carrier_Unit__c();
		carrierUnit.Carrier__c = carrier.Id;
		carrierUnit.Unit_Email__c = 'bond@jamesbond.com';
		carrierUnit.Name = 'Bond1';
		carrierUnit.Enabled__c = true;
		insert carrierUnit;

		Business_Department__c business = new Business_Department__c();
		business.Name = 'Husky Business';
		insert business;

		Operating_District__c district = new Operating_District__c();
		district.Name = 'District 9';
		district.Business_Department__c = business.Id;
		insert district;

		Field__c field = new Field__c();
		field.Name = 'AMU Field';
		field.Operating_District__c = district.Id;
		insert field;

		Route__c route = new Route__c();
		route.Fluid_Management__c = true;
		route.Name = '007';
		route.Route_Number__c = '007';
		insert route;

		Location__c loc = new Location__c();
		loc.Name = 'London';
		loc.Surface_Location__c = 'London';
		loc.Fluid_Location_Ind__c = true;
		loc.Location_Name_for_Fluid__c = 'London HQ';
		loc.Operating_Field_AMU__c = field.Id;
		loc.Route__c = route.Id;
		loc.Functional_Location_Category__c	= 4;
		insert loc;

		Equipment__c eq = EquipmentTestData.createEquipment(loc);
		eq.Equipment_Category__c = 'D';
		eq.Object_Type__c = 'VESS_ATMOS';
		update eq;

		Equipment_Tank__c eqt = new Equipment_Tank__c();
		eqt.Equipment__c = eq.id;
		eqt.Low_Level__c = 10;
		eqt.Tank_Size_m3__c = 30;
		eqt.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt;

		//create run sheet for runSheet test
		FM_Run_Sheet__c rs = new FM_Run_Sheet__c();
		rs.Date__c = Date.today();
		rs.Well__c = loc.Id;
		rs.Act_Tank_Level__c = 40;
		rs.Standing_Comments__c = 'Test comment';
		rs.Axle__c = '6';
		rs.Load_Weight__c = 'Primary';
		rs.Act_Flow_Rate__c = 5;
		rs.Flowline_Volume__c = 10;
		rs.Tomorrow_Oil__c = 1;
		rs.Tomorrow_Water__c = 1;
		rs.Tonight_Oil__c = 1;
		rs.Tonight_Water__c = 1;
		insert rs;

		List<FM_Load_Request__c> rlList = new List<FM_Load_Request__c>();

		FM_Load_Request__c lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		Location__c loc2 = new Location__c();
		loc2.Name = 'Brezno';
		loc2.Fluid_Location_Ind__c = true;
		loc2.Location_Name_for_Fluid__c = 'Brezno HQ';
		loc2.Operating_Field_AMU__c = field.Id;
		loc2.Route__c = route.Id;
		loc2.Functional_Location_Category__c = 4;
		insert loc2;

		//Create load request for nonRunSheet test
		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		Facility__c fac = new Facility__c();
		fac.Name = 'Test fac';
		fac.Facility_Name_for_Fluid__c = 'Tst Facilty';
		fac.Fluid_Facility_Ind__c = true;
		fac.Fluid_Facility_Ind_Origin__c = true;
		fac.Operating_Field_AMU__c = field.Id;
		fac.Functional_Location_Category__c	= 3;
		fac.Plant_Section__c = route.id;

		insert fac;

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		rlList.add(lr);

		insert rlList;
	}

	private static User createUser() {
		User user = new User();
		Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

		Double random = Math.Random();

		user.Email = 'TestUser' +  random + '@hog.com';
		user.Alias = 'TestUser' ;
		user.EmailEncodingKey = 'UTF-8';
		user.LastName = 'User';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_US';
		user.ProfileId = p.Id;
		user.TimeZoneSidKey = 'America/Los_Angeles';
		user.UserName = 'TestUser' + random + '@hog.com.unittest';

		insert user;
		return user;
	}

	private static User assignPermissionSet(User user, String userPermissionSetName){
		 PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];
		 PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();

		 permissionSetAssignment.PermissionSetId = permissionSet.Id;
		 permissionSetAssignment.AssigneeId = user.Id;

		 insert permissionSetAssignment;

		 return user;
	 }

	private static User assignProfile (User user, String profileName){
		Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
		user.ProfileId = profile.Id;
		update user;
		return user;
	}

}