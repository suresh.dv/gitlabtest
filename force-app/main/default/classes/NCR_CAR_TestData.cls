@isTest
public class NCR_CAR_TestData {

    /*Create a NCR State object for unit testing*/
    public static NCR_State__c createNCRState(String name, String viewScreenDescription, String screenSections, String sourceSystemId, 
        String ncrObject, String recordType, Boolean isSave){
        NCR_State__c state = new NCR_State__c();
        
        state.Name = name;
        state.ViewScreen_Description__c = viewScreenDescription;
        state.Screen_Sections__c = screenSections;
        state.source_system_id__c = sourceSystemId;
        state.Record_Type__c = recordType;
        state.NCR_Object__c = ncrObject;
        
        if(isSave){
            insert(state);
        }
        
        return state;
    }
    
    /*Create NCR Action object for unit testing*/
    public static NCR_Action__c createNCRAction(String name, String sourceSystemId, Boolean isSave){
        NCR_Action__c action = new NCR_Action__c();
        
        action.Name = name;
        action.source_system_id__c = sourceSystemId;        
        
        if(isSave){
            insert(action);
        }        
        
        return action;
    }
    
    /*Create junction object between State & Action*/
    public static NCR_StateAction__c createNCRStateAction(NCR_State__c state, NCR_Action__c action, String confirmationMessage, String fieldSet, 
        NCR_State__c newState, String dateFieldName, String newOwner, Boolean isSave){
        NCR_StateAction__c stateAction = new NCR_StateAction__c();
        stateAction.NCR_State__c = state.Id;
        stateAction.NCR_Action__c = action.id;
        stateAction.Confirmation_Message__c = confirmationMessage;
        stateAction.Edit_Screen_Required__c = true;
        stateAction.Edit_Section_Description__c = 'Test description';
        stateAction.Edit_Section_Title__c = 'Test title';
        stateAction.FieldSet__c = fieldSet;
        stateAction.NCR_New_State__c = newState.id;
        stateAction.Order__c = 1;
        stateAction.Save_Button_Label__c = 'Save button';
        stateAction.Date_FieldName__c = dateFieldName;
        stateAction.Change_Owner__c = newOwner; 
        stateAction.Redirect__c = 'List Screen';
        
        if(isSave){
            insert(stateAction);
        }             
        
        return stateAction;
    }
    
    /*Create Corretive Action record*/
    public static NCR_CAR__c createNCRCAR(String shortDescription, String detailedDescription, Date responseDate, Boolean isSave) {
        NCR_CAR__c car = new NCR_CAR__c();
        
        car.Short_Description__c = shortDescription;
        car.Detailed_Description__c = detailedDescription;
        car.Response_Required__c = responseDate;
        
        if(isSave){
            insert(car);
        }          
        
        return car;
    }
    
    
    public static NCR_Case__c createProjectNCRCase(String businessUnit, String projectName, Id vendor, String discipline, String problemType,
        String supplierName, String severity, String shortDescription, String detailedDescription, String causeNonconformance, String immediateActions,
        String proposedDisposition, String proposedDispositionPlan, Id qALead, Id originator, Id assignee, String ncrSate, Boolean isSave){
        
        Business_Unit__c b = createBusinessUnit(businessUnit);
        Milestone1_Project__c m = createProject(projectName,b);
        
        NCR_Case__c ncrCase = new NCR_Case__c();
        ncrCase.NCR_Type__c = 'Project';
        //ncrCase.Business_Unit__c = businessUnit;
        //ncrCase.Project_Name__c = projectName;
        ncrCase.Business_Unit_Lookup__c = b.id;
        ncrCase.Project__c = m.id;
        ncrCase.Vendor__c = vendor;
        ncrCase.Discipline__c = discipline;
        ncrCase.Problem_Type__c = problemType;
        //ncrCase.Supplier_Name__c = supplierName;
        ncrCase.Severity__c = severity;
        ncrCase.Short_Description_of_Non_Conformance__c = shortDescription;
        ncrCase.Detailed_Description_of_Nonconformance__c = detailedDescription;
        ncrCase.Cause_of_nonconformance__c = causeNonconformance;    
        ncrCase.Immediate_Actions_Taken__c = immediateActions;
        ncrCase.Proposed_Correction__c = proposedDisposition;
        ncrCase.Proposed_Correction_Plan_Detail__c = proposedDispositionPlan;
        ncrCase.QA_Lead__c = qALead;
        ncrCase.Originator__c = originator;
        ncrCase.Assignee__c = assignee;
        ncrCase.NCR_Case_State__c = ncrSate;
        
        //assign recordtype SMakarov
        List<RecordType>  recTypes = [select id, name, sobjectType,developername from recordtype where sobjecttype = 'NCR_Case__c' and Name = :ncrCase.NCR_Type__c LIMIT 1];
        if(recTypes .size()>0)
        {
            ncrCase.RecordTypeID = recTypes[0].id; 
        }        
        
        if(isSave){
            insert(ncrCase);
        }          
        
        return ncrCase;
    
    }
    
    public static NCR_CAR__c createCorrectiveAction(Date assignedDate, Id assignee, String state, Id qaLead, String shortDescription, Boolean isSave){
        NCR_CAR__c car = new NCR_CAR__c();
        car.Assigned_Date__c = assignedDate;
        car.Assignee__c = assignee;
        car.State__c = state;
        car.QA_Lead__c = qaLead;
        car.Short_Description__c = shortDescription;
        
        if(isSave){
            insert(car);
        }          
        
        return car;        
    }
    
    public static Account createVendorAccount(String accountName, Id parentAccountId) {    
        Account vendorAccount = new Account(Name = accountName);
        vendorAccount.ParentId = (parentAccountId != Null) ? parentAccountId : Null;         
        
        insert vendorAccount;
        
        return vendorAccount;
    }  
    
    public static Business_Unit__c createBusinessUnit(String buName){
        Business_Unit__c b = new Business_Unit__c();
        RecordType recordType = [Select Id From RecordType  Where SobjectType='Business_Unit__c' and Name = 'SAP Profit Center Level 4'];
        b.Name = buName;
        b.RecordTypeId = recordType.id;
        
        insert b;
        
        return b;        
    } 
    
    public static Milestone1_Project__c createProject(String pName,Business_Unit__c bu) {
        Milestone1_Project__c p = new Milestone1_Project__c();
        RecordType recordType = [Select Id From RecordType  Where SobjectType='Milestone1_Project__c' and Name = 'SAP Project'];
        p.name = pName;
        p.RecordTypeId = recordType.id;
        p.Business_Unit__c = bu.id;
        
        insert p;
        
        return p;
    } 
    
}