global class PCR_AddFunctionalAreas{
    
    @InvocableMethod
    public static void addFunctionalAreas(List<String> options) {
        Map<String, Id> functionMap = new Map<String, Id>();
        List<CR_Affected_Impacted_Function__c> crf = new List<CR_Affected_Impacted_Function__c>();
        
        for(CR_Impacted_Functions__c f : [select id,name from CR_Impacted_Functions__c]){
            functionMap.put(f.Name, f.id);
        }
        
        String selectedOptions = options.get(0);
        String[] optionArray = selectedOptions.split(';');
        
        for(String s : optionArray) {
            CR_Affected_Impacted_Function__c f = new CR_Affected_Impacted_Function__c();
            f.Name = s;
            f.Change_Request__c = ApexPages.currentPage().getParameters().get('id');
            f.CR_Impacted_Function__c = functionMap.get(s.trim());
            f.ChangeRequestAndImpactedFuntionKey__c = ApexPages.currentPage().getParameters().get('id') + functionMap.get(s.trim());
            
            crf.add(f);

        }

        try{           
            upsert crf;       
        } catch(Exception exp){
            
        }            
    }
}