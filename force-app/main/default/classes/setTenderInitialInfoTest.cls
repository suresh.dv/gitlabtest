@isTest (SeeAllData = True)
private class setTenderInitialInfoTest{
  static testMethod void myUnitTest() {
         
        Date currentDate = Date.Today();
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
	        ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
	        
	        //Query for tender again
	        tender = [select id, Price_Valid_To__c, Acceptance_Deadline__c, Price_Valid_From__c from ATS_Parent_Opportunity__c where id=:tender.id];
	        
	        //test the trigger to confirm data was inputted into date fields
            System.AssertNotEquals(null, tender.Price_Valid_To__c);
            System.AssertNotEquals(null, tender.Acceptance_Deadline__c);
            System.AssertNotEquals(null, tender.Price_Valid_From__c);
        }
    }
}