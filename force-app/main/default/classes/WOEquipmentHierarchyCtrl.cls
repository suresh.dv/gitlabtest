/*------------------------------------------------------------------------------------------------------------
Author     : Marek Kosar
Company    : IBM
Description: Controller for WOEquipmentHierarchy AND WOActivityEquipmentHierarchypage
				for displaying Equipment Hierarchy for WO/WOA
Test Class : WOEquipmentHierarchyCtrlTest
History    :
            06.05.16    Class Created     
            09.05.16	Class Finished and ready for testing
            01.03.17    Miro Zelina - included Well Events
            04.07.17    Miro Zelina - included System & Subsystem
            04.09.17    Marcel Brimus - added FEL and Yard
-------------------------------------------------------------------------------------------------------------*/

public with sharing class WOEquipmentHierarchyCtrl {

	Id recordId;
	SObjectType sObjectType;
	HOG_Maintenance_Servicing_Form__c workOrder;
	Work_Order_Activity__c workOrderActivity;

	private List<Equipment__c> equipmentList;
    private List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentNodes;
    private List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> sortedNodeList;

    private static final String locationStr = 'Location';
    private static final String facilityStr = 'Facility';
    private static final String eventStr = 'WellEvent';
    private static final String SysStr = 'System';
    private static final String SubSysStr = 'SubSystem';
    private static final String felStr = 'Functional Equipment Level';
    private static final String yardStr = 'Yard';

	public WOEquipmentHierarchyCtrl(ApexPages.StandardController stdController) {
		
		if(!Test.isRunningTest())
			stdController.addFields(new List<String> {'Facility__c','Location__c','Well_Event__c','System__c', 'Sub_System__c', 'Yard__c', 'Functional_Equipment_Level__c'});

		//condition to decide what object type we are using - because of same extension, for 2 VF pages
		sObjectType = stdController.getRecord().getSObjectType();
		if(sObjectType == HOG_Maintenance_Servicing_Form__c.sObjectType){
        	workOrder = (HOG_Maintenance_Servicing_Form__c)stdController.getRecord();
        	workOrderActivity = null;
    	} else if (sObjectType == Work_Order_Activity__c.sObjectType){
    		workOrder = null;
    		workOrderActivity = (Work_Order_Activity__c)stdController.getRecord();
    	}

    	equipmentList = new List<Equipment__c>();

    	system.debug('sObjectType: ' +sObjectType);
        system.debug('workOrder: ' +workOrder);
        system.debug('workOrderActivity: ' +workOrderActivity);
	}

	public String getType(){

		/*  conditions:
		        - lowest level has priority
				- if we don't have any, we are not showing this page at all
			conditions duplicated because of 2 objects used
		*/
		if(sObjectType == HOG_Maintenance_Servicing_Form__c.sObjectType){
		    
		    if (workOrder.Well_Event__c != null){
		        return eventStr;
            }

            else if (workOrder.Functional_Equipment_Level__c != null){
                return felStr;
            }

            else if (workOrder.Yard__c != null){
                return yardStr;
            }
		    
		    else if (workOrder.Sub_System__c != null){
		        return SubSysStr;
            }
		    
		    else if (workOrder.System__c != null){
		        return SysStr;
            }

			else if (workOrder.Location__c != null){
				return locationStr;
            }
			
			else if (workOrder.Facility__c != null){
				return facilityStr;
            }
			
			else {
				return '';
            }

		} else if (sObjectType == Work_Order_Activity__c.sObjectType){
		    
		    if (workOrderActivity.Well_Event__c != null){
		        return eventStr;
            }

            else if (workOrderActivity.Functional_Equipment_Level__c != null){
                return felStr;
            }

            else if (workOrderActivity.Yard__c != null){
                return yardStr;
            }
	
		    else if (workOrderActivity.Sub_System__c != null){
		        return SubSysStr;
            }
		    
		    else if (workOrderActivity.System__c != null){
		        return SysStr;
            }

			else if (workOrderActivity.Location__c != null){
				return locationStr;
            }
			
			else if (workOrderActivity.Facility__c != null){
				return facilityStr;
            }
			
			else {
				return '';
            }
		}

		return null;
	}
	


	public List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> getEquipmentHierarchy() {
        if(sortedNodeList == null || sortedNodeList.isEmpty()) {

        	//condition: different query for location, facility and well event
        	String typeStr = getType();
        	String locationId;
        	String facilityId;
        	String eventId;
        	String systemId;
        	String subSystemId;
            String felId;
            String yardId;

        	system.debug('typeStr: '+typeStr);

        	if(sObjectType == HOG_Maintenance_Servicing_Form__c.sObjectType){
        		locationId = workOrder.Location__c;
        		facilityId = workOrder.Facility__c;
        		eventId = workOrder.Well_Event__c;
        		systemId = workOrder.System__c;
        		subSystemId = workOrder.Sub_System__c;
                felId = workOrder.Functional_Equipment_Level__c;
                yardId = workOrder.Yard__c;
        		
        	} else if (sObjectType == Work_Order_Activity__c.sObjectType){
         		locationId = workOrderActivity.Location__c;
        		facilityId = workOrderActivity.Facility__c;
        		eventId = workOrderActivity.Well_Event__c;
        		systemId = workOrderActivity.System__c;
        		subSystemId = workOrderActivity.Sub_System__c;
                felId = workOrderActivity.Functional_Equipment_Level__c;
                yardId = workOrderActivity.Yard__c;
        	}
        	
        	
        	system.debug('***getCpuTime()***'+Limits.getCpuTime());
        	if (typeStr == eventStr){
        	    equipmentList = [Select Id, Name, Superior_Equipment__c,
	                                    Description_of_Equipment__c, Manufacturer__c,
	                                    Model_Number__c, Manufacturer_Serial_No__c,
	                                    Tag_Number__c
	                             From Equipment__c
	                             Where Well_Event__c =: eventId];
            } else if (typeStr == felStr){
                
                equipmentList = [Select Id, Name, Superior_Equipment__c,
                                        Description_of_Equipment__c, Manufacturer__c,
                                        Model_Number__c, Manufacturer_Serial_No__c,
                                        Tag_Number__c
                                 From Equipment__c
                                 Where Functional_Equipment_Level__c =: felId];
                                 
            } else if (typeStr == yardStr){
                
                equipmentList = [Select Id, Name, Superior_Equipment__c,
                                        Description_of_Equipment__c, Manufacturer__c,
                                        Model_Number__c, Manufacturer_Serial_No__c,
                                        Tag_Number__c
                                 From Equipment__c
                                 Where Yard__c =: yardId LIMIT 1000];
                                 
            } else if (typeStr == subSysStr){
                
        	    equipmentList = [Select Id, Name, Superior_Equipment__c,
	                                    Description_of_Equipment__c, Manufacturer__c,
	                                    Model_Number__c, Manufacturer_Serial_No__c,
	                                    Tag_Number__c
	                             From Equipment__c
	                             Where Sub_System__c =: subSystemId];
	                             
        	} else if (typeStr == sysStr){
        	    equipmentList = [Select Id, Name, Superior_Equipment__c,
	                                    Description_of_Equipment__c, Manufacturer__c,
	                                    Model_Number__c, Manufacturer_Serial_No__c,
	                                    Tag_Number__c
	                             From Equipment__c
	                             Where System__c =: systemId];
            } else if(typeStr == locationStr){
	            equipmentList = [Select Id, Name, Superior_Equipment__c,
	                                    Description_of_Equipment__c, Manufacturer__c,
	                                    Model_Number__c, Manufacturer_Serial_No__c,
	                                    Tag_Number__c
	                             From Equipment__c
	                             Where Location__c =: locationId];
	                             
        	} else if (typeStr == facilityStr) {
	            equipmentList = [Select Id, Name, Superior_Equipment__c,
	                                    Description_of_Equipment__c, Manufacturer__c,
	                                    Model_Number__c, Manufacturer_Serial_No__c,
	                                    Tag_Number__c
	                             From Equipment__c
	                             Where Facility__c =: facilityId];
        	}  
        	
            system.debug('***getCpuTime()***'+Limits.getCpuTime());
            system.debug('***getCpuTime()***'+equipmentList);

            sortedNodeList = new List<WellLocationequipmentHierarchyCtrlX.EquipmentNode>();
            for(Equipment__c equipment : equipmentList) {
                if(equipment.Superior_Equipment__c == null) {
                    WellLocationequipmentHierarchyCtrlX.EquipmentNode node = new WellLocationequipmentHierarchyCtrlX.EquipmentNode(0, 0, equipment, true, null);
                    sortedNodeList.add(node);
                    sortedNodeList.addAll(getChildren(node));
                }
            }
        }

        return sortedNodeList;
    }

    public List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> getEquipmentHierarchyVisible() {

        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> resultNodeList = new List<WellLocationequipmentHierarchyCtrlX.EquipmentNode>();

        if(sortedNodeList == null || sortedNodeList.isEmpty()) {
            resultNodeList = getEquipmentHierarchy();
        }

        system.debug('***getCpuTime()***'+Limits.getCpuTime());

        for(WellLocationequipmentHierarchyCtrlX.EquipmentNode node : sortedNodeList) {
            if(!node.getIsParentClosed()) {
                resultNodeList.add(node);
            }
        }  

        system.debug('***getCpuTime()***'+Limits.getCpuTime()); 



        return resultNodeList;
    }


    public void toggleNodeState() {
        Id nodeId = ApexPages.currentPage().getParameters().get('nodeId');
        getNode(nodeId).toggleState();
    }

    ///////////////////////
    // Utility Functions //
    ///////////////////////

    //Get Node From NodeId
    public WellLocationequipmentHierarchyCtrlX.EquipmentNode getNode(Id nodeId) {
        for(WellLocationequipmentHierarchyCtrlX.EquipmentNode node : sortedNodeList) {
            if(node.getId() == nodeId) {
                return node;
            }
        }
        return null;
    }

    public List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> getChildren(WellLocationequipmentHierarchyCtrlX.EquipmentNode pNode) {
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> nodes = new List<WellLocationequipmentHierarchyCtrlX.EquipmentNode>();
        for(Equipment__c equipment : equipmentList) {
            if(equipment.Superior_Equipment__c == pNode.equipment.Id) {
                pNode.hasChildren = true;
                WellLocationequipmentHierarchyCtrlX.EquipmentNode childNode = new WellLocationequipmentHierarchyCtrlX.EquipmentNode(0, pNode.level+1, equipment, true, pNode);
                nodes.add(childNode);
                nodes.addAll(getChildren(childNode));
            }
        }
        return nodes;
    }

}