@isTest
private class Create_Inbound_Email_Test
{
    static testMethod void testInboundEmail()
    {
        Test.startTest();
        Milestone1_Project__c project= new Milestone1_Project__c(Name='Test Project');
        insert project;
        Ticket__c ticketRecord = new Ticket__c(Project__c = project.Id);
        ticketRecord.Ticket_Stage__c = 'New';
        ticketRecord.Requester_Email__c='test@test.com';
        insert ticketRecord;
        String subject = [Select Name from Ticket__c where Id=:ticketRecord.Id][0].Name;
        EmailMessage[] newEmail = new EmailMessage[0];
 
        newEmail.add(new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        CcAddress = 'testto@test.com',
        BccAddress = 'testto@test.com',
        Subject = 'Ticket: '+subject,
        TextBody = 'plainTextBody',
        Status='0',
        HtmlBody = 'htmlBody'
        //ParentId = ticketRecord.Id
        
        //,ActivityId = newTask[0].Id
        ));   
        EmailMessage e2 = new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        CcAddress = 'testto@test.com',
        BccAddress = 'testto@test.com',
        Subject = 'Ticket: '+subject,
        TextBody = 'plainTextBody',
        Status='1',
        HtmlBody = 'htmlBody');  
        newEmail.add(e2);
        EmailMessage e3 = new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        CcAddress = 'testto@test.com',
        BccAddress = 'testto@test.com',
        Subject = 'Ticket: '+subject,
        TextBody = 'plainTextBody',
        Status='2',
        HtmlBody = 'htmlBody');  
        newEmail.add(e3);
        
        EmailMessage e5 = new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        CcAddress = 'testto@test.com',
        BccAddress = 'testto@test.com',
        Subject = 'Ticket: '+subject,
        TextBody = 'plainTextBody',
        Status='3',
        HtmlBody = 'htmlBody');  
        newEmail.add(e5);
        
        
        EmailMessage e4 = new EmailMessage(FromAddress = 'testfrom@test.com',
        FromName = 'Test Firstname',
        ToAddress = 'testto@test.com',
        CcAddress = 'testto@test.com',
        BccAddress = 'testto@test.com',
        Subject = 'Ticket: '+subject,
        TextBody = 'plainTextBody',
        Status='4',
        HtmlBody = 'htmlBody');  
        newEmail.add(e4);           
        insert newEmail;
        Attachment a = new Attachment();
        a.name = 'test attachment';
        a.body = blob.valueof('attachment body');
        a.parentid = newEmail[0].Id;
        insert a;
        Test.stopTest();
    }
}