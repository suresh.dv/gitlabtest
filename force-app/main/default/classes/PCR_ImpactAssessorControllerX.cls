public class PCR_ImpactAssessorControllerX{

    private ApexPages.StandardController controller;
    private Change_Request__c c {get; set;}
    public Impact_Assessor__c impact {get; private set;}
    private Id crId {get; set;}
    public String crNumber {get; private set;}
    public Boolean bypass {get; private set;}
    
    
    public PCR_ImpactAssessorControllerX(ApexPages.StandardController controller) {
        List<string> fields = new List<String>();
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.Impact_Assessor__c.FieldSets.getMap();
        
        crId = ApexPages.currentPage().getParameters().get('Change_Request');
        
        for (String fieldSetName : fieldSetMap.keySet()){
            for(Schema.FieldSetMember f : SObjectType.Impact_Assessor__c.FieldSets.getMap().get(fieldSetName).getFields()){
                fields.add(f.getFieldPath());
            }        
        }
                
        if(!Test.isRunningTest()) {
            controller.addFields(fields);   
        }  
        System.debug('crId - ' + crId);  
        this.controller= controller; 
        c = [select id ,name, PCN_Number__c, Bypass_Endorsement_and_Impact_Assessment__c,Change_Status__c,SPA__c from Change_Request__c where id =: crId];  
        crNumber = c.PCN_Number__c;
        
        if(c.Change_Status__c == 'Impact Assessment and Review'){
            bypass = true;
        } else {
            bypass = c.Bypass_Endorsement_and_Impact_Assessment__c;        
        }
        
        if(c.Change_Status__c == 'Verification') {
            impact = new Impact_Assessor__c();        
            impact.Change_Request__c = crId;
            impact.Name = c.Name;
            impact.ChangeRequestImpactedFuntionAssessorKey__c = crId; 
            impact.Assessor__c = UserInfo.getUserId();  
            impact.SPA__c = c.SPA__c;     
        } else {
        
            //try{
            List<Impact_Assessor__c> impacts = [select id, name, Contract_Commercial_Impact__c,Assessor__c,Scheduled_Impact_in_Days__c,Required_Resources__c,Impact_Assessment_Description__c, 
                                                Scheduled_Impact_Comments__c,Implementation_Plan__c,Regulatory_Implications__c,Impact_Assessment_Complete__c,Change_Request__c,
                                                Risk_Description__c,TIC_Cost__c,FEED_Cost__c,OPEX_Cost__c,Requested_By__c,Cost_Impact__c,Change_Impact_Hours__c,TIC_Schedule_Impact__c
                                             from Impact_Assessor__c where Change_Request__c =: crId limit 1];
            if( impacts.size() > 0 )
                impact = impacts[0];
            /*} catch(System.DMLException e) {
                ApexPages.addMessages(e);
            }*/
                         
            if (impact == null) {    
                impact = new Impact_Assessor__c();        
                impact.Change_Request__c = crId;
                impact.Name = c.Name;
                impact.ChangeRequestImpactedFuntionAssessorKey__c = crId;   
                impact.SPA__c = c.SPA__c; 
                impact.Assessor__c = UserInfo.getUserId();          
            }    
        }              
    }
    
    public PageReference save() {
        try{
            upsert impact;
            
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            System.debug('Error..' + e);
            return null;
        }
        
        return new Pagereference('/apex/Change_Request_View?id=' + crId);
    }
    
    public PageReference cancel() {
        return new Pagereference('/apex/Change_Request_View?id=' + crId);
    }    
}