@isTest
private class PREP_PSG_Test {

    static testMethod void myUnitTest() 
    {       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Discipline__c disp2 = new PREP_Discipline__c();
        disp2.Name = 'Atlantic MM';
        disp2.Active__c = true;
        disp2.SCM_Manager__c = u.Id;
        
        insert disp2;
        
        
        Id bvsRecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('PS&G/MM Business Value Submission').getRecordTypeId();        
        
        PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
        BVS.Discipline__c = disp.Id;
        BVS.SCM_Manager__c = u.Id;
        BVS.Specialist_Rep__c = u.Id;
        BVS.RecordTypeId = bvsRecordTypeId;
        BVS.Value_Created__c = 40000;
        BVS.Year__c = '2015';
        BVS.Savings_Month__c = 'April';
        BVS.Comments__c = 'Test';
        
        insert BVS;
        
        BVS.Discipline__c = disp2.Id;
        update BVS;
        
        disp.Name = 'WCP MM';
        update disp;
        
        PREP_Business_Value_Submission__c BVS2 = new PREP_Business_Value_Submission__c();
        BVS2.Discipline__c = disp2.Id;
        BVS2.SCM_Manager__c = u.Id;
        BVS2.Specialist_Rep__c = u.Id;
        BVS2.RecordTypeId = bvsRecordTypeId;
        BVS2.Value_Created__c = 40000;
        BVS2.Year__c = '2015';
        BVS2.Savings_Month__c = 'April';
        BVS2.Comments__c = 'Test';
        
        insert BVS2;
        
        BVS2.Discipline__c = disp.Id;
        update BVS2;

     }     
}