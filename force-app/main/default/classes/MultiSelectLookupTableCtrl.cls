public class MultiSelectLookupTableCtrl 
{
    //select/deselect item in contect from the page
    public String contextItem {get;set;}
    public string searchString{get;set;} // search keyword
    public String dynamicSObject {get;set;}
    public String rtype {get;set;}
    // Number of results listing page
    private final Integer RESULT_PER_PAGE = 20;
    private Set<Id> selectedObjIds;
    
    public MultiSelectLookupTableCtrl() 
    {
        // get the current search string
        selectedObjIds = new Set<Id>();
        rtype = ApexPages.currentPage().getParameters().get('rt'); 
       // runSearch();  
    }
    public List<CustomWrapper> getResults()
    {
        system.debug('get result');
        List<CustomWrapper> results = new List<CustomWrapper>();
        for(SObject obj : (List<SObject>)queryController.getRecords())
        {
            String id = (String)obj.get('Id');
            String name = (String)obj.get('Name');
            
            
            CustomWrapper row = new CustomWrapper(id, name);
            if (obj.getSObjectType() == Process_Area__c.sObjectType)
            {
            	row.RecordTypeName = ((Process_Area__c)obj).RecordType.Name;
            }    
            else if (obj.getSObjectType() == Team__c.sObjectType)
            {
                row.RecordTypeName = ((Team__c)obj).RecordType.Name;
            }    
            else if (obj.getSObjectType() == Team_Role__c.sObjectType)
            {
                row.RecordTypeName = ((Team_Role__c)obj).RecordType.Name;
            }    
            if(this.selectedObjIds.contains(id))
            {
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            results.add(row);
        }     
        
        return results;
    }   
      // performs the keyword search
    public PageReference runSearch() 
    {
    	System.debug('run search');
        try
        {
            queryController = new Apexpages.Standardsetcontroller(
                      Database.getQueryLocator(performSearch()));
            queryController.setPageSize(RESULT_PER_PAGE);
        }
        catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                       String.valueOf(e)));
        }
        return null;
    }
    private Apexpages.Standardsetcontroller queryController
    {
        get
        {
            if (queryController == null)
            {
                queryController = new Apexpages.Standardsetcontroller(
                      Database.getQueryLocator(performSearch()));
                queryController.setPageSize(RESULT_PER_PAGE);
            }
            return queryController;
        }
        set;
    }  
    
    //handle item selected
    public void doSelectItem()
    {
        selectedObjIds.add(contextItem);
        system.debug('select item');
    }
    //handle item deselected
    public void doDeselectItem()
    {
        selectedObjIds.remove(contextItem);
    }
      // --------------------------------------------------------------------------
    // Go to the first page
    public void first() {
        queryController.first();
        
    }
    
    // --------------------------------------------------------------------------
    // Go to the last page
    public void last() {
        queryController.last();
        
    }
    
    // --------------------------------------------------------------------------
    // Go to the previous page
    public void previous() {
        if (hasPrevious)
            queryController.previous();
    }
    
    // --------------------------------------------------------------------------
    // Go to the next page
    public void next() {
        if (hasNext)
            queryController.next();
    }
    
    // Checks whether there are more search result.
    public Boolean hasNext {
        get{
            return queryController.getHasNext();
        }
    }
    
    // Checks whther there are previous result
    public Boolean hasPrevious {
        get{
            return queryController.getHasPrevious();
        }
    }
    
    // Current page number
   public Integer pageNumber {
        get{
            return queryController.getPageNumber();
        }
   }
   public Integer resultSize
   {
        get{
            return queryController.getResultSize();
        }
   } 
   public integer pageSize
   {
        get{
            return queryController.getPageSize();
        }
   }
   public String getPaginationInfo()
   {
     // {!IF(queryController.PageNumber == 1,1,((queryController.PageNumber -1) * queryController.PageSize)+1)}-{!IF(queryController.resultSize < queryController.PageSize * queryController.PageNumber,queryController.resultSize,queryController.PageNumber * queryController.pageSize)} of {!queryController.resultSize}
        String txt = '';
        if (pageNumber == 1)
            txt += 1;
        else
            txt += String.valueOf((pageNumber - 1) * pageSize + 1);
        txt += '-';
        if (resultSize < pageSize * pageNumber)
            txt += resultSize;
        else
            txt += pageSize * pageNumber;
        
        txt += ' of ' + resultSize;        
        return txt;        
   } 
      // run the search and return the records found. 
    private String performSearch() 
    {
        String soql = 'select id, name ' ;
        if (dynamicSObject == 'Process_Area__c' || dynamicSObject == 'Team__c' || dynamicSObject == 'Team_Role__c')
            soql += ', RecordType.Name ';
            
        soql += ' from ' + dynamicSObject;
        String tmp = '';
        if(searchString == '' || searchString == null)
            tmp = '%';
        else
            tmp = searchString + '%';   
        
        soql +=  ' where name LIKE \'' + tmp + '\'';
        
        if (dynamicSObject == 'Contact')
        {
        	soql += ' and Id in (select Operator_Contact__c from Operational_Event__c) ';
        }
        else if (dynamicSObject == 'Process_Area__c' || dynamicSObject == 'Team__c' || dynamicSObject == 'Team_Role__c')
        {
        	if (rtype != '' && rtype != null)
                soql += ' and RecordType.Name = : rtype';    
        }
        soql += ' order by Name ';
        if (dynamicSObject == 'Process_Area__c' || dynamicSObject == 'Team__c' || dynamicSObject == 'Team_Role__c')
            soql += ' , RecordType.Name';
        
        System.debug('soql =' + soql);
        return soql;
   
    }
    public class CustomWrapper
    {
        public boolean isSelected {get;set;}
        public String Id {get;set;}
        public String Name {get;set;}
        public String RecordTypeName {get;set;}
        
        public CustomWrapper(String id, String name)
        {
            isSelected = false;
            this.Id = id;
            this.Name = name;
            
        }
    }  
    
}