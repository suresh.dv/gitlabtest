public class WCPBudgetController
{
    public Id workPackageId {get;set;}
    
    public String hoverPosition {get;set;}
    
    public Milestone1_Project__c workPackage {get;set;}
    
    public List<CostWrapper> costEstimates {get;set;}
    
    public class CostWrapper
    {
        public String Cost {get;set;}
        public Decimal Amount {get;set;}
        
        public CostWrapper(String Cost, Decimal Amount)
        {
            this.Cost = Cost;
            this.Amount = Amount;
        }
    }
    
    public WCPBudgetController()
    {
        // Get project id
        workPackageId = ApexPages.CurrentPage().getParameters().get('id');
        
        // Get vertical position
        hoverPosition = ApexPages.CurrentPage().getParameters().get('hoverPosition');
        
        workPackage = [SELECT Id,
                              Current_Budget__r.MostLikelyCostEst__c,
                              Current_Budget__r.OptimisticCostEst__c,
                              Current_Budget__r.PessimisticCostEst__c,
                              Current_Budget__r.PERTCostEst__c
                       FROM Milestone1_Project__c
                       WHERE Id =: workPackageId];
        
        costEstimates = new List<CostWrapper>();
        
        costEstimates.add(new CostWrapper('Pessimistic',workPackage.Current_Budget__r.PessimisticCostEst__c));
        costEstimates.add(new CostWrapper('Most Likely',workPackage.Current_Budget__r.MostLikelyCostEst__c));
        costEstimates.add(new CostWrapper('Optimistic',workPackage.Current_Budget__r.OptimisticCostEst__c));
        costEstimates.add(new CostWrapper('PERT',workPackage.Current_Budget__r.PERTCostEst__c));
    }
}