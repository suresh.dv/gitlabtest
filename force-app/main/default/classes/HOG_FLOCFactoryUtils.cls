public with sharing class HOG_FLOCFactoryUtils {
	public class FunctionalLocationFactory {
      
      public FunctionalLocation getFloc(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse, 
        Set<String> existingSystems, Set<String> existingSubSystems, Set<String> existingFacilities){
          if(flocResponse == null || 
            String.isBlank(flocResponse.RIHIFLO_LST.TPLNR) ||
            String.isBlank(flocResponse.RIHIFLO_LST.FLTYP)){
             return null;
          }

          Integer level = flocResponse.RIHIFLO_LST.TPLNR.split('-').size();
          Integer category = (flocResponse.RIHIFLO_LST.FLTYP.isNumeric()) ? 
            Integer.valueOf(flocResponse.RIHIFLO_LST.FLTYP) : null;

          if(level == 1 && category == 1){
            return new BusinessUnit(flocResponse);
          }

          if(level == 2 && category == 1){
            return new OperatingDistrict(flocResponse);
          }

          if(level == 3 && category == 1){
            return new OperatingField(flocResponse);
          }

          if(category == 3){
            return new Facility(flocResponse);
          }

          if(category == 4){
            return new WellId(flocResponse);
          }

          if(category == 5) {
            return new SystemFLOC(flocResponse);
          }

          if(category == 6) {
            return new SubSystem(flocResponse, existingSystems, existingFacilities);
          }

          if(category == 7) {
            return new FuntionalEquipmentLevel(flocResponse, existingSystems, existingSubSystems);
          }

          if(category == 8) {
            return new Yard(flocResponse);
          }

          if (category == 9)
            return new WellEvent(flocResponse);

          return null;
       }
    }

    public virtual class FunctionalLocation {
      protected SObject flocRecord;
      protected HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse;
      protected HOG_SAPApiUtils.DT_SAP_RIHIFLO_LIST input;
      protected HOG_SAPApiUtils.DT_SAP_CI_IFLOT wellData;

      public FunctionalLocation() {

      }

      public FunctionalLocation(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        this.flocResponse = flocResponse;
        this.input = this.flocResponse.RIHIFLO_LST;
        this.wellData = this.flocResponse.CI_IFLOT;
      }

      public String getFlocString() {
      	return this.input.TPLNR.toUpperCase();
      }

      public virtual SObject getRecord() {
        return this.flocRecord;
      }

      public String getRouteNumber() {
      	if(Integer.valueOf(this.input.FLTYP) > 1 && !String.isBlank(this.input.BEBER)) {
      		return this.input.BEBER;
      	}
      	return null;
      }
    }

    public class BusinessUnit extends FunctionalLocation {
      public BusinessUnit(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for BusinessUnit
          Business_Unit__c businessUnit = new Business_Unit__c();
          businessUnit.Functional_Location__c = this.input.TPLNR.toUpperCase();
          businessUnit.Name = this.input.PLTXT;
          businessUnit.SAP_Object_ID__c = this.input.OBJNR;
          businessUnit.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          this.flocRecord = businessUnit;
        }
        return this.flocRecord;
      }
    }

    public class OperatingDistrict extends FunctionalLocation {
   	  public String businessUnitFloc {get; private set;}
   	  public String businessDepartmentName {get; private set;}

      public OperatingDistrict(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for OperatingDistrict
          Operating_District__c operatingDistrict = new Operating_District__c();
          operatingDistrict.Functional_Location__c = this.input.TPLNR.toUpperCase();
          operatingDistrict.Name = this.input.PLTXT;
          operatingDistrict.SAP_Object_ID__c = this.input.OBJNR;
          operatingDistrict.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          operatingDistrict.Planner_Group__c = this.input.INGRP;

          // Assign Parents
          operatingDistrict.Business_Unit__r = new Business_Unit__c(
          	Functional_Location__c = this.input.TPLMA.toUpperCase()
          );
          operatingDistrict.Business_Department__r = new Business_Department__c(
          	Name = this.flocResponse.RECTYPE.startsWith('I') ? 
          	HOG_Settings__c.getInstance().Default_Business_Department__c : null
          );

          this.flocRecord = operatingDistrict;
        }
        return this.flocRecord;
      }
    }

    public class OperatingField extends FunctionalLocation {
      public OperatingField(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for OperatingField
          Field__c operatingField = new Field__c();
          operatingField.Functional_Location__c = this.input.TPLNR.toUpperCase();
          operatingField.Name = this.input.PLTXT;
          operatingField.SAP_Object_ID__c = this.input.OBJNR;
          operatingField.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          operatingField.Maintenance_Plant__c = this.input.SWERK;
          operatingField.Planner_Group__c = this.input.INGRP;
          operatingField.Superior_Function_Location__c = this.input.TPLMA;

          // Assign Parents
          operatingField.Operating_District__r = new Operating_District__c(
          	Functional_Location__c = this.input.TPLMA.toUpperCase()
          );

          this.flocRecord = operatingField;
        }
        return this.flocRecord;
      }
    }

    public class Facility extends FunctionalLocation {
      public Facility(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for Facility
          Facility__c facility = new Facility__c();
          facility.Functional_Location__c = this.input.TPLNR;
          facility.SAP_Object_ID__c = this.input.TPLNR_INT;
          facility.Name = this.input.PLTXT;
          facility.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
          	'' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
          	this.input.KOSTL.length());
    		  facility.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
    		  	null : Integer.valueOf(this.input.FLTYP);
    		  facility.Maintenance_Plant__c = this.input.SWERK;
    		  facility.Planner_Group__c = this.input.INGRP;
    		  facility.Status__c = this.flocResponse.USER_STAT;
    		  facility.Surface_Location__c = this.wellData.ZZ_LSD;
    		  facility.UWI_Sort_Order__c = this.input.TPLNR_2;
    		  facility.Facility_Type__c = this.wellData.ZZ_WELL_TYPE;
    		  facility.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          // Added H2S For Facility mbrimus 1.8.2017
          facility.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';
    		  // Assign Parents
    		  facility.Plant_Section__r = new Route__c(
    		  	Name = String.isBlank(this.input.BEBER) ? 
    		  		HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
    		  		this.input.BEBER
    		  );
    		  if(this.input.TPLNR.split('-').size() < 5) {
      			facility.Operating_Field_AMU__r = new Field__c(
      			 Functional_Location__c = this.input.TPLMA.toUpperCase()
      			);
    		  }
    		  if(this.input.TPLNR.split('-').size() >= 5 && !String.isBlank(this.input.TPLMA_INT)) {
      			facility.Superior_Facility__r = new Facility__c(
      			 SAP_Object_ID__c = this.input.TPLMA_INT
      			);
		      }

		      this.flocRecord = facility;
        }
        return this.flocRecord;
      }
    }

    /**
     * MarcelB changes to FLOC - adding logic for System 03.07.2017
     */
    public class SystemFLOC extends FunctionalLocation {
      public SystemFLOC(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for System object
          System__c systemFLOC = new System__c();
          systemFLOC.Functional_Location__c = this.input.TPLNR;
          systemFLOC.Functional_Location_Description_SAP__c = this.input.TPLNR;
          systemFLOC.SAP_Object_ID__c = this.input.TPLNR_INT;
          systemFLOC.Name = this.input.PLTXT;
          systemFLOC.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
            '' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
            this.input.KOSTL.length());
          systemFLOC.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
            null : Integer.valueOf(this.input.FLTYP);
          systemFLOC.Maintenance_Plant__c = this.input.SWERK;
          systemFLOC.Planner_Group__c = this.input.INGRP;
          systemFLOC.Surface_Location__c = this.wellData.ZZ_LSD;
          systemFLOC.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          systemFLOC.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';
          systemFLOC.Unit_Configuration__c = String.isBlank(this.wellData.ZZ_UNIT_CONFIG) ?
            '' : this.wellData.ZZ_UNIT_CONFIG;
          systemFLOC.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? 
            false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          systemFLOC.Tag_Number__c = String.isBlank(this.input.EQFNR) ?
            '' : this.input.EQFNR;
          systemFLOC.Record_Type__c = this.flocResponse.RECTYPE;
          
          // Assign Parents to systemFLOC
          systemFLOC.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );

          // System can have two parent - Facility or AMU
          // In any cas AMU will be populated either from AMU itself or parent facility AMU
          // This takes care of it
          if(!String.isBlank(this.input.TPLMA)) {
            systemFLOC.Operating_Field_AMU__r = new Field__c(
              Functional_Location__c = this.input.TPLMA.split('-')[0].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[1].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[2].toUpperCase()
            );
          }

          // Populate facility parent
          // All Cat 5 FLOCS at level 5 should have a Facility object parent
          if(this.input.TPLNR.split('-').size() == 5) {
            systemFLOC.Facility__r = new Facility__c(
              SAP_Object_ID__c = this.input.TPLMA_INT
            );
          } else {
            systemFLOC.Facility__c = null;
          }

          this.flocRecord = systemFLOC;
        }
        return this.flocRecord;
      }
    }

     /**
     * MarcelB changes to FLOC - adding logic for SubSystem 06.07.2017
     */
    public class SubSystem extends FunctionalLocation {
      private Set<String> existingSystems = new Set<String>(); 
      private Set<String> existingFacilities = new Set<String>();

      public SubSystem(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse, Set<String> existingSystems, Set<String> existingFacilities) {
        super(flocResponse);
        this.existingSystems = existingSystems;
        this.existingFacilities = existingFacilities;
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for System object
          Sub_System__c subSystem = new Sub_System__c();
          subSystem.Functional_Location__c = this.input.TPLNR;
          subSystem.Functional_Location_Description_SAP__c = this.input.TPLNR; 
          subSystem.SAP_Object_ID__c = this.input.TPLNR_INT;
          subSystem.Name = this.input.PLTXT;
          subSystem.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
            '' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
            this.input.KOSTL.length());
          subSystem.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
            null : Integer.valueOf(this.input.FLTYP);
          subSystem.Maintenance_Plant__c = this.input.SWERK;
          subSystem.Planner_Group__c = this.input.INGRP;
          subSystem.Surface_Location__c = this.wellData.ZZ_LSD;
          subSystem.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          subSystem.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';
          subSystem.Unit_Configuration__c = String.isBlank(this.wellData.ZZ_UNIT_CONFIG) ?
            '' : this.wellData.ZZ_UNIT_CONFIG;
          subSystem.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? 
            false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          subSystem.Tag_Number__c = String.isBlank(this.input.EQFNR) ?
            '' : this.input.EQFNR;
          subSystem.Record_Type__c = this.flocResponse.RECTYPE;

          // Assign Parents to subSystem
          subSystem.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );

          // Sub System can have 3 parents - Facility, Well ID and System
          // In any case AMU will be populated either from AMU itself or parents AMU
          if(!String.isBlank(this.input.TPLMA)) {
            subSystem.Operating_Field_AMU__r = new Field__c(
              Functional_Location__c = this.input.TPLMA.split('-')[0].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[1].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[2].toUpperCase()
            );
          }

          // Figure out parent
          if(existingSystems.contains(this.input.TPLMA.toUpperCase())){
            // Parent is System
            subSystem.System__r = new System__c(
              Functional_Location_Description_SAP__c = this.input.TPLMA.toUpperCase()
            );
            subSystem.Well_ID__c = null;
            subSystem.Facility__c = null;
          
          } else if(existingFacilities.contains(this.input.TPLMA.toUpperCase())){
            // Parent is Facility
            subSystem.Facility__r = new Facility__c(
              Functional_Location__c = this.input.TPLMA.toUpperCase()
            );
            subSystem.Well_ID__c = null;
            subSystem.System__c = null;
 
          // All Cat 6 FLOCs at level 5+ should have a Location Cat 5 parent in the same Location object
          } else {
            // If these two are not parent then parent is Well ID
            // Think we could also use this.input.TPLMA.toUpperCase() but i am not sure
            subSystem.Well_ID__r = new Location__c(
              SAP_Object_ID__c = this.input.TPLMA_INT 
            );
            subSystem.Facility__c = null;
            subSystem.System__c = null;
          }
                 
          this.flocRecord = subSystem;
        }
        return this.flocRecord;
      }
    }

    /**
     * MarcelB changes to FLOC - adding logic for Yards 23.08.2017
     */
    public class Yard extends FunctionalLocation {
   
      public Yard(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for Yard
          Yard__c yard = new Yard__c();
          yard.Surface_Location__c = String.isBlank(this.wellData.ZZ_LSD) ? '' : this.wellData.ZZ_LSD;
          yard.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          yard.Name = this.input.PLTXT;
          yard.Functional_Location__c = this.input.TPLNR;
          yard.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ? null : Integer.valueOf(this.input.FLTYP);
          yard.Functional_Location_Description_SAP__c = input.TPLNR;
          yard.Maintenance_Plant__c = String.isBlank(this.input.SWERK) ? '' : this.input.SWERK;
          yard.Planner_Group__c = String.isBlank(this.input.INGRP) ? '' : this.input.INGRP;
          yard.SAP_Object_ID__c = this.input.TPLNR_INT;
          yard.Record_Type__c = this.flocResponse.RECTYPE;
          yard.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          yard.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';

          // Assign Parents
          yard.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );

          // AMU - equals first three levels of the parent 'H1-002-00004' from 'H1-002-00004-000002'
          if(!String.isBlank(this.input.TPLMA) && this.input.TPLMA.split('-').size() >= 3) {
            yard.Operating_Field_AMU__r = new Field__c(
              Functional_Location__c = this.input.TPLMA.split('-')[0].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[1].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[2].toUpperCase()
            );
          }

          // All Cat 8 FLOCS at level 5 should have a Facility object parent
          if((Integer.valueOf(this.input.FLTYP) == 8 && this.input.TPLNR.split('-').size() == 5)) {
                yard.Facility__r = new Facility__c(
                  SAP_Object_ID__c = this.input.TPLMA_INT
                );
            }              
          
          // All Cat 8 FLOCs at level 6+ should have a Well ID Cat 4 parent
          if((Integer.valueOf(this.input.FLTYP) == 8 && this.input.TPLNR.split('-').size() > 5)) {
            yard.Well_ID__r = new Location__c(
              SAP_Object_ID__c = this.input.TPLMA_INT     
            );
          }

          this.flocRecord = yard;
        }
        
        return this.flocRecord;
      }
    }

    /**
     * MarcelB changes to FLOC - adding logic for Yards 23.08.2017
     */
    public class FuntionalEquipmentLevel extends FunctionalLocation {
      private Set<String> existingSystems = new Set<String>(); 
      private Set<String> existingSubSystems = new Set<String>();

      public FuntionalEquipmentLevel(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse, Set<String> existingSystems, Set<String> existingSubSystems) {
        super(flocResponse);
        this.existingSystems = existingSystems;
        this.existingSubSystems = existingSubSystems;
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {

          // Processing for Funtional Equipment Level
          Functional_Equipment_Level__c functionalEquipmentLevel = new Functional_Equipment_Level__c();
          functionalEquipmentLevel.Surface_Location__c = String.isBlank(this.wellData.ZZ_LSD) ? '' : this.wellData.ZZ_LSD;
          functionalEquipmentLevel.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          functionalEquipmentLevel.Name = this.input.PLTXT;
          functionalEquipmentLevel.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
            '' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
            this.input.KOSTL.length());
          functionalEquipmentLevel.Functional_Location__c = this.input.TPLNR;
          functionalEquipmentLevel.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
            null : Integer.valueOf(this.input.FLTYP);
          functionalEquipmentLevel.Maintenance_Plant__c = String.isBlank(this.input.SWERK) ? '' : this.input.SWERK;
          functionalEquipmentLevel.Tag_Number__c = String.isBlank(this.input.EQFNR) ? '' : this.input.EQFNR;
          functionalEquipmentLevel.Planner_Group__c = String.isBlank(this.input.INGRP) ? '' : this.input.INGRP;
          functionalEquipmentLevel.SAP_Object_ID__c = this.input.TPLNR_INT;
          functionalEquipmentLevel.Record_Type__c = this.flocResponse.RECTYPE;
          functionalEquipmentLevel.Functional_Location_Description_SAP__c = input.TPLNR;
          functionalEquipmentLevel.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          functionalEquipmentLevel.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';

          // Assign Parents
          functionalEquipmentLevel.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );

          // AMU - equals first three levels of the parent
          if(!String.isBlank(this.input.TPLMA) && this.input.TPLMA.split('-').size() >= 3) {
            functionalEquipmentLevel.Operating_Field_AMU__r = new Field__c(
              Functional_Location__c = this.input.TPLMA.split('-')[0].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[1].toUpperCase() + '-' +
                this.input.TPLMA.split('-')[2].toUpperCase()
            );
          }

           // All Cat 7 FLOCs at Level 5 should be parented by Facilit TOOD verify this assumption
          if( Integer.valueOf(this.input.FLTYP) == 7 && this.input.TPLNR.split('-').size() == 5) {
              functionalEquipmentLevel.Facility__r = new Facility__c(
                  SAP_Object_ID__c = this.input.TPLMA_INT
              );
          }

          // Logic for All Cat 7 FLOCs at level 6+ 
          // Checks if any of two sets (System and SubSystem) contains parent FLOC number, if not then parent should be FEL
          if( Integer.valueOf(this.input.FLTYP) == 7 && this.input.TPLNR.split('-').size() > 5) {
            
            if(existingSystems.contains(this.input.TPLMA.toUpperCase())){
              functionalEquipmentLevel.System__r = new System__c(
                SAP_Object_ID__c = this.input.TPLMA_INT     
              );
            
            } else if (existingSubSystems.contains(this.input.TPLMA.toUpperCase())) {
              functionalEquipmentLevel.Sub_System__r = new Sub_System__c(
                SAP_Object_ID__c = this.input.TPLMA_INT     
              );
              
            } else {
              functionalEquipmentLevel.Functional_Equipment_Level__r = new Functional_Equipment_Level__c (
                  SAP_Object_ID__c = this.input.TPLMA_INT
              );
            }

          }
          
          this.flocRecord = functionalEquipmentLevel;
        }
        
        return this.flocRecord;
      }
    }

    public class WellId extends FunctionalLocation {
      

      public WellId(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
        
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for WellId
          Location__c wellIdentifier = new Location__c();
          wellIdentifier.Surface_Location__c = String.isBlank(this.wellData.ZZ_LSD) ? 
          	'' : this.wellData.ZZ_LSD;
          wellIdentifier.Unit_Configuration__c = String.isBlank(this.wellData.ZZ_UNIT_CONFIG) ?
          	'' : this.wellData.ZZ_UNIT_CONFIG;
          wellIdentifier.Well_Orientation__c = String.isBlank(this.wellData.ZZ_WELL_ORIENT) ?
          	'' : this.wellData.ZZ_WELL_ORIENT;
          wellIdentifier.Well_Type__c = String.isBlank(this.wellData.ZZ_WELL_TYPE) ? 
            '' : this.wellData.ZZ_WELL_TYPE;
          wellIdentifier.Oil_Specific_Gravity__c = String.isBlank(this.wellData.ZZ_OIL_GRAVITY) ?
          	null : Decimal.valueOf(this.wellData.ZZ_OIL_GRAVITY.trim());
          wellIdentifier.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? 
          	false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          wellIdentifier.Single_Well_Monitoring__c = String.isBlank(this.wellData.ZZ_SWM_FLAG) ?
          	false : this.wellData.ZZ_SWM_FLAG.equalsIgnoreCase('X');
          wellIdentifier.Name = this.input.PLTXT;
          wellIdentifier.ABC_Indicator__c = String.isBlank(this.input.ABCKZ) ? 
          	'' : this.input.ABCKZ;
          wellIdentifier.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
          	'' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
          	this.input.KOSTL.length());
          wellIdentifier.Functional_Location__c = this.input.TPLNR;
          wellIdentifier.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
          	null : Integer.valueOf(this.input.FLTYP);
          wellIdentifier.Maintenance_Plant__c = String.isBlank(this.input.SWERK) ? 
          	'' : this.input.SWERK;
          wellIdentifier.Main_Work_Centre__c = String.isBlank(this.input.GEWRK) ?
          	'' : this.input.GEWRK;
          wellIdentifier.Object_Type__c = String.isBlank(this.input.EQART) ? 
          	'' : this.input.EQART;
          wellIdentifier.Tag_Number__c = String.isBlank(this.input.EQFNR) ?
          	'' : this.input.EQFNR;
          wellIdentifier.Planner_Group__c = String.isBlank(this.input.INGRP) ?
          	'' : this.input.INGRP;
          wellIdentifier.Start_Up_Date__c = this.input.DATAB == '00000000' ?
          	null : HOG_SAPApiUtils.parseDate(this.input.DATAB);
          wellIdentifier.SAP_Object_ID__c = this.input.TPLNR_INT;
          wellIdentifier.SCADA_ID__c = String.isBlank(this.wellData.ZZ_SCADA_ID) ?
          	'' : this.wellData.ZZ_SCADA_ID;
          wellIdentifier.Status__c = this.flocResponse.USER_STAT;
          wellIdentifier.Record_Type__c = this.flocResponse.RECTYPE;
          wellIdentifier.UWI_Sort_Order__c = String.isBlank(this.input.TPLNR_2) ?
          	'' : this.input.TPLNR_2;
          wellIdentifier.Functional_Location_Description_SAP__c = input.TPLNR;
          wellIdentifier.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          wellIdentifier.PVR_UWI_Raw__c = String.isBlank(input.TPLNR_1) ? 
          	null : input.TPLNR_1.replaceAll('[/-]','');
          wellIdentifier.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';

          // Assign Parents
          wellIdentifier.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );

          // AMU - equals first three levels of the parent 'H1-002-00004' from 'H1-002-00004-000002'
          if(!String.isBlank(this.input.TPLMA) && this.input.TPLMA.split('-').size() >= 3) {
	          wellIdentifier.Operating_Field_AMU__r = new Field__c(
	          	Functional_Location__c = this.input.TPLMA.split('-')[0].toUpperCase() + '-' +
	          		this.input.TPLMA.split('-')[1].toUpperCase() + '-' +
	          		this.input.TPLMA.split('-')[2].toUpperCase()
	          );
	        }

          // All Cat 4 FLOCs at level 5+ should have a Facility object parent
    		  if((Integer.valueOf(this.input.FLTYP) == 4 && this.input.TPLNR.split('-').size() > 4)) {
    	          wellIdentifier.Facility__r = new Facility__c(
    	          	SAP_Object_ID__c = this.input.TPLMA_INT
    	          );
    	    }                   

    		  // Assign recordtype
    		  HOG_FLOCFactoryUtils.assignRecordType(wellIdentifier, Integer.valueOf(this.input.FLTYP));
          this.flocRecord = wellIdentifier;
        }
        
        return this.flocRecord;
      }
    }

    public class WellEvent extends FunctionalLocation {
      public WellEvent(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocResponse) {
        super(flocResponse);
      }

      public override SObject getRecord() {
        if(this.flocRecord == null) {
          // Processing for Well Event
          Well_Event__c wellEvent = new Well_Event__c();
          wellEvent.Surface_Location__c = String.isBlank(this.wellData.ZZ_LSD) ? 
          	'' : this.wellData.ZZ_LSD;
          wellEvent.Unit_Configuration__c = String.isBlank(this.wellData.ZZ_UNIT_CONFIG) ?
          	'' : this.wellData.ZZ_UNIT_CONFIG;
          wellEvent.Well_Orientation__c = String.isBlank(this.wellData.ZZ_WELL_ORIENT) ?
          	'' : this.wellData.ZZ_WELL_ORIENT;
          wellEvent.Well_Type__c = String.isBlank(this.wellData.ZZ_WELL_TYPE) ? 
            '' : this.wellData.ZZ_WELL_TYPE;
          wellEvent.Oil_Specific_Gravity__c = String.isBlank(this.wellData.ZZ_OIL_GRAVITY) ?
          	null : Decimal.valueOf(this.wellData.ZZ_OIL_GRAVITY.trim());
          wellEvent.No_Equipment_Flag__c = String.isBlank(this.wellData.ZZ_NOEQ) ? 
          	false : this.wellData.ZZ_NOEQ.equalsIgnoreCase('X');
          wellEvent.Single_Well_Monitoring__c = String.isBlank(this.wellData.ZZ_SWM_FLAG) ?
          	false : this.wellData.ZZ_SWM_FLAG.equalsIgnoreCase('X');
          wellEvent.Name = this.input.PLTXT;
          wellEvent.ABC_Indicator__c = String.isBlank(this.input.ABCKZ) ? 
          	'' : this.input.ABCKZ;
          wellEvent.Cost_Center__c = String.isBlank(this.input.KOSTL) ?
          	'' : this.input.KOSTL.substring(this.input.KOSTL.indexOfAny('123456789'), 
          	this.input.KOSTL.length());
          wellEvent.Functional_Location__c = this.input.TPLNR;
          wellEvent.Functional_Location_Category__c = String.isBlank(this.input.FLTYP) ?
          	null : Integer.valueOf(this.input.FLTYP);
          wellEvent.Maintenance_Plant__c = String.isBlank(this.input.SWERK) ? 
          	'' : this.input.SWERK;
          wellEvent.Main_Work_Centre__c = String.isBlank(this.input.GEWRK) ?
          	'' : this.input.GEWRK;
          wellEvent.Object_Type__c = String.isBlank(this.input.EQART) ? 
          	'' : this.input.EQART;
          wellEvent.Tag_Number__c = String.isBlank(this.input.EQFNR) ?
          	'' : this.input.EQFNR;
          wellEvent.Planner_Group__c = String.isBlank(this.input.INGRP) ?
          	'' : this.input.INGRP;
          wellEvent.Start_Up_Date__c = this.input.DATAB == '00000000' ?
          	null : HOG_SAPApiUtils.parseDate(this.input.DATAB);
          wellEvent.SAP_Object_ID__c = this.input.TPLNR_INT;
          wellEvent.SCADA_ID__c = String.isBlank(this.wellData.ZZ_SCADA_ID) ?
          	'' : this.wellData.ZZ_SCADA_ID;
          wellEvent.Status__c = this.flocResponse.USER_STAT;
          wellEvent.Record_Type__c = this.flocResponse.RECTYPE;
          wellEvent.UWI_Sort_Order__c = String.isBlank(this.input.TPLNR_2) ?
          	'' : this.input.TPLNR_2;
          wellEvent.Functional_Location_Description_SAP__c = input.TPLNR;
          wellEvent.DLFL__c = this.flocResponse.RECTYPE.startsWith('D');
          wellEvent.PVR_UWI_Raw__c = String.isBlank(input.TPLNR_1) ? 
          	null : input.TPLNR_1.replaceAll('[/-]','');
          wellEvent.Hazardous_H2S__c = (this.wellData.ZZ_H2S_LOCATION == 'YES') ? 'Y':'N';


          // Assign Parents
          wellEvent.Route__r = new Route__c(
            Name = String.isBlank(this.input.BEBER) ? 
              HOG_Settings__c.getInstance().Operating_Route_Default_Name__c :
              this.input.BEBER
          );
          if((Integer.valueOf(this.input.FLTYP) == 9 && this.input.TPLNR.split('-').size() > 3)) {
	          wellEvent.Well_ID__r = new Location__c(
    			  	SAP_Object_ID__c = this.input.TPLMA_INT
    			  );
		      }

          this.flocRecord = wellEvent;
        }
        return this.flocRecord;
      }
    }

    /**************************************** Utility Functions ***************************************/
    private static Map<String, Id> wellLocationRecordTypeMap {
    	get {
    		if(wellLocationRecordTypeMap == null || wellLocationRecordTypeMap.isEmpty()) {
    			wellLocationRecordTypeMap = new Map<String, Id>();
    			Set<String> recordTypeStrings = new Set<String>{'Yard', 'Well_ID', 'Sub_system',
    				'Functional_Equipment_Level', 'System'};
    			for(RecordType rType : [Select Id, DeveloperName
    									From RecordType
    									Where SobjectType = 'Location__c'
    									And DeveloperName In :recordTypeStrings]) {
    				wellLocationRecordTypeMap.put(rType.DeveloperName, rType.Id);
    			}
    		}
    		return wellLocationRecordTypeMap;
    	}
    	private set;
    }


    private static void assignRecordType(Location__c wellIdentifier, Integer category) {
      wellIdentifier.RecordTypeId = (category == 4) ? wellLocationRecordTypeMap.get('Well_ID'):null;
    }
    /**************************************************************************************************/
}