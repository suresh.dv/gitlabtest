/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Batch class for sending notifications on Vent Gas Alert Tasks.
Test class:     HOG_VGTasksEmailNotificationsTest
History:        jschn 04.25.2018 - Created.
**************************************************************************************************/
global class HOG_VGTasksEmailNotifications implements Database.Batchable<sObject> {
	
	public static final String NOTIFICATIONTYPE_NEW_TASKS = 'NEW_TASKS';

	private static final Set<String> NEW_STATUSES = new Set<String>{HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED};

	private String notificationType;
	private String query;
	private String queryNewTasks = 'SELECT Id, Name'
								+		', Assignee1__c, Assignee1__r.Name'
								+ 		', Assignee2__c, Assignee2__r.Name'
								+		', Priority__c, Remind__c'
								+		', Comments__c, Subject__c'
								+		', Vent_Gas_Alert__c'
								+		', Vent_Gas_Alert__r.Type__c'
								+		', Vent_Gas_Alert__r.Well_Location__c'
								+		', Vent_Gas_Alert__r.Well_Location__r.Name'
								+ ' FROM HOG_Vent_Gas_Alert_Task__c'
								+ ' WHERE Status__c in : NEW_STATUSES'
								+ 	' AND Remind__c = true';
	
	global HOG_VGTasksEmailNotifications(String type) {
		notificationType = type;
		if(isCorrectNotificationType())
			query = queryNewTasks;
	}
	
	/**
	IMPLEMENTATIONS OF BATCHABLE INTERFACE
	*/
	/**
	* Get data.
	*/
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
	* generate emails list and send it if within limits.
	*/
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<HOG_Vent_Gas_Alert_Task__c> ventGasAlertTasksList = (List<HOG_Vent_Gas_Alert_Task__c>) scope;
		Messaging.SingleEmailMessage[] emailList = new Messaging.SingleEmailMessage[] {};

		if(isCorrectNotificationType())
			emailList = generateNewVentGasAlertTaskEmails(ventGasAlertTasksList);

		if(emailListIsWithinLimits())
			Messaging.sendEmail(emailList);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	/**
	HELPERS
	*/
	/**
	* Generate lists of emails for assignees (both 1 and 2).
	* First it will create Map of Assignees(ID) with list of related tasks.
	* Then generate email for every assignee and return list of all emails
	*/
	private Messaging.SingleEmailMessage[] generateNewVentGasAlertTaskEmails(List<HOG_Vent_Gas_Alert_Task__c> ventGasAlertTasksList) {
		Messaging.SingleEmailMessage[] emailList = new Messaging.SingleEmailMessage[] {};
		Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> newTasksAssigneeMap = new Map<Id, List<HOG_Vent_Gas_Alert_Task__c>>();

		for (HOG_Vent_Gas_Alert_Task__c task : ventGasAlertTasksList) {
			addTaskToAssignee(newTasksAssigneeMap, task, task.Assignee1__c);
			if(String.isNotBlank(task.Assignee2__c))
				addTaskToAssignee(newTasksAssigneeMap, task, task.Assignee2__c);
		}
		
		for (Id assigneeId : newTasksAssigneeMap.keySet())
			emailList.add(generateNewVentGasAlertTaskEmail(assigneeId, newTasksAssigneeMap.get(assigneeId)));

		return emailList;
	}

	/**
	* Adds task to list under assignee(ID)
	*/
	private void addTaskToAssignee(Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> tasksAssigneeMap, 
									HOG_Vent_Gas_Alert_Task__c task, 
									Id assigneeId) {
		if(!tasksAssigneeMap.containsKey(assigneeId))
			tasksAssigneeMap.put(assigneeId, new List<HOG_Vent_Gas_Alert_Task__c>());
		tasksAssigneeMap.get(assigneeId).add(task);
	}

	/**
	* Generates email with it's content based on assigne ID and all related tasks for him.
	*/
	private Messaging.SingleEmailMessage generateNewVentGasAlertTaskEmail(Id assignee, 
		List<HOG_Vent_Gas_Alert_Task__c> tasksList) {
		String messageBody_Html = '';
		String urlOrgBase = 'https://' + System.URL.getSalesforceBaseUrl().getHost() + '/';
		String link = '<a target="#TARGET#" href="' + urlOrgBase + '#ID#">#NAME#</a>';
		
		for (HOG_Vent_Gas_Alert_Task__c task : tasksList) {
			String taskLink = link.replace('#TARGET#', 'vg_task')
									.replace('#ID#', task.Id)
									.replace('#NAME#', task.Name);
			String locationLink = link.replace('#TARGET#', 'wellLocation')
										.replace('#ID#', task.Vent_Gas_Alert__r.Well_Location__c)
										.replace('#NAME#', task.Vent_Gas_Alert__r.Well_Location__r.Name);
			messageBody_Html += '<table>';
			messageBody_Html += '<tr><td>Task: </td><td>' + taskLink + '</td></tr>';
			messageBody_Html += '<tr><td>Subject: </td><td>' + task.Subject__c + '</td></tr>';
			messageBody_Html += '<tr><td>Assignee #1: </td><td>' + task.Assignee1__r.Name + '</td></tr>';
			if(String.isNotBlank(task.Assignee2__c))
				messageBody_Html += '<tr><td>Assignee #2: </td><td>' + task.Assignee2__r.Name + '</td></tr>';
			messageBody_Html += '<tr><td>Comments: </td><td>' + (task.Comments__c <> null ? task.Comments__c : '') + '</td></tr>';
			messageBody_Html += '<tr><td>Location: </td><td>' + locationLink + '</td></tr>';
			messageBody_Html += '<tr><td>Priority: </td><td>' + task.Priority__c + '</td></tr>';
			messageBody_Html += '</table>';
			messageBody_Html += '<hr/>';
		}

		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setSubject('HOG Vent Gas Alert Tasks Daily Summary');
		email.setTargetObjectId(assignee);
		email.setHtmlBody(messageBody_Html);
		email.setSaveAsActivity(false);

		return email;
	}

	/**
	VALIDATIONS
	*/
	/**
	* Checks if all new created emails are within limits.
	*/
	private Boolean emailListIsWithinLimits() {
		return Limits.getEmailInvocations() < Limits.getLimitEmailInvocations();
	}

	/**
	* Checks if this class was constructed with correct notification type.
	*/
	private Boolean isCorrectNotificationType() {
		return this.notificationType == NOTIFICATIONTYPE_NEW_TASKS;
	}

}