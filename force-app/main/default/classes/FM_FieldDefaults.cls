/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
Description:    Fluid Management SObject field defaults for Testing purposes. 
History:        jschn 05.09.2018 - Created.
TODO: 			getDefaultPicklistValue
**************************************************************************************************/
@isTest
public class FM_FieldDefaults {
	
	public static final String CLASS_NAME = 'FM_FieldDefaults';
	
	public class FM_Run_SheetDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		Date today = Date.today();
		String defaultLoadWeight = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Run_Sheet__c.Load_Weight__c.getDescribe().getSObjectField());
		String defaultCounty 	 = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Run_Sheet__c.RM_County__c.getDescribe().getSObjectField());
		String defaultStatus 	 = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Run_Sheet__c.Status__c.getDescribe().getSObjectField());
		String defaultCommentsCat= HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Run_Sheet__c.Comments_Category__c.getDescribe().getSObjectField());

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Run_Sheet__c.Name => 'Test RunSheet',
				FM_Run_Sheet__c.Axle__c => '6',
				FM_Run_Sheet__c.Standing_Comments__c => 'Test Run Sheet',
				FM_Run_Sheet__c.Date__c => today,
				FM_Run_Sheet__c.Flowline_Volume__c => 5, 
				FM_Run_Sheet__c.Load_Type__c => null, 
				FM_Run_Sheet__c.Load_Weight__c => defaultLoadWeight, 
				FM_Run_Sheet__c.RM_County__c => defaultCounty, 
				FM_Run_Sheet__c.Status__c => defaultStatus, 
				FM_Run_Sheet__c.Sour__c => false, 
				FM_Run_Sheet__c.Act_Flow_Rate__c => 20,
				FM_Run_Sheet__c.Comments_Category__c => defaultCommentsCat
			};
		}
	}

	public class FM_Runsheet_Tank_InformationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		String defaultTank = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Tank__c.getDescribe().getSObjectField());

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Runsheet_Tank_Information__c.Label__c => defaultTank,
				FM_Runsheet_Tank_Information__c.Level__c => 120,
				FM_Runsheet_Tank_Information__c.Low_Level__c => 60,
				FM_Runsheet_Tank_Information__c.Size__c => 160,
				FM_Runsheet_Tank_Information__c.Tomorrow_Oil__c => 1,
				FM_Runsheet_Tank_Information__c.Tomorrow_Water__c => 1,
				FM_Runsheet_Tank_Information__c.Tonight_Oil__c => 1,
				FM_Runsheet_Tank_Information__c.Tonight_Water__c => 1
			};
		}
	}

	public class FM_Done_for_the_dayDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		Date today = Date.today();

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Done_for_the_day__c.Done_Date__c => today,
				FM_Done_for_the_day__c.Runsheet_Count__c => 1
			};
		}
	}
	
	public class FM_Load_RequestDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		String defaultTank 			= HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Tank__c.getDescribe().getSObjectField());
		String defaultCreateReason 	= HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Create_Reason__c.getDescribe().getSObjectField());
		String defaultAxle 			= 'T6X';

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Load_Request__c.Tank__c => defaultTank,
				FM_Load_Request__c.Standing_Comments__c => 'Test Comment',
				FM_Load_Request__c.Axle__c => defaultAxle,
				FM_Load_Request__c.Load_Weight__c => FM_LoadRequest_Utilities.LOADREQUEST_LOADWEIGHT_PRIMARY,
				FM_Load_Request__c.Act_Flow_Rate__c => 20,
				FM_Load_Request__c.Flowline_Volume__c => 5,
				FM_Load_Request__c.Status__c => FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW,
				FM_Load_Request__c.Shift__c => FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_NIGHT,
				FM_Load_Request__c.Product__c => FM_LoadRequest_Utilities.LOADREQUEST_PRODUCT_OIL,
				FM_Load_Request__c.Load_Type__c => FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD,
				FM_Load_Request__c.Cancel_Comments__c => '',
				FM_Load_Request__c.Cancel_Reason__c => '',
				FM_Load_Request__c.Create_Comments__c => '',
				FM_Load_Request__c.Create_Reason__c => defaultCreateReason,
				FM_Load_Request__c.Act_Tank_Level__c => 120,
				FM_Load_Request__c.Tank_Low_Level__c => 60,
				FM_Load_Request__c.Tank_Size__c => 160,
				FM_Load_Request__c.Unit_Configuration__c => defaultAxle
			};
		}
	}

	public class FM_Truck_TripDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		String defaultPriority = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Truck_Trip__c.Priority__c.getDescribe().getSObjectField());

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Truck_Trip__c.Product__c => FM_LoadRequest_Utilities.LOADREQUEST_PRODUCT_OIL,
				FM_Truck_Trip__c.Shift__c => FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_NIGHT,
				FM_Truck_Trip__c.Shift_Day__c => 'Today',
				FM_Truck_Trip__c.Axle__c => '6',
				FM_Truck_Trip__c.Truck_Trip_Status__c => FM_Utilities.TRUCKTRIP_STATUS_NEW,
				FM_Truck_Trip__c.Add_On__c => false,
				FM_Truck_Trip__c.Standing_Comments__c => 'Truck Trip Test',
				FM_Truck_Trip__c.Is_Reroute__c => false,
				FM_Truck_Trip__c.Load_Request_Cancel_Comments__c => '',
				FM_Truck_Trip__c.Load_Request_Cancel_Reason__c => '',
				FM_Truck_Trip__c.Load_Type__c => null,
				FM_Truck_Trip__c.Priority__c => defaultPriority,
				FM_Truck_Trip__c.Rebook_Comments__c => '',
				FM_Truck_Trip__c.Rebook_Reason__c => '',
				FM_Truck_Trip__c.Unit_Change_Other_Reason__c => '',
				FM_Truck_Trip__c.Unit_Change_Reason__c => ''
			};
		}
	}
	
	public class FM_Stuck_TruckDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		Datetime now = Datetime.now();
		String defaultStatus = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Stuck_Truck__c.Status__c.getDescribe().getSObjectField());

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Stuck_Truck__c.Comments__c => 'Stuck truck test',
				FM_Stuck_Truck__c.Fluid_Loaded__c => false,
				FM_Stuck_Truck__c.On_Lease__c => false,
				FM_Stuck_Truck__c.Status__c => defaultStatus,
				FM_Stuck_Truck__c.Stuck_From__c => now
			};
		}
	}

	public class FM_Load_ConfirmationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				FM_Load_Confirmation__c.Product__c => FM_LoadRequest_Utilities.LOADREQUEST_PRODUCT_OIL,
				FM_Load_Confirmation__c.Volume__c => 30
			};
		}
	}

	public class CarrierDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Carrier__c.Carrier_Name_For_Fluid__c => 'Test Carrier',
				Carrier__c.Enabled__c => true 
			};
		}
	}

	public class Carrier_UnitDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Carrier_Unit__c.Name => 'General Unit',
				Carrier_Unit__c.Enabled__c => true,
				Carrier_Unit__c.Unit_Email__c => 'optimus.prime@transformers.com.test'
			};
		}
	}

	public class Towing_CompanyDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Towing_Company__c.Name => 'Test TowingCompany'
			};
		}
	}


}