/*------------------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Manages creation of Work Orders for Service Request with reccuring services
Inputs     : N/A
Test Class : TestServiceRequestNotificationManager
History    :
            02.03.15    rbo Modifications regarding Main Description and Reported By fields
            02.13.15    rbo Modifications regarding Main Operator fields
            10.11.15    rbo Modifications to accommodate the refactored service notification
                            Tech Debt fixes 
            01.08.16    rbo Modifications due to Refactoring
                            replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c
                            replaced all Production_Engineer2__c with Production_Engineer_User__c
                            modified HOG_Maintenance_Servicing_Form__c RecordTypeId to refer to HOG Work Order Type
            10.30.17    mz  added operator detail on wo for new snr - wo email notification [W-000843]                
-------------------------------------------------------------------------------------------------------------------*/ 
public class ServiceRequestNotificationManager
{       
    // SAP Request settings
    static HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();

    /* rbo 10.13.15 recurring only applies to a work order and therefore commenting this out
    static SAPHOGNotificationServices.CreateNotificationRequest[] notificationsRequest = new List<SAPHOGNotificationServices.CreateNotificationRequest>();
    static SAPHOGNotificationServices.CreateNotificationRequest notificationRequest;
    */
    
    static SAPHOGWorkOrderServices.CreateWorkOrderRequest[] workOrdersRequest = new List<SAPHOGWorkOrderServices.CreateWorkOrderRequest>();
    static SAPHOGWorkOrderServices.CreateWorkOrderRequest workOrderRequest;
    // SAP Request settings

    @future (callout=true)
    public static void process() 
    {        
        System.debug('\n*******************************************\n'
            + 'METHOD: ServiceRequestNotificationManager.process()'
            + '\nStart Time : ' + Datetime.now()
            + '\n**************************************************\n');    

        Integer recurringServiceMaxBulkCreateOrders = 
            (settingsHOG.Recurring_Service_Max_Bulk_Create_Orders__c < 0) ? 0 : Integer.valueof(settingsHOG.Recurring_Service_Max_Bulk_Create_Orders__c);
        
        List<Task> taskRecords = 
            [Select
                Id,
                Status,
                WhatId
            From Task
            Where ActivityDate = :Date.today() And IsClosed = false 
            And What.Type ='HOG_Service_Request_Notification_Form__c'
            Order By WhatId Limit :recurringServiceMaxBulkCreateOrders];
            
        System.debug('\n***************************************\n'
            + 'METHOD: ServiceRequestNotificationManager.process()'
            + '\ntaskRecords.size(): ' + taskRecords.size()
            + '\n***************************************\n');                        

        if (taskRecords.size() > 0)
        {                                    
            // put the Task ids into a set and get the associated service request records
            Set<Id> taskIds = new Set<Id>();    
            Set<Id> taskReferenceIds = new Set<Id>();
            for (Task tr : taskRecords) 
            {
                taskIds.add(tr.WhatId);
                taskReferenceIds.add(tr.Id);
            }
            
            // read records of recurring SNR
            List<HOG_Service_Request_Notification_Form__c> serviceRequestForms = ServiceRequestNotificationUtilities.readServiceNotificationRequests(taskIds);
                                        
            // read record types of the work order form and put these into a map
            List<RecordType> workOrderRecordTypes =
                [Select
                    Name, 
                    Id 
                From RecordType Where SobjectType = 'HOG_Maintenance_Servicing_Form__c'];

            Map<String, Id> recordTypeMap = new Map<String, Id>();                                            
            for (RecordType wr: workOrderRecordTypes)                    
                recordTypeMap.put(wr.Name.trim(), wr.Id);
            //
                                                                                 
            // put the service request records into a map
            Map<Id, HOG_Service_Request_Notification_Form__c> serviceRequestMap = new Map<Id, HOG_Service_Request_Notification_Form__c>();
            for (HOG_Service_Request_Notification_Form__c sr : serviceRequestForms)
                serviceRequestMap.put(sr.Id, sr);
            //
            
            // Get the associated work order count for each service request record and put it into a map
            List<AggregateResult> workOrders =
                [Select 
                        HOG_Service_Request_Notification_Form__c,
                        Count(Id) Quantity
                    From HOG_Maintenance_Servicing_Form__c                     
                    Where HOG_Service_Request_Notification_Form__c In :taskIds
                    Group By HOG_Service_Request_Notification_Form__c];
            
            // WARNING: if workOrderMap is empty, associated WO record of SNR is missing and therefore recurring WO will not be created
            // missing WO is due to the fact that someone manually deleted the record
            // logic to prevent deletion of SNR has not been put into place
            Map<Id, Integer> workOrderMap = new Map<Id, Integer>();
            for (AggregateResult ar : workOrders)
                workOrderMap.put((Id)ar.get('HOG_Service_Request_Notification_Form__c'), (Integer)ar.get('Quantity'));                
            //
                    
            // Get the associated work order count for each well tracker record and put it into a map
            Set<Id> wellTrackerIds = new Set<Id>();
            for (HOG_Service_Request_Notification_Form__c sr : serviceRequestForms)
            {
                if (sr.Well_Tracker__c != null)
                {
                    wellTrackerIds.add(sr.Well_Tracker__c);
                }
            }
                
            List<Well_Tracker__c> wellTrackerOrders =
                [Select
                    Id,
                    Number_Of_Work_Orders__c
                From Well_Tracker__c
                Where Id In :wellTrackerIds];
                        
            Map<Id, Decimal> wellTrackerOrderMap = new Map<Id, Decimal>();
            for (Well_Tracker__c wto : wellTrackerOrders)
            {
                wellTrackerOrderMap.put(wto.Id, wto.Number_Of_Work_Orders__c);
            }                
            //

            Map<Id, HOG_Maintenance_Servicing_Form__c> riglessServicingForms = new Map<Id, HOG_Maintenance_Servicing_Form__c>();

            System.debug('\n***************************************\n'
                + 'METHOD: ServiceRequestNotificationManager.process()'
                + '\ntaskRecords: ' + taskRecords
                + '\ntaskIds: ' + taskIds
                + '\nworkOrders: ' + workOrders
                + '\nworkOrderMap: ' + workOrderMap
                + '\nserviceRequestForms: ' + serviceRequestForms
                + '\nworkOrderRecordTypes: ' + workOrderRecordTypes
                + '\n***************************************\n');    
            
            String functionalLocationObjectName;
            String functionalLocationRecordName;
            Id functionalLocationRecordId;
            Id facilityId;
            Id locationId;
            Id equipmentId;         
            
            ServiceRequestNotificationUtilities.NotificationRecordType notificationRecordType = 
                new ServiceRequestNotificationUtilities.NotificationRecordType();
                            
            for (Task tr : taskRecords)
            {                
                HOG_Service_Request_Notification_Form__c sr = serviceRequestMap.get(tr.WhatId);
                
                if (sr != null /*&& workOrderMap.get(tr.WhatId) != null*/)
                {                                                                                                
                    // check if Facility or Location has been flagged as DLFL/Deleted
                    //if ((sr.Well_Tracker__r.Facility__c != null && sr.Well_Tracker__r.Facility__r.DLFL__c) || 
                    //(sr.Well_Tracker__r.Location__c != null && sr.Well_Tracker__r.Location__r.DLFL__c))
                    
                    functionalLocationObjectName =  notificationRecordType.MAPOBJECTNAME.get(sr.RecordType.Name);

                    if (sr.RecordType.Name == notificationRecordType.FACILITYTRACKER ||
                    sr.RecordType.Name == notificationRecordType.FACILITY)
                    {
                        facilityId = sr.Facility_Lookup__c;
                        functionalLocationRecordId = sr.Facility_Lookup__c;
                        functionalLocationRecordName = sr.Facility_Lookup__r.Name;              
                    }
            
                    if (sr.RecordType.Name == notificationRecordType.WELL ||
                    sr.RecordType.Name == notificationRecordType.MAINSYSTEM ||
                    sr.RecordType.Name == notificationRecordType.SUBSYSTEM ||
                    sr.RecordType.Name == notificationRecordType.YARD ||
                    sr.RecordType.Name == notificationRecordType.FUNCTIONALEQUIPMENT ||
                    sr.RecordType.Name == notificationRecordType.WELLTRACKER ||
                    sr.RecordType.Name == notificationRecordType.WELLEVENT)
                    {
                        locationId = sr.Location_Lookup__c;
                        facilityId = sr.Facility_Lookup__c;
                        functionalLocationRecordId = sr.Location_Lookup__c;
                        functionalLocationRecordName = sr.Location_Lookup__r.Name;              
                    }

                    if (sr.RecordType.Name == notificationRecordType.EQUIPMENT)
                    {
                        locationId = sr.Location_Lookup__c;
                        facilityId = sr.Facility_Lookup__c;
                        equipmentId = sr.Equipment__c;
                        functionalLocationRecordId = sr.Equipment__c; 
                        functionalLocationRecordName = sr.Equipment__r.Name;                
                    }

                    System.debug('\n***************************************\n'
                        + 'METHOD: ServiceRequestNotificationManager.process()'
                        + '\nfunctionalLocationRecordName: ' + functionalLocationRecordName
                        + '\nfunctionalLocationObjectName: ' + functionalLocationObjectName
                        + '\nfunctionalLocationRecordId: ' + functionalLocationRecordId
                        + '\nsr: ' + sr
                        + '\n***************************************\n');    
                    
                    if (String.IsBlank(functionalLocationRecordName) || 
                    ServiceRequestNotificationUtilities.readDLFL(functionalLocationObjectName, functionalLocationRecordId))
                        tr.Status = 'Completed';   // close the task but do not create a work order                            
                    else                    
                    {                                            
                        Integer workOrderCount = ((workOrderMap.get(sr.Id) == null) ? 0 : workOrderMap.get(sr.Id)) + 1;
                        workOrderMap.put(sr.Id, workOrderCount);
                        
                        if (sr.Well_Tracker__c != null)
                        {
                            // update work order counter of Well Tracker map                    
                            Decimal numberOfWorkOrders = wellTrackerOrderMap.get(sr.Well_Tracker__c);                                                            
                            wellTrackerOrderMap.put(sr.Well_Tracker__c, numberOfWorkOrders == null ? 1 : numberOfWorkOrders + 1);                                                
                        }
                                                                                                
                        String workOrderNumber = 
                            sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Work_Order_Number__c
                            ? null
                            : (sr.Work_Order_Number__c == null ? null : sr.Work_Order_Number__c + ' - ' + string.valueof(workOrderCount));
                                                            
                        String workOrderName;
                        Id operator1;
                        Id operator2;
                        Id operator3;
                        Id operator4;
                        Id operator5;
                        Id operator6;
                        Id fieldSenior;
                        Id productionEngineer2;
                        String operator1Detail;
                        String operator2Detail;
                        String operator3Detail;
                        String operator4Detail;
                        String operator5Detail;
                        String operator6Detail;
                        String operator1_2Z;
                        String operator2_2Z;
                        String operator3_2Z;
                        String operator4_2Z;
                        String operator5_2Z;
                        String operator6_2Z;
                        String operator1Name;
                        String operator2Name;
                        String operator3Name;
                        String operator4Name;
                        String operator5Name;
                        String operator6Name;
                        String operator1Mobile;
                        String operator2Mobile;
                        String operator3Mobile;
                        String operator4Mobile;
                        String operator5Mobile;
                        String operator6Mobile;
                        
                        workOrderName = functionalLocationRecordName;
                        operator1 = sr.Operator_Route_Lookup__r.Operator_1_User__c;
                        operator2 = sr.Operator_Route_Lookup__r.Operator_2_User__c;
                        operator3 = sr.Operator_Route_Lookup__r.Operator_3_User__c;
                        operator4 = sr.Operator_Route_Lookup__r.Operator_4_User__c;
                        operator5 = sr.Operator_Route_Lookup__r.Operator_5_User__c;
                        operator6 = sr.Operator_Route_Lookup__r.Operator_6_User__c;
                        Set<Id> operatorIdSet = new Set<Id>{operator1, operator2, operator3,
                            operator4, operator5, operator6};
                        operator1Name = sr.Operator_Route_Lookup__r.Operator_1_Name__c;
                        operator2Name = sr.Operator_Route_Lookup__r.Operator_2_Name__c;
                        operator3Name = sr.Operator_Route_Lookup__r.Operator_3_Name__c;
                        operator4Name = sr.Operator_Route_Lookup__r.Operator_4_Name__c;
                        operator5Name = sr.Operator_Route_Lookup__r.Operator_5_Name__c;
                        operator6Name = sr.Operator_Route_Lookup__r.Operator_6_Name__c;
                        operator1Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone1__c;
                        operator2Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone2__c;
                        operator3Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone3__c;
                        operator4Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone4__c;
                        operator5Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone5__c;
                        operator6Mobile = sr.Operator_Route_Lookup__r.Mobile_Phone6__c;
                        
                        
                        fieldSenior = sr.Operator_Route_Lookup__r.Field_Senior__c;
                        productionEngineer2 = sr.Operating_Field_AMU_Lookup__r.Production_Engineer_User__c;
                        
                        
                        if (sr.Location_Lookup__c <> null){
                    
                        List<Contact> contacts = [SELECT Name, User__c, X2Z_Employee_ID__c 
                                                  FROM Contact 
                                                  WHERE User__c In :operatorIdSet
                                                  AND X2Z_Employee_ID__c <> NULL];         
                    
                        for (Contact c : contacts){
                             if (c.User__c == operator1){
                                operator1_2Z = c.X2Z_Employee_ID__c; 
                             }
                             
                             if (c.User__c == operator2){
                                operator2_2Z = c.X2Z_Employee_ID__c; 
                             }
                             
                             if (c.User__c == operator3){
                                operator3_2Z = c.X2Z_Employee_ID__c; 
                             }
                             
                             if (c.User__c == operator4){
                                operator4_2Z = c.X2Z_Employee_ID__c; 
                             }

                             if (c.User__c == operator5) {
                                operator5_2Z = c.X2Z_Employee_ID__c;
                             }

                             if (c.User__c == operator6) {
                                operator6_2Z = c.X2Z_Employee_ID__c;
                             }
                        } 
                     
                    
                        operator1Detail = (operator1 <> null ? operator1Name : 'N/A') + ' | ' + 
                                          (operator1_2Z <> null ? operator1_2Z : 'N/A') + ' | ' + 
                                          (operator1Mobile <> null ? operator1Mobile : 'N/A');
                        operator2Detail = (operator2 <> null ? operator2Name : 'N/A') + ' | ' + 
                                          (operator2_2Z <> null ? operator2_2Z : 'N/A') + ' | ' + 
                                          (operator2Mobile <> null ? operator2Mobile : 'N/A');
                        operator3Detail = (operator3 <> null ? operator3Name : 'N/A') + ' | ' + 
                                          (operator3_2Z <> null ? operator3_2Z : 'N/A') + ' | ' + 
                                          (operator1Mobile <> null ? operator3Mobile : 'N/A');
                        operator4Detail = (operator4 <> null ? operator4Name : 'N/A') + ' | ' + 
                                          (operator4_2Z <> null ? operator4_2Z : 'N/A') + ' | ' + 
                                          (operator4Mobile <> null ? operator4Mobile : 'N/A');
                        operator5Detail = (operator5 <> null ? operator5Name : 'N/A') + ' | ' + 
                                          (operator5_2Z <> null ? operator5_2Z : 'N/A') + ' | ' + 
                                          (operator5Mobile <> null ? operator5Mobile : 'N/A');
                        operator6Detail = (operator6 <> null ? operator6Name : 'N/A') + ' | ' + 
                                          (operator6_2Z <> null ? operator6_2Z : 'N/A') + ' | ' + 
                                          (operator6Mobile <> null ? operator6Mobile : 'N/A');
                    }
                        
                        String pendingDescription = 
                            sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Generate_Numbers_From_SAP__c &&
                            sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Notification_Number__c &&
                            !sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Work_Order_Number__c
                            ? 'Pending Notification Order'
                            : 'Pending Work Order';
                                                      
                        HOG_Maintenance_Servicing_Form__c riglessServicingForm = new HOG_Maintenance_Servicing_Form__c
                            (
                                Name = 
                                    workOrderNumber == null
                                    ? workOrderName + ' - ' + pendingDescription
                                    : workOrderName + ' - ' + workOrderNumber,
                                Work_Order_Number__c = workOrderNumber,
                                //RecordTypeId = recordTypeMap.get
                                //    (sr.HOG_Work_Order_Type__r.HOG_Service_Code_MAT__r.Record_Type_Name__c.trim()),
                                RecordTypeId = recordTypeMap.get
                                    (sr.HOG_Work_Order_Type__r.Record_Type_Name__c.trim()),                                    
                                Work_Details__c = sr.Work_Details__c,
                                Service_Status__c = sr.Recurring_Start_Date__c != null 
                                    ? 'On Program' : sr.Vendor_Company__c != null ? 'Booked' : 'New Request',
                                HOG_Service_Request_Notification_Form__c = sr.Id,
                                Vendor_Company__c = sr.Vendor_Company__c,
                                Well_Tracker__c = sr.Well_Tracker__c,
                                Supervised__c = sr.Supervised__c,                                
                                Business_Unit_Lookup__c = sr.Business_Unit_Lookup__c,
                                Operating_District_Lookup__c = sr.Operating_District_Lookup__c,
                                Operating_Field_AMU_Lookup__c = sr.Operating_Field_AMU_Lookup__c,
                                Facility__c = facilityId,
                                Location__c = locationId,
                                Equipment__c = equipmentId,
                                Control_Centre__c = sr.Well_Tracker__r.Control_Centre__c,
                                Operator_1_User__c = operator1,
                                Operator_2_User__c = operator2,
                                Operator_3_User__c = operator3,
                                Operator_4_User__c = operator4,
                                Operator_5_User__c = operator5,
                                Operator_6_User__c = operator6,
                                Operator_1_detail__c = operator1Detail,
                                Operator_2_detail__c = operator2Detail,
                                Operator_3_detail__c = operator3Detail,
                                Operator_4_detail__c = operator4Detail,
                                Operator_5_detail__c = operator5Detail,
                                Operator_6_detail__c = operator6Detail,
                                Field_Senior__c = fieldSenior,
                                Production_Engineer_User__c = productionEngineer2,
                                Fixed_Service_Cost_Estimated__c = sr.HOG_Work_Order_Type__r.HOG_Service_Code_MAT__r.Fixed_Service_Cost_Estimated__c,
                                Recurring__c = sr.Recurring_Start_Date__c != null,
                                Request_Generate_Number_Message_Id__c = tr.Id,
                                Main_Work_Centre__c = sr.Main_Work_Centre__c,  //settingsHOG.SAP_Create_Work_Order_WSR_Work_Centre__c,
                                SAP_Generated_Notification_Number__c = sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Generate_Numbers_From_SAP__c &&
                                    sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Notification_Number__c,
                                SAP_Generated_Work_Order_Number__c = sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Generate_Numbers_From_SAP__c &&
                                    sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Work_Order_Number__c,
                                SAP_Request_Returned_Critical_Error__c = false,
                                HOG_Notification_Type__c = sr.HOG_Work_Order_Type__r.HOG_Notification_Type__c,
                                Vendor_Invoicing_Personnel_Is_Required__c = sr.HOG_Work_Order_Type__r.HOG_Service_Required__r.Vendor_Invoicing_Personnel_Is_Required__c,
                                Service_Time_Is_Required__c = sr.HOG_Work_Order_Type__r.HOG_Service_Required__r.Service_Time_Is_Required__c                                                             
                             );
        
                        System.debug('\n***************************************\n'
                            + 'METHOD: ServiceRequestNotificationManager.process()'
                            + '\ntask: ' + tr
                            + '\nriglessServicingForm: ' + riglessServicingForm
                            + '\n***************************************\n');    
    
                        if (riglessServicingForm != null)
                        {            
                            // add item to the list
                            riglessServicingForms.put(tr.Id, riglessServicingForm);
                    
                            // update Task record    
                            tr.Status = 'Completed';                    
    
                            // begin create SAP Orders Request
                            If (sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Generate_Numbers_From_SAP__c &&
                                (sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Notification_Number__c ||
                                sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Work_Order_Number__c))
                            {                                                         
                                Integer lengthDescription = 
                                    String.IsBlank(sr.Work_Details__c)
                                    ? 0
                                    : sr.Work_Details__c.length();

                                String mainDescription = 
                                    String.IsBlank(sr.Work_Details__c) 
                                    ? '' 
                                    : sr.Work_Details__c.substring(0, lengthDescription < 40 ? lengthDescription : 40);

                                // request for create SAP Create WO, no need to create NO
                                if (sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Auto_Generate_Work_Order_Number__c)
                                {                                   
                                    workOrderRequest = new SAPHOGWorkOrderServices.CreateWorkOrderRequest();
                                    workOrderRequest.FunctionalLocationObjectId = String.isBlank(sr.Equipment__r.Equipment_Number__c) ? sr.SAP_Object_ID__c : null;                                    
                                    workOrderRequest.MaintenancePlanningPlant = sr.Maintenance_Plant__c;                                                                        
                                    workOrderRequest.MainDescription = mainDescription;
                                    workOrderRequest.MainLongDescription = sr.Work_Details__c;
                                    workOrderRequest.Priority = sr.HOG_Notification_Type_Priority_WO__r.Priority_Code__c;
                                    workOrderRequest.OrderType = sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Order_Type__c;
                                    workOrderRequest.MainWorkCenter = settingsHOG.SAP_Create_Work_Order_WSR_Work_Centre__c;
                                    workOrderRequest.MaintenanceActivityType = sr.HOG_Work_Order_Type__r.HOG_Service_Code_MAT__r.MAT_Code__c;
                                    workOrderRequest.ActivityNumber = settingsHOG.SAP_Create_Work_Order_Activity_Code__c;
                                    workOrderRequest.ControlKey = settingsHOG.SAP_Create_Work_Order_Control_Key__c;
                                    workOrderRequest.UserStatus = sr.HOG_Work_Order_Type__r.HOG_Notification_Type__r.HOG_User_Status__r.Code__c;                                    
                                    workOrderRequest.ReferenceId = tr.Id;
                                    workOrderRequest.EquipmentNumber = sr.Equipment__r.Equipment_Number__c;
                                    workOrdersRequest.add(workOrderRequest);
                                }
                            }                                 
                            // end create SAP Orders Request                    
                                
                            System.debug('\n***************************************\n'
                                + 'METHOD: ServiceRequestNotificationManager.process()'
                                + '\nriglessServicingForm: item inserted'
                                + '\ntask: item updated'                                
                                + '\n***************************************\n');    
                        }                        
                    }                        
                }                
            }

            System.debug('\n***************************************\n'
                + 'METHOD: ServiceRequestNotificationManager.process()'
                + '\nriglessServicingForms: ' + riglessServicingForms
                + '\ntaskRecords: ' + taskRecords                    
                + '\n***************************************\n');    
                
            String exceptionMessage;
            Boolean requestReturnedCriticalError = false;

            // update work order counter of Well Tracker and request SAP numbers
            if (riglessServicingForms.size() > 0)
            {
                for (Well_Tracker__c wto : wellTrackerOrders)
                    wto.Number_Of_Work_Orders__c = wellTrackerOrderMap.get(wto.Id);
                
                // request SAP work order
                if (workOrdersRequest.size() > 0)
                {                                          
                    SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();        
                    SAPHOGWorkOrderServices.CreateWorkOrdersResponse workOrdersResponse;

                    try
                    {                        
                        // call web service
                        workOrdersResponse = workOrder.CreateWorkOrders(workOrdersRequest);                            
                    }                          
                    catch (Exception e)
                    {                
                        requestReturnedCriticalError = true;

                        exceptionMessage = 'METHOD: ServiceRequestNotificationManager.process() - CreateWorkOrders'
                            + '\nException getMessage(): ' + e.getMessage();
                        
                        System.debug('\n****************************************************************\n'
                            + exceptionMessage
                            + '\n***********************************************************************\n');
                    }
                    
                    System.debug('\n*******************************************\n'
                        + 'METHOD: ServiceRequestNotificationManager.process()'
                        + '\nworkOrdersResponse : ' + workOrdersResponse
                        + '\nworkOrdersRequest : ' + workOrdersRequest
                        + '\n**************************************************\n');                    
            
                    if (workOrdersResponse != null)
                    {
                        for (SAPHOGWorkOrderServices.CreateWorkOrderResponse responseList : workOrdersResponse.WorkOrderResponseList)
                        {
                            System.debug('\n*******************************************\n'
                                + 'METHOD: ServiceRequestNotificationManager.process()'
                                + '\nresponseList : ' + responseList
                                + '\nresponseList.ReferenceId : ' + responseList.ReferenceId                                
                                + '\nresponseList.Message : ' + responseList.Message                                
                                + '\n**************************************************\n');                    

                            if (riglessServicingForms.containskey(responseList.ReferenceId))
                            {
                                HOG_Maintenance_Servicing_Form__c orderTransaction = riglessServicingForms.get(responseList.ReferenceId);
                                    
                                orderTransaction.Request_Generate_Number_Message__c = responseList.Message;
                                                            
                                if (responseList.WorkOrder != null)
                                {
                                    orderTransaction.Work_Order_Number__c = responseList.WorkOrder.WorkOrderNumber;
                                    orderTransaction.SAP_Changed_Date__c = responseList.WorkOrder.ChangedDate;
                                    orderTransaction.SAP_Changed_By__c = responseList.WorkOrder.ChangedBy;
                                    orderTransaction.System_Status_Code__c = responseList.WorkOrder.SystemStatus;
                                    orderTransaction.User_Status_Code__c = responseList.WorkOrder.UserStatus;
                                    
                                    HOG_Service_Request_Notification_Form__c serviceRecord = serviceRequestMap.get(orderTransaction.HOG_Service_Request_Notification_Form__c);
                                    
                                    /*                                                                     
                                    orderTransaction.Name = (serviceRecord.Well_Tracker__r.Facility__c == null
                                        ? serviceRecord.Well_Tracker__r.Location__r.Name 
                                        : serviceRecord.Well_Tracker__r.Facility__r.Name) + ' - ' + responseList.WorkOrder.WorkOrderNumber;
                                    */

                                    orderTransaction.Name = (orderTransaction.Location__c != null
                                        ? serviceRecord.Location_Lookup__r.Name
                                        : serviceRecord.Facility_Lookup__r.Name) + ' - ' + responseList.WorkOrder.WorkOrderNumber;

                                }
                                
                                riglessServicingForms.put(responseList.ReferenceId, orderTransaction);
                            }                            
                        }
                    }
                    else
                    {

                        System.debug('\n*******************************************\n'
                            + 'METHOD: ServiceRequestNotificationManager.process()'
                            + '\nriglessServicingForms.values() : ' + riglessServicingForms.values()
                            + '\n**************************************************\n');                    
    
                        for (HOG_Maintenance_Servicing_Form__c orderTransaction : riglessServicingForms.values())
                        {
                            if (orderTransaction.SAP_Generated_Work_Order_Number__c)
                            {
                                orderTransaction.Request_Generate_Number_Message__c = exceptionMessage;
                                orderTransaction.SAP_Request_Returned_Critical_Error__c = requestReturnedCriticalError;                                
                            }   
                        }
                    }                                                                                                                                                                                                 
                }                                             
            }
                                                            
            Savepoint savePoint1 = Database.setSavepoint(); 
            try
            {                                
                if (riglessServicingForms.size() > 0)
                {
                    WellServicingUtilities.executeTriggerCode = false;
                    MaintenanceServicingUtilities.executeTriggerCode = false;
                                                            
                    insert riglessServicingForms.values(); 
                    
                    if (!wellTrackerOrders.isEmpty())
                    {                   
                        update wellTrackerOrders;
                    }
                }
                                
                update taskRecords;                                    
            }
            catch (System.DmlException e) 
            {
                Database.rollback(savePoint1);
                                
                System.debug('\n***************************************\n'
                    + 'METHOD: ServiceRequestNotificationManager.process() - Create Work Order');

                for (Integer i = 0; i < e.getNumDml(); i++) 
                    System.debug('\n' + e.getDmlMessage(i));                                                                                                    
            }            
        }
                   
        System.debug('\n*******************************************\n'
            + 'METHOD: ServiceRequestNotificationManager.process()'
            + '\nEnd Time : ' + Datetime.now()
            + '\n**************************************************\n');                    
    }      
}