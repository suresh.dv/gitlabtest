@isTest
private class ScaffoldInspectionActionsTest{
    static testmethod void createTestData(){
        Scaffold__c scaffold = ScaffoldTestData.createScaffold();
        Scaffold_Inspection__c scaffoldInspection = ScaffoldInspectionTestData.createScaffoldInspection(scaffold);
        ScaffoldInspectionTestData.approveScaffoldInspection( scaffoldInspection );
    }
}